import { History } from 'history'
import * as React from 'react'
import { connect } from 'react-redux'
import { Redirect, Route, Router as BaseRouter, Switch } from 'react-router-dom'
// @ts-ignore
import ym, { YMInitializer } from 'react-yandex-metrika'
import { selectCity as selectCityAction } from './actions/cities.actions'
import LayoutComponent from './components/layout/layout.component'
import Config from './core/config'
import ForgetScreenScreen from './screens/auth/forget.screen'
import LoginScreenScreen from './screens/auth/login.screen'
import RegScreenScreen from './screens/auth/reg.screen'
import EventScreen from './screens/event/event.screen'
import FriendsScreen from './screens/friends.screen'
import GamePageScreen from './screens/game/game-page.screen'
import PlaygProfileScreen from './screens/playg/playg-profile.screen'
import NotFoundScreen from './screens/public/not-found.screen'
import PublicScreen from './screens/public/public.screen'

import DevScreen from './screens/dev.screen'
import ClubLandingScreen from './screens/landing/club-landing.screen'
import PlaygLandingScreen from './screens/landing/playg-landing.screen'
import { initialLogin } from './services/auth.service'
import { getCities } from './services/cities.service'
import { getGameLevels } from './services/game-levels.service'

// Code Splitting
const TestScreen = React.lazy(() => import('./screens/development/test.screen'))
const ProfileEditScreen = React.lazy(() => import('./screens/profile/profile-edit.screen'))
const ProfileFriendsScreen = React.lazy(() => import('./screens/profile//profile-friends.screen'))
const ProfileOtherScreen = React.lazy(() => import('./screens/profile/profile-other.screen'))
const ProfileScreen = React.lazy(() => import('./screens/profile/profile.screen'))
const UIKitScreen = React.lazy(() => import('./screens/development/uikit.screen'))

interface IProps {
  history: History
  user: any
  selectCity: any
  selectedCity: number
}

interface IState {
  loading: boolean
}

class AppComponent extends React.Component<IProps, IState> {
  public state: IState = {
    loading: true
  }

  public componentDidMount() {
    const { history } = this.props
    history.listen(location => {
      ym('hit', location.pathname)
    })

    this.bootstrap()
  }

  public bootstrap = () => {
    getCities()
        .then(this.selectFirstCity)
        .then(getGameLevels)
        .then(initialLogin)
        .then(() => this.setState({ loading: false }))
  }

  public selectFirstCity = args => {
    const { selectedCity, selectCity } = this.props

    if (selectedCity === null && args.data.length > 0) {
      selectCity(args.data[0].id)
    }
  }

  public render() {
    const { history, user } = this.props
    const { loading } = this.state
    const ymCounter = parseInt(Config.ymCounter, 10)
    let routes: React.ReactNode[] = [
      <Route key='EventScreen' exact path='/events/:slug' component={EventScreen} />,
      <Route key='FriendsScreen' exact path='/friends' component={FriendsScreen} />,
      <Route key='PlaygProfileScreen' exact path='/club/:clubId/:id/:tab' component={PlaygProfileScreen} />,
      <Route key='PlaygLandingScreen' exact path='/club/:clubId/:id' component={PlaygLandingScreen} />,
      <Route key='ClubLandingScreen' exact path='/club/:id' component={ClubLandingScreen} />,
      <Route key='NotFoundScreen' path='/404' component={NotFoundScreen} />,
      <Route key='PublicScreen' exact path='/' component={PublicScreen} />,
      <Route key='GamePageScreen' path='/game/:id' component={GamePageScreen}  />
    ]
    const loggedInRoutes: React.ReactNode[] = [
      <Route key='ProfileFriendsScreen' path='/profile/friends' component={props => <ProfileFriendsScreen {...props} />} />,
      <Route key='ProfileEditScreen' path='/profile/edit' component={props => <ProfileEditScreen {...props} />} />,
      <Route key='ProfileOtherScreen' path='/profile/:id' component={props => <ProfileOtherScreen {...props} />} />,
      <Route key='ProfileScreen' path='/profile' component={props => <ProfileScreen {...props} />} />
    ]
    const loggedOutRoutes: React.ReactNode[] = [
      <Route key='ForgetScreenScreen' path='/forget-password' component={ForgetScreenScreen} />,
      <Route key='RegScreenScreen' path='/registration' component={RegScreenScreen} />,
      <Route key='LoginScreenScreen' path='/login' component={LoginScreenScreen} />
    ]
    const testRoutes: React.ReactNode[] = [
      <Route key='DevScreen' exact path='/dev' component={DevScreen} />,
      <Route key='UIKitScreen' path='/ui' component={props => <UIKitScreen {...props} />} />,
      <Route key='TestScreen' path='/test' component={props => <TestScreen {...props} />} />
    ]

    if (!!user) {
      routes = [
        ...routes,
        ...loggedInRoutes
      ]
    } else {
      routes = [
        ...routes,
        ...loggedOutRoutes
      ]
    }

    if (process.env.NODE_ENV !== 'production') {
      routes = [
        ...routes,
        ...testRoutes
      ]
    }

    return <BaseRouter history={history}>
      { loading ? null
        : <LayoutComponent user={user}>
          <YMInitializer accounts={[ymCounter]} />
          <React.Suspense fallback={<div>Загрузка…</div>}>
            <Switch>
              { routes }
              <Route path='*'>
                <Redirect to='/' />
              </Route>
            </Switch>
          </React.Suspense>
        </LayoutComponent>
      }
    </BaseRouter>
  }
}

const mapStateToProps = state => ({
  user: state.authState.user,
  selectedCity: state.citiesState.selectedCity
})

const mapDispatchToProps = {
  selectCity: selectCityAction
}

export default connect(mapStateToProps, mapDispatchToProps)(AppComponent)
