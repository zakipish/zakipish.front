import * as React from 'react'

import { authLogin } from '../../actions/auth.actions'
import { connect } from 'react-redux'
import { History } from 'history'
import { User } from '../../models/user'
import { Game } from '../../models/game'
import { FriendRequest } from '../../models/friend-request'

import { getGames, getUserGames, inviteFriend, attachToGame } from '../../services/games-service'
import { getFriends, getFriendRequests, acceptFriendRequest, declineFriendRequest } from '../../services/friends-service'

import SideProfile from '../../components/layout/profile/side/side.profile'
import ContentProfile from '../../components/layout/profile/content/content.profile'

import MobilePopup from '../../components/layout/mobile-popup'
import PopuplineComponent from '../../components/layout/popupline.component'
import Popup from '../../components/layout/popup'

import TabLinksComponent from '../../components/layout/tab-links.component'
import SideFriendsProfile from '../../components/layout/profile/side/side-friends.profile'
import FriendLine from '../../components/blocks/lines/friend.line'
import InviteFriend from '../../components/layout/friends/invite.friend'
import Loader from '../../components/layout/loader'

interface IProps {
    history: History
    user: User
    updateUser?: (user: User) => void
}

interface IState {
    selectedTab: number,
    hideInvitePopup: boolean,
    activeTabKey: string,
    friendRequests?: FriendRequest[],
    games?: Game[],
    invitationDisabled: boolean,
    friendGames?: Game[],
    selectedFriendId?: number,

    gamesLoading: boolean,
    friendGamesLoading: boolean
}

class ProfileFriendsScreen extends React.Component<IProps, IState> {

    state: IState = {
        selectedTab: 0,
        hideInvitePopup: true,
        gamesLoading: true,
        activeTabKey: 'my',
        invitationDisabled: false,
        friendGamesLoading: true
    }

    componentDidMount() {
        this.bootstrap()
    }

    bootstrap = () => {
        getFriendRequests()
        .then(friendRequests => friendRequests ? this.setState({ friendRequests }) : false)

        getGames()
        .then(games => games ? this.setState({ games, gamesLoading: false }) : false)
    }

    acceptFriendRequest = (id: number) => () => {
        acceptFriendRequest(id)
        .then(() => this.addFriendFromRequest(id))
        .then(() => this.removeFriendRequest(id))
    }

    declineFriendRequest = (id: number) => () => {
        declineFriendRequest(id)
        .then(() => this.removeFriendRequest(id))
    }

    addFriendFromRequest = (id: number) => {
        const { friendRequests } = this.state
        let { updateUser, user } = this.props
        const friendRequestIndex = friendRequests.findIndex(fr => fr.id == id)

        if (friendRequestIndex > -1) {
            user.friends.push(friendRequests[friendRequestIndex].user)
            updateUser(user)
        }
    }

    removeFriendRequest = (id: number) => {
        let { friendRequests } = this.state
        const friendRequestIndex = friendRequests.findIndex(fr => fr.id == id)

        if (friendRequestIndex > -1) {
            friendRequests.splice(friendRequestIndex, 1)
            this.setState({ friendRequests })
        }
    }

    get sportsCollection() {
        const { user } = this.props
        return user.sports.map(sport => {
            return {
                title: sport.name,
                icon: sport.icon_url,
                level: {
                    display: sport.game_level ? sport.game_level.engName : null,
                    bg: sport.game_level ? sport.game_level.color : null
                }
            }
        })
    }

    onSelectTab(selectedTab: number) {
        this.setState({selectedTab})
    }

    toggleInvitePopup = (hide) => {
        let { hideInvitePopup } = this.state
        if (hideInvitePopup != hide) {
            hideInvitePopup = hide
            this.setState({hideInvitePopup})
        }
    }

    onClickTab = (key: string) => {
        this.setState({activeTabKey: key})
    }

    handleInvitation = (game_id: number) => () => {
        const { selectedFriendId } = this.state
        const { history } = this.props

        this.setState({ invitationDisabled: true }, () => {
            inviteFriend(game_id, selectedFriendId)
            .then(() => {
                history.push(`/game/${game_id}`)
            })
        })
    }

    handleAttachToGame = (game_id:number) => () => {
        const { history } = this.props

        this.setState({ invitationDisabled: true }, () => {
            attachToGame(game_id)
            .then(() => {
                history.push(`/game/${game_id}`)
            })
        })
    }

    handleClickInvite = (user_id: number) => () => {
        const { selectedFriendId } = this.state

        if (selectedFriendId == user_id) {
            this.toggleInvitePopup(false)
            return true
        }

        this.setState({ selectedFriendId: user_id, friendGames: [], friendGamesLoading: true }, () => {
            getUserGames(user_id)
            .then(data => {
                this.setState({ friendGames: data, friendGamesLoading: false })
            })

            this.toggleInvitePopup(false)
        })
    }

    render() {
        const { user } = this.props
        const { selectedTab, friendRequests, hideInvitePopup, gamesLoading, activeTabKey, selectedFriendId, friendGamesLoading, games, friendGames, invitationDisabled } = this.state
        const friends = user.friends

        const friend = friends ? friends.find(friend => friend.id == selectedFriendId) : null

        let userName = user.fullName()
        let sports = this.sportsCollection

        if (!friends || !friendRequests)
            return null

        return (
            <div className="profile">
                <div className="profile__wrap">
                    <MobilePopup preview={
                        <PopuplineComponent
                            name={userName}
                            avatar={user.avatar_thumb_url}
                        />
                    }>
                        <SideProfile
                            edit={true}
                            games={sports}
                            avatar={user.avatar_url}
                        >
                            <SideFriendsProfile
                                link={true}
                                friends={user.friends.map(man => ({
                                    url: `/profile/${man.id}`,
                                    name: man.fullName(),
                                    image: man.avatar_thumb_url
                                }))}
                            />
                        </SideProfile>
                    </MobilePopup>
                    <ContentProfile>
                        <div className="profileTabs">
                            <TabLinksComponent
                                active={selectedTab}
                                onSelect={(i) => this.onSelectTab(i)}
                            >
                                <span>Друзья ({friends.length})</span>
                                <span>Заявки в друзья ({friendRequests.length})</span>
                            </TabLinksComponent>
                        </div>
                        <div className="profileContent__body">
                            {(selectedTab === 0) && (
                                <React.Fragment>
                                    {friends.length > 0 ?
                                        friends.map((friend, i) => (
                                            <FriendLine
                                                key={i}
                                                viewState="list"
                                                title={friend.fullName()}
                                                img={friend.avatar_big_thumb_url}
                                                //statusText="Участник 2 из 3 игр"
                                                url={`/profile/${friend.id}`}
                                                onInvite={this.handleClickInvite(friend.id)}
                                                // onBell={() => {console.log('belll')}}
                                            />
                                        ))
                                    :
                                        <span>У вас пока еще нет друзей</span>
                                    }
                                </React.Fragment>
                            )}
                            {(selectedTab === 1) && (
                                <React.Fragment>
                                    {friendRequests.length > 0 ?
                                        friendRequests.map(friendRequest => (
                                            <FriendLine
                                                viewState="req"
                                                title={friendRequest.user.fullName()}
                                                img={friendRequest.user.avatar_big_thumb_url}
                                                url={`/profile/${friendRequest.user.id}`}
                                                onAdd={this.acceptFriendRequest(friendRequest.id)}
                                                onCancel={this.declineFriendRequest(friendRequest.id)}
                                            />
                                        ))
                                    :
                                        <span>У вас пока нет новых заявок</span>
                                    }
                                </React.Fragment>
                            )}
                        </div>
                        <Popup hide={hideInvitePopup} onToggle={(hide) => this.toggleInvitePopup(hide)}>
                            { friend ? (
                                <Loader state={gamesLoading && friendGamesLoading}>
                                    <InviteFriend
                                        activeTabKey={activeTabKey}
                                        tabs={[
                                            { key: 'my', title: 'Мои игры' },
                                            { key: 'friend', title: `Игры ${friend ? friend.fullName() : ''}` },
                                            { key: 'common', title: 'Совместные игры' },
                                        ]}
                                        onClickTab={this.onClickTab}
                                        myGames={games}
                                        friendGames={friendGames}
                                        me={user}
                                        friend={friend}
                                        onInvite={this.handleInvitation}
                                        onAttach={this.handleAttachToGame}
                                        invitationDisabled={invitationDisabled}
                                    />
                                </Loader>
                            ) : '' }
                        </Popup>
                    </ContentProfile>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.authState.user
})

const mapDispatchToProps = dispatch => ({
    updateUser: (user) => dispatch(authLogin(user))
})

export default connect(mapStateToProps, mapDispatchToProps)(ProfileFriendsScreen)
