import * as React from 'react'
import { connect } from 'react-redux'
import { Switch, Route } from 'react-router'
import { History } from 'history'
import { Sport } from '../../models/sport'
import SideProfile from '../../components/layout/profile/side/side.profile'
import ProfileEditMainComponent from '../../components/layout/profile-edit/profile-edit-main.component'
import ProfileEditSocComponent from '../../components/layout/profile-edit/profile-edit-soc.component'
import ProfileEditSportComponent from '../../components/layout/profile-edit/profile-edit-sport.component'
import ProfileEditAvatarComponent from '../../components/layout/profile-edit/profile-edit-avatar.component'
import CrossPopupComponent from '../../components/layout/cross-popup.component'

interface IProps {
    user: any
    history: History
}

class ProfileEditScreen extends React.Component<IProps, {}> {

    get games() {
        const { user } = this.props

        return user.sports.map(function(sport) {
            return {
                title: sport.name,
                icon: sport.icon_url,
                level: {
                    display: sport.game_level ? sport.game_level.engName : null,
                    bg: sport.game_level ? sport.game_level.color : null
                }
            }
        })
    }

    onSubmit = () => {
        const { history } = this.props
        history.push('/profile')
    }

    render() {
        const { user, history } = this.props
        return (
            <div className="profile profile--edit">
                <div className="profile__wrap">
                    <SideProfile
                        // {...Data.side}
                        edit={true}
                        history={history}
                        games={this.games}
                        avatar={user.avatar_url}
                    />
                    <div className="profileEdit">
                        <Switch>
                            <Route
                                path="/profile/edit/avatar"
                                render={(props) => <ProfileEditAvatarComponent onSubmit={this.onSubmit} />}
                            />
                            <Route
                                path="/profile/edit/sport"
                                render={(props) => <ProfileEditSportComponent onSubmit={this.onSubmit} />}
                            />
                            <Route
                                path="/profile/edit/soc"
                                render={(props) => <ProfileEditSocComponent/>}
                            />
                            <Route
                                path="/profile/edit"
                                render={(props) => <ProfileEditMainComponent onSubmit={this.onSubmit} />}
                            />
                        </Switch>
                        <CrossPopupComponent className="profileEdit__cross" history={history}/>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.authState.user
})

export default connect(mapStateToProps)(ProfileEditScreen)
