import { History } from 'history'
// @ts-ignore
import moment from 'moment'
import * as React from 'react'
import { connect } from 'react-redux'
import MobilePopup from '../../components/layout/mobile-popup'
import PopupLineComponent from '../../components/layout/popupline.component'
import ContentProfile from '../../components/layout/profile/content/content.profile'
import HeaderProfile from '../../components/layout/profile/header/header.profile'
import SideFriendsProfile from '../../components/layout/profile/side/side-friends.profile'
import SideProfile from '../../components/layout/profile/side/side.profile'
import TabLinksComponent from '../../components/layout/tab-links.component'
import SectionCardsComponent from '../../components/sections/section-cards.component'
import {resizeImage} from '../../components/UI/Image'
import { reqGet } from '../../core/api'
import { Game } from '../../models/game'
import { User } from '../../models/user'

interface IProps {
  history: History
  user: User
}

interface IState {
  selectedTab: number
  pastGames: Game[]
  upcomingGames: Game[]
}

class ProfileScreen extends React.Component<IProps, IState> {

  get pastGamesCollection() {
    const { pastGames } = this.state
    return this.gamesCollection(pastGames)
  }

  get upcomingGamesCollection() {
    const { upcomingGames } = this.state
    return this.gamesCollection(upcomingGames)
  }

  get sportsCollection() {
    const { user } = this.props
    return user.sports.map(sport => {
      return {
        title: sport.name,
        icon: sport.icon_url,
        level: {
          display: sport.game_level ? sport.game_level.engName : null,
          bg: sport.game_level ? sport.game_level.color : null
        }
      }
    })
  }

  public state = {
    selectedTab: 0,
    upcomingGames: null,
    pastGames: null
  }

  constructor(props) {
    super(props)
    // noinspection JSIgnoredPromiseFromCall
    this.getGames()
  }

  public render() {
    const { user } = this.props
    const { selectedTab } = this.state

    const upcomingGames = this.upcomingGamesCollection
    const pastGames = this.pastGamesCollection
    const selectedGames = selectedTab === 0 ? upcomingGames : pastGames

    const userName = user.fullName()
    const avatarThumb = user.avatar_thumb_url ? resizeImage(user.avatar_thumb_url, 100, 100) : '/images/avatar-md.jpg'
    const avatar = user.avatar_url ? resizeImage(user.avatar_url, 330, 330) : '/images/avatar-lg.jpg'

    return <div className='profile'>
      <div className='profile__wrap'>
        <MobilePopup preview={
          <PopupLineComponent
            name={userName}
            avatar={avatarThumb}
          />
        }>
          <HeaderProfile
            title={userName}
            soc={user.social_accounts}
          />
          <SideProfile
            edit={true}
            games={this.sportsCollection}
            avatar={avatar}
          >
            <SideFriendsProfile
              link={true}
              friends={user.friends.map(man => ({
                url: `/profile/${man.id}`,
                name: man.fullName(),
                image: man.avatar_thumb_url ? resizeImage(user.avatar_thumb_url, 100, 100) : '/images/avatar-md.jpg'
              }))}
            />
          </SideProfile>
        </MobilePopup>
        <ContentProfile title='Мои игры'>
          <div className='profileTabs'>
            <TabLinksComponent
              active={selectedTab}
              onSelect={i => this.onSelectTab(i)}
            >
              <span>Предстоящие ({upcomingGames.length})</span>
              <span>Прошедшие ({pastGames.length})</span>
            </TabLinksComponent>
          </div>
          <div className='profileContent__body'>
            <SectionCardsComponent list={selectedGames} cols={1}/>
          </div>
        </ContentProfile>
      </div>
    </div>
  }

  protected getGames = async () => {
    reqGet('/games/my')
      .then(response => {
        const upcomingGames = response.data.filter(game => {
          const dt = moment(`${game.date} ${game.interval[1]}`)
          return dt > moment()
        })

        const pastGames = response.data.filter(game => {
          const dt = moment(`${game.date} ${game.interval[1]}`)
          return dt <= moment()
        })

        this.setState({ pastGames, upcomingGames })
      })
  }

  protected gamesCollection = games => {
    if (!games) {
      return []
    }

    return games.map(game => ({
      type: 'game',
      card: {
        url: `/game/${game.id}`,
        id: game.id,
        header: {
          title: `${moment(game.date).format('D MMM')}, ${game.interval[0]} - ${game.interval[1]}`
          /*
          tags: [
              { title: 'PRO', bg: 'red' },
              { title: 'LIGHT', bg: 'green' }
          ]
           */
        },
        title: game.name || '-',
        text: game.playground.club.name,
        options: [
          { text: game.playground.club.address }
        ],
        state: { must: game.max_players, now: game.users.length, min: game.min_players },
        party: this.gameUsersCollection(game)
      }
    }))
  }

  protected gameUsersCollection = (game: Game) => {
    const collection: Array<{
      image?: string,
      bg?: string,
      url?: string,
      text?: string
    }> = game.users.slice(0, 7).map(user => ({
      image: user.avatar_thumb_url ? resizeImage(user.avatar_thumb_url, 24, 24) : '/images/avatar-md.jpg',
      bg: 'green',
      url: `/profile/${user.id}`
    }))

    if (game.users.length > 7) {
      collection.push({ text: `+${game.users.length - 7}`, url: '' })
    }

    return collection
  }

  protected onSelectTab(selectedTab: number) {
    this.setState({selectedTab})
  }
}

const mapStateToProps = state => ({
  user: state.authState.user
})

export default connect(mapStateToProps)(ProfileScreen)
