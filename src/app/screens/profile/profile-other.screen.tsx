import * as React from 'react'
import moment from 'moment'
import { connect } from 'react-redux'

import { History } from 'history'
import { reqGet } from '../../core/api'
import { getGames, getUserGames, inviteFriend, attachToGame } from '../../services/games-service'
import { User } from '../../models/user'
import { FriendRequest } from '../../models/friend-request'
import { Game } from '../../models/game'

import HeaderProfile from '../../components/layout/profile/header/header.profile'
import SideProfile from '../../components/layout/profile/side/side.profile'
import ContentProfile from '../../components/layout/profile/content/content.profile'

import Popup from '../../components/layout/popup'
import Loader from '../../components/layout/loader'
import InviteFriend from '../../components/layout/friends/invite.friend'
import SideFriendsProfile from '../../components/layout/profile/side/side-friends.profile'

import TabLinksComponent from '../../components/layout/tab-links.component'
import SectionCardsComponent from '../../components/sections/section-cards.component'

import { createFriendRequest, removeFriendRequest } from '../../services/friends-service'

interface IProps {
    history: History
    match: any
    currentUser: User
}

interface IState {
    user: User
    selectedTab: number
    hideInvitePopup: boolean
    activeTabInvite: string
    invitationDisabled: boolean
    selectedFriendId?: number

    games?: Game[]
    gamesLoading: boolean

    upcomingGames?: Game[]
    pastGames?: Game[]
    friendGames?: Game[]
    friendGamesLoading: boolean
}

class ProfileOtherScreen extends React.Component<IProps, IState> {

    state: IState = {
        user: null,
        selectedTab: 0,
        hideInvitePopup: true,
        activeTabInvite: 'my',
        gamesLoading: false,
        invitationDisabled: false,
        friendGamesLoading: true,
    }

    componentDidMount() {
        this.bootstrap()
    }

    componentDidUpdate(prevProps) {
        const prevId = prevProps.match.params.id
        const { id } = this.props.match.params

        if (prevId !== id)
            this.bootstrap()
    }

    bootstrap = () => {
        const { currentUser, history } = this.props
        const { id } = this.props.match.params

        if (currentUser.id == id) {
            history.push('/profile')
        } else {
            this.getUser(id)
            getGames()
                .then(games => games ? this.setState({ games, gamesLoading: false }) : false)

            getUserGames(id)
            .then(games => {
                const upcomingGames = games.filter(game => {
                    const dt = moment(`${game.date} ${game.interval[1]}`)
                    return dt > moment()
                })

                const pastGames = games.filter(game => {
                    const dt = moment(`${game.date} ${game.interval[1]}`)
                    return dt <= moment()
                })

                this.setState({ pastGames, upcomingGames, friendGames: games, friendGamesLoading: false })
            })
        }
    }

    getUser = async (id: number)  => {
        try {
            const response = await reqGet(`/users/${id}`)
            this.setState({ user: new User(response.data) })
        } catch(e) {
            console.log(e.status)
            if (e.status === 404) {
                this.props.history.push('/404')
            }
        }
    }

    get games() {
        const { user } = this.state

        return user.sports.map(function(sport) {
            return {
                title: sport.name,
                icon: sport.icon_url,
                level: {
                    display: sport.game_level.engName,
                    bg: sport.game_level.color
                }
            }
        })
    }

    toggleInvitePopup = (hide) => {
        let { hideInvitePopup } = this.state
        if (hideInvitePopup != hide) {
            hideInvitePopup = hide
            this.setState({hideInvitePopup})
        }
    }

    onClickTabInvent = (key) => {
        this.setState({activeTabInvite: key})
    }

    get buttons() {
        let buttons = []
        buttons.push({ theme: 'action', title: this.friendButtonTitle, act: this.friendButtonAction })
        if (this.isFriend) {
            buttons.push({ theme: 'invite', title: 'пригласить в игру', icon: 'plus', act: this.handleClickInvite})
        }

        return buttons
    }

    get isFriend(){
        const { friend_request } = this.state.user
        const { currentUser } = this.props
        return (friend_request && friend_request.user.id == currentUser.id && friend_request.accepted)
    }

    get friendButtonTitle() {
        const { friend_request } = this.state.user
        const { currentUser } = this.props

        if (friend_request)
            if (friend_request.accepted) {
                return 'удалить из друзей'
            } else {
                if (friend_request.user.id == currentUser.id)
                    return 'отменить заявку'
            }

        return 'добавить в друзья'
    }

    get friendButtonAction() {
        const { id, friend_request } = this.state.user
        const { currentUser } = this.props
        let { user } = this.state

        if (friend_request)
            return () => {
                removeFriendRequest(id)
                .then(() => {
                    user.friend_request = null
                    this.setState({ user })
                })
            }

        return () => {
            createFriendRequest(id)
            .then(response => {
                user.friend_request = new FriendRequest(response.data)
                this.setState({ user })
            })
        }
    }

    handleInvitation = (game_id: number) => () => {
        const { history } = this.props
        const { user } = this.state

        this.setState({ invitationDisabled: true }, () => {
            inviteFriend(game_id, user.id)
            .then(() => {
                history.push(`/game/${game_id}`)
            })
        })
    }

    handleAttachToGame = (game_id:number) => () => {
        const { history } = this.props

        this.setState({ invitationDisabled: true }, () => {
            attachToGame(game_id)
            .then(() => {
                history.push(`/game/${game_id}`)
            })
        })
    }

    getGames = async () => {
        reqGet('/games/my')
        .then(response => {
            const upcomingGames = response.data.filter(game => {
                const dt = moment(`${game.date} ${game.interval[1]}`)
                return dt > moment()
            })

            const pastGames = response.data.filter(game => {
                const dt = moment(`${game.date} ${game.interval[1]}`)
                return dt <= moment()
            })

            this.setState({ pastGames, upcomingGames })
        })
    }

    handleClickInvite = () => {
        this.toggleInvitePopup(false)
    }

    onSelectTab(selectedTab: number) {
        this.setState({selectedTab})
    }

    get upcomingGamesCollection() {
        const { upcomingGames } = this.state
        return this.gamesCollection(upcomingGames)
    }

    get pastGamesCollection() {
        const { pastGames } = this.state
        return this.gamesCollection(pastGames)
    }

    gamesCollection = (games) => {
        if (!games) return []

        return games.map(game => ({
            type: 'game',
            card: {
                url: `/game/${game.id}`,
                id: game.id,
                header: {
                    title: `${moment(game.date).format('D MMM')}, ${game.interval[0]} - ${game.interval[1]}`,
                    /*
                    tags: [
                        { title: 'PRO', bg: 'red' },
                        { title: 'LIGHT', bg: 'green' }
                    ]
                     */
                },
                title: game.name || '-',
                text: game.playground.club.name,
                options: [
                    { text: game.playground.club.address }
                ],
                state: { must: game.max_players, now: game.users.length, min: game.min_players },
                party: this.gameUsersCollection(game)
            }
        }))
    }

    gameUsersCollection = (game: Game) => {
        let collection = []
        collection = game.users.slice(0, 7).map(user => ({
            image: user.avatar_thumb_url ? user.avatar_thumb_url : '/images/avatar-md.jpg',
            bg: 'green',
            url: `/profile/${user.id}`
        }))

        if (game.users.length > 7) {
            collection.push({ text: `+${game.users.length - 7}`, url: '' })
        }

        return collection
    }

    render() {
        let { hideInvitePopup, activeTabInvite, friendGames, friendGamesLoading, gamesLoading, games, user, invitationDisabled, selectedTab } = this.state
        const { currentUser } = this.props

        let upcomingGames = this.upcomingGamesCollection
        let pastGames = this.pastGamesCollection
        let selectedGames = selectedTab === 0 ? upcomingGames : pastGames

        if (user) {
            return (
                <div className="profile">
                    <div className="profile__wrap">
                        <HeaderProfile
                            // {...Data.header}
                            title={user.fullName()}
                            soc={user.social_accounts}
                        />
                        <SideProfile
                            games={this.games}
                            avatar={user.avatar_url}
                            buttons={this.buttons}
                        >
                            <SideFriendsProfile
                                friends={user.friends.map(man => ({
                                    url: `/profile/${man.id}`,
                                    name: man.fullName(),
                                    image: man.avatar_thumb_url
                                }))}
                            />
                            <Popup hide={hideInvitePopup} onToggle={(hide) => this.toggleInvitePopup(hide)}>
                                <Loader state={gamesLoading}>
                                    <InviteFriend
                                        activeTabKey={activeTabInvite}
                                        tabs={[
                                            { key: 'my', title: 'Мои игры' },
                                            { key: 'friend', title: `Игры ${user.fullName()}` },
                                            { key: 'common', title: 'Совместные игры' },
                                        ]}
                                        onClickTab={this.onClickTabInvent}
                                        myGames={games}
                                        friendGames={friendGames}
                                        me={currentUser}
                                        friend={user}
                                        onInvite={this.handleInvitation}
                                        onAttach={this.handleAttachToGame}
                                        invitationDisabled={invitationDisabled}
                                    />
                                </Loader>
                            </Popup>
                        </SideProfile>
                        <ContentProfile title="Мои игры">
                            <Loader state={friendGamesLoading}>
                                <div className="profileTabs">
                                    <TabLinksComponent
                                        active={selectedTab}
                                        onSelect={(i) => this.onSelectTab(i)}
                                    >
                                        <span>Предстоящие ({upcomingGames.length})</span>
                                        <span>Прошедшие ({pastGames.length})</span>
                                    </TabLinksComponent>
                                </div>
                                <div className="profileContent__body">
                                    <SectionCardsComponent list={selectedGames} cols={1}/>
                                </div>
                            </Loader>
                        </ContentProfile>
                    </div>
                </div>
            )
        } else {
            return ''
        }
    }
}

const mapStateToProps = state => ({
    currentUser: state.authState.user
})

export default connect(mapStateToProps)(ProfileOtherScreen)
