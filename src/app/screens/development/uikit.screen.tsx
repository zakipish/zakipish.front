import * as React from 'react'
import Block from '../../components/UI/block/block'
import Grid from '../../components/UI/grid/grid'

export default class UIKitScreen extends React.Component<{}> {
  public render() {
    return <div className='wrap'>
      <h1>ZaKipish UI kit</h1>
      <Grid>
        <Grid vertical>
          <Block
            title='Block'
          >
            {/*<Label>Block</Label>*/}
            тест
          </Block>
        </Grid>
        <Grid vertical>
          <Block
            title='Inputs'
          >
            {/*<InputWithSave*/}
            {/*  placeholder='placeholder'*/}
            {/*  saveText='saveText'*/}
            {/*  onSave={() => alert('onSave')}*/}
            {/*/>*/}
          </Block>
        </Grid>
      </Grid>
    </div>
  }
}
