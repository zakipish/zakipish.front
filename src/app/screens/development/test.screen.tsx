import parseDate from 'date-fns/parse'
import * as React from 'react'
import GameDateInfo from '../../components/blocks/game/GameDateInfo/GameDateInfo'
import GameLocationInfo from '../../components/blocks/game/GameLocationInfo/GameLocationInfo'
import GamePriceInfo from '../../components/blocks/game/GamePriceInfo/GamePriceInfo'
import {IconList} from '../../components/blocks/icon/icon.component'
import CommandRightPad from '../../components/sections/command-right-pad.section'
import Block from '../../components/UI/block/block'
import Button, {ButtonColors} from '../../components/UI/button/button'
import Grid from '../../components/UI/grid/grid'
import InputWithSubmit from '../../components/UI/inputWithSubmit/inputWithSubmit'
import TitleInput from '../../components/UI/titleInput/titleInput'
import './test.sass'

/**
 * Экран для тестирования ваших компонентов
 */
export default class TestScreen extends React.Component<{}> {
  public render() {
    return <React.Fragment>
      <div className='GamePage__Wrap'>
        <div style={{
          display: 'flex',
          justifyContent: 'space-between',
          margin: '40px 0'
        }}>
          <TitleInput />
          <Button
            text='Настроить'
            icon={IconList.setting}
            iconAlignment='right'
            onClick={() => alert('Настроить!')}
          />
        </div>
        <Grid
          gap={48}
          columns='1fr 500px'
        >
          <Grid
            gap={14}
            columns='352px 1fr'
          >
            <Block style={{
              gridArea: '1 / 1 / 4 / 1',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              paddingTop: '20px'
            }}>
              <div style={{
                background: '#2A83FB',
                width: '127px',
                height: '127px',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                color: '#FFFFFF',
                fontSize: '12px',
                borderRadius: '133px'
              }}>
                Нет фото
              </div>
              <div
                style={{
                  flex: 1,
                  display: 'flex',
                  textAlign: 'center',
                  alignItems: 'center',
                  width: '178px',
                  color: '#797e8e',
                  fontSize: '12px',
                  lineHeight: '15px'
                }}
              >
                К игре пока никто не присоединился.<br/>
                Создате игру и вы сможете приглашать в её своих друзей, а так же любых других игрков.
              </div>
            </Block>
            <Block className='GamePage__TopGrid__SmallBlock'>
              <GameLocationInfo
                place='г. Санкт-Петербург, ул. марата, 86 ТРК «Планета Нептун», 2 этаж'
              />
            </Block>
            <Block className='GamePage__TopGrid__SmallBlock'>
              <GameDateInfo
                interval={{
                  start: parseDate('2019-02-28 15:00'),
                  end: parseDate('2019-02-28 19:00')
                }}
              />
            </Block>
            <Block className='GamePage__TopGrid__SmallBlock'>
              <GamePriceInfo
                price='1500₽'
              />
            </Block>
          </Grid>
          <Block style={{
            height: '440px',
            background: '#2A83FB'
          }} />
          {/* <GameImageUpload /> */}
        </Grid>
      </div>
      <nav className='GamePage__Menu'>
        <ul className='GamePage__Wrap'>
          <li>Игра</li>
          <li>Описание</li>
          <li>Фото <span>46</span></li>
          <li>Участники</li>
          <li>Место</li>
          <li>Чат</li>
          <li>Итоги</li>
        </ul>
      </nav>
      <div className='GamePage__Wrap' style={{
        marginBottom: '40px'
      }}>
          <Grid>
            <Grid vertical>
              <InputWithSubmit
                size='large'
                placeholder='Введите название игры'
              />
              <Block
                title='Создание команды'
                className='CommandWidget'
              >
                <div style={{
                  marginTop: '31px',
                  position: 'relative'
                }}>
                  <InputWithSubmit
                    size='small'
                    placeholder='Введите название команды'
                    label='Название команды'
                    submitText='Сохранить'
                  />
                  <Button
                    text='Создать команду'
                    style={{
                      position: 'absolute',
                      right: 0,
                      bottom: '20px'
                    }}
                  />
                </div>
                <CommandRightPad new/>
              </Block>
              <div className='center'>
                <Button
                  size='medium'
                  text='Добавить команду'
                  icon={IconList.plus}
                  iconAlignment='right'
                  color={ButtonColors.darkGray}
                  iconOutline={false}
                />
              </div>
            </Grid>
            <Grid vertical>
              <Block
                title='Участники игры'
              >
                <div className={'GamePage__Help'}>
                  <p>
                    К игре пока никто не присоединился.<br/>
                    Создайте игру и вы сможете приглашать в неё своих друзей а так же любых других игроков.
                  </p>
                  <p>
                    После того, как игра будет создана, она будет доступна всем пользователям на экране поиска.
                  </p>
                  <p>
                    Если Вы хотите, чтобы игра была доступна только Вам и Вашим друзьям, сделайте её приватной.
                  </p>
                </div>
              </Block>
              <Block
                title='Основная информация'
              >
                {/* <Form
                  onSubmit={() => alert('submit')}
                  alignment='vertical-with-offset-labels'
                >
                  <FormRow
                    label='Дата игры'
                    input={<DatePicker
                      value={new Date()}
                    />}
                  />
                  <FormRow
                    label='Возраст'
                    input={<AgeSelector />}
                  />
                  <FormRow
                    label='Количество команд'
                    input={<CommandsNumberSelector />}
                  />*/}
                  <div className='center'>
                    <Button
                      size='small'
                      text='Сохранить'
                      color={ButtonColors.darkGray}
                      type='submit'
                    />
                  </div>
                {/*</Form> */}
              </Block>
              <div className='center'>
                <Button
                  size='large'
                  text='Создать игру'
                  color={ButtonColors.green}
                />
              </div>
            </Grid>
          </Grid>
      </div>
    </React.Fragment>
  }
}
