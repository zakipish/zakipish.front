import * as React from 'react'
import { Link, match, Switch, Route, Redirect } from 'react-router-dom'
import { Location } from 'history'
import { history } from '../../core/history'
import { reqGet } from '../../core/api'
import { Playground } from '../../models/playground'

import HeaderProfile from '../../components/layout/profile/header/header.profile'
import TabLinksComponent from '../../components/layout/tab-links.component'

import SideProfile from '../../components/layout/profile/side/side.profile'
import SideParamsProfile from '../../components/layout/profile/side/side-params.profile'
import SideClubProfile from '../../components/layout/profile/side/side-club.profile'
import SideReviewProfile from '../../components/layout/profile/side/side-review.profile'

import ContentProfile from '../../components/layout/profile/content/content.profile'

import MobilePopup from '../../components/layout/mobile-popup'
import PopuplineComponent from '../../components/layout/popupline.component'

import PlaygProfileCreateComponent from './playg-profile-create.screen'
import PlaygProfileBookingComponent from './playg-profile-booking.screen'

interface IProps {
    match: match<any>
    location: Location
}

interface IState {
    selectedTab: number
    playground: Playground
}

class PlaygProfileScreen extends React.Component<IProps, IState> {
    links: string[]

    state: IState = {
        selectedTab: 0,
        playground: null
    }

    componentDidMount() {
        this.initLinksTab()
        this.getPlayground()
    }

    getPlayground = async () => {
        const { id } = this.props.match.params
        reqGet(`/playgrounds/${id}`)
        .then(response => { this.setState({ playground: new Playground(response.data) }) })
        .catch(e => {
            if (e.status == 404)
                history.push('/404')
            console.log(e.message)
        })
    }

    initLinksTab() {
        let { clubId, id } = this.props.match.params
        this.links = [
            `/club/${clubId}/${id}/create${window.location.search}`,
            `/club/${clubId}/${id}/booking${window.location.search}`,
        ]

        const selectedTab = this.links.findIndex(link => link == window.location.pathname + window.location.search)
        if (selectedTab > -1)
            this.setState({ selectedTab })
    }

    onSelectTab = (selectedTab: number) => {
        console.log('called onSelectTab', selectedTab)
        this.setState({ selectedTab })
    }

    get sideParamsCollection() {
        const { playground } = this.state

        return [
            { title: 'Время работы', value: playground.weekSchedule ? `${playground.weekSchedule[0]} - ${playground.weekSchedule[1]}` : '-' },
            { title: '-', value: '-' },
            { title: 'Тип площадки', value: playground.type ? playground.typeName : '-' },
            { title: 'Тип покрытия', value: playground.surfaces ? playground.surfacesNames : '-' }
        ]
    }

    render() {
        const { playground } = this.state

        if (!playground) {
            return ''
        } else {
            const { id } = playground
            const { pathname } = this.props.location
            const { selectedTab } = this.state
            const title = playground.name

            return (
                <div className="profile">
                    <div className="profile__wrap">
                        <MobilePopup preview={
                            <PopuplineComponent
                                name={title}
                                avatar={playground.avatar_url ? playground.avatar_url : "/images/default-playground.png"}
                            />
                        }>
                            <HeaderProfile title={title}/>
                            <SideProfile
                                avatar={playground.avatar_url ? playground.avatar_url : "/images/default-playground.png"}
                                name={playground.name}
                                switcher={!!playground.club.coordinatesArr}
                                geo={ playground.club && playground.club.address ? {
                                    title: playground.club.address,
                                    src: playground.club.coordinatesArr ? `https://static-maps.yandex.ru/1.x/?ll=${playground.club.coordinatesArr[1]},${playground.club.coordinatesArr[0]}&size=200,200&z=17&l=map&pt=${playground.club.coordinatesArr[1]},${playground.club.coordinatesArr[0]},pmwtm` : '/images/playg-geo.png',
                                    coordinates: playground.club.coordinatesArr
                                } : null}
                            >
                                <SideClubProfile
                                    title={`Клуб “${playground.club.name}”`}
                                    offer="Площадка в составе Клуба"
                                    src={playground.club.photo_url}
                                    url={`/club/${playground.club.slug}`}
                                />
                                <SideParamsProfile
                                    list={this.sideParamsCollection}
                                    icons={playground.club && playground.club.infrastructures ?
                                        playground.club.infrastructures.map(i => ({ title: i.name }))
                                    : null}
                                />
                                {/*
                                <SideReviewProfile
                                    title="Отзывы 68"
                                    rating={{
                                        act: 2
                                    }}
                                />
                                */}
                            </SideProfile>
                        </MobilePopup>
                        <ContentProfile>
                            <div className="profileTabs">
                                <TabLinksComponent
                                    urls={this.links}
                                    active={selectedTab}
                                    onSelect={this.onSelectTab}
                                >
                                    <Link to={this.links[0]}>Создать игру</Link>
                                    <Link to={this.links[1]}>Забронировать площадку</Link>
                                </TabLinksComponent>
                            </div>
                            <Switch>
                                <Route path="/club/:clubId/:id/create" render={() => <PlaygProfileCreateComponent id={id} />}/>
                                <Route path="/club/:clubId/:id/booking" render={() => <PlaygProfileBookingComponent id={id} />}/>
                                <Route path={pathname}>
                                    <Redirect to={this.links[0]}/>
                                </Route>
                            </Switch>
                        </ContentProfile>
                    </div>
                </div>
            )
        }
    }
}

export default PlaygProfileScreen
