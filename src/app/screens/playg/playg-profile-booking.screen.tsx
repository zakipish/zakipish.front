import * as React from 'react'
import moment from 'moment'
import { searchParams } from '../../core/utils'
import { reqGet, reqPost } from '../../core/api'
import { history } from "../../core/history"
import { LabelStatus } from '../../components/blocks/label.component'
import { IconList } from '../../components/blocks/icon/icon.component'

import SectionComponent from '../../components/layout/section.component'
import SectionDtComponent from '../../components/sections/section-dt.component'
import SectionTtComponent from '../../components/sections/section-tt.component'

import ContentInfoProfile from '../../components/layout/profile/content/content-info.profile';
import ContenListProfile from '../../components/layout/profile/content/conten-list.profile';
import ContentActionProfile from '../../components/layout/profile/content/content-action.profile';

import WrapForm from '../../components/layout/form/wrap.form'
import RowForm from '../../components/layout/form/row.form'
import FieldForm from '../../components/layout/form/field.form'
import InputControl from '../../components/form/input-control'
import SelectControl from '../../components/form/select-control'
import PriceBlock from '../../components/blocks/price.block'

interface IProps {
    id: number
}

interface IState {
    busy: boolean
    sport_id?: number
    date: any
    dates?: {
        date: string,
        count: number
    }[]
    dateIndex?: number

    times?: {
        start: string,
        end: string,
        status: string,
        price: string
    }[]
    timeStartIndex?: number
    timeEndIndex?: number
    currentDate?: moment.Moment
}

class PlaygProfileBookingScreen extends React.Component<IProps, IState> {

    state = {
        sport_id: null,
        busy: false,
        date: moment(),
        dates: null,
        times: null,
        dateIndex: null,
        timeStartIndex: null,
        timeEndIndex: null,
        currentDate: null
    }

    get searchParams() {
        return (new URL(window.location.href)).searchParams
    }

    constructor(props) {
        super(props)
        this.getDates()
        .then(this.setInitialSport)
        .then(this.getTimes)
    }

    setInitialSport = () => {
        const sport_id = parseInt(this.searchParams.get('sport_id'))
        this.setState({ sport_id })
    }

    setInitialDate = () => {
        const { dates } = this.state
        const dateRaw = this.searchParams.get('date')
        if (dateRaw) {
            const dateIndex = dates.findIndex(date => moment(date['date']).isSame(dateRaw));

            if (dateIndex > -1)
                this.setState({ dateIndex })
        }
    }

    setInitialTime = () => {
        const timeStart = this.searchParams.get('timeStart')
        const timeEnd = this.searchParams.get('timeEnd')
        const { times } = this.state

        const timeStartIndex = times.findIndex(time => moment(time['start']).utcOffset(3).format("HH:mm") == timeStart)
        const timeEndIndex = times.findIndex(time => moment(time['end']).utcOffset(3).format("HH:mm") == timeEnd)

        if (timeStartIndex > -1 && timeEndIndex > -1) {
            if (timeStartIndex != timeEndIndex) {
                this.setState({ timeStartIndex, timeEndIndex })
            } else {
                this.setState({ timeStartIndex })
            }
        }
    }

    onChangeCalendar = (date) => {
        this.setState({ date })
    }

    getDates = async () => {
        const { id } = this.props
        const { currentDate } = this.state

        const getDate = searchParams().get('date')
        const date = currentDate ? currentDate.format('YYYY-MM-DD') : getDate ? getDate : moment()


        return reqGet(`/playgrounds/${id}/dates`, { date })
            .then(response => this.setState({ dates: response.data }, this.setInitialDate))
    }

    get datesCollection() {
        const { dates } = this.state

        if (dates) {
            return dates.map(date => (
                {
                    title: moment(date.date).format("D MMMM"),
                    disabled: date.count === 0
                }
            ))
        } else {
            return null
        }
    }

    onDateChange = (dateIndex: number) => {
        this.setState({ dateIndex, times: null, timeStartIndex: null, timeEndIndex: null }, this.getTimes)
    }

    onChangeCurrentDate = (currentDate: moment.Moment) => {
        this.setState({ dateIndex: null, timeStartIndex: null, timeEndIndex: null, times: null, currentDate }, this.getDates)
    }

    getTimes = () => {
        const { id } = this.props
        const { dates, dateIndex } = this.state
        if (dates && dateIndex != null) {
            const date = dates[dateIndex].date

            reqGet(`/playgrounds/${id}/times`, { date })
            .then(response => this.setState({ times: response.data }, this.setInitialTime))
        }
    }

    get timesCollection() {
        const { times } = this.state
        if (times) {
            return times.map((time) => {
                const start = moment(time.start).utcOffset(3)
                const end = moment(time.end).utcOffset(3)
                const title = `${start.format('H:mm')} - ${end.format('H:mm')}`
                const disabled = time.status !== 'available'
                const price = time.price ? +time.price : '0'
                return {
                    title: title,
                    text: `${price} РУБ`,
                    disabled: disabled
                }
            })
        }
    }

    onTimeStartChange = (timeStartIndex: number) => {
        this.setState({ timeStartIndex })
    }

    onTimeEndChange = (timeEndIndex: number) => {
        this.setState({ timeEndIndex })
    }

    handleSubmit = async () => {
        this.setState({ busy: true }, async () => {
            const { sport_id, dates, dateIndex, times, timeStartIndex, timeEndIndex } = this.state
            const { id } = this.props
            const date = dates[dateIndex]['date']
            const interval = timeEndIndex ?
                // FIXME: zone
                [ moment(times[timeStartIndex]['start']).zone('+0300').format('HH:mm'), moment(times[timeEndIndex]['end']).zone('+0300').format('HH:mm') ]
                    :
                [ moment(times[timeStartIndex]['start']).zone('+0300').format('HH:mm'), moment(times[timeStartIndex]['end']).zone('+0300').format('HH:mm') ]

            const data = {
                playground_id: id,
                booking: true,
                sport_id: sport_id,
                date: date,
                interval: interval
            }

            reqPost('/games', JSON.stringify(data))
            .then(response => history.push(`/game/${response.data.id}`))
            .catch(e => console.log(e.message))
            .finally(() => this.setState({ busy: false }))
        })
    }

    render() {
        const { busy, dates, times, dateIndex, timeStartIndex, timeEndIndex, currentDate } = this.state

        let price = 0
        if (timeStartIndex != null)
            price = times.reduce((acc, time, i) => {
                if (timeEndIndex == null) {
                    if (i == timeStartIndex)
                        acc += +time.price
                } else {
                    if (i >= timeStartIndex && i <= timeEndIndex)
                        acc += +time.price
                }
                return acc
            }, 0)

        return (
            <div>
                { dates ?
                    <SectionComponent title="Выберите дату" classname="--noPad">
                        <SectionDtComponent
                            list={this.datesCollection}
                            selected={dateIndex}
                            onChange={this.onDateChange}
                            date={currentDate || moment()}
                            onChangeDate={this.onChangeCurrentDate}
                        />
                    </SectionComponent>
                : '' }

                { times ?
                <SectionComponent title="Выберите время" classname="section--border --noPad">
                    <SectionTtComponent
                        selectedStart={timeStartIndex}
                        selectedEnd={timeEndIndex}
                        list={this.timesCollection}
                        onStartChange={this.onTimeStartChange}
                        onEndChange={this.onTimeEndChange}
                    />
                </SectionComponent>
                : ''}

                { dateIndex !== null && timeStartIndex !== null ?
                    <React.Fragment>
                        <PriceBlock price={price} className="--offset" />
                        <ContentActionProfile
                            list={[
                              { title: 'Забронировать', bg: 'orange', icon: IconList.dpArrRight, onClick: this.handleSubmit, disabled: busy },
                            ]}
                        />
                    </React.Fragment>
                : '' }
                {/*
                <ContentInfoProfile
                    info="Что будет, если к вам никто не присоединится?"
                >
                    <ContenListProfile
                        type="options"
                        list={prices}
                    />
                </ContentInfoProfile>
                */}
            </div>
        )
    }
}

export default PlaygProfileBookingScreen
