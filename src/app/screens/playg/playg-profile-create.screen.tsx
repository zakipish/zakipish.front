import * as React from 'react'
import moment from 'moment'
import { searchParams } from '../../core/utils'
import { reqGet, reqPost } from '../../core/api'
import { history } from "../../core/history"
import { LabelStatus } from '../../components/blocks/label.component'
import { IconList } from '../../components/blocks/icon/icon.component'

import SectionComponent from '../../components/layout/section.component'
import SectionDtComponent from '../../components/sections/section-dt.component'
import SectionTtComponent from '../../components/sections/section-tt.component'

import ContentInfoProfile from '../../components/layout/profile/content/content-info.profile'
import ContenListProfile from '../../components/layout/profile/content/conten-list.profile'
import ContentActionProfile from '../../components/layout/profile/content/content-action.profile'

import WrapForm from '../../components/layout/form/wrap.form'
import RowForm from '../../components/layout/form/row.form'
import FieldForm from '../../components/layout/form/field.form'
import InputControl from '../../components/form/input-control'
import SelectControl from '../../components/form/select-control'
import PriceBlock from '../../components/blocks/price.block'

interface IProps {
    id: number
}

interface IState {
    busy: boolean
    sport_id?: number
    gameLevels: any[]
    date: any
    dates?: {
        date: string,
        count: number
    }[]
    dateIndex?: number

    times?: {
        start: string,
        end: string,
        status: string,
        price: string
    }[]
    timeStartIndex?: number
    timeEndIndex?: number
    friend_id?: number

    game: {
        name: string
        age?: number
        min_players?: number
        max_players?: number
        game_level_id?: number
    },
    currentDate?: moment.Moment
}

class PlaygProfileCreateScreen extends React.Component<IProps, IState> {

    state = {
        sport_id: null,
        busy: false,
        gameLevels: null,
        date: moment(),
        dates: null,
        times: null,
        dateIndex: null,
        timeStartIndex: null,
        timeEndIndex: null,
        friend_id: null,
        game: {
            name: null,
            age: null,
            min_players: null,
            max_players: null,
            game_level_id: null
        },
        currentDate: null
    }

    componentDidMount() {
        this.getDates()
        .then(this.setInitialSport)
        .then(this.getTimes)
        this.getGameLevels()
    }

    setInitialSport = () => {
        const sport_id = parseInt(searchParams().get('sport_id'))
        this.setState({ sport_id })
    }

    setInitialDate = () => {
        const { dates } = this.state
        const dateRaw = searchParams().get('date')
        if (dateRaw) {
            const dateIndex = dates.findIndex(date => moment(date['date']).isSame(dateRaw));

            if (dateIndex > -1)
                this.setState({ dateIndex })
        }
    }

    setInitialTime = () => {
        const timeStart = searchParams().get('timeStart')
        const timeEnd = searchParams().get('timeEnd')
        const { times } = this.state

        const timeStartIndex = times.findIndex(time => moment(time['start']).utcOffset(3).format("HH:mm") == timeStart)
        const timeEndIndex = times.findIndex(time => moment(time['end']).utcOffset(3).format("HH:mm") == timeEnd)

        if (timeStartIndex > -1 && timeEndIndex > -1) {
            if (timeStartIndex != timeEndIndex) {
                this.setState({ timeStartIndex, timeEndIndex })
            } else {
                this.setState({ timeStartIndex })
            }
        }
    }

    onChangeCalendar = (date) => {
        this.setState({ date })
    }

    getGameLevels = () => {
        reqGet('/game_levels')
        .then(response => this.setState({ gameLevels: response.data }))
    }

    get gameLevelsCollection() {
        const { gameLevels } = this.state

        let result = {}
        if (gameLevels)
            gameLevels.forEach(gameLevel => {
                result[gameLevel.id] = gameLevel.name
            })

        return result
    }

    getDates = async () => {
        const { id } = this.props
        const { currentDate } = this.state

        const getDate = searchParams().get('date')
        const date = currentDate ? currentDate.format('YYYY-MM-DD') : getDate ? getDate : moment()


        return reqGet(`/playgrounds/${id}/dates`, { date })
            .then(response => this.setState({ dates: response.data }, this.setInitialDate))
    }

    get datesCollection() {
        const { dates } = this.state

        if (dates) {
            return dates.map(date => (
                {
                    title: moment(date.date).format("D MMMM"),
                    disabled: date.count === 0
                }
            ))
        } else {
            return null
        }
    }

    onDateChange = (dateIndex: number) => {
        this.setState({ dateIndex, times: null, timeStartIndex: null, timeEndIndex: null }, this.getTimes)
    }

    onChangeCurrentDate = (currentDate: moment.Moment) => {
        this.setState({ dateIndex: null, timeStartIndex: null, timeEndIndex: null, times: null, currentDate }, this.getDates)
    }

    getTimes = () => {
        const { id } = this.props
        const { dates, dateIndex } = this.state
        if (dates && dateIndex != null) {
            const date = dates[dateIndex].date

            reqGet(`/playgrounds/${id}/times`, { date })
            .then(response => this.setState({ times: response.data }, this.setInitialTime))
        }
    }

    get timesCollection() {
        const { times } = this.state
        if (times) {
            return times.map((time) => {
                const start = moment(time.start).utcOffset(3)
                const end = moment(time.end).utcOffset(3)
                const title = `${start.format('H:mm')} - ${end.format('H:mm')}`
                const disabled = time.status !== 'available'
                const price = time.price ? +time.price : '0'
                return {
                    title: title,
                    text: `${price} РУБ`,
                    disabled: disabled
                }
            })
        }
    }

    onTimeStartChange = (timeStartIndex: number) => {
        this.setState({ timeStartIndex })
    }

    getFullPrice = () => {
        let {times, timeStartIndex, timeEndIndex} = this.state
        if (!timeEndIndex) {
            timeEndIndex = timeStartIndex
        }

        let newTimes = times.filter((item, i) => (i >= timeStartIndex && i <= timeEndIndex))
        return newTimes.reduce((reducer,time)=>reducer+=parseInt(time.price),0)
    }

    onTimeEndChange = (timeEndIndex: number) => {
        this.setState({ timeEndIndex })
    }

    handleChange = (key: string) => (value: any) => {
        const { game } = this.state
        game[key] = value
        this.setState({ game })
    }

    handleSubmit = async () => {
        this.setState({ busy: true }, async () => {
            const { sport_id, game, dates, dateIndex, times, timeStartIndex, timeEndIndex } = this.state
            const { id } = this.props

            let age_start = null
            let age_end = null

            switch(parseInt(game.age)) {
                case 0:
                    age_end = 15
                break
                case 1:
                    age_start = 15
                    age_end = 17
                break
                case 2:
                    age_start = 18
                    age_end = 24
                break
                case 3:
                    age_start = 25
                    age_end = 29
                break
                case 4:
                    age_start = 30
                    age_end = 35
                break
                case 5:
                    age_start = 36
                    age_end = 45
                break
                case 6:
                    age_start = 45
                break
                default:
                    age_start = 99
                    age_end = 99
            }

            // TODO
            const date = dates[dateIndex]['date']
            const interval = timeEndIndex ?
                // FIXME: zone
                [ moment(times[timeStartIndex]['start']).zone('+0300').format('HH:mm'), moment(times[timeEndIndex]['end']).zone('+0300').format('HH:mm') ]
                    :
                [ moment(times[timeStartIndex]['start']).zone('+0300').format('HH:mm'), moment(times[timeStartIndex]['end']).zone('+0300').format('HH:mm') ]

            const friend_id = searchParams().get('friend_id')

            const data = {
                name: game.name,
                playground_id: id,
                booking: false,
                min_players: game.min_players,
                max_players: game.max_players,
                game_level_id: game.game_level_id,
                age_start: age_start,
                age_end: age_end,
                sport_id: sport_id,
                date: date,
                interval: interval,
                friend_id: friend_id
            }

            reqPost('/games', JSON.stringify(data))
            .then(response => history.push(`/game/${response.data.id}`))
            .catch(e => console.log(e.message))
            .finally(() => this.setState({ busy: false }))
        })
    }

    get minPlayersCollection() {
        let playersCollection = {}
        for (let i = 2; i <= 25; i++) {
            playersCollection[i] = i
        }

        return playersCollection
    }

    get maxPlayersCollection() {
        const { min_players } = this.state.game
        let playersCollection = {}
        const start = min_players ? min_players : 2
        for (let i = start; i <= 25; i++) {
            playersCollection[i] = i
        }

        return playersCollection
    }

    render() {
        const { busy, game, dates, times, dateIndex, timeStartIndex, timeEndIndex, gameLevels, currentDate } = this.state
        const valid = game.min_players && game.max_players && parseInt(game.max_players) >= parseInt(game.min_players)

        const prices = [
            { title: 'Осталось заплатить', price: 800, priceBg: 'green' },
            { title: 'Осталось заплатить', price: 200, priceBg: 'darkBlue' },
        ]

        const agesCollection = {
            0: 'до 15',
            1: '15-17',
            2: '18-24',
            3: '25-29',
            4: '30-35',
            5: '36-45',
            6: '45+'
        }

        return (
            <div>
                { dates ?
                    <SectionComponent title="Выберите дату" classname="--noPad">
                        <SectionDtComponent
                            list={this.datesCollection}
                            selected={dateIndex}
                            onChange={this.onDateChange}
                            date={currentDate || moment()}
                            onChangeDate={this.onChangeCurrentDate}
                        />
                    </SectionComponent>
                : '' }

                { times ?
                    <SectionComponent title="Выберите время" classname="--noPad section--border">
                        <SectionTtComponent
                            selectedStart={timeStartIndex}
                            selectedEnd={timeEndIndex}
                            list={this.timesCollection}
                            onStartChange={this.onTimeStartChange}
                            onEndChange={this.onTimeEndChange}
                        />
                    </SectionComponent>
                : ''}

                { dateIndex !== null && timeStartIndex !== null && gameLevels ?
                    <React.Fragment>
                        <WrapForm
                            className="--col playg-profile-create-form"
                            title="Настройки игры"
                        >
                            <RowForm className="--col6">
                                <FieldForm title="Название игры">
                                    <InputControl
                                        value={game.name}
                                        onChange={this.handleChange('name')}
                                    />
                                </FieldForm>
                            </RowForm>
                            <RowForm className="--col6">
                                <FieldForm title="Возраст икроков">
                                    <SelectControl
                                        options={agesCollection}
                                        value={game.age}
                                        onChange={this.handleChange('age')}
                                    />
                                </FieldForm>
                            </RowForm>
                            <RowForm className="--col6">
                                <FieldForm title="Игроков">
                                    <SelectControl
                                        options={this.maxPlayersCollection}
                                        value={game.max_players}
                                        onChange={this.handleChange('max_players')}
                                    />
                                </FieldForm>
                                <FieldForm title="Минимум" info="Дополнительная информация">
                                    <SelectControl
                                        options={this.minPlayersCollection}
                                        value={game.min_players}
                                        onChange={this.handleChange('min_players')}
                                    />
                                </FieldForm>
                            </RowForm>
                            <RowForm className="--col6">
                                <FieldForm title="Уровень игроков">
                                    <SelectControl
                                        options={this.gameLevelsCollection}
                                        value={game.game_level_id}
                                        onChange={this.handleChange('game_level_id')}
                                    />
                                </FieldForm>
                            </RowForm>
                        </WrapForm>
                        <PriceBlock price={this.getFullPrice()} className="--offset" />
                        <ContentActionProfile
                            list={[
                              { title: 'создать игру', bg: 'green', icon: IconList.dpArrRight, onClick: this.handleSubmit, disabled: busy || !valid },
                            ]}
                        />
                    </React.Fragment>
                : '' }
                {/*
                <ContentInfoProfile
                    info="Что будет, если к вам никто не присоединится?"
                >
                    <ContenListProfile
                        type="options"
                        list={prices}
                    />
                </ContentInfoProfile>
                */}
            </div>
        )
    }
}

export default PlaygProfileCreateScreen
