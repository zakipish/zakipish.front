import * as React from 'react'

import { Club } from '../models/club'
import { GameLevel } from '../models/game-level'

import SectionComponent from '../components/layout/section.component'
import GridEvent from '../components/layout/event/grid.event'
import EventCard from '../components/cards/event.card'
import MoreEventCard from '../components/cards/more-event.card'
import Button from '../components/blocks/button'
import ClubSection from '../components/sections/club.section'
import PlaygCard from '../components/cards/playg.card'
import { IconList } from '../components/blocks/icon/icon.component';

interface Props {}

interface State {}

class DevScreen extends React.Component<Props, State> {
    render() {
        return (
            <>
                <SectionComponent title="НАЙДЕНО ПЛОЩАДОК 19">
                    <ClubSection
                        title="Спортивный клуб Alex Fitnes имени Ивана Ивановича при поддержке Газпром корпрорации"
                        address="Левашовский пр. 12а"
                    >
                        <PlaygCard
                            title="Бассейн имени Майлка Фелпса имени вв Путина"
                            params={[
                                { tit: 'Время работы',  val: '9:00 - 21:00' },
                                { tit: 'Тип площадки',  val: 'Закрытая' },
                                { tit: 'Тип покрытия',  val: 'Хорошее' },
                            ]}
                            status="В зале есть расписание"
                            price={2300}
                        />
                        <PlaygCard/>
                        <PlaygCard/>
                        <PlaygCard/>
                        <PlaygCard/>
                        <PlaygCard/>
                    </ClubSection>
                    <ClubSection
                        title="Спортивный клуб Alex Fitnes"
                        address="Левашовский пр. 12а"
                    >
                        <PlaygCard/>
                    </ClubSection>
                </SectionComponent>
                <SectionComponent title="НАЙДЕНО КИПИША 13.000">
                    <GridEvent mobileSwipe={true}>
                        <EventCard
                            type="meeting"
                            title="Корпоративный выездной футбол команды Google"
                            club={new Club({name: 'Спортивный клуб Alex Fitnes имени Ивана Ивановича при поддержке Газпром корпрорации'})}
                            address="Малый средний индустриальный проспект 2088"
                            min_players={10}
                            gameLevel={new GameLevel({name: 'PRO'})}
                            sportName="Мини Футбол 10х10"
                        />
                        <EventCard
                            type="meeting"
                            title="Корпоративный выездной футбол команды Google"
                            club={new Club({name: 'Спортивный клуб Alex Fitnes имени Ивана Ивановича при поддержке Газпром корпрорации'})}
                            address="Малый средний индустриальный проспект 2088"
                            min_players={10}
                            gameLevel={new GameLevel({name: 'PRO'})}
                            sportName="Мини Футбол 10х10"
                        />
                        <MoreEventCard
                            title="Откройте для себя увлекательный мир спорта и наших событий вместе с командой ZaKipish"
                            btnTitle="Показать всё"
                        />
                    </GridEvent>
                    <hr/>
                    <br/>
                    <Button>Покзать еще</Button>
                </SectionComponent>
                <SectionComponent title="НАЙДЕНО ИГР 777">
                    <GridEvent>
                        <EventCard
                            title="Корпоративный выездной футбол команды Google"
                            club={new Club({name: 'Спортивный клуб Alex Fitnes имени Ивана Ивановича при поддержке Газпром корпрорации'})}
                            address="Малый средний индустриальный проспект 2088"
                            min_players={10}
                        />
                        <EventCard
                            title="Корпоративный выездной футбол команды Google"
                            club={new Club({name: 'Спортивный клуб Alex Fitnes имени Ивана Ивановича при поддержке Газпром корпрорации'})}
                            address="Малый средний индустриальный проспект 2088"
                            min_players={10}
                        />
                        <EventCard
                            title="Корпоративный выездной футбол команды Google"
                            club={new Club({name: 'Спортивный клуб Alex Fitnes имени Ивана Ивановича при поддержке Газпром корпрорации'})}
                            address="Малый средний индустриальный проспект 2088"
                            min_players={10}
                            price="2000"
                        />
                    </GridEvent>
                    <hr/>
                    <br/>
                    <Button
                        offerTop="Вы посомтрели 77 из 13 000"
                    >Загрузить еще</Button>
                </SectionComponent>
            </>
        )
    }
}

export default DevScreen
