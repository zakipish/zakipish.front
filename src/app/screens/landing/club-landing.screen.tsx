import * as React from 'react'
import { history } from '../../core/history'

import { IconList } from '../../components/blocks/icon/icon.component'
import { prepareGarmoshka, prepareInfrastructures, prepareImages, weekSchedule } from './util.landing'

import SectionComponent from '../../components/layout/section.component'
import SectionLandingFace from '../../components/sections/section-landing-face'
import FaceCard from '../../components/layout/face-card/_face-card'
import SliderFaceCard from '../../components/layout/face-card/slider.face-card'
import InformFaceCard from '../../components/layout/face-card/inform.face-card'
import ListFaceCard from '../../components/layout/face-card/list.face-card'
import ListItemFaceCard from '../../components/layout/face-card/list-item.face-card'
import LandNavSection from '../../components/sections/land-nav.section'
import LandDescriptionSection from '../../components/sections/land-description.section'
import LandSliderSection from '../../components/sections/land-slider.section'
import ReviewLineSection from '../../components/sections/review-line.section'
import { reqGet } from "../../core/api";
import { Club } from "../../models/club"
import GarmoshkaSlider from '../../components/blocks/garmoshka.slider'
import PriceListComponent from '../../components/blocks/price-list.component'
import ContactSection from '../../components/sections/contact.section'
import ExternalLink from '../../components/blocks/external-link'

interface IState {
    club: Club
}

interface IProps {
    match: any
}

class ClubLandingScreen extends React.Component<IProps, IState> {
    constructor(props) {
        super(props)
        this.state = {
            club: null
        }
    }

    componentDidMount(): void {
        this.bootstrap()
    }

    bootstrap = () => {
        const { id } = this.props.match.params;
        reqGet(`/club/${id}`)
            .then(response => this.setState({ club: new Club(response.data) }))
            .catch(e => {
                if (e.status == 404)
                    history.push('/404')
                console.log(e.message)
            })
    }

    getContactData = () => {
        const { club } = this.state
        const weekDaysSchedule = club.weekDaysSchedule && club.weekDaysSchedule[0] != null ? `${club.weekDaysSchedule[0]} - ${club.weekDaysSchedule[1]}` : 'Не указано'

        const weekEndSchedule = club.weekEndSchedule && club.weekEndSchedule[0] != null ? `${club.weekDaysSchedule[0]} - ${club.weekDaysSchedule[1]}` : 'Не указано'

        let data =[]

        if (club.phone)
            data.push({ key: 'Телефон:', value: <ExternalLink href={`callto:${club.phone}`}>{club.phone}</ExternalLink>})

        if (club.address)
            data.push({ key: 'Адрес:', value: <ExternalLink href="#">{club.address}</ExternalLink>})

        data.push({ key: 'Время работы:', value: <div><span>Пн-Пт {weekDaysSchedule}</span><span>Суб-Вс {weekEndSchedule}</span></div> })

        if (club.sajt)
            data.push({ key: 'Сайт:', value: <ExternalLink href={`https://${club.sajt}`} target="_blank">{club.sajt}</ExternalLink> })

        return data
    }

    getClubInfo = () => {
        const {club} = this.state;
        let out = [];
        club.phone ? out.push({ tit: 'Контакты', val: club.phone, className: '--fill' }) : null
        out.push({ tit: 'Время работы', val: weekSchedule(club.weekSchedule), className: '--fill' });
        club.address ? out.push({ tit: 'Адрес', val: club.address }):null;
        return out;
    }

    getClubContacts = ()=>{
        const { club } = this.state
        let out = []
        club.vkontakte ? out.push({ provider: 'vkontakte', url: club.vkontakte }) : null
        club.facebook ? out.push({ provider: 'facebook', url: club.facebook }) : null
        return out
    }

    get menuList() {
        const { club } = this.state
        let list = []

        list = list.concat([
            {tit: 'Клуб', anchor: '.land-page'},
            {tit: 'О клубе', anchor: '#description'},
            {tit: 'Фото', addTxt: String(club.images.length), anchor: '#photo'},
            {tit: 'Отзывы', addTxt: '', anchor: '#review'},
            {
                tit: 'Инфраструктура',
                addTxt: String(club.infrastructures.length),
                anchor: '#infstruct'
            },
            {tit: 'Контакты', anchor: '#contact'},
        ])

        if (club.hasFeatureSchedules)
            list.push({tit: 'Забронировать', className: '--btn --green',anchor:""})

        return list
    }

    render() {
        const { club } = this.state

        let images = club ? prepareImages(club.images) : ['/images/default-club.png']
        let garmoshkaSlider = club ? prepareGarmoshka(club.sports) : null
        let infstructList = club ? prepareInfrastructures(club.infrastructures) : null

        let clubLogo = (club && club.logo_url) ? club.logo_url : '/images/club-logo.png'

        return (
            <div>
                { club?
                    <div className="land-page">
                        <SectionLandingFace>
                            <FaceCard type="avatar" bookingLink={club.hasFeatureSchedules ? "#" : null}>
                                <SliderFaceCard list={images}/>
                                <InformFaceCard
                                    avatar={clubLogo}
                                    title={club.name}
                                    reviews={[]}
                                    options={this.getClubInfo()}
                                    params={infstructList && {
                                        title: 'Инфраструктура',
                                        list: infstructList,
                                    }}
                                />
                            </FaceCard>
                            <ListFaceCard>
                                { club.playgrounds.map((playground,i)=>
                                    <ListItemFaceCard
                                        avatar={playground.avatar_url ? playground.avatar_url: "/images/default-playground.png"}
                                        link={`/club/${club.slug}/${playground.slug}`}
                                        type="зал"
                                        key={i}
                                        title={playground.name}
                                        options={[
                                            { tit: 'Тип площадки', val: playground.typeName },
                                            { tit: 'Тип покрытия', val: playground.surfaces.map(surface => surface.name).join(', ') },
                                        ]}
                                        reviews={[0,0,0,0,0,0,0,0]}
                                        params={{
                                            title: 'Спорт',
                                            list: playground.sports.map(sport => ({ title: sport.name,src:sport.icon_url })),
                                        }}
                                    />
                                )}
                            </ListFaceCard>
                        </SectionLandingFace>
                        <LandNavSection
                            stopSelector=".hdr"
                            list={this.menuList}
                        />
                        <div id="description"></div>
                        <LandDescriptionSection
                            subtitle="О зале"
                            title={club.name}
                            text={club.description}
                        />
                        {(images && images.length > 1) && (
                            <React.Fragment>
                                <div id="photo"></div>
                                <LandSliderSection title="Фото" list={images}/>
                            </React.Fragment>
                        )}
                        <div id="review"></div>
                        {/*<ReviewLineSection
                            title="Отзывы"
                            length={219}
                            list={[
                                { name: 'Дима', date: '07.10.2018 17:09', rating: 5, comment: 'Супер зал для супер людей. '},
                                { name: 'Света', date: '', rating: 3, comment: 'Суууууууууууууууууууууууууууууууууупер пупер'},
                                { name: 'Олег', date: '', rating: 1, comment: 'Супер зал для супер людей.'},
                            ]}
                        />*/}
                        {garmoshkaSlider && (
                            <SectionComponent title="Спорты">
                                <GarmoshkaSlider list={garmoshkaSlider}/>
                            </SectionComponent>
                        )}
                        {infstructList && (
                            <React.Fragment>
                                <div id="infstruct"></div>
                                <SectionComponent title="Инфраструктура">
                                    <PriceListComponent list={infstructList}/>
                                </SectionComponent>
                            </React.Fragment>
                        )}
                        <div id="contact"></div>
                        <SectionComponent classname="--blue --last secContactAnchor">
                            <ContactSection
                                list={this.getContactData()}
                                soc={this.getClubContacts()}
                                coordinates={club.coordinatesArr}
                            />
                        </SectionComponent>
                    </div>
                : null}
            </div>
        )
    }
}

export default ClubLandingScreen
