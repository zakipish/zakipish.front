import * as React from 'react'
import { match } from 'react-router'
import { history } from '../../core/history'

import { IconList } from '../../components/blocks/icon/icon.component'

import { reqGet } from '../../core/api'
import { Playground } from '../../models/playground'
import { goToAnchor } from "../../core/utils"
import { prepareGarmoshka, preparePriceList, prepareImages, weekSchedule } from './util.landing'

import SectionLandingFace from '../../components/sections/section-landing-face'
import FaceCard from '../../components/layout/face-card/_face-card'
import AvatarFaceCard from '../../components/layout/face-card/avatar.face-card'
import InformFaceCard from '../../components/layout/face-card/inform.face-card'
import SliderFaceCard from '../../components/layout/face-card/slider.face-card'
import LandNavSection from '../../components/sections/land-nav.section'
import LandDescriptionSection from '../../components/sections/land-description.section'
import LandSliderSection from '../../components/sections/land-slider.section'
import SectionComponent from '../../components/layout/section.component'
import GarmoshkaSlider from '../../components/blocks/garmoshka.slider'
import PriceListComponent from '../../components/blocks/price-list.component'
import ContactSection from '../../components/sections/contact.section'
import Planner from '../../components/planner/planner'
import ExternalLink from '../../components/blocks/external-link'

interface IProps {
    match: match<any>
    location: any
}

interface IState {
    playg: Playground
}

class PlaygLandingScreen extends React.Component<IProps, IState> {

    timeId: any

    state = {
        playg: null
    }

    componentDidMount() {
        this.getPlayg()
    }

    componentDidUpdate() {
        if (this.checkAnchor && !this.timeId) {
            this.timeId = setTimeout(goToAnchor(this.props.location.hash), 500)
        } else {
            clearTimeout(this.timeId)
        }
    }

    checkAnchor = () => {
        return this.props.location.hash
    }

    getPlayg = async () => {
        let { clubId, id } = this.props.match.params
        reqGet(`/clubs/${clubId}/playgrounds/${id}`)
            .then(response => {
                this.setState({ playg: new Playground(response.data) })
            })
            .catch(e => {
                if (e.status == 404)
                    history.push('/404')
                console.log(e.message)
            })
    }

    getContactData = () => {
        const { playg } = this.state
        const club = playg.club
        const weekDaysSchedule = playg.weekDaysSchedule && playg.weekDaysSchedule[0] != null ? `${playg.weekDaysSchedule[0]} - ${playg.weekDaysSchedule[1]}` : 'Не указано'
        const weekEndSchedule = playg.weekEndSchedule && playg.weekEndSchedule[0] != null ? `${playg.weekDaysSchedule[0]} - ${playg.weekDaysSchedule[1]}` : 'Не указано'
        let data = []

        if (club.phone)
            data.push({ key: 'Телефон:', value: <ExternalLink href={`callto:${club.phone}`}>{club.phone}</ExternalLink>})

        if (club.address)
            data.push({ key: 'Адрес:', value: <ExternalLink href="#">{club.address}</ExternalLink>})

        data.push({ key: 'Время работы:', value: <div><span>Пн-Пт {weekDaysSchedule}</span><span>Суб-Вс {weekEndSchedule}</span></div> })

        if (club.sajt)
            data.push({ key: 'Сайт:', value: <ExternalLink href={`https://${club.sajt}`} target="_blank">{club.sajt}</ExternalLink> })

        return data
    }

    getClubContacts = () => {
        const { club } = this.state.playg
        let data = []
        if (club.vkontakte) {
            data.push({ provider: 'vkontakte', url: club.vkontakte })
        }
        if (club.facebook) {
            data.push({ provider: 'facebook', url: club.facebook })
        }
        return data
    }

    get menuList() {
        const { playg } = this.state

        let list = []

        list = list.concat([
            { tit: 'Зал', anchor: '.land-page' },
            { tit: 'О зале', anchor: '#description' },
            { tit: 'Фото', addTxt: String(playg.images.length), anchor: '#photo' },
            { tit: 'Отзывы', addTxt: '', anchor: '#review' },
            { tit: 'Инвентарь', addTxt: String(playg.equipments.length), anchor: '#infstruct' },
            { tit: 'Контакты', anchor: '#contact' },
        ])

        if(playg.hasFeatureSchedules)
            list.push({ tit: 'Играть', className: '--btn --green' })

        return list
    }

    render() {
        const { playg } = this.state
        let { id, clubId } = this.props.match.params

        let images = playg && playg.images.length > 0 ? prepareImages(playg.images) : ["/images/default-playground.png"]
        let garmoshkaSlider = playg ? prepareGarmoshka(playg.sports) : null
        let infstructList = playg ? preparePriceList(playg.equipments) : null
        let serviseList = playg ? preparePriceList(playg.services) : null

        let clubAvatar = (playg && playg.club && playg.club.avatar_url) ? playg.club.avatar_url : '/images/default-club.png'

        return (
            playg ?
                <div className="land-page">
                    <SectionLandingFace>
                        <FaceCard type="small">
                            <AvatarFaceCard
                                name={playg.club.name}
                                src={clubAvatar}
                                switcher={!!playg.club.coordinatesArr}
                                geoSrc={playg.club.coordinatesArr ? `https://static-maps.yandex.ru/1.x/?ll=${playg.club.coordinatesArr[1]},${playg.club.coordinatesArr[0]}&size=200,200&z=17&l=map&pt=${playg.club.coordinatesArr[1]},${playg.club.coordinatesArr[0]},pmwtm` : '/images/playg-geo.png'}
                                adress={playg.club.address}
                                coordinates={playg.club.coordinatesArr}
                            />
                            <InformFaceCard
                                type="клуб"
                                title={playg.club.name}
                                link={`/club/${playg.club.slug}`}
                                reviews={[]}
                                options={[
                                    { tit: 'Контакты', val: '8-800-200-06-00' },
                                ]}
                                params={{
                                    title: 'Инфраструктура',
                                    defaultIcon: IconList.rocket,
                                    list: playg.club.infrastructures,
                                }}
                            />
                        </FaceCard>
                        <FaceCard type="large">
                            <SliderFaceCard list={images}/>
                            <InformFaceCard
                                type="зал"
                                title={playg.name}
                                reviews={[]}
                                options={[
                                    { tit: 'Время работы', val: weekSchedule(playg.weekSchedule), className: '--fill' },
                                    { tit: 'Тип площадки', val: playg.typeName },
                                    { tit: 'Тип покрытия', val: playg.surfacesNames },
                                ]}
                                params={{
                                    title: 'Спорт',
                                    list: playg.sports.map(sport => ({
                                        title: sport.name,
                                        src: sport.icon_url,
                                    }))
                                }}
                            />
                        </FaceCard>
                    </SectionLandingFace>
                    <LandNavSection
                        stopSelector=".hdr"
                        list={this.menuList}
                    />
                    <div id="description"></div>
                    <LandDescriptionSection
                        subtitle="О зале"
                        title={playg.name}
                        text={playg.description}
                    />
                    {(images && images.length > 1) && (
                        <React.Fragment>
                            <div id="photo"></div>
                            <LandSliderSection title="Фото" list={images}/>
                        </React.Fragment>
                    )}
                    <div id="review"></div>
                    {/* <ReviewLineSection
                        title="Отзывы"
                        length={219}
                        list={[
                            { name: 'Дима', date: '07.10.2018 17:09', rating: 5, comment: 'Супер зал для супер людей. '},
                            { name: 'Света', date: '', rating: 3, comment: 'Суууууууууууууууууууууууууууууууууупер пупер'},
                            { name: 'Олег', date: '', rating: 1, comment: 'Супер зал для супер людей.'},
                        ]}
                    />*/}
                    {garmoshkaSlider && (
                        <SectionComponent title="Спорт" classname="--land">
                            <GarmoshkaSlider list={garmoshkaSlider}/>
                        </SectionComponent>
                    )}
                    {infstructList && (
                        <React.Fragment>
                            <div id="infstruct"></div>
                            <SectionComponent title="Инвентарь" classname="--land">
                                <PriceListComponent list={infstructList}/>
                            </SectionComponent>
                        </React.Fragment>
                    )}
                    {serviseList && (
                        <SectionComponent title="Услуги" classname="--land">
                            <PriceListComponent list={serviseList}/>
                        </SectionComponent> 
                    )}
                    <div id="booking"></div>
                    <SectionComponent title="Бронирование" classname="--land --rplScroll">
                        <Planner id={id} clubId={clubId} />
                    </SectionComponent>
                    <div id="contact"></div>
                    <SectionComponent classname="--blue --last">
                        <ContactSection
                            list={this.getContactData()}
                            soc={this.getClubContacts()}
                            coordinates={playg.club.coordinatesArr}
                        />
                    </SectionComponent>
                </div>
                : null
        )
    }
}

export default PlaygLandingScreen
