export const prepareGarmoshka = (list) => {
    if (list && list.length > 0) {
        let out = []
        const rootSports = list.filter(item => item.label.split('.').length === 1);
        for (let rootSport of rootSports) {
            let childrenSports = list.filter(sport => sport.label.split('.')[0] == rootSport.label && sport.id != rootSport.id)
            out.push({
                name: rootSport.name, 
                img: rootSport.photo_url ? rootSport.photo_url : '/assets/userfiles/garmoshka-1.png', 
                items: childrenSports.map(childrenSport => childrenSport.name) 
            })
        }
        return out.length > 0 ? out : null
    }
    return null
}

export const preparePriceList = (list) => {
    if (list && list.length > 0) {
        return list.map(item => ({
            title: item.name,
            params: [item.price + ' РУБ', `За ${item.unit}`]
        }))
    } 
    return null
}

export const prepareInfrastructures = (list) => {
    if (list && list.length > 0) {
        return list.map(item => ({ 
            title: item.name, 
            src: item.icon, 
            href: "#infstruct", 
            params: ['']/*['0 руб.', 'За -']*/ 
        }))
    } 
    return null
}

export const prepareImages = (list) => {
    if (list) {
        return list
            .sort((a, b) => a.order - b.order)
            .map(img => img.url)
    } 
    return null
}

export const weekSchedule = (weekSchedule) => {
    return weekSchedule && weekSchedule[0] != null ? `${weekSchedule[0]} - ${weekSchedule[1]}` : 'Не указано'
}
