import { addHours, format as formatDate, parse as parseDate } from 'date-fns'
import locale from 'date-fns/locale/ru'
import * as React from 'react'
import { connect } from 'react-redux'
import LabelComponent, { LabelStatus } from '../../components/blocks/label.component'
import ScrollIconComponent from '../../components/blocks/scroll-icon/scroll-icon.component'
import InputControl from '../../components/form/input-control'
import FilterComponent from '../../components/layout/filter.component'
import Loader from '../../components/layout/loader'
import SectionComponent from '../../components/layout/section.component'
import SectionDtComponent from '../../components/sections/section-dt.component'
import SectionEventsComponent from '../../components/sections/section-events.component'
import SectionGamesComponent from '../../components/sections/section-games.component'
import SectionMapComponent from '../../components/sections/section-map.component'
import SectionPlaygroundsComponent from '../../components/sections/section-playgrounds.component'
import SectionTtComponent from '../../components/sections/section-tt.component'
import { reqGet } from '../../core/api'
import { pluralize } from '../../core/utils'
import { searchParams } from '../../core/utils'
import { Club } from '../../models/club'
import { Event, EventType } from '../../models/event'
import { Game } from '../../models/game'
import { Sport } from '../../models/sport'

export enum ActionType {
  Search,
  Create,
  Book
}

enum DisplayType {
  Cards,
  Map
}

interface IProps {
  cities: any[]
  selectedCity: any
}

interface IState {
  actionType: ActionType
  displayType: DisplayType
  sportIndex: number
  dateIndex: number
  timeStartIndex: number
  timeEndIndex: number
  sports?: Sport[]

  events?: Event[]
  eventsPage?: number
  eventsTotal?: number
  eventsLoading?: boolean

  games?: Game[]
  gamesPage?: number
  gamesTotal?: number
  gamesLoading?: boolean

  clubs?: any
  dates: any
  times: any
  gameName?: string
  playgroundType?: string
  playgroundSurface?: string
  playgroundDepartment?: string
  surfaces?: any
  departments?: any
  currentDate?: Date
  mapWidth?: number
  datesLoading: boolean
  timesLoading: boolean
  cardsLoading: boolean
}

class PublicScreen extends React.Component<IProps, IState> {

  get surfacesCollection() {
    const { surfaces } = this.state
    if (surfaces) {
      return surfaces.map(surface => ({ label: surface.name, value: surface.id }))
    }

    return []
  }

  get departmentsCollection() {
    const { departments } = this.state
    if (departments) {
      return departments.map(department => ({ label: department.name, value: department.id }))
    }

    return []
  }

  get timeInterval() {
    const { timeStartIndex, timeEndIndex, times } = this.state

    // tslint:disable-next-line:variable-name
    const time_start = timeStartIndex == null ? null : times[timeStartIndex].start
    // tslint:disable-next-line:variable-name
    const time_end = timeEndIndex == null ? (timeStartIndex == null ? null : times[timeStartIndex].end) : times[timeEndIndex].end
    return time_start && time_end ? { time_start, time_end } : {}
  }

  get sportsCollection() {
    const { sports } = this.state
    return sports ? sports.map(sport => ({ label: sport.name, icon: sport.icon_url ? '' : 'defaultGame', iconUrl: sport.icon_url ? sport.icon_url : '', className: 'icon' })) : null
  }

  get datesCollection() {
    const { dates, actionType } = this.state

    if (!dates) { return null }
    return dates.map(date => {
      return {
        title: formatDate(date.date, 'D MMMM'),
        addtext: actionType === ActionType.Search ? `${date.count} игр` : `${date.count} ${pluralize(date.count, ['зал', 'зала', 'залов'])}`,
        disabled: date.count === 0
      }
    })
  }

  get timesCollection() {
    const { times } = this.state
    if (times) {
      return times.map(time => {
        const start =  addHours(parseDate(time.start), 3)
        const end = addHours(parseDate(time.end), 3)
        const title = `${formatDate(start, 'H:mm')} - ${formatDate(end, 'H:mm')}`
        const disabled = time.status !== 'available'
        return {
          title,
          text: disabled ? '' : `${+time.price} РУБ`,
          disabled
        }
      })
    }
  }

  get queryParams() {
    const { sports, sportIndex, dates, dateIndex, times, timeStartIndex, timeEndIndex } = this.state
    const params: {
      sport_id: number,
      date: string,
      time_start?: string,
      time_end?: string
    } = {
      sport_id: sports[sportIndex].id,
      date: formatDate(addHours(parseDate(dates[dateIndex].date), 3), 'YYYY-MM-DD')
    }

    if (timeStartIndex != null) {
      params.time_start = formatDate(addHours(parseDate(times[timeStartIndex].start), 3), 'HH:mm')

      if (timeEndIndex != null) {
        params.time_end = formatDate(addHours(parseDate(times[timeEndIndex].end), 3), 'HH:mm')
      } else {
        params.time_end = formatDate(addHours(parseDate(times[timeStartIndex].end), 3), 'HH:mm')
      }
    }

    return this.serialize(params)
  }

  get gamesPointsCollection() {
    const { times, timeStartIndex, timeEndIndex, sports, dates, sportIndex, dateIndex, games, actionType } = this.state

    if (!games) { return null }
    const params: {
      sport_id: number,
      date: string,
      time_start?: string,
      time_end?: string
    } = {
      sport_id: sports[sportIndex].id,
      date: formatDate(addHours(parseDate(dates[dateIndex].date), 3), 'YYYY-MM-DD')
    }

    if (timeStartIndex != null) {
      params.time_start = formatDate(addHours(parseDate(times[timeStartIndex].start), 3), 'HH:mm')

      if (timeEndIndex != null) {
        params.time_end = formatDate(addHours(parseDate(times[timeEndIndex].end), 3), 'HH:mm')
      } else {
        params.time_end = formatDate(addHours(parseDate(times[timeStartIndex].end), 3), 'HH:mm')
      }
    }

    return games.map(game => ({
      type: 'game',
      coordinates: game.playground.coordinatesArr,
      card: {
        mobileView: true,
        type: 'game',
        image: game.playground.avatar && game.playground.avatar.url,
        title: game.name,
        club: game.playground.club,
        gameLevel: game.gameLevel,
        sportName: game.sport.name,
        address: game.playground.club.address,
        price: game.price + '',
        players: game.users,
        max_players: game.max_players,
        min_players: game.min_players,
        date: {
          from: game.startTime,
          to: game.endTime
        },
        url: `/game/${game.id}`
      }
    }))
  }

  get eventsPointsCollection() {
    const { times, timeStartIndex, timeEndIndex, sports, dates, sportIndex, dateIndex, events, actionType } = this.state

    if (!events) { return [] }

    const params: {
      sport_id: number,
      date: string,
      time_start?: string,
      time_end?: string
    } = {
      sport_id: sports[sportIndex].id,
      date: formatDate(addHours(parseDate(dates[dateIndex].date), 3), 'YYYY-MM-DD', {locale})
    }

    if (timeStartIndex != null) {
      params.time_start = formatDate(addHours(parseDate(times[timeStartIndex].start), 3), 'HH:mm')

      if (timeEndIndex != null) {
        params.time_end = formatDate(addHours(parseDate(times[timeEndIndex].end), 3), 'HH:mm')
      } else {
        params.time_end = formatDate(addHours(parseDate(times[timeStartIndex].end), 3), 'HH:mm')
      }
    }

    const queryParams = this.serialize(params)

    return events.map(event => ({
      type: 'event',
      coordinates: event.coordinates || event.club.coordinatesArr,
      card: {
        mobileView: true,
        type: event.type === EventType.Game ? 'game' : 'meeting',
        image: event.images && event.images.length > 0 && event.images[0].url,
        title: event.title,
        club: event.club,
        gameLevel: event.gameLevel,
        sportName: event.sport.name,
        address: event.address || event.club.address,
        price: event.price + '',
        players: event.users,
        max_players: event.max_players,
        min_players: event.min_players,
        date: {
          from: event.intervalStartMoment,
          to: event.intervalEndMoment
        },
        url: `/events/${event.slug}`
      }
    }))
  }

  get playgroundsPointsCollection() {
    const { times, timeStartIndex, timeEndIndex, sports, dates, sportIndex, dateIndex, clubs, actionType } = this.state

    if (clubs) {
      const params: {
        sport_id: number,
        date: string,
        time_start?: string,
        time_end?: string
      } = {
        sport_id: sports[sportIndex].id,
        date: formatDate(addHours(parseDate(dates[dateIndex].date), 3), 'YYYY-MM-DD')
      }

      if (timeStartIndex != null) {
        params.time_start = formatDate(addHours(parseDate(times[timeStartIndex].start), 3), 'HH:mm')

        if (timeEndIndex != null) {
          params.time_end = formatDate(addHours(parseDate(times[timeEndIndex].end), 3), 'HH:mm')
        } else {
          params.time_end = formatDate(addHours(parseDate(times[timeStartIndex].end), 3), 'HH:mm')
        }
      }

      const queryParams = this.serialize(params)

      const playgrounds = clubs.reduce((acc, club) => {
        const cards = club.playgrounds.map(playground => ({
          type: 'playground',
          coordinates: playground.coordinatesArr,
          card: {
            title: playground.name,
            text: club.address,
            price: playground.min_price,
            url: actionType === ActionType.Create ?
              `/club/${club.slug}/${playground.slug}/create?${queryParams}` : `/club/${club.slug}/${playground.slug}/booking?${queryParams}`,
            image: playground.avatar_mid_url
          }
        }))

        return acc.concat(cards)
      }, [])

      return playgrounds
    } else {
      return []
    }
  }
  public filterTimeout
  public wrapRef = React.createRef<HTMLDivElement>()

  public state: IState = {
    actionType: ActionType.Create,
    displayType: DisplayType.Cards,

    dates: null,
    times: null,

    gamesPage: 0,
    eventsPage: 0,

    sportIndex: null,
    dateIndex: null,
    timeStartIndex: null,
    timeEndIndex: null,

    datesLoading: true,
    timesLoading: true,
    cardsLoading: true
  }

  public actionTypes = [
    { label: 'Найти игру', icon: 'search', active: true },
    { label: 'Создать игру', icon: 'plus' },
    { label: 'Забронировать', icon: 'court' }
  ]

  public sportsFilterCollection = [
    {
      value: '1',
      list: [
        { label: 'Активный спорт', value: '1' },
        { label: 'Активный спорт2', value: '2' }
      ]
    },
    {
      value: '1',
      list: [
        { label: 'Маленькая команда', value: '1' },
        { label: 'Маленькая команда2', value: '2' }
      ]
    }
  ]

  public componentDidMount() {
    window.addEventListener('resize', this.setMapWidth)
    this.getDepartments(this.props.selectedCity)
    this.getSports()
    this.getSurfaces()
    this.setMapWidth()
  }

  public componentDidUpdate(prevProps) {
    const { selectedCity } = this.props
    if (prevProps.selectedCity !== selectedCity) {
      this.getDepartments(selectedCity)
      this.getDates()
    }
  }

  public componentWillUnmount() {
    window.removeEventListener('resize', this.setMapWidth)
  }

  public setMapWidth = () => {
    setTimeout(() => {
      this.setState({ mapWidth: window.innerWidth })
    }, 200)
  }

  public handleFilterChange = (name: string) => (value: string) => {
    const state = this.state
    state[name] = value
    this.setState({ ...state }, this.getPlaygrounds)
  }

  public getSports = async () => {
    reqGet('/sports?root=1')
      .then(response => { this.setState({ sports: response.data }, this.setSportIndex) })
      // tslint:disable-next-line:no-console
      .catch(err => console.log(err.message))
  }

  public getSurfaces = () => {
    reqGet('/surfaces')
      .then(response => { this.setState({ surfaces: response.data }) })
      // tslint:disable-next-line:no-console
      .catch(err => console.log(err.message))
  }

  public getDepartments = (cityId: number) => {
    reqGet(`/cities/${cityId}/departments`)
      .then(response => { this.setState({ departments: response.data }) })
      // tslint:disable-next-line:no-console
      .catch(err => console.log(err.message))
  }

  public getDates = async () => {
    const { actionType, sportIndex, sports, currentDate } = this.state
    const cityId = this.props.selectedCity

    if (sportIndex != null) {
      const sportId = sports[sportIndex].id

      const url = actionType === ActionType.Search ? '/games/dates' : '/playgrounds/dates'

      const getDate = searchParams().get('dateStart')
      const date = currentDate ? formatDate(currentDate, 'YYYY-M-D') : getDate ? getDate : formatDate(new Date(), 'YYYY-M-D')

      this.setState({ datesLoading: true }, () => {
        reqGet(url, { city_id: cityId, sport_id: sportId, date })
          .then(response => { if (this.state.sportIndex === sportIndex) { this.setState({ dates: response.data, datesLoading: false }, this.setDateIndex) } })
          // tslint:disable-next-line:no-console
          .catch(err => console.log(err.message))
      })
    }
  }

  public getTimes = async () => {
    const { actionType, sportIndex, dateIndex, dates, sports } = this.state
    const date = dates[dateIndex].date
    const sportId = sports[sportIndex].id
    const cityId = this.props.selectedCity

    const url = actionType === ActionType.Search ? '/games/times' : '/playgrounds/times'

    await this.setState({ timesLoading: true }, async () => {
      await reqGet(url, { city_id: cityId, sport_id: sportId, date })
        .then(response => { if (this.state.sportIndex === sportIndex && this.state.dateIndex === dateIndex) { this.setState({ times: response.data, timesLoading: false }, this.getCards) } })
        // tslint:disable-next-line:no-console
        .catch(err => console.log(err.message))
    })
  }

  public getCards = async () => {
    const { actionType } = this.state

    this.getEvents(true)

    if (actionType === ActionType.Search) {
      this.getGames(true)
    } else {
      this.getPlaygrounds()
    }
  }

  public getEvents = async (reload: boolean = false) => {
    const { sportIndex, sports, actionType, events, eventsPage } = this.state

    if (sportIndex == null) { return false }
    const cityId = this.props.selectedCity
    const sportId = sports[sportIndex].id
    const timeInterval = this.timeInterval

    this.setState({ eventsLoading: true }, () => {
      const currentEventsPage = reload ? 1 : eventsPage + 1
      reqGet('/events', { perPage: 2, page: currentEventsPage, city_id: cityId, sport_id: sportId, ...timeInterval })
        .then(response => {
          const isParamsRight = this.state.sportIndex === sportIndex
            && timeInterval.time_start === this.timeInterval.time_start
            && timeInterval.time_end === this.timeInterval.time_end

          if (!isParamsRight) { return false }

          const newEvents = response.data.map(data => new Event(data))

          if (currentEventsPage === 1) {
            this.setState({ events: newEvents, eventsTotal: response.total, eventsPage: currentEventsPage, eventsLoading: false })
          } else {
            this.setState({ events: events.concat(newEvents), eventsPage: currentEventsPage, eventsLoading: false })
          }
        })
        // tslint:disable-next-line:no-console
        .catch(err => console.log(err.message))
    })
  }

  public getGames = async (reload: boolean = false) => {
    const { sportIndex, sports, actionType, games, gamesPage } = this.state
    if (sportIndex == null) { return false }
    const cityId = this.props.selectedCity
    const sportId = sports[sportIndex].id
    const timeInterval = this.timeInterval

    this.setState({ cardsLoading: true, gamesLoading: true }, () => {
      const currentGamesPage = reload ? 1 : gamesPage + 1
      reqGet('/games', { perPage: 6, page: currentGamesPage, city_id: cityId, sport_id: sportId, ...timeInterval })
        .then(response => {
          const isParamsRight = this.state.sportIndex === sportIndex
            && timeInterval.time_start === this.timeInterval.time_start
            && timeInterval.time_end === this.timeInterval.time_end
            && actionType === ActionType.Search

          if (!isParamsRight) { return false }

          const newGames = response.data.map(data => new Game(data))

          if (currentGamesPage === 1) {
            this.setState({ games: newGames, gamesTotal: response.total, gamesPage: currentGamesPage, gamesLoading: false, cardsLoading: false })
          } else {
            this.setState({ games: games.concat(newGames), gamesPage: currentGamesPage, gamesLoading: false, cardsLoading: false })
          }
        })
        // tslint:disable-next-line:no-console
        .catch(err => console.log(err.message))
    })
  }

  public getPlaygrounds = async () => {
    if (this.filterTimeout != null) {
      clearTimeout(this.filterTimeout)
    }

    this.filterTimeout = setTimeout(() => {
      const { actionType, sportIndex, sports, dateIndex, dates, gameName, playgroundSurface, playgroundType, playgroundDepartment } = this.state
      const cityId = this.props.selectedCity

      if (typeof sportIndex === 'undefined' || sportIndex === null) {
        return false
      }

      const sportId = sports[sportIndex].id

      const timeInterval = this.timeInterval
      const filters: {
        city_id: number,
        sport_id: number,
        time_start?: string,
        time_end?: string,
        date?: string,
        query?: string,
        surface_id?: number,
        type?: number,
        department_id?: number
      } = { city_id: cityId, sport_id: sportId, ...timeInterval }

      const date = dates[dateIndex]

      if (date) {
        filters.date = date.date
      }

      if (gameName) {
        filters.query = gameName
      }

      if (+playgroundSurface) {
        filters.surface_id = +playgroundSurface
      }

      if (+playgroundType) {
        filters.type = +playgroundType
      }

      if (+playgroundDepartment) {
        filters.department_id = +playgroundDepartment
      }

      this.setState({ cardsLoading: true }, () => {
        reqGet('/playgrounds', filters)
          .then(response => {
            if (this.state.sportIndex === sportIndex
              && this.state.dateIndex === dateIndex
              && timeInterval.time_start === this.timeInterval.time_start
              && timeInterval.time_end === this.timeInterval.time_end
              && actionType !== ActionType.Search) {
              this.setState({ clubs: response.data.map(data => new Club(data)), cardsLoading: false })
            }
          })
          // tslint:disable-next-line:no-console
          .catch(err => console.log(err.message))
      })
    }, 700)
  }

  public setSportIndex = () => {
    const { sports } = this.state
    if (!sports || sports && sports.length < 1) { return null }

    let sportIndex = 0

    const getSportId = searchParams().get('sport_id')
    if (getSportId) {
      const getSportIndex = sports.findIndex(sport => sport.id === parseInt(getSportId, 10))
      if (getSportIndex > -1) { sportIndex = getSportIndex }
    }

    this.setState({ sportIndex }, this.getDates)
  }

  public setDateIndex = () => {
    const { dates } = this.state
    const getDate = searchParams().get('date')
    dates.some((date, i) => {
      let expression = date.count !== 0
      if (getDate) { expression = date.date === getDate }

      if (expression) {
        this.setState({ dateIndex: i, timeStartIndex: null, timeEndIndex: null }, () => {
          this.getTimes()
            .then(this.setTimeIndexes)
            .then(this.getCards)
        })
        return true
      }
    })

    if (!this.state.dateIndex) {
      this.setState({ times: null, games: null, gamesTotal: null, clubs: null, timesLoading: false, cardsLoading: false })
    }
  }

  public setTimeIndexes = () => {
    const { times } = this.state

    const getTimeStart = searchParams().get('time_start')
    const getTimeEnd = searchParams().get('time_end')

    if (!times || !getTimeStart) { return true }

    times.some((time, i) => {
      if (time.start === getTimeStart) {
        this.setState({ timeStartIndex: i })

        if (!getTimeEnd) { return true }
      }

      if (getTimeEnd && time.end === getTimeEnd) {
        this.setState({ timeEndIndex: i })
        return true
      }
    })
  }

  public onActionTypeChange = async (actionType: number) => {
    this.setState({ actionType, datesLoading: true, timesLoading: true, cardsLoading: true }, this.getDates)
  }

  public onSportChange = async (sportIndex: number) => {
    this.setState({ sportIndex, datesLoading: true, timesLoading: true, cardsLoading: true }, this.getDates)
  }

  public onDateChange = async (dateIndex: number) => {
    this.setState({ dateIndex, timesLoading: true, cardsLoading: true }, this.getTimes)
  }

  public onChangeCurrentDate = (currentDate: Date) => {
    this.setState({ currentDate, datesLoading: true, timesLoading: true, cardsLoading: true }, this.getDates)
  }

  public onTimeStartChange = i => {
    this.setState({ timeStartIndex: i, cardsLoading: true }, this.getCards)
  }

  public onTimeEndChange = i => {
    this.setState({ timeEndIndex: i, cardsLoading: true }, this.getCards)
  }

  public handleDisplayTypeChange = (i: number) => {
    this.setState({ displayType: i })
  }

  public serialize = obj => {
    const str = []
    for (const p in obj) {
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]))
      }
    }
    return str.join('&')
  }

  public render() {
    const { games, gamesTotal, gamesLoading, events, eventsTotal, eventsLoading, actionType, sportIndex, dateIndex, timeStartIndex, timeEndIndex, datesLoading, timesLoading, displayType, cardsLoading, clubs } = this.state

    const times = this.timesCollection
    const dates = this.datesCollection
    const sports = this.sportsCollection

    const gamesPoints = this.gamesPointsCollection
    const eventsPoints = this.eventsPointsCollection
    const playgroundsPoints = this.playgroundsPointsCollection

    const { gameName, playgroundType, playgroundSurface, currentDate, mapWidth, playgroundDepartment } = this.state

    return (
      <div>
        <div className='face'>
          <div className='face__wrap' ref={this.wrapRef}>
            <div className='face__scroll'>
              <ScrollIconComponent
                list={this.actionTypes}
                onChange={this.onActionTypeChange}
                activeIndex={actionType}
              />
            </div>

            { sports && (
              <div className='face__scroll'>
                <ScrollIconComponent
                  list={sports}
                  onChange={this.onSportChange}
                  activeIndex={sportIndex}
                />
              </div>
            )}
          </div>
        </div>

        <Loader state={datesLoading} className='--auto-height'>
          <SectionComponent title='Выберите дату' classname='section--dates'>
            { dates && (
              <SectionDtComponent
                list={dates}
                onChange={this.onDateChange}
                selected={dateIndex}
                date={currentDate || new Date()}
                onChangeDate={this.onChangeCurrentDate}
              />
            )}
          </SectionComponent>
        </Loader>

        <Loader state={timesLoading} className='--auto-height'>
          { (timesLoading || times && times.length > 0) && (
            <SectionComponent title='Выберите время' classname='section--border section--times'>
              { times && (
                <SectionTtComponent
                  selectedStart={timeStartIndex}
                  selectedEnd={timeEndIndex}
                  list={times}
                  onStartChange={this.onTimeStartChange}
                  onEndChange={this.onTimeEndChange}
                />
              )}
            </SectionComponent>
          )}
        </Loader>

        <div className='section'>
          <div className='section__wrap'>
            <div className='section__header'>
              <div className='section__tabs'>
                <LabelComponent
                  onClick={() => this.handleDisplayTypeChange(0)}
                  status={displayType === DisplayType.Cards ? LabelStatus.animation : LabelStatus.normal}
                  title='Список'
                  className='--textCenter'
                />
                <LabelComponent
                  onClick={() => this.handleDisplayTypeChange(1)}
                  status={displayType === DisplayType.Map ? LabelStatus.animation : LabelStatus.normal}
                  title='Карта'
                  className='--textCenter'
                />
              </div>

              { actionType !== ActionType.Search && (
                <FilterComponent
                  dark={true}
                  list={[
                    {
                      value: playgroundType || '0',
                      list: [
                        { label: 'Тип площадки', value: '0' },
                        { label: 'Открытая', value: '1' },
                        { label: 'Закрытая', value: '2' }
                      ],
                      onChange: this.handleFilterChange('playgroundType')
                    },
                    {
                      value: playgroundSurface || '0',
                      list: [ { label: 'Тип покрытия', value: '0' }, ...this.surfacesCollection ],
                      onChange: this.handleFilterChange('playgroundSurface')
                    },
                    {
                      value: playgroundDepartment || '0',
                      list: [ { label: 'Район города', value: '0' }, ...this.departmentsCollection ],
                      onChange: this.handleFilterChange('playgroundDepartment')
                    }
                  ]}
                  search={<InputControl value={gameName} onChange={this.handleFilterChange('gameName')}/>}
                  classname='filter--dark'
                />
              )}
            </div>
          </div>
        </div>

        <div className='section__cards'>
          { displayType === DisplayType.Cards && events != null && (
            <SectionEventsComponent events={events} total={eventsTotal} loading={eventsLoading} onDownload={() => this.getEvents()} />
          )}
          <Loader state={cardsLoading} loaderClassName='--cards-loading'>
            { actionType === ActionType.Search ?
              <>
                {displayType === DisplayType.Cards && games && (
                  <SectionGamesComponent games={games} total={gamesTotal} loading={gamesLoading} onDownload={() => this.getGames()} />
                )}

                {displayType === DisplayType.Map && games && (
                  <SectionMapComponent points={gamesPoints.concat(eventsPoints)} width={mapWidth} height={mapWidth * 0.53} />
                )}
              </>
              : (
                <>
                  {displayType === DisplayType.Cards && clubs && (
                    <SectionPlaygroundsComponent clubs={clubs} queryParams={this.queryParams} mode={actionType === ActionType.Create ? 'create' : 'booking'} />
                  )}

                  {displayType === DisplayType.Map && clubs && (
                    <SectionMapComponent points={playgroundsPoints.concat(eventsPoints)} width={mapWidth} height={mapWidth * 0.53} />
                  )}
                </>
              )}
          </Loader>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  cities: state.citiesState.cities,
  selectedCity: state.citiesState.selectedCity
})

export default connect(mapStateToProps)(PublicScreen)
