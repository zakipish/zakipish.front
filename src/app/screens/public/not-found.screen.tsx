import * as React from 'react'

class NotFoundScreen extends React.Component<{}, {}> {
    render() {
        return (
            <React.Fragment>
                <h1>404</h1>
                <p>Не найдено</p>
            </React.Fragment>
        )
    }
}



export default NotFoundScreen
