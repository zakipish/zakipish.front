
// @ts-ignore
import moment from 'moment'
import * as React from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps, withRouter } from 'react-router'
import ChatComponent from '../../components/blocks/chat/chat.component'
import DisabledChat from '../../components/blocks/chat/disabled.chat'
import Post from '../../components/blocks/post'
import SearchLine from '../../components/blocks/search.line'
import SharingComponent from '../../components/blocks/sharing.component'
import FriendCard from '../../components/cards/friend.card'
import ActionLineEvent from '../../components/layout/event/action-line.event'
import EventLabel from '../../components/layout/event/event.label'
import SliderEvent from '../../components/layout/event/slider.event'
import StatusLineEvent from '../../components/layout/event/status-line.event'
import StatusEvent from '../../components/layout/event/status.event'
import FaceCard from '../../components/layout/face-card/_face-card'
import InformFaceCard from '../../components/layout/face-card/inform.face-card'
import SliderFaceCard from '../../components/layout/face-card/slider.face-card'
import { HeaderPublicComponent } from '../../components/layout/header-public.component'
import MobilePopup from '../../components/layout/mobile-popup'
import PopupLineComponent from '../../components/layout/popupline.component'
import ContentCourtProfile, { ICourtParty } from '../../components/layout/profile/content/content-court.profile'
import TabLinksComponent from '../../components/layout/tab-links.component'
import SectionMapComponent from '../../components/sections/section-map.component'
import { pluralize } from '../../core/utils'
import { Event, EventType } from '../../models/event'
import { User } from '../../models/user'
import { buildDataForScreen, getEvent } from '../../services/events.service'

interface IMatchParams {
  slug: string
}

interface IProps extends RouteComponentProps<IMatchParams> {
  currentUser: User
}

interface IState {
  event?: Event,
  usersFilter?: string
  data?: {
    header?: {
      slides?: Array<{
        image: string
        title: string
      }>
    }

    club?: {
      link: string
      images: string[]
      avatar: string
      title: string
      description: string
      contacts: Array<{
        tit: string
        val: string
      }>
      infrastructures: Array<{
        title: string
      }>
      geo: number[]
    }

    event?: {
      title: string
      description: string
      price: string

      interval: {
        from: moment.Moment
        to: moment.Moment
      }

      players_count: {
        min: number
        max: number
      }

      definitelyPlayers: Array<{
        id: number
        image: string
      }>

      possiblePlayers: Array<{
        image: string
      }>

      statusButtons: {
        onJoin?: {
          onClick: () => void
          order: number
        }

        onLike?: {
          onClick: () => void
          order: number
        }

        onRemove?: {
          onClick: () => void
          order: number
        }
      }

      miniStatusButton: {
        type: string
        onClick: () => void
      }
    }

    place?: {
      coordinates: number[]
    }

    players: {
      meeting?: {
        players: Array<{
          id: number
          name: string
          img: string
          url: string
        }>
      }

      game?: {
        statusLine: Array<{
          tit: string
          val: string
        }>

        court: {
          party: ICourtParty[]
          state: {
            now: number
            min: number
            max: number
            must: number
          }
          game: Event
          isInTheGame: boolean
          possibleActionButton?: {
            type: string
            onClick?: () => void
          }
          onInvite: () => void
        }
      }
    }

    chat: {
      enabled: boolean
      id?: number
    }
  }
  activeTabIndex: number
  typeParty: number
}

class EventScreen extends React.Component<IProps, IState> {

  public state: IState = {
    activeTabIndex: 0,
    typeParty: 0
  }

  public componentDidMount() {
    const { slug } = this.props.match.params
    getEvent(slug).then(this.bootstrap)
  }

  public componentDidUpdate(prevProps) {
    const { slug } = this.props.match.params
    const { currentUser } = this.props
    const { event } = this.state

    if (prevProps.currentUser !== currentUser) {
      this.bootstrap(event)
    }

    if (prevProps.match.params.slug !== slug) {
      this.setState({ data: null, event: null }, () => {
        getEvent(slug)
          .then(this.bootstrap)
      })
    }
  }

  public bootstrap = (event: Event) => {
    if (event) {
      this.updateData(event)
    }
  }

  public onClickTab = i => {
    this.setState({ activeTabIndex: i })
  }

  public setTypeParty = i => {
    this.setState({ typeParty: i })
  }

  public openRegModal = () => {
    HeaderPublicComponent.togglePopup('reg')
  }

  public updateData = (event: Event) => {
    const { currentUser } = this.props
    const data = buildDataForScreen(event, currentUser, this.updateData, this.openRegModal)
    this.setState({ event, data })
  }

  public handleChangeUsersFilter = (usersFilter: string) => {
    this.setState({ usersFilter })
  }

  public render() {
    const { activeTabIndex, event, data, typeParty, usersFilter } = this.state
    const { currentUser } = this.props

    const users = data && data.players && data.players.meeting && (usersFilter ? data.players.meeting.players.filter(player => player.name.toLowerCase().indexOf(usersFilter.toLowerCase()) > -1) : data.players.meeting.players)

    return (
      <React.Fragment>
        { !data ? (
          <div className='eventPageLoader'>
            <img src='/images/loader.gif' alt='#'/>
          </div>
        ) : (
          <div className='eventPage'>
            <SliderEvent slides={data.header.slides} />
            <div className='eventPage__wrap'>
              <div className='eventPage__side'>
                {(!(data.chat.enabled && data.chat.id) || activeTabIndex !== 2) && (
                  <React.Fragment>
                    {data.club &&
                    <EventLabel
                      vip={true}
                      image={data.club.avatar}
                      title={data.club.title}
                    />
                    }
                    <StatusEvent
                      price={data.event.price}
                      interval={data.event.interval}
                      min={data.event.players_count.min}
                      max={data.event.players_count.max}
                      justLen={data.event.definitelyPlayers.length}
                      likeLen={data.event.possiblePlayers.length}
                      party={typeParty === 0 ? data.event.definitelyPlayers : data.event.possiblePlayers}
                      onJoin={data.event.statusButtons.onJoin}
                      onLike={data.event.statusButtons.onLike}
                      onRemove={data.event.statusButtons.onRemove}
                      typeParty={typeParty}
                      onSetTypeParty={this.setTypeParty}
                    />
                    <SharingComponent list={['facebook', 'odnoklassniki', 'telegram', 'vkontakte', 'twitter']} title='' className='sharing' classNameLikely='likely-big' />
                  </React.Fragment>
                )}
                {data.chat.enabled && data.chat.id && activeTabIndex === 2 && (
                  <React.Fragment>
                    <EventLabel
                      price={data.event.price}
                      btn={data.event.miniStatusButton}
                    />
                    <MobilePopup
                      classname='mobilePopup--chat'
                      preview={
                        <PopupLineComponent
                          icon={{ icon: 'chat', fill: '#2A83FB' }}
                          title={`Чат для участников (${data.event.definitelyPlayers.length})`}
                        />
                      }
                    >
                      <ChatComponent
                        chat_id={data.chat.id}
                        playersCount={data.event.definitelyPlayers.length}
                        disabled={!currentUser || !data.event.definitelyPlayers.map(p => p.id).includes(currentUser.id)}
                        disabledElem={
                          <>
                            {!currentUser &&
                            <DisabledChat
                              image='/images/disChat.png'
                              text='Чтобы пользоваться функцией чата пройдите 2-х минутную регистрация на сайте ZaKipish, и откройте для себя безграничный мир развлечений'
                              btn={{
                                text: 'Регистрация',
                                onClick: this.openRegModal
                              }}
                            />
                            }
                            {currentUser && !data.event.definitelyPlayers.map(p => p.id).includes(currentUser.id) &&
                            <DisabledChat
                              image='/images/disChat.png'
                              text='Станьте участником мероприятия чтобы получить доступ к чату'
                              btn={{
                                text: 'Присоединиться',
                                onClick: data.event.statusButtons.onJoin.onClick
                              }}
                            />
                            }
                          </>
                        }
                      />
                    </MobilePopup>
                  </React.Fragment>
                )}
              </div>
              <div className='eventPage__content'>
                <TabLinksComponent
                  active={activeTabIndex}
                  onSelect={this.onClickTab}
                >
                  <span>Организатор</span>
                  <span>Описание</span>
                  <span>Участники {data.event.definitelyPlayers.length}</span>
                </TabLinksComponent>
                {activeTabIndex === 0 && (
                  <React.Fragment>
                    <FaceCard type='avatar'>
                      <SliderFaceCard list={data.club.images}/>
                      <InformFaceCard
                        link={data.club.link}
                        avatar={data.club.avatar}
                        title={data.club.title}
                        reviews={[]}
                        options={data.club.contacts}
                        params={{
                          list: data.club.infrastructures
                        }}
                        geo={data.club.geo}
                      />
                    </FaceCard>
                    <Post
                      title={data.club.title}
                      content={data.club.description}
                    />
                  </React.Fragment>
                )}
                {activeTabIndex === 1 && (
                  <Post
                    title={data.event.title}
                    content={data.event.description}
                    geo={data.place && <SectionMapComponent width={680} height={300} points={[{coordinates: data.place.coordinates}]} />}
                  />
                )}
                {activeTabIndex === 2 && event.type === EventType.Meeting && (
                  <div className='friend-list'>
                    <SearchLine placeholder='Введите имя и фалимию' onChange={this.handleChangeUsersFilter} />
                    {users.map(player => (
                      <FriendCard key={player.id} title={player.name} img={player.img} url={player.url} />
                    ))}
                  </div>
                )}
                {activeTabIndex === 2 && event.type === EventType.Game && (
                  <React.Fragment>
                    <StatusLineEvent
                      list={data.players.game.statusLine}
                    />
                    <hr/>
                    <ContentCourtProfile
                      party={data.players.game.court.party}
                      state={data.players.game.court.state}
                      game={data.players.game.court.game}
                      isInTheGame={data.players.game.court.isInTheGame}
                      onInvite={data.players.game.court.onInvite}
                    />
                    <hr/>
                    <ActionLineEvent
                      title='Возможно пойдут'
                      subtit={`${data.event.possiblePlayers.length} ${pluralize(data.event.possiblePlayers.length, ['человек', 'человека', 'человек'])}`}
                      party={data.event.possiblePlayers}
                      btn={data.players.game.court.possibleActionButton}
                    />
                  </React.Fragment>
                )}
              </div>
            </div>
          </div>
        )}
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  currentUser: state.authState.user
})

export default withRouter(connect(mapStateToProps)(EventScreen))
