import * as React from 'react'
import { RouterProps } from 'react-router'
import AuthPageComponent from '../../components/layout/auth/auth-page.component'
import FormRegComponent from '../../components/layout/auth/form-reg.component'

interface IProps {}

class RegScreen extends React.Component<IProps & RouterProps, {}> {
    render() {
        return (
            <AuthPageComponent {...this.props}>
                <FormRegComponent/>
            </AuthPageComponent>
        )
    }
}

export default RegScreen
