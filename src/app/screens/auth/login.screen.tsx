import * as React from 'react'
import { RouterProps } from 'react-router'
import AuthPageComponent from '../../components/layout/auth/auth-page.component'
import FormLoginComponent from '../../components/layout/auth/form-login.component'

interface IProps {}

class LoginScreen extends React.Component<IProps & RouterProps, {}> {
    render() {
        return (
            <AuthPageComponent {...this.props}>
                <FormLoginComponent/>
            </AuthPageComponent>
        )
    }
}

export default LoginScreen
