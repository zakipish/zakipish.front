import * as React from 'react'
import { RouterProps } from 'react-router'
import AuthPageComponent from '../../components/layout/auth/auth-page.component'
import FormForgetComponent from '../../components/layout/auth/form-forget.component'

interface IProps {}

class ForgetScreen extends React.Component<IProps & RouterProps, {}> {
    render() {
        return (
            <AuthPageComponent {...this.props}>
                <FormForgetComponent/>
            </AuthPageComponent>
        )
    }
}

export default ForgetScreen