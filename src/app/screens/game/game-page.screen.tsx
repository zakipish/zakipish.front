import * as React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { History } from 'history'

import { reqGet, reqPost } from "../../core/api"
import { User } from '../../models/user'
import { Game } from '../../models/game'
import { GameLevel } from '../../models/game-level'
import { IconList } from '../../components/blocks/icon/icon.component'
import ChatComponent from '../../components/blocks/chat/chat.component'
import MobilePopup from '../../components/layout/mobile-popup'
import PopuplineComponent from '../../components/layout/popupline.component'
import { HeaderPublicComponent } from '../../components/layout/header-public.component'

import HeaderProfile from '../../components/layout/profile/header/header.profile'
import ContentProfile from '../../components/layout/profile/content/content.profile'
import ContenListProfile from '../../components/layout/profile/content/conten-list.profile'
import ContentCourtProfile from '../../components/layout/profile/content/content-court.profile'
import ContentInfoProfile from '../../components/layout/profile/content/content-info.profile'
import ContentActionProfile from '../../components/layout/profile/content/content-action.profile'
import SideProfile from '../../components/layout/profile/side/side.profile'
import SettingGameForm from '../../components/layout/game/setting-game.form'
import Loader from '../../components/layout/loader'
import ManList from '../../components/blocks/list/man.list'
import FilterList from '../../components/blocks/list/filter.list'

interface IProps {
    match: any
    user: User
    history: History
}

interface IState {
    game: Game
    loading: boolean
    gameLevels: GameLevel[]
    settingsHide: boolean
}

class GamePageScreen extends React.Component<IProps, IState> {

    state: IState = {
        game: null,
        loading: false,
        gameLevels: null,
        settingsHide: true
    }

    toggleSettings = (state?: boolean) => {
        const settingsHide = state ? state : !this.state.settingsHide
        this.setState({ settingsHide })
    }

    componentDidMount(): void {
        this.getGame()
        this.getGameLevels()
    }

    getGameLevels = async () => {
        reqGet('/game_levels')
        .then(response => this.setState({ gameLevels: response.data.map(data => new GameLevel(data)) }))
    }

    getGame = async () => {
        const { id } = this.props.match.params;
        this.setState({ game: null }, () => {
            reqGet(`/games/${id}`)
                .then((response) => {
                    this.setState({ game: new Game(response.data) })
                })
                .catch((err) => console.log(err.message))
        })
    }

    getParams = () => {
        const { game } = this.state;
        const date = `${moment(game.date).format('Do MMMM')} ${game.interval[0] ? game.interval[0] : ''} - ${game.interval[1] ? game.interval[1] : ''}`;
        let logo = 'assets/userfiles/playground-avatar.png';
        if (game.playground.images.length > 0) {
            logo = game.playground.images[0].url;
        } else if (game.playground.club.logo_url) {
            logo = game.playground.club.logo_url;
        }
        return [
            { value: date, title: 'Дата игры' },
            { value: game.playground.name, title: 'Площадка', url: `/club/${game.playground.club.slug}/${game.playground.slug}`, image: logo },
        ];
    }

    getOptions = () => {
        const { game, gameLevels } = this.state;
        let actual_game_level = null;
        for (let game_level of gameLevels) {
            if (game_level.id === game.game_level_id) {
                actual_game_level = game_level.name;
                break;
            }
        }

        const age = game.age_start ? (game.age_end ? `${game.age_start} - ${game.age_end}` : `${game.age_start}+`) : `до ${game.age_end}`

        return [
            { value: age, title: 'Возраст' },
            { value: actual_game_level, title: 'Уровень игроков' },
            { value: 'Не забронировано', title: 'Статус бронирования', url: '#', image: 'assets/userfiles/icon1.png' },
        ]
    }

    getState() {
        const { game } = this.state;
        return { min: game.min_players, now: game.users.length, must: game.max_players, max: game.max_players }
    }

    getPrices() {
        const { game } = this.state;
        return [
            { title: 'Стоимость игры', price: Math.ceil(game.price), priceBg: 'green' },
            { title: 'Ваша часть', price: Math.ceil(game.price / game.max_players), priceBg: 'darkBlue' },
        ]
    }

    getParty() {
        const { game } = this.state;

        let result = [];
        for (let userRaw of game.users.slice(0, 8)) {
            const user = new User(userRaw)

            let u = {
                image: user.avatar_thumb_url ? user.avatar_thumb_url : '/images/avatar-md.jpg',
                url: `/profile/${user.id}`
            }

            if (user.hasSport(game.sport_id)) {
                const game_level = user.gameLevelFor(game.sport_id)
                if (game_level) {
                    u['level'] = {
                        title: game_level.engName,
                        bg: game_level.color
                    }
                }
            }

            result.push(u)
        }

        return result

    }

    onGameSaved = (game) => {
        this.setState({ game }, () => { this.toggleSettings(false) })
    }

    get isInTheGame() {
        const { users } = this.state.game
        const { user } = this.props

        if (!user)
            return false

        return users.map(user => user.id).includes(user.id)
    }

    submitAttach = async () => {
        const { id } = this.props.match.params;
        this.setState({ loading: true })
        reqPost(`/games/${id}/attach`, null)
            .then(response => this.setState({ game: response.data, loading: false }))
    }

    submitUnAttach = async () => {
        const { id } = this.props.match.params;
        this.setState({ loading: true })
        reqPost(`/games/${id}/unattach`, null)
            .then(response => this.setState({ game: response.data, loading: false }))
    }

    openLoginModal = () => {
        HeaderPublicComponent.togglePopup('login')
    }

    render() {
        const { game, gameLevels, settingsHide, loading } = this.state
        const { user } = this.props
        const { id } = this.props.match.params

        return (
            <div>
                {game && gameLevels ?
                    <div className="profile">
                        <div className="profile__wrap">
                            <HeaderProfile
                                className="--lowerDown"
                                title={game.name || '-'}
                                icon={IconList.football}
                                onSettingsToggle={this.toggleSettings}
                                settingsHide={settingsHide}
                                setting={user && game.owner_id == user.id ? <SettingGameForm gameLevels={gameLevels} onSave={this.onGameSaved} game={game} /> : ''}

                            />
                            <ContentProfile>
                                <ContenListProfile
                                    className="--lastBoard"
                                    type="params"
                                    list={this.getParams()}
                                />
                                <ContenListProfile
                                    className="--lastBoard"
                                    type="options"
                                    list={this.getOptions()}
                                />
                                <hr />
                                <ContentCourtProfile
                                    party={this.getParty()}
                                    state={this.getState()}
                                    game={game}
                                    isInTheGame={this.isInTheGame}
                                    onInvite={() => this.getGame()}
                                />
                                {/* <ManList
                                head={ <FilterList/> }
                                list={[
                                    { name: 'Сергей', surname: 'Бурунов', subtitle: '36 лет', level: 'BEGINNER',
                                        btns: [{title: 'приграсить', icon: 'plus', onClick: () => {}}]
                                    },
                                ]}
                            /> */}
                                <hr />

                                <ContentInfoProfile
                                /*info="Что будет, если к вам никто не присоединится?"*/
                                >
                                    <ContenListProfile
                                        type="options"
                                        list={this.getPrices()}
                                    />
                                </ContentInfoProfile>
                                {!game.isPast && !this.isInTheGame &&
                                    <ContentActionProfile
                                        list={[
                                            { title: 'Присоединиться', bg: 'green', onClick: user ? this.submitAttach : this.openLoginModal, disabled: loading },
                                        ]}
                                    />
                                }

                                {!game.isPast && user && this.isInTheGame && game.owner_id != user.id &&
                                    <ContentActionProfile
                                        list={[
                                            { title: 'Отказаться', bg: 'orange', onClick: this.submitUnAttach, disabled: loading },
                                        ]}
                                    />
                                }
                            </ContentProfile>
                            {this.isInTheGame && game.chat_id ?
                                <SideProfile className="--chat">
                                    <MobilePopup
                                        classname="mobilePopup--chat"
                                        preview={
                                            <PopuplineComponent
                                                icon={{ icon: 'chat', fill: '#2A83FB' }}
                                                title={`Чат для участников (${game.users.length})`}
                                            />
                                        }
                                    >
                                        <ChatComponent chat_id={game.chat_id} playersCount={game.users.length} fixed={true} />
                                    </MobilePopup>
                                </SideProfile>
                            : ''}
                        </div>
                    </div>
                    : null}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.authState.user,
    selectedCity: state.citiesState.selectedCity
})

export default connect(mapStateToProps)(GamePageScreen)
