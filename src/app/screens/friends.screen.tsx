import * as React from 'react'
import ContentProfile from '../components/layout/profile/content/content.profile'
import SideWrapProfile from '../components/layout/profile/side/side-wrap.profile'
import FriendCard from '../components/cards/friend.card'
import FilterFriend from '../components/layout/friends/filter.friend'

interface IProps {}

class FriendsScreen extends React.Component<IProps, {}> {
    render() {
        return (
            <div className="profile">
                <div className="profile__wrap">
                    <ContentProfile>
                        <div className="friend-list">
                            <FriendCard title="Вадим Куча" img="/assets/userfiles/avatar_0.jpg"/>
                            <FriendCard/>
                            <FriendCard/>
                            <FriendCard/>
                        </div>
                    </ContentProfile>
                    <SideWrapProfile>
                        <FilterFriend/>
                    </SideWrapProfile>
                </div>
            </div>
        )
    }
}

export default FriendsScreen
