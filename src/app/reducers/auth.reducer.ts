import types from '../actions/_types.actions'
import { User } from '../models/user'

const initialState = {
    user: null
}

const authReducer = function(state = initialState, action) {
    switch (action.type) {
        case types.AUTH_LOGIN:
            return {
                ...state,
                user: action.payload ? new User(action.payload) : null
            }
        case types.AUTH_CLEAR:
            return initialState
        default:
            return state
    }
}

export default authReducer
