import types from '../actions/_types.actions'
import { City } from '../models/city'

const initialState = {
    cities: [],
    selectedCity: null
}

const citiesReducer = function(state = initialState, action) {
    switch (action.type) {
        case types.SELECT_CITY:
            return {
                ...state,
                selectedCity: action.payload
            }
        case types.DOWNLOAD_CITIES:
            return {
                ...state,
                cities: action.payload ? action.payload.map(data => new City(data)) : []
            }
        default:
            return state
    }
}

export default citiesReducer
