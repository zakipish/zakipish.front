import { combineReducers } from 'redux'

// Reducers
import authReducer from './auth.reducer'
import citiesReducer from './cities.reducer'
import gameLevelsReducer from './game-levels.reducer'

const appReducer = combineReducers({
    authState: authReducer,
    citiesState: citiesReducer,
    gameLevelsState: gameLevelsReducer
})

const rootReducer = (state, action) => {
    return appReducer(state, action)
}

export default rootReducer
