import types from '../actions/_types.actions'
import { GameLevel } from '../models/game-level'

const initialState = {
    gameLevels: []
}

const gameLevelsReducer = function(state = initialState, action) {
    switch (action.type) {
        case types.DOWNLOAD_GAME_LEVELS:
            return {
                ...state,
                gameLevels: action.payload ? action.payload.map(data => new GameLevel(data)) : []
            }
        default:
            return state
    }
}

export default gameLevelsReducer
