import { reqGet, reqPost, reqDelete } from '../core/api'

import { Sport } from '../models/sport'

export const getSports = async () => {
    try {
        const response = await reqGet('/sports')
        return response.data.map(data => new Sport(data))
    } catch(e) {
        return false
    }
}
