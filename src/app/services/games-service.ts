import { reqGet, reqPost, reqDelete } from '../core/api'

import { Game } from '../models/game'

export const getGames = async () => {
    try {
        const response = await reqGet('/games/my')
        return response.data.map(data => new Game(data))
    } catch(e) {
        return false
    }
}

export const inviteFriend = async (game_id: number, user_id: number) => {
    try {
        const data = {
            user_id
        }

        const response = await reqPost(`/games/${game_id}/invites`, JSON.stringify(data))
        return true
    } catch(e) {
        return false
    }
}

export const getUserGames = async (user_id: number) => {
    try {
        const response = await reqGet(`/users/${user_id}/games`)
        return response.data.map(data => new Game(data))
    } catch(e) {
        return false
    }
}

export const attachToGame = async (game_id: number) => {
    try {
        const response = await reqPost(`/games/${game_id}/attach`, null)
        return true
    } catch(e) {
        return false
    }
}
