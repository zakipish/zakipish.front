import store from '../../store'
import {reqPost, reqPatch, reqGet} from '../core/api'
import { authLogin } from '../actions/auth.actions'

export const getClub = async (clubId:number) => {
    //TODO Убрать коменты
    reqGet(`/club/${clubId}`)
        .then(response => response.data);
}
