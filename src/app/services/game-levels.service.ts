import store from '../../store'
import { reqGet } from '../core/api'
import { downloadGameLevels } from '../actions/game-levels.actions'

export const getGameLevels = async () => {
    return reqGet('/game_levels')
           .then(dispatch)
           .catch(e => console.log(e.message))
}

const dispatch = async (response) => {
    store.dispatch(downloadGameLevels(response.data))
    return response
}
