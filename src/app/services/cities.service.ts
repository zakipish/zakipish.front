import store from '../../store'
import { reqGet } from '../core/api'
import { downloadCities } from '../actions/cities.actions'

export const getCities = async () => {
    return reqGet('/cities')
           .then(dispatch)
           .catch(e => console.log(e.message))
}

const dispatch = async (response) => {
    store.dispatch(downloadCities(response.data))
    return response
}
