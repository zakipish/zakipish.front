import { reqGet, reqPost } from '../core/api'
import { Event, EventType } from '../models/event'
import { EventParticipationType, User } from '../models/user'

import { pluralize } from '../core/utils'

export const getEvents = async () => {
  try {
    const response = await reqGet('/events')
    return response.data.map(event => new Event(event))
  } catch (e) {
    return false
  }
}

export const getEvent = async (slug: string) => {
  try {
    const response = await reqGet(`/events/${slug}`)
    return new Event(response.data)
  } catch (e) {
    return false
  }
}

export const inviteFriend = async (eventId: number, userId: number) => {
  try {
    const data = {
      user_id: userId
    }

    const response = await reqPost(`/events/${eventId}/invites`, JSON.stringify(data))
    return new User(response.data)
  } catch (e) {
    return false
  }
}

export const onInvite = (event: Event, onAction: (event: Event) => void) => (user: User) => {
  const newEvent = new Event({...event})
  newEvent.users.push(user)
  onAction(newEvent)
}

export const sendParticipate = async (slug: string, type: EventParticipationType) => {
  try {
    const data = { type }
    const response = await reqPost(`/events/${slug}/participate`, JSON.stringify(data))
    return new User(response.data)
  } catch (e) {
    return false
  }
}

const onSendParticipate = (type: EventParticipationType, event: Event, onAction: (event: Event) => void) => () => {
  this.sendParticipate(event.slug, type)
    .then((user: User) => {
      const nUser = new User(user)
      const prevUserIndex = event.users.findIndex(prevUser => prevUser.id === user.id)
      if (prevUserIndex > -1) {
        event.users[prevUserIndex] = nUser
      } else {
        event.users.push(nUser)
      }

      onAction(event)
    })
}

export const buildDataForScreen = (event: Event, currentUser: User, onAction: (event: Event) => void, openRegModal: () => void) => {
  const definitelyPlayers = event.users.filter(user => user.type === EventParticipationType.Defenitely)
  const possiblePlayers = event.users.filter(user => user.type === EventParticipationType.Possible)
  const currentUserInEvent = currentUser ? event.users.find(user => user.id === currentUser.id) : null

  const canPossible = !currentUserInEvent || currentUserInEvent.type !== EventParticipationType.Possible && currentUserInEvent.type !== EventParticipationType.Defenitely
  const canDefinitely = !currentUserInEvent || currentUserInEvent.type !== EventParticipationType.Defenitely
  const canDeclineDefinitely = currentUserInEvent && currentUserInEvent.type === EventParticipationType.Defenitely
  const canDeclinePossible = currentUserInEvent && currentUserInEvent.type === EventParticipationType.Possible
  const diffMin = event.intervalEndMoment.diff(event.intervalStartMoment, 'minute')

  const miniStatusButton = canDeclineDefinitely || canDeclinePossible ? {
    type: 'remove',
    onClick: currentUser ? onSendParticipate(EventParticipationType.Declined, event, onAction) : openRegModal
  } : canDefinitely ? {
    type: 'join',
    onClick: currentUser ? onSendParticipate(EventParticipationType.Defenitely, event, onAction) : openRegModal
  } : null

  const statusButtons = canPossible ? {
    onJoin: {
      order: 1,
      onClick: currentUser ? onSendParticipate(EventParticipationType.Defenitely, event, onAction) : openRegModal
    },

    onLike: {
      order: 2,
      onClick: currentUser ? onSendParticipate(EventParticipationType.Possible, event, onAction) : openRegModal
    }
  } : canDefinitely ? {
    onJoin: {
      order: 1,
      onClick: currentUser ? onSendParticipate(EventParticipationType.Defenitely, event, onAction) : openRegModal
    },

    onRemove: {
      order: 2,
      onClick: currentUser ? onSendParticipate(EventParticipationType.Declined, event, onAction) : openRegModal
    }
  } : canDeclineDefinitely ? {
    onRemove: {
      order: 1,
      onClick: currentUser ? onSendParticipate(EventParticipationType.Declined, event, onAction) : openRegModal
    },

    onLike: {
      order: 2,
      onClick: currentUser ? onSendParticipate(EventParticipationType.Possible, event, onAction) : openRegModal
    }
  } : {}

  return {
    header: {
      slides: event.images.map(image => ({
        image: image.url,
        title: event.title
      }))
    },

    club: {
      link: `/club/${event.club.slug}`,
      images: event.club.safe_images.map(image => image.url),
      avatar: event.club.safe_big_logo_url,
      title: event.club.name,
      description: event.club.description,
      contacts: [
        { tit: 'Контакты', val: event.club.phone },
        { tit: 'Время работы', val: event.club.weekSchedule && event.club.weekSchedule.length > 1 ? `${event.club.weekSchedule[0]} - ${event.club.weekSchedule[1]}` : 'Неизвестно' },
        { tit: 'Адрес:', val: event.club.address }
      ],
      infrastructures: event.club.infrastructures.map(infr => ({
        title: infr.name
      })),
      geo: event.club.coordinatesArr
    },

    event: {
      title: event.title,
      description: event.description,
      price: `${event.price}`,
      interval: {
        from: event.intervalStartMoment,
        to: event.intervalEndMoment
      },

      players_count: {
        min: event.min_players,
        max: event.max_players
      },

      definitelyPlayers: definitelyPlayers.map(user => ({
        id: user.id,
        image: user.avatar_thumb_url || '/images/avatar-md.jpg'
      })),

      possiblePlayers: possiblePlayers.map(user => ({
        image: user.avatar_thumb_url || '/images/avatar-md.jpg'
      })),

      statusButtons,
      miniStatusButton
    },

    place: event.coordinates ? {
      coordinates: event.coordinates
    } : event.club.coordinatesArr ? {
      coordinates: event.club.coordinatesArr
    } : null,

    players: {
      game: event.type === EventType.Game ? {
        statusLine: [
          { val: event.intervalStartMoment.format('DD MMMM, HH:mm') + event.intervalEndMoment.format(' - HH:mm'), tit: 'Дата игры'},
          { val: `${Math.floor(diffMin / 60)} ${pluralize(Math.floor(diffMin / 60), ['час', 'часа', 'часов'])}${diffMin % 60 > 0 ? ` ${diffMin % 60} ${pluralize(diffMin % 60, ['минута', 'минуты', 'минут'])}` : ''}`, tit: 'Продолжительность' },
          { val: `${event.age_start ? event.age_start : '∞'}-${event.age_end ? event.age_end : '∞'}`, tit: 'Возраст' },
          { val: event.gameLevel ? event.gameLevel.name : '-', tit: 'Уровень игроков' }
        ],
        court: {
          party: definitelyPlayers.map(user => ({
            level: user.sports.find(sport => sport.id === event.sport_id) ? {
              bg: user.sports.find(sport => sport.id === event.sport_id).game_level.color,
              title: user.sports.find(sport => sport.id === event.sport_id).game_level.engName

            } : null,
            url: `/profile/${user.id}`,
            image: user.avatar_thumb_url || '/images/avatar-md.jpg'
          })),
          state: {
            now: definitelyPlayers.length,
            min: event.min_players,
            max: event.max_players,
            must: event.min_players
          },
          game: event,
          isInTheGame: !!currentUserInEvent,
          possibleActionButton: canPossible ? {
            type: 'like',
            onClick: currentUser ? onSendParticipate(EventParticipationType.Possible, event, onAction) : openRegModal
          } : canDeclinePossible ? {
            type: 'remove',
            onClick: currentUser ? onSendParticipate(EventParticipationType.Declined, event, onAction) : openRegModal
          } : null,
          onInvite: (this && typeof this.onInvite === 'function') ? this.onInvite(event, onAction) : () => null
        }
      } : null,
      meeting: event.type === EventType.Meeting ? {
        players: definitelyPlayers.map(user => ({
          id: user.id,
          name: user.fullName(),
          img: user.avatar_big_thumb_url,
          url: `/profile/${user.id}`
        }))
      } : null
    },

    chat: {
      enabled: !event.chat_disabled,
      id: event.chat_id
    }
  }
}
