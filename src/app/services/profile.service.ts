import store from '../../store'
import { reqPost, reqPatch } from '../core/api'
import { authLogin } from '../actions/auth.actions'

export const reg = async (user) => {
    return reqPost('/reg', JSON.stringify(user))
           .then(dispatch)
}

export const update = async (user) => {
    console.log('Пользователь',user)
    return reqPatch('/user', JSON.stringify(user))
           .then(dispatch)
}

export const update_sports = async (sports) => {
    return reqPatch('/user', JSON.stringify({ sports }))
           .then(dispatch)
}

export const updateAvatar = async (file) => {
    let form = new FormData();
    form.append('_method', 'PATCH');
    form.append('image', file);

    return reqPost('/user', form)
           .then(dispatch)
}

const dispatch = async (response) => {
    store.dispatch(authLogin(response.data))
    return response
}
