import store from '../../store'
import { reqGet, reqPost, reqDelete } from '../core/api'
import { authLogin, authClear } from '../actions/auth.actions'

export const initialLogin = async () => {
    return reqGet('/auth')
           .then(dispatch)
           .catch(dispatchNull)
}

export const login = async (login, password) => {
    return reqPost('/auth', JSON.stringify({ login, password }))
           .then(dispatch)
}

export const logout = async () => {
    return reqDelete('/auth')
           .then(dispatch)
}

const dispatch = async (response) => {
    store.dispatch(authLogin(response.data))
    return response
}

const dispatchNull = async (response) => {
    store.dispatch(authLogin(null))
    return response
}
