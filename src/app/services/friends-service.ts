import { reqGet, reqPost, reqDelete } from '../core/api'

import { User } from '../models/user'
import { FriendRequest } from '../models/friend-request'

export const getFriends = async () => {
    try {
        const response = await reqGet('/friends')
        return response.data.map(data => new User(data))
    } catch(e) {
        return false
    }
}

export const getFriendRequests = async () => {
    try {
        const response = await reqGet('/friend_requests')
        return response.data.map(data => new FriendRequest(data))
    } catch(e) {
        return false
    }
}

export const acceptFriendRequest = async (id: number) => {
    return reqPost(`/friend_requests/${id}/accept`, null)
}

export const declineFriendRequest = async (id: number) => {
    return reqPost(`/friend_requests/${id}/decline`, null)
}

export const createFriendRequest = async (responser_id: number) => {
    const body = {
        responser_id
    }

    return reqPost('/friend_requests', JSON.stringify(body))
}

export const removeFriendRequest = async (responser_id: number) => {
    return reqDelete(`/friend_requests/${responser_id}/byId`)
}
