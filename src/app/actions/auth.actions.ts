import types from './_types.actions'

export function authLogin(data) {
    return {
        type: types.AUTH_LOGIN,
        payload: data
    }
}

export function authClear() {
    return {
        type: types.AUTH_CLEAR,
        payload: null,
    }
}
