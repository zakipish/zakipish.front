import types from './_types.actions'

export function downloadCities(data) {
    return {
        type: types.DOWNLOAD_CITIES,
        payload: data,
    }
}

export function selectCity(data) {
    return {
        type: types.SELECT_CITY,
        payload: data,
    }
}
