import types from './_types.actions'

export function downloadGameLevels(data) {
    return {
        type: types.DOWNLOAD_GAME_LEVELS,
        payload: data,
    }
}
