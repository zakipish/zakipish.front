import * as React from 'react'

import { User } from '../../../models/user'

import { reg } from '../../../services/profile.service'
import ValidatesForm from '../../form/validates-form'
import InputControl from '../../form/input-control'
import PhoneControl from '../../form/phone-control'

import AuthSocComponent from './auth-soc.component'
import InputPassComponent from './input-pass.component'

import ExternalLink from '../../blocks/external-link'

import { history } from '../../../core/history'

class FormRegComponent extends ValidatesForm {

    commonValidationConfig = {
        requiredAtLeast: [
            {
                field: 'email',
                label: 'email'
            },
            {
                field: 'phone',
                label: 'телефон'
            }
        ]
    }

    validationConfig = {
        name: {
            present: true
        },

        phone: {
            maxLength: 10,
            phone: true
        },

        password: {
            present: true,
            minLength: 6
        },

        email: {
            email: true
        }
    }

    onSubmit = (e: any) => {
        e.preventDefault()

        if(this.validateWholeForm()) {
            this.setBusy()

            const { item } = this.state
            reg(item)
            .then(this.handleSubmit)
            .catch(this.handleErrors)
            .finally(this.unsetBusy)
        }
    }

    inputClasses = (name: string) => {
        return `authForm__input ${ this.isValid(name) ? '' : 'authForm__input--error'}`
    }

    render() {
        const { item, commonErrors, busy } = this.state

        return (
            <form className="authForm" onSubmit={this.onSubmit}>
                <AuthSocComponent/>
                <div className={this.inputClasses('name')}>
                    <InputControl type="text" placeholder="Имя" value={item.name} onChange={this.handleChange('name')} disabled={busy} />
                    <span className="_error-message">{this.errorMessage('name')}</span>
                </div>

                <div className={this.inputClasses('phone')}>
                    <PhoneControl type="text" placeholder="Телефон" value={item.phone} onChange={this.handleChange('phone')} disabled={busy} />
                    <span className="_error-message">{this.errorMessage('phone')}</span>
                </div>

                <div className={this.inputClasses('email')}>
                    <InputControl type="text" placeholder="Email" value={item.email} onChange={this.handleChange('email')} disabled={busy} />
                    <span className="_error-message">{this.errorMessage('email')}</span>
                </div>

                <InputPassComponent
                    value={item.password}
                    change={this.handleChange('password')}
                    valid={this.isValid('password')}
                    errorMessage={this.errorMessage('password')}
                    disabled={busy}
                />

                <div className="commonErrorMessage">{commonErrors}</div>

                <button type="submit" className="authForm__btn authForm__btn--reg" disabled={busy}>
                    <span>Регистрация</span>
                </button>
                <div className="authForm__terms">
                    <p>Регистрируясь в ZaKipish вы принимаете</p>
                    <p><ExternalLink href="https://clubs.zakipish.ru/doc/soglashenie-s-polzovatelem">Правила пользования</ExternalLink></p>
                </div>
            </form>
        )
    }
}

const propsDecorator = (props) => {
    const wrapper = (WrappedComponent) => {
        return class extends React.Component {
            render() { return <WrappedComponent {...props}/> }
        }
    }

    return wrapper
}

const item = new User()

export default propsDecorator({ item })(FormRegComponent)
