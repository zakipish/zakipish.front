import * as React from 'react'
import IconComponent, { IconList } from '../../blocks/icon/icon.component'
import config from '../../../core/config'
import ExternalLink from '../../blocks/external-link'

class AuthSocComponent extends React.Component<{}, {}> {

    getLinkUrl(nameSoc) {
        const currentUrl = window.location.href

        return `${config.api_host}/auth/social/${nameSoc}?target=${currentUrl ||config.social_redirect_url}`
    }

    render() {
        return (
            <ul className="authForm__soc">
                <li>
                    <ExternalLink href={this.getLinkUrl('facebook')}>
                        <IconComponent icon={IconList.sqFacebook}/>
                    </ExternalLink>
                </li>
                <li>
                    <ExternalLink href={this.getLinkUrl('odnoklassniki')}>
                        <IconComponent icon={IconList.sqOdnoklassniki}/>
                    </ExternalLink>
                </li>
                <li>
                    <ExternalLink href={this.getLinkUrl('mailru')}>
                        <IconComponent icon={IconList.sqMailru}/>
                    </ExternalLink>
                </li>
                <li>
                    <ExternalLink href={this.getLinkUrl('vkontakte')}>
                        <IconComponent icon={IconList.sqVkontakte}/>
                    </ExternalLink>
                </li>
            </ul>
        )
    }
}

export default AuthSocComponent
