import * as React from 'react'
import { ID_LINK_FORGET } from './form-login.component'

interface IProps {
    hide: boolean
    btnWrapRef: React.RefObject<HTMLDivElement>
    onToggle?: Function
    hideKey: string
    showKey?: string
}

class AuthPopupComponent extends React.Component<IProps, {}> {

    constructor(props) {
        super(props)
        this.onClickOutside = this.onClickOutside.bind(this)
    }

    componentDidMount() {
        document.addEventListener('mouseup', this.onClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mouseup', this.onClickOutside);
    }

    onShow() {
        let { onToggle, showKey } = this.props
        if (showKey) onToggle(showKey)
    }
    
    onHide() {
        let { onToggle, hideKey } = this.props
        onToggle(hideKey)
    }

    onClickOutside(e) {
        let { hide, btnWrapRef } = this.props
        if (!hide && btnWrapRef) {
            if (!btnWrapRef.current.contains(e.target)) {
                this.onHide()
            }
        }
        this.forgetPopup(e)
    }

    forgetPopup(e) {
        if (e.path[0].id === ID_LINK_FORGET) {
            this.onShow()
        }
    }

    render() {
        let { children, hide } = this.props
        return (
            <div className={`authPopup ${hide ? '_hide' : ''}`} >
                {children}
            </div>
        )
    }
}

export default AuthPopupComponent