import * as React from 'react'
import { RouteProps } from 'react-router'
import { History } from 'history'
import AuthHeaderComponent from './auth-header.component'

interface IProps {
    history: History
}

class AuthPageComponent extends React.Component<IProps & RouteProps, {}> {
    render() {
        let { history, children } = this.props
        return (
            <div className="authPage">
                <div className="authPage__wrap">
                    <AuthHeaderComponent history={history}/>
                    {children}
                </div>
            </div>
        )
    }
}

export default AuthPageComponent