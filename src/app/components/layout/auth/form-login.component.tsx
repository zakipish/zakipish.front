import * as React from 'react'
import { Link } from 'react-router-dom'
import AuthSocComponent from './auth-soc.component'
import InputPassComponent from './input-pass.component'

import ValidatesForm from '../../form/validates-form'
import InputControl from '../../form/input-control'
import { login as Login } from '../../../services/auth.service'

import { history } from '../../../core/history'


export const ID_LINK_FORGET: string = 'loginPopuplinkForget'

class FormLoginComponent extends ValidatesForm {
    validationConfig = {
        login: {
            present: true
        },

        password: {
            present: true
        },
    }

    onSubmit = (e: any) => {
        e.preventDefault()

        if(this.validateWholeForm()) {
            this.setState({ busy: true })

            const { item } = this.state
            Login(item.login, item.password)
            .then(this.handleSubmit)
            .catch(this.handleErrors)
            .finally(() => this.setState({ busy: false }))
        }
    }

    inputClasses = (name: string) => {
        return `authForm__input ${ this.isValid(name) ? '' : 'authForm__input--error'}`
    }

    render() {
        const { item, busy } = this.state
        return (
            <form className="authForm" onSubmit={this.onSubmit}>
                <AuthSocComponent/>
                <div>
                    <div className={this.inputClasses('login')}>
                        <InputControl type="text" placeholder="Телефон или Еmail" value={item.login} onChange={this.handleChange('login')} disabled={busy} />
                        <span className="_error-message">{this.errorMessage('login')}</span>
                    </div>
                    <InputPassComponent
                        value={item.password}
                        change={this.handleChange('password')}
                        disabled={busy}
                        valid={this.isValid('password')}
                        errorMessage={this.errorMessage('password')}
                    />
                    <div className="authForm__toggleLink">
                        <span className="mobileHide" id={ID_LINK_FORGET}>Забыли пароль?</span>
                        <Link className="desktopHide" to="/forget-passward">Забыли пароль?</Link>
                    </div>
                    <button type="submit" className="authForm__btn authForm__btn--login" disabled={busy}>
                        Войти
                    </button>
                </div>
            </form>
        )
    }
}

const propsDecorator = (props) => {
    const wrapper = (WrappedComponent) => {
        return class extends React.Component {
            render() { return <WrappedComponent {...props}/> }
        }
    }

    return wrapper
}

const item = { login: '', password: '' }

export default propsDecorator({ item })(FormLoginComponent)
