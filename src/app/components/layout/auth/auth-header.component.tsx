import * as React from 'react'
import { History } from 'history'
import { Link } from 'react-router-dom'
import CrossPopupComponent from '../cross-popup.component'

interface IProps {
    className?: string
    history: History
}

class AuthHeaderComponent extends React.Component<IProps, {}> {

    get activeLogin(): boolean {
        let { pathname } = this.props.history.location
        if (pathname === '/login' || pathname === '/forget-passward'){
            return true
        } else {
            return false
        }
    }

    render() {
        let { className, history } = this.props
        return (
            <div className={`authHeader ${className ? className : ''}`}>
                <Link to="/" className="authHeader__logo"></Link>
                <div className="authHeader__nav">
                    <span 
                        className={`${this.activeLogin ? '_act' : ''}`}
                        onClick={() => history.replace('/login')}
                    >Вход</span>
                    <span 
                        className={`${!this.activeLogin ? '_act' : ''}`}
                        onClick={() => history.replace('/registration')}
                    >Зарегистрироваться</span>
                </div>
                <CrossPopupComponent className="authHeader__cross" history={history}/>
            </div>
        )
    }
}

export default AuthHeaderComponent