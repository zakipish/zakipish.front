import * as React from 'react'
import AuthSocComponent from './auth-soc.component'
import { reqPost } from '../../../core/api'

interface IState {
    email: string
    success: boolean
    notfound: boolean
}

interface IProps {}

class FormForgetComponent extends React.Component<IProps, IState> {
    state: IState = {
        email: '',
        notfound: false,
        success: false
    }

    handleChange = (name: string) => (e: any) => {
        const state = this.state
        state[name] = e.target.value
        this.setState({ ...state })
    }

    resetPassword = (e: any) => {
        e.preventDefault()

        const { email } = this.state
        if (!email)
            return false

        const data = { email }
        reqPost('/auth/reset', JSON.stringify(data))
        .then(() => {
            this.setState({ success: true, notfound: false })
        })
        .catch((e) => {
            this.setState({ notfound: true })
        })
    }

    render() {
        const { success, notfound } = this.state

        return (
            <form className="authForm" onSubmit={this.resetPassword}>
                <AuthSocComponent/>
                <div className="authForm__input authForm__input--forgot">
                    <label>Восстановление пароля</label>
                    <input type="text" placeholder="Введите Email" onChange={this.handleChange('email')} />
                    {notfound &&
                        <div className="authForm__notfound">Пользователь с таким Email не найден</div>
                    }
                </div>
                {success ? (
                    <div className="authForm__success">Письмо с временным паролем отправлено на ввденный адрес</div>
                ) : (
                    <button type="submit" className="authForm__btn authForm__btn--login">
                        <span>Отправить</span>
                    </button>
                )}
            </form>
        )
    }
}

export default FormForgetComponent
