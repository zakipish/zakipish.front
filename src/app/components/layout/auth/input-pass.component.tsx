import * as React from 'react'
import IconComponent, { IconList } from '../../blocks/icon/icon.component'

interface IProps {
    value?: string
    errorMessage?: string
    valid?: boolean
    change: (value: string) => void
    disabled?: boolean
}

interface IState {
    passMask: boolean
}

class InputPassComponent extends React.Component<IProps, IState> {
    state = {
        passMask: true
    }

    togglePassMask() {
        this.setState({passMask: !this.state.passMask})
    }

    constructor(props) {
        super(props)
    }

    render() {
        let { passMask } = this.state
        const { value, change, errorMessage, disabled } = this.props

        let valid = true
        if (typeof this.props.valid === 'boolean') {
            valid = this.props.valid
        }
        const className = `authForm__input ${ valid ? '' : 'authForm__input--error'}`
        return (
            <div className={className}>
                <input
                    type={ passMask ? 'password' : 'text' }
                    placeholder="Пароль"
                    onChange={(e) => change(e.target.value)}
                    value={value || ''}
                    disabled={disabled}
                />
                <div
                    className={`_iconEye ${!passMask ? '_iconEye--act' : ''}`}
                    onClick={() => this.togglePassMask()}
                >
                    <IconComponent icon={IconList.eye}/>
                </div>
                <span className="_error-message">{errorMessage}</span>
            </div>
        )
    }
}

export default InputPassComponent
