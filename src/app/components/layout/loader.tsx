import * as React from 'react'

interface IProps {
    className?: string
    state: boolean
    children?: any
    resize?: boolean
    loaderClassName?: string
}

class Loader extends React.Component<IProps, {}> {

    elem: React.RefObject<HTMLDivElement> = React.createRef();

    state = {
        resize: this.props.resize
    }

    componentDidMount() {
        window.addEventListener('resize', this.setHeight)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.setHeight)
    }

    get className() {
        let outClassName = 'loader__img --hide'
        let { className } = this.props 
        if (className) outClassName += ` ${className}`
        return outClassName
    }

    setHeight = () => {
        if (this.state.resize !== false) {
            setTimeout(() => {
                let elem = this.elem.current
                let rectElem = elem.getBoundingClientRect()
                let { innerHeight } = window
                elem.style.height = `${innerHeight - rectElem.top - 50}px`
            }, 100)
        }
    }

    toggle = () => {
        setTimeout(() => {
            let { state } = this.props 
            let elem = this.elem.current
            if (state) {
                elem.classList.add('--show')
                elem.classList.remove('--hide')
            } else {
                elem.classList.remove('--show')
                elem.classList.add('--hide')
            }
        }, 100)
    }

    render() {
        this.setHeight()
        this.toggle()
        let { children, loaderClassName } = this.props
        return (
            <div className={`loader ${loaderClassName || ''}`}>
                <div className={this.className} ref={this.elem}>
                    <img src="/images/loader.gif" alt="#"/>
                </div>
                <div className="loader__body">
                    {children}
                </div>
            </div>
        )
    }
}

export default Loader
