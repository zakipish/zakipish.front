import * as React from 'react'
import moment from 'moment'

import { User } from '../../../models/user'
import { Game } from '../../../models/game'
import { Sport } from '../../../models/sport'

import SectionCardsComponent from '../../sections/section-cards.component'
import LabelComponent, { LabelStatus } from '../../blocks/label.component'

import { IconList } from '../../blocks/icon/icon.component'
import UiSelectControl from '../../form/ui-select-control'
import LabelDatepicker from '../../blocks/datepicker/label.datepicker'
import LabelTimepicker from '../../blocks/datepicker/label.timepicker';

import NewGameFriend from './new-game.friend'

interface IProps {
    tabs?: Array<{
        key: string
        title: string
    }>
    onClickTab: (key: string) => void
    activeTabKey?: string
    myGames: Game[]
    friendGames: Game[]
    me: User
    friend: User
    invitationDisabled: boolean
    onInvite: (game_id: number) => void
    onAttach: (game_id: number) => void
}

interface IState {}

class InviteFriend extends React.Component<IProps, IState> {

    state = {}

    onClickTab = (key) => () => {
        this.props.onClickTab(key)
    }

    get myGamesCollection() {
        const { myGames, friend, onInvite } = this.props
        const { invitationDisabled } = this.props

        if (myGames == null)
            return []

        const button = {
            title: 'Пригласить в игру',
            bg: 'blue',
            onClick: onInvite
        }

        const games = myGames.filter(game => !game.users.map(user => user.id).includes(friend.id) && moment(game.date + " " + game.interval[0]).isAfter(moment()))
        return this.gamesCollection(games, button)
    }

    get friendGamesCollection() {
        const { friendGames, me, friend, onAttach } = this.props

        if (friendGames == null)
            return []

        const button = {
            title: `Попроситься к ${friend.fullName()}`,
            onClick: onAttach
        }

        const games = friendGames.filter(game => !game.users.map(user => user.id).includes(me.id) && moment(game.date + " " + game.interval[0]).isAfter(moment()))
        return this.gamesCollection(games, button)
    }

    get commonGamesCollection() {
        const { myGames, friend } = this.props

        if (myGames == null)
            return []

        const button = {
            title: `Состоите в игре с ${friend.fullName()}`
        }

        const games = myGames.filter(game => game.users.map(user => user.id).includes(friend.id) && moment(game.date + " " + game.interval[0]).isAfter(moment()))
        return this.gamesCollection(games, button)
    }

    gamesCollection = (games: Game[], button: { title: string, bg?: string, onClick?: (game_id: number) => void }) => {
        const { invitationDisabled } = this.props

        return games.map(game => {
            return {
                type: 'game',
                card: {
                    url: `/game/${game.id}`,
                    header: {
                        title: `${moment(game.date).format('D MMM')}, ${game.interval[0]} - ${game.interval[1]}`,
                    },
                    title: game.name || '-',
                    text: game.playground.club.name,
                    state: { must: game.max_players, now: game.players_count },
                    actions: [{
                        title: button.title,
                        bg: button.bg,
                        disabled: invitationDisabled,
                        onClick: button.onClick ? button.onClick(game.id) : null
                    }]
                }
            }
        })
    }

    get list() {
        const { activeTabKey } = this.props

        if (activeTabKey === 'my')
            return this.myGamesCollection

        if (activeTabKey === 'friend')
            return this.friendGamesCollection

        if (activeTabKey === 'common')
            return this.commonGamesCollection
    }

    render() {
        let { tabs, activeTabKey, friend } = this.props

        return (
            <div className="invite">
                <div className="invite__tab">
                    {tabs.map(el => {
                        let num = 0
                        if (el.key === 'my')
                            num = this.myGamesCollection.length

                        if (el.key === 'friend')
                            num = this.friendGamesCollection.length

                        if (el.key === 'common')
                            num = this.commonGamesCollection.length

                        return <div
                            key={el.key}
                            className={`invite__btn ${(el.key === activeTabKey) ? '--blue' : ''}`}
                            onClick={this.onClickTab(el.key)}
                        >
                            <span>{el.title}</span>
                            {(num >= 0) && <i>{num}</i>}
                        </div>
                    })}
                </div>
                <NewGameFriend friendId={friend.id} />
                <div className="invite__content">
                    <div className="invite__tit">Создание игры</div>
                    <SectionCardsComponent cols={1} list={this.list}/>
                </div>
            </div>
        )
   }
}

export default InviteFriend
