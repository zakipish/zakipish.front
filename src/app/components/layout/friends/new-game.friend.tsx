import * as React from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'

import { reqGet } from '../../../core/api'
import { serialize } from '../../../core/utils'

import { Sport } from '../../../models/sport'

import { getSports } from '../../../services/sports-service'

import { LabelStatus } from '../../blocks/label.component'

import UiSelectControl from '../../form/ui-select-control'
import LabelDatepicker from '../../blocks/datepicker/label.datepicker'
import LabelTimepicker from '../../blocks/datepicker/label.timepicker';

interface IProps {
    friendId: number
}

interface IState {
    sports?: Sport[]
    times?: any[]
    filter: any
}

class NewGameFriend extends React.Component<IProps, IState> {
    state: IState = {
        filter: {
            sport_id: '',
            date: moment(),
            time: null
        }
    }

    componentDidMount() {
        getSports()
        .then(sports => this.setState({ sports }))
    }

    handleChange = (key) => (value) =>{
        let { filter } = this.state
        filter[key] = value
        this.setState({ filter })

        if (key != 'time') {
            if (filter.date && filter.sport_id)
                this.replaceTimes()
        }
    }

    replaceTimes = () => {
        const { filter } = this.state
        filter.time = null
        this.setState({ filter, times: null }, this.getTimes)
    }

    getTimes = () => {
        const { sport_id, date } = this.state.filter
        const params = {
            sport_id,
            date
        }

        reqGet('/playgrounds/times', params)
        .then(response => {
            if (this.state.filter.sport_id == sport_id && this.state.filter.date == date)
                this.setState({ times: response.data })
        })
    }

    get sports() {
        const { sports } = this.state
        if (!sports)
            return {}
        return sports.reduce((acc, sport) => {
            acc[sport.id] = sport.name
            return acc
        }, {})
    }

    get times() {
        const { times } = this.state

        if (!times)
            return []

        return times.map(time => ({
            from: moment(time.start).utcOffset(3),
            to: moment(time.end).utcOffset(3),
            status: time.status == 'available' ? LabelStatus.normal : LabelStatus.disable
        }))
    }

    get buttonUrl() {
        const { friendId } = this.props
        const { sport_id, date, time } = this.state.filter

        let params = {
            actionType: 'newGame',
            friend_id: friendId,
        }

        if (date) {
            params['date'] = date.format('YYYY-MM-DD')
            params['dateStart'] = date.clone().subtract(2, 'days').format('YYYY-MM-DD')
        }
        if (sport_id)
            params['sport_id'] = sport_id
        if (time) {
            if (time.from)
                params['time_start'] = time.from.format('YYYY-MM-DDTHH:mm:ssZ')
            if (time.to)
                params['time_end'] = time.to.format('YYYY-MM-DDTHH:mm:ssZ')
        }

        return `/?${serialize(params)}`
    }

    render() {
        let { filter } = this.state

        return (
            <div className="invite__top">
                <div className="invite__header">
                    <UiSelectControl
                        className="invite__select"
                        options={this.sports}
                        placeholder="Выбирете спорт"
                        onSelect={this.handleChange('sport_id')}
                        value={filter.sport}
                    />
                    <LabelDatepicker
                        date={filter.date}
                        onChange={this.handleChange('date')}
                        title="-"
                        status={!!filter.date ? LabelStatus.active : LabelStatus.disable}
                    />
                    <LabelTimepicker
                        title="-"
                        timepicker={{
                            emptyTitle: 'Нет доступных залов',
                            value: filter.time,
                            times: this.times,
                            onChange: this.handleChange('time')
                        }}
                        status={!!filter.time ? LabelStatus.active : LabelStatus.disable}
                        disabled={!filter.date || !filter.sport_id}
                    />
                </div>
                <div className="invite__btn --fill --white --lg">
                    <Link to={this.buttonUrl}>
                        <span>Начать игру</span>
                    </Link>
                </div>
            </div>
        )
    }
}

export default NewGameFriend
