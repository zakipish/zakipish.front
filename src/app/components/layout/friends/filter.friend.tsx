import * as React from 'react'

import WrapForm from '../form/wrap.form'
import RowForm from '../form/row.form'
import FieldForm from '../form/field.form'
import BtnsForm from '../form/btns.form'
import InputControl from '../../form/input-control'
import SelectControl from '../../form/select-control'
import { reqPut } from "../../../core/api"
import { Game } from '../../../models/game'
import * as _ from "lodash"
import RadioControl from '../../form/radio-control';

interface IProps {
    onSave?: (data:any) => void
}

interface IState {
    data: any
}

class FilterFriend extends React.Component<IProps, IState> {

    state = {
        data: {
            region: '',
            min_old: '',
            max_old: '',
            gender: '3',
        }
    }

    onChange = (key) => (value) => {
        let { data } = this.state
        data[key] = value
        this.setState({ data })
    }

    handleSubmit = (e) => {
        e.preventDefault()
    }

    render() {
        let { data } = this.state

        return (
            <form onSubmit={this.handleSubmit}>
                <WrapForm className="--filter" title="Параметры поиска">
                    <RowForm>
                        <FieldForm title="Регион">
                            <SelectControl
                                options={{}}
                                value={data.region}
                                onChange={this.onChange('region')}
                            />
                        </FieldForm>
                    </RowForm>
                    <RowForm>
                        <FieldForm title="Город">
                            <SelectControl
                                options={{}}
                                value={data.region}
                                onChange={this.onChange('region')}
                            />
                        </FieldForm>
                    </RowForm>
                    <RowForm>
                        <FieldForm title="Возраст от">
                            <SelectControl
                                options={{}}
                                value={data.min_old}
                                onChange={this.onChange('min_old')}
                            />
                        </FieldForm>
                        <FieldForm title="до">
                            <SelectControl
                                options={{}}
                                value={data.max_old}
                                onChange={this.onChange('max_old')}
                            />
                        </FieldForm>
                    </RowForm>
                    <RowForm>
                        <FieldForm title="пол">
                            <div className="checkRadio --line">
                                <RadioControl
                                    options={{
                                        1: 'Мужчина',
                                        2: 'Женщина',
                                        3: 'Любой',
                                    }}
                                    value={data.gender}
                                    onChange={this.onChange('gender')}
                                />
                            </div>
                        </FieldForm>
                    </RowForm>
                </WrapForm>
            </form>
        )
    }
}

export default FilterFriend
