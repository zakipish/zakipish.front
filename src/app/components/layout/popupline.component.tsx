import * as React from 'react'
import IconComponent, { IIcon } from '../blocks/icon/icon.component'

export interface IPopupLine {
    title?: string
    name?: string
    icon?: IIcon
    avatar?: string
    status?: string
}

class PopuplineComponent extends React.Component<IPopupLine, {}> {
    render() {
        let { title, name, icon, avatar, status } = this.props
        return (
            <div className="popupLine">
                { icon ?
                    <div className="popupLine__img">
                        <IconComponent {...icon}/>
                    </div>
                :  
                    <div className="popupLine__img">
                        <img src={avatar ? avatar : '/images/avatar-md.jpg'} alt="#"/>
                    </div>
                }
                <div className="popupLine__text">
                    { title ? <div className="popupLine__tit">{title}</div> : '' }
                    { name ? <div className="popupLine__tit">{name}</div> : '' }
                    { status ? <div className="popupLine__status">
                        <span>{status}</span>
                    </div> : '' }
                </div>
            </div>
        )
    }
}

export default PopuplineComponent
