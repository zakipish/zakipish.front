import * as React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { selectCity as selectCityAction } from '../../actions/cities.actions'
import NavSelectComponent from '../blocks/select/nav-select.component'
import SelectComponent from '../blocks/select/select.component'

import { logout } from '../../services/auth.service'
import {resizeImage} from '../UI/Image'

interface IHeaderProfileComponentProps {
  user: any
  cities: any[]
  selectedCity: any
  selectCity: (value) => void
}

interface IHeaderProfileComponentState {
  hideProfileMenu: boolean
}

class HeaderProfileComponent extends React.Component<IHeaderProfileComponentProps, IHeaderProfileComponentState> {

  public refAuthStatus: React.RefObject<HTMLDivElement> = React.createRef()

  constructor(props) {
    super(props)
    this.state = {
      hideProfileMenu: true
    }
    this.toggleProfileMenu = this.toggleProfileMenu.bind(this)
    this.handleLogoutClick = this.handleLogoutClick.bind(this)
  }

  public toggleProfileMenu = () => {
    this.setState({ hideProfileMenu: !this.state.hideProfileMenu })
  }

  public handleLogoutClick = () => {
    this.toggleProfileMenu()
    logout()
  }

  public render() {
    const { user, cities, selectedCity, selectCity } = this.props
    const { hideProfileMenu } = this.state

    return (
      <div className='hdr hdr--authStatus'>
        <div className='hdr__wrap'>
          <div className='hdr__logoWrap'>
            <Link to='/'>
              <div className='hdr__logo' />
            </Link>
            <div className='hdr__city'>
              <SelectComponent list={cities.map(city => ({ label: city.name, value: city.id }))} onChange={selectCity} value={selectedCity} />
            </div>
          </div>
          <div className='hdr__authStatus' ref={this.refAuthStatus}>
            <div className='hdr__authNav'>
              <NavSelectComponent
                refWrap={this.refAuthStatus}
                title={user.fullName()}
                hide={hideProfileMenu}
                onToggle={this.toggleProfileMenu}
              >
                <Link onClick={this.toggleProfileMenu} to='/profile' className='_authNavLink'>Профиль</Link>
                <Link onClick={this.toggleProfileMenu} to='/profile/friends' className='_authNavLink'>Друзья</Link>
                <span onClick={this.handleLogoutClick} className='_authNavLink'>Выйти</span>
              </NavSelectComponent>
            </div>
            <Link to='/profile' className='hdr__avatar' >
              <img alt={`${user}`} src={ user.avatar_header_url ? resizeImage(user.avatar_header_url, 24, 24) : '/images/avatar-xs.jpg' }/>
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.authState.user,
    cities: state.citiesState.cities,
    selectedCity: state.citiesState.selectedCity
  }
}

const mapDispatchToProps = {
  selectCity: selectCityAction
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderProfileComponent)
