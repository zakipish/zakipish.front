import * as React from 'react'
import { withRouter, RouteComponentProps } from 'react-router'
import HeaderPublicComponent from './header-public.component'
import HeaderProfileComponent from './header-profile.component'
import FooterComponent from './footer.component'

interface IProps extends RouteComponentProps {
    user: any
}

class LayoutComponent extends React.Component<IProps, {}> {

    checkTmp = () => {
        const { user } = this.props
        const { pathname } = this.props.location
        if (pathname != '/profile/edit' && user && user.tmp)
            this.redirectToEdit()
    }

    redirectToEdit = () => {
        const { history } = this.props
        history.push('/profile/edit')
    }

    render() {
        this.checkTmp()

        const { children, user } = this.props
        const auth = !!user

        return (
            <div className="page">
                { auth ?
                    <HeaderProfileComponent/>
                :
                    <HeaderPublicComponent/>
                }
                <div className="page__body">
                    {children}
                </div>
                <FooterComponent/>
            </div>
        )
    }
}

export default withRouter(LayoutComponent)
