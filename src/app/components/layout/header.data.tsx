import { ISelectItem } from '../blocks/select/select.component'

const city: ISelectItem[] = [
    { label: 'Санкт-Петербург', value: '1' },
    { label: 'Москва', value: '2' },
    { label: 'Рим', value: '3' },
    { label: 'Вена', value: '4' },
    { label: 'Вена', value: '5' },
    { label: 'Вена', value: '6' },
    { label: 'Вена', value: '7' },
    { label: 'Вена', value: '8' },
    { label: 'Вена', value: '9' },
    { label: 'Вена', value: '10' },
    { label: 'Вена', value: '11' },
    { label: 'Вена', value: '12' },
    { label: 'Вена', value: '13' },
    { label: 'Вена', value: '14' },
]

const nav: ISelectItem[] = [
    { label: 'Максим Иванов', value: '1' },
]

const avatar: string = 'assets/userfiles/avatar.png'

export { city, nav, avatar }