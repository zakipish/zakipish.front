import * as React from 'react'
import IconComponent from '../blocks/icon/icon.component'

interface IProps {
    preview: React.ReactFragment
    classname?: string
}

interface IState {
    hide: boolean
}

class MobilePopup extends React.Component<IProps, IState> {

    anim: boolean

    constructor(props) {
        super(props)

        this.state = {
            hide: true,
        }
        this.anim = false
    }

    componentDidMount() {
        this.anim = false
    }

    componentWillUnmount() {
        document.body.classList.remove('no-scroll-mobile')
    }

    onToggle() {
        let { hide } = this.state
        if (hide) {
            document.body.classList.add('no-scroll-mobile')
        } else {
            document.body.classList.remove('no-scroll-mobile')
        }
        this.setState({hide: !hide })
    }
    
    popupClick() {
        this.anim = true
        if (this.state.hide) this.onToggle()
    }
    
    getClassName() {
        let strClass = 'mobilePopup'
        let { classname } = this.props
        let { hide } = this.state
        if (classname) strClass += ` ${classname}`
        strClass += ` ${hide ? '_hide' : '_show'}`
        if (this.anim) strClass += ' _anim'
        return strClass
    }

    render() {
        let { children, preview } = this.props
        return (
            <div className={this.getClassName()} onClick={() => this.popupClick()}>
                <div className="mobilePopup__line">
                    {preview}
                    <div className="mobilePopup__arr"onClick={() => this.onToggle()}>
                        <IconComponent icon="selectArr" fill="#D1D1D1" width="20px" height="20px"/>
                    </div>
                </div>
                <div className="mobilePopup__body">
                    {children}
                </div>
                <div className="mobilePopup__cross" onClick={() => this.onToggle()}>
                    <IconComponent icon="cross" fill="#646464" width="16px" height="16px"/>
                </div>
            </div>
        )
    }
}

export default MobilePopup