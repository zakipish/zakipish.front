import * as React from 'react'

interface IProps {
    className?: string
    children?: any
    classItems?: string[]
}

class BtnsForm extends React.Component<IProps, {}> {

    getItemClassName(i) {
        let className = 'formBtns__item'
        let { classItems } = this.props
        if (classItems && classItems[i]) className += ` --${classItems[i]}`
        return className
    }

    render() {
        let { className, children } = this.props
        if (children && !Array.isArray(children)) {
            children = [children]
        }
        return (
            <div className={`formBtns ${className ? ` ${className}` : ''}`}>
                { children.map((el, i) => { return(
                    <div key={i} className={this.getItemClassName(i)}>
                        {el}
                    </div>
                )}) }
            </div>
        ) 
    }
}

export default BtnsForm