import * as React from 'react'

interface IProps {
    className?: string
    title?: string
    info?: string
    children?: any
    valid?: boolean
}

class FieldForm extends React.Component<IProps, {}> {
    render() {
        let { className, title, info, children, valid } = this.props
        return (
            <div className={`formField ${className ? ` ${className}` : ''}`}>
                { title && 
                    <div className={`formField__tit ${valid == null || valid ? '' : '--error'}`}>
                        {title}
                        {info && <i title={info}>?</i>}
                    </div> 
                }
                <div className="formField__body">
                    {children}
                </div>
            </div>
        )
    }
}

export default FieldForm
