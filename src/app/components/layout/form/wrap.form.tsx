import * as React from 'react'

interface IProps {
    className?: string
    title?: string
    children?: any
}

class WrapForm extends React.Component<IProps, {}> {
    render() {
        let { className, title, children } = this.props
        return (
            <div className={`formWrap ${className ? ` ${className}` : ''}`}>
                { title && <div className="formWrap__tit">{title}</div> }
                <div className="formWrap__body">
                    {children}
                </div>
            </div>
        )
    }
}

export default WrapForm