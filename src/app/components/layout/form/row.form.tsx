import * as React from 'react'

interface IProps {
    className?: string
    title?: string
    children?: any
    classItems?: string[]
}

class RowForm extends React.Component<IProps, {}> {

    getItemClassName(i) {
        let className = 'formRow__item'
        let { classItems } = this.props
        if (classItems && classItems[i]) className += ` --${classItems[i]}`
        return className
    }

    render() {
        let { className, title, children } = this.props
        if (children && !Array.isArray(children)) {
            children = [children]
        }
        return (
            <div className={`formRow ${className ? ` ${className}` : ''}`}>
                { title && <div className="formRow__tit">{title}</div> }
                <div className="formRow__body">
                    { children.map((el, i) => { return(
                        <div key={i} className={this.getItemClassName(i)}>
                            {el}
                        </div>
                    )}) }
                </div>
            </div>
        )
    }
}

export default RowForm