import * as React from 'react'
import IconComponent from '../blocks/icon/icon.component'

interface IProps {
    className?: string
    hide?: boolean
    onToggle?: (hide: boolean) => void
}

class Popup extends React.Component<IProps, {}> {

    get className() {
        let str = 'popup'
        let { className, hide } = this.props
        str += className ? ` ${className}` : ''
        str += hide ? ` --hide` : ''
        return str
    }

    render() {
        let { children, onToggle } = this.props
        return (
            <div className={this.className}>
                <div className="popup__bg" onClick={() => onToggle(true)}></div>
                <div className="popup__content">
                    <IconComponent 
                        className="popup__cross" 
                        icon="cross"
                        onClick={() => onToggle(true)}
                    />
                    {children}
                </div>
            </div>
        )
    }
}

export default Popup