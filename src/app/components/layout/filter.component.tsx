import * as React from 'react'
import IconComponent from '../blocks/icon/icon.component'
import SelectComponent, { ISelectItem } from '../blocks/select/select.component'
import InputControl from '../form/input-control';

export interface IFilterProps {
    search?: React.ReactChild
    list?: IFilterSelect[]
    classname?: string
    dark?: boolean
}

interface IFilterSelect {
    list: ISelectItem[]
    value: string
    onChange?: (value?: any) => void
}

class FilterComponent extends React.Component<IFilterProps, {}> {

    searchRef: React.RefObject<HTMLDivElement> = React.createRef()

    state = {
        hideSearch: true
    }

    toggleSearch = () => {
        let hideSearch = !this.state.hideSearch
        let inputElem = this.searchRef.current.querySelector('input')
        if (inputElem && !hideSearch) {
            setTimeout(() => inputElem.focus(), 100)
        }
        this.setState({hideSearch})
    }

    get searchClassName() {
        let className = 'filter__search'
        let { hideSearch } = this.state
        className += hideSearch ? ' --hide' : ' --show'
        return className
    }

    render() {
        let { search, list, classname, dark } = this.props
        return (
            <div className={`filter ${classname}`}>
                { !!search && (
                    <div className={this.searchClassName} ref={this.searchRef}>
                        {search}
                        <IconComponent 
                            onClick={this.toggleSearch}
                            className="filter__icon"
                            icon="search" 
                            fill={dark ? '#374B88' : 'rgba(255,255,255,0.7)'} 
                            width="17px" 
                            height="17px"
                        />
                    </div>
                ) }
                { !!list && list.map((el, i) => {return (
                    <div className="filter__select" key={i}>
                        <SelectComponent 
                            list={el.list} 
                            value={el.value}
                            color={dark ? '#374B88' : 'rgba(255,255,255,0.7)'} 
                            onChange={el.onChange}
                            right={true}
                        />
                    </div>
                )})}
            </div>
        )
    }
}

export default FilterComponent
