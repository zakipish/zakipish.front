import * as React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { selectCity } from '../../actions/cities.actions'

import SelectComponent from '../blocks/select/select.component'
import AuthPopupComponent from './auth/auth-popup.component'
import FormLoginComponent from './auth/form-login.component'
import FormRegComponent from './auth/form-reg.component'
import FormForgetComponent from './auth/form-forget.component'

interface IProps {
    cities: any[]
    selectedCity: any
    selectCity: (value) => void
}

interface IState {
    hideLogin: boolean
    hideReg: boolean
    hideForget: boolean
}

export class HeaderPublicComponent extends React.Component<IProps, IState> {

    btnWrapRef: React.RefObject<HTMLDivElement>;
    static _this

    constructor(props) {
        super(props)
        this.state = {
            hideLogin: true,
            hideReg: true,
            hideForget: true
        }
        this.btnWrapRef = React.createRef()
        this.togglePopup = this.togglePopup.bind(this)
        HeaderPublicComponent._this = this
    }

    static togglePopup(key: string): void {
        this._this.togglePopup(key)
    }

    togglePopup(key: string): void {
        switch (key) {
            case 'login':
                this.setState({
                    hideLogin: !this.state.hideLogin,
                    hideReg: true,
                    hideForget: true,
                })
                break;
            case 'reg':
                this.setState({
                    hideLogin: true,
                    hideReg: !this.state.hideReg,
                    hideForget: true,
                })
                break;
            case 'forget':
                this.setState({
                    hideLogin: true,
                    hideReg: true,
                    hideForget: !this.state.hideForget,
                })
                break;
            default:
                break;
        }
    }

    render() {
        const { cities, selectedCity, selectCity } = this.props
        const { hideLogin, hideForget, hideReg } = this.state

        return (
            <div className="hdr hdr--authAction">
                <div className="hdr__wrap">
                    <div className="hdr__logoWrap">
                        <Link to="/">
                            <div className="hdr__logo"></div>
                        </Link>
                        <Link to="/login" className="desktopHide">
                            <div className="hdr__link">
                                <span>Вход</span>
                            </div>
                        </Link>
                        <div className="hdr__city">
                            <SelectComponent list={cities.map(city => ({ label: city.name, value: city.id }))} onChange={selectCity} value={selectedCity} />
                        </div>
                    </div>
                    <div  ref={this.btnWrapRef} className="hdr__authAction mobileHide">
                        <div className="hdr__btnWrap">
                            <div
                                className="hdr__btn hdr__btn--darkBlue"
                                onClick={() => this.togglePopup('login')}
                            >
                                <span>Вход</span>
                            </div>
                            <AuthPopupComponent
                                hide={hideLogin}
                                btnWrapRef={this.btnWrapRef}
                                onToggle={this.togglePopup}
                                hideKey="login"
                                showKey="forget"
                            >
                                <FormLoginComponent />
                            </AuthPopupComponent>
                            <AuthPopupComponent
                                hide={hideForget}
                                btnWrapRef={this.btnWrapRef}
                                onToggle={this.togglePopup}
                                hideKey="forget"
                            >
                                <FormForgetComponent/>
                            </AuthPopupComponent>
                        </div>
                        <div className="hdr__btnWrap">
                            <div
                                className="hdr__btn hdr__btn--green"
                                onClick={() => this.togglePopup('reg')}
                            >
                                <span>Регистрация</span>
                            </div>
                            <AuthPopupComponent
                                hide={hideReg}
                                btnWrapRef={this.btnWrapRef}
                                onToggle={this.togglePopup}
                                hideKey="reg"
                            >
                                <FormRegComponent/>
                            </AuthPopupComponent>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        cities: state.citiesState.cities,
        selectedCity: state.citiesState.selectedCity,
    }
}

const mapDispatchToProps = {
    selectCity
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderPublicComponent)
