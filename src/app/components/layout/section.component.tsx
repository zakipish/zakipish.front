import * as React from 'react'
import FilterComponent, { IFilterProps } from './filter.component'

interface IProps {
    title?: string
    tabs?: React.ReactNode
    filter?: IFilterProps
    classname?: string
}

class SectionComponent extends React.Component<IProps, {}> {
    render() {
        let {children, title, tabs, filter, classname} = this.props
        return (
            <div className={`section ${classname ? classname: ''}`}>
                <div className="section__wrap">
                    { tabs || filter ?
                        <div className="section__header">
                            { tabs && <div className="section__tabs">{tabs}</div> }
                            { filter ? <FilterComponent {...filter} classname="filter--dark"/> : ''}
                        </div>
                    : '' }
                    { title ? <div className="section__title">{title}</div> : ''}
                    <div className="section__content">{children}</div>
                </div>
            </div>
        )
    }
}

export default SectionComponent
