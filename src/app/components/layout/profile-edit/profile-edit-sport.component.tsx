import * as React from 'react'
import * as _ from "lodash"
import { connect } from 'react-redux'
import { reqGet } from '../../../core/api'
import { Sport } from '../../../models/sport'
import { GameLevel } from '../../../models/game-level'
import { update_sports } from '../../../services/profile.service'

import IconComponent, { IconList } from '../../blocks/icon/icon.component'
import SelectComponent, { ISelectItem } from '../../blocks/select/select.component'
import AddingSelectControl from '../../form/adding-select-control'
import InputControl from '../../form/input-control'

interface IProps {
    user_sports: Sport[]
    onSubmit?: () => void
    gameLevels: GameLevel[]
}

interface IState {
    popups: {
        levelPopupHide: boolean
        duratonPopupHide: boolean
        selectSportsHide: boolean
        [x: string]: boolean
    }
    sports: any[]
    user_sports: Sport[]
    busy?: boolean
    changed: boolean
}

export interface ISportSelected {
    icon: IconList
    name: string
    level: string
    duration: string
    count: number
}

class Item {
    data: any
    items: Item[]

    constructor(obj) {
        this.data = obj
        this.items = []
    }
}

class ProfileEditSportComponent extends React.Component<IProps, IState> {
    popupParamIndex: number

    constructor(props) {
        super(props)

        this.state = {
            popups: {
                levelPopupHide: true,
                duratonPopupHide: true,
                selectSportsHide: true
            },
            sports: [],
            user_sports: this.cloneModel(props.user_sports),
            busy: false,
            changed: false
        }

        this.popupParamIndex = -1

        this.getSports()
    }

    get gameLevels(): ISelectItem[] {
        const { gameLevels } = this.props
        return gameLevels.map(gameLevel => ({ label: gameLevel.name, value: gameLevel.id }))
    }

    get gameExps(): ISelectItem[] {
        return Sport.game_exps.map(({ label, value }) => ({ label, value }))
    }

    cloneModel = (model) => {
        return _.cloneDeep(model)
    }

    submit = () => {
        const { user_sports } = this.state
        for (const user_sport of user_sports){
            if (!(user_sport.game_exp && user_sport.game_level_id && user_sport.games_count)){
                return
            }
        }
        this.setState({ busy: true })

        update_sports(user_sports)
        .then(this.callOnSubmit)
        .then(() => this.setState({ changed: false }))
        .catch((err) => console.log(err.message))
        .finally(() => this.setState({ busy: false }))
    }

    callOnSubmit = () => {
        const { onSubmit } = this.props
        if (typeof onSubmit == 'function') {
            onSubmit()
        }
    }

    handleChange = (i: number, name: string) => (value: string) => {
        if (!/^\d*$/.test(value))
            return false

        let { user_sports } = this.state
        if (user_sports[i][name] != value) {
            user_sports[i][name] = value
            this.setState({ user_sports, changed: true })
        }
    }

    remove = (i: number) => (e: any) => {
        let { user_sports } = this.state
        user_sports.splice(i, 1)

        this.setState({ user_sports, changed: true })
    }

    getSports = async () => {
        try {
            const response = await reqGet('/sports')
            const sportsRoot = this.compileSportsToTree(response.data.map(el => new Sport(el)))
            const sports = this.getItems(sportsRoot, 0)
            this.setState({ sports })
        } catch(e) {
            console.log(e.message)
        }
    }

    getItems(item: Item, lvl: number) {
        let items = []

        if(item.data) {
            items.push(this.getOption(item, lvl))
        }

        if(Array.isArray(item.items)) {
            items = items.concat(item.items.reduce((acc, nextItem) => acc.concat(this.getItems(nextItem, lvl + 1)), []))
        }

        return items
    }

    getOption(item: Item, lvl: number) {
        const spacing = "  "
        return { label: `${spacing.repeat(lvl-1)}${item.data.name}`, value: item.data.id, obj: item.data }
    }

    compileSportsToTree = (sports: Sport[]) => {
        let itemsByLabel = {}
        let rootItem = new Item(null)
        for (let sport of sports) {
            var item = new Item(sport)

            if (itemsByLabel.hasOwnProperty(sport.label)) {
                if (Array.isArray(itemsByLabel[sport.label])) {
                    item.items = itemsByLabel[sport.label]
                } else {
                    throw new Error()
                }
            }

            itemsByLabel[sport.label] = item

            var arrayLabel = sport.label.split('.')
            if (arrayLabel.length === 1) {
                rootItem.items.push(item)
            } else {
                var parentLabel = arrayLabel.slice(0, arrayLabel.length - 1).join('.')
                if (itemsByLabel.hasOwnProperty(parentLabel)) {
                    if (Array.isArray(itemsByLabel[parentLabel])) {
                        itemsByLabel[parentLabel].push(item)
                    } else {
                        itemsByLabel[parentLabel].items.push(item)
                    }
                } else {
                    itemsByLabel[parentLabel] = [item]
                }
            }
        }

        return rootItem
    }

    get sports(): any[] {
        let { sports } = this.state
        const { user_sports } = this.props
        sports.forEach(sport => sport.disabled = user_sports.map(us => us.id).includes(sport.value))
        return sports
    }

    componentWillUnmount() {
        let profileEditElem = document.querySelector('.profileEdit')
        if (profileEditElem) {
            profileEditElem.classList.remove('no-scroll-mobile')
        }
    }

    toggleLevelPopup = (index: number) => () => {
        this.toggleSelectPopup('levelPopupHide', index)
    }

    toggleDurationPopup = (index: number) => () => {
        this.toggleSelectPopup('duratonPopupHide', index)
    }

    toggleSportsPopup = () => {
        this.toggleSelectPopup('selectSportsHide', -1)
    }

    hideSelectPopup = () => {
        let profileEditElem = document.querySelector('.profileEdit')
        this.popupParamIndex = -1
        if (profileEditElem) {
            profileEditElem.classList.remove('no-scroll-mobile')
        }
    }

    toggleSelectPopup(key: string, index: number) {
        let { popups } = this.state
        let hide = popups[key]
        let profileEditElem = document.querySelector('.profileEdit')
        if (hide) {
            this.popupParamIndex = index
            if (profileEditElem) {
                profileEditElem.classList.add('no-scroll-mobile')
            }
        } else {
            this.popupParamIndex = -1
            if (profileEditElem) {
                profileEditElem.classList.remove('no-scroll-mobile')
            }
        }

        popups[key] = !hide
        this.setState({ popups })
    }

    onAdd = (el) => {
        let { user_sports } = this.state
        const user_sport = new Sport(el.obj)
        user_sports.unshift(user_sport)
        this.setState({ user_sports, changed: true })
        this.hideSelectPopup()
    }

    get valid() {
        const { user_sports } = this.state
        return user_sports.reduce((arr, us) => (arr && us.game_level_id != null && us.game_exp != null && us.games_count != null), true)
    }

    render() {
        const { user_sports, busy, changed } = this.state
        const { levelPopupHide, duratonPopupHide, selectSportsHide } = this.state.popups
        return (
            <div className="profileEdit__wrap">
                <div className="profileEdit__title">Спорт</div>
                <div className={`profileSportSelect ${!selectSportsHide ? '--show' : ''}`}>
                    <AddingSelectControl
                        onAdd={this.onAdd}
                        options={this.sports}
                        onCross={this.toggleSportsPopup}
                    />
                </div>
                { user_sports ?
                    <div className="profileSports">
                        <div className="_header">
                            <div className="_paramTit">Уровень игры</div>
                            <div className="_paramTit">Стаж игры</div>
                            <div className="_paramTit">Число игр</div>
                        </div>
                        { user_sports.map((el, i) => {return(
                            <div className="_item" key={i}>
                                <div className="_name">
                                    {/*<IconComponent icon={el.icon} width="24px" height="23px" fill="#414658"/>*/}
                                    <span>{el.name}</span>
                                </div>
                                <div className={`_param ${!levelPopupHide && i === this.popupParamIndex ? '_paramPopup' : ''}`}>
                                    <div className="_paramTit">Уровень игры</div>
                                    <div className="_select">
                                        <SelectComponent
                                            list={this.gameLevels}
                                            value={el.game_level_id}
                                            hideArr
                                            theme="light"
                                            onSelect={this.toggleLevelPopup(i)}
                                            onChange={this.handleChange(i, 'game_level_id')}
                                        />
                                        <div className="_paramPopup__toggle" onClick={this.toggleLevelPopup(i)}>
                                            <IconComponent icon={IconList.cross}/>
                                        </div>
                                    </div>
                                </div>
                                <div className={`_param ${!duratonPopupHide && i === this.popupParamIndex ? '_paramPopup' : ''}`}>
                                    <div className="_paramTit">Стаж игры</div>
                                    <div className="_select">
                                        <SelectComponent
                                            list={this.gameExps}
                                            value={el.game_exp}
                                            hideArr
                                            theme="light"
                                            onSelect={this.toggleDurationPopup(i)}
                                            onChange={this.handleChange(i, 'game_exp')}
                                        />
                                        <div className="_paramPopup__toggle" onClick={this.toggleDurationPopup(i)}>
                                            <IconComponent icon={IconList.cross}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="_param">
                                    <div className="_paramTit">Число игр</div>
                                    <InputControl type="text" className="_input" value={el.games_count} onChange={this.handleChange(i, 'games_count')} />
                                </div>
                                <div className="_cross">
                                    <button type="button" onClick={this.remove(i)}>
                                        <IconComponent icon={IconList.cross} width="12px" height="12px" fill="#C4C4C4"/>
                                    </button>
                                </div>
                            </div>
                        )})}
                        <div className="_footer desktopProfileHide">
                            <div
                                className="_afterLink"
                                onClick={this.toggleSportsPopup}
                            >Добавить спорт</div>
                        </div>
                    </div>
                : ''}
                <button type="button" className="profileEdit__btn profileEdit__btn--green" onClick={this.submit} disabled={busy || !changed || !this.valid}>Сохранить</button>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user_sports: state.authState.user.sports,
    gameLevels: state.gameLevelsState.gameLevels
})

export default connect(mapStateToProps)(ProfileEditSportComponent)
