import * as React from 'react'
import IconComponent from '../../blocks/icon/icon.component'
import { updateAvatar } from '../../../services/profile.service'
import ReactCrop, {getPixelCrop, PixelCrop} from 'react-image-crop'
//import 'react-image-crop/lib/ReactCrop.scss'

interface IProps {
    src?: string
    onSubmit?: () => void
}

interface IState {
    src: string | ArrayBuffer,
    srcExt: string | ArrayBuffer,
    crop: any,
    busy?: boolean
}

class ProfileEditAvatarComponent extends React.Component<IProps, IState> {

    input: React.RefObject<HTMLInputElement>
    imagePreviewCanvasRef: React.RefObject<HTMLCanvasElement>
    imagePreviewCanvasRefMain: React.RefObject<HTMLCanvasElement>

    iWidth = 33.2
    iHeight = 39.6
    iAspect = 332/396
    iX = 35
    iY = 35

    initialCrop = {
        x: this.iX,
        y: this.iY,
        width: this.iWidth,
        aspect: this.iAspect,
    }

    initialPreviewCrop = {
        x: this.iX,
        y: this.iY,
        width: this.iWidth,
        height: this.iHeight,
        aspect: this.iAspect,
    }

    constructor(props) {
        super(props)

        this.state = {
            src: props.src || '',
            srcExt: "",
            crop: this.initialCrop,
            busy: false
        }

        this.input = React.createRef()
        this.imagePreviewCanvasRef = React.createRef()
        this.imagePreviewCanvasRefMain = React.createRef()

        this.handleImageLoad = this.handleImageLoad.bind(this)
        this.handleOnCrop = this.handleOnCrop.bind(this)
        this.handleOnCropComplete = this.handleOnCropComplete.bind(this)
    }

    onChange(e) {
        const file = e.target.files[0],
            reader = new FileReader()
        reader.onloadend = () => {
            const myResult = reader.result
            this.setState({
                src: myResult,
                srcExt: this.extractImageFileExtensionFromBase64(myResult)
            })
        }
        file && reader.readAsDataURL(file)
    }

    clearUpload() {
        this.input.current.value = null
        this.setState({src: ''})
    }

    downloadImage = async (e) => {
        let { src, srcExt } = this.state
        if (src) {
            let canvasRef = this.imagePreviewCanvasRefMain.current
            let imageData64 = canvasRef.toDataURL('image/' + srcExt)
            let myFilename = "previewFile." + srcExt
            let file = this.base64StringtoFile(imageData64, myFilename)
            if (file) {
                this.setState({ busy: true })

                updateAvatar(file)
                .then(this.callOnSubmit)
                .catch((err) => console.log(err.message))
                .finally(() => this.setState({ busy: false }))
            }
        }
    }

    callOnSubmit = () => {
        const { onSubmit } = this.props
        if (typeof onSubmit == 'function') {
            onSubmit()
        }
    }

    handleImageLoad(image) {
        let src = this.state.src.toString()
        let canvasRef = this.imagePreviewCanvasRef.current
        let canvasRefMain = this.imagePreviewCanvasRefMain.current
        let initalPixelCrop = getPixelCrop(image, this.initialPreviewCrop)

        this.image64toCanvasRef(canvasRef, src, initalPixelCrop)
        this.image64toCanvasRef(canvasRefMain, src, initalPixelCrop, false)
    }

    handleOnCrop(crop, pixelCrop) {
        this.setState({crop: crop})
    }

    handleOnCropComplete(crop, pixelCrop) {
        let src = this.state.src.toString()
        let canvasRef = this.imagePreviewCanvasRef.current
        let canvasRefMain = this.imagePreviewCanvasRefMain.current

        this.image64toCanvasRef(canvasRef, src, pixelCrop)
        this.image64toCanvasRef(canvasRefMain, src, pixelCrop, false)
    }

    getCropdata(pixelCrop, mini = true){
        let { x, y, width, height } = pixelCrop
        if (mini) {
            return {
                x, width, height,
                y: y + (height - width) / 2,
                sheight: width,
            }
        } else {
            return {
                x, y, width, height,
                sheight: height,
            }
        }
    }

    base64StringtoFile(base64String, filename) {
        let arr = base64String.split(',')
        let mime = arr[0].match(/:(.*?)/)[1]
        let bstr = atob(arr[1])
        let n = bstr.length
        let u8arr = new Uint8Array(n)
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n)
        }
        return new File([u8arr], filename, {type: mime})
    }

    extractImageFileExtensionFromBase64(base64Data) {
        return base64Data.substring('data:image/'.length, base64Data.indexOf('base64'))
    }

    image64toCanvasRef(canvasRef, image64, pixelCrop, mini = true) {
        let canvas = canvasRef
        canvas.width = pixelCrop.width
        canvas.height = pixelCrop.height
        let ctx = canvas.getContext('2d')
        let image = new Image()
        image.src = image64
        let cropData = this.getCropdata(pixelCrop, mini)
        image.onload = () => {
            ctx.drawImage(
                image,
                cropData.x,
                cropData.y,
                cropData.width,
                cropData.sheight,
                0,
                0,
                cropData.width,
                cropData.height
            )
        }
    }

    render() {
        const { src, crop, busy } = this.state
        return (
            <div className="profileEdit__wrap">
                <div className="profileEdit__title">загрузка фотографии</div>
                <div className="profileEditAvatar">
                    {(!src || src === '#') ?
                        <label className="_preview" htmlFor="avatarInput">
                            <div className="_icon">
                                <IconComponent icon="upload" fill="#2A83FB"/>
                            </div>
                            <div className="_text">Друзьям будет проще узнать Вас, если<br/>Вы загрузите свою настоящую фотографию.<br/>Вы можете загрузить изображение в формате<br/>JPG, GIF или PNG.</div>
                        </label>
                    :
                        <div className="_present">
                            <div className="_preview _preview--small">
                                <canvas className={"_preview-canvas"} ref={this.imagePreviewCanvasRef}></canvas>
                            </div>
                            <div className="_preview _preview--img">
                                <ReactCrop 
                                    src={src.toString()}
                                    crop={crop}
                                    onImageLoaded={this.handleImageLoad}
                                    onChange={this.handleOnCrop}
                                    onComplete={this.handleOnCropComplete}
                                />
                            </div>
                            <div className="_cross" onClick={() => this.clearUpload()}>
                                <IconComponent icon="cross"/>
                            </div>
                        </div>

                    }
                    <input
                        ref={this.input}
                        id="avatarInput"
                        type="file"
                        className="_input"
                        onChange={(e) => this.onChange(e)}
                        disabled={busy}
                    />
                    <div className="_btns">
                        <label className={`profileEdit__btn profileEdit__btn--darkBlue${busy ? ' disabled' : ''}`} htmlFor="avatarInput">Выбрать файл</label>
                        <button className="profileEdit__btn profileEdit__btn--green" onClick={this.downloadImage} disabled={busy}>Сохранить</button>
                    </div>
                </div>
                <canvas style={{display:"none"}} ref={this.imagePreviewCanvasRefMain}></canvas>
            </div>
        )
    }
}

export default ProfileEditAvatarComponent
