import * as React from 'react'
import { connect } from 'react-redux'
import { reqGet } from '../../../core/api'

import { update } from '../../../services/profile.service'

import ValidatesForm, { ValidatesFormProps, ValidatesFormState } from '../../form/validates-form'
import InputControl from '../../form/input-control'
import PhoneControl from '../../form/phone-control'
import SelectControl from '../../form/select-control'
import RadioControl from '../../form/radio-control'
import InputDatepicker from '../../blocks/datepicker/input.datepicker'

interface ProfileEditMainComponentState extends ValidatesFormState {
    cities: { [x: number]: string }
}

class ProfileEditMainComponent extends ValidatesForm<ValidatesFormProps, ProfileEditMainComponentState> {
    
    constructor(props) {
        super(props)
        this.state.cities = {}
        this.getCities()
    }
    
    getCities = async () => {
        try {
            const response = await reqGet('/cities')
            let cities = {}
            response.data.forEach(city => {
                cities[city.id] = city.name
            })
            this.setState({ cities })
        } catch(e) {
            console.log(e.message)
        }
    }

    commonValidationConfig = {
        requiredAtLeast: [
            {
                field: 'email',
                label: 'email'
            },
            {
                field: 'phone',
                label: 'телефон'
            }
        ],
        equalFields: [
            {
                field: 'newPassword',
                label: 'Новый пароль'
            },
            {
                field: 'confirmedPassword',
                label: 'подтверждение пароля'
            }
        ]
    }

    validationConfig = {
        name: {
            present: true
        },

        phone: {
            phone: true
        },

        email: {
            email: true
        },

        newPassword: {
            requiredWith: ['oldPassword']
        },
        confirmedPassword: {
            requiredWith: ['oldPassword']
        }
    }

    onSubmit = (e: any) => {
        if(!!e) {
            e.preventDefault()
        }


        if(this.validateWholeForm()) {
            this.setBusy()
            const { item } = this.state

            update(item)
            .then(this.handleSubmit)
            .catch(this.handleErrors)
            .finally(this.unsetBusy)
        }
    }

    inputClasses = (name: string) => {
        return `_input ${ this.isValid(name) ? '' : '--error'}`
    }

    render() {
        const {cities, item, commonErrors, busy, old_password, new_password, renew_password} = this.state
        return (
            <form onSubmit={this.onSubmit}>
                <div className="profileEdit__wrap">
                    {item.tmp &&
                        <div className="profileEdit__announcement">СМЕНИТЕ ПАРОЛЬ</div>
                    }
                    <div className="profileEdit__title">Основное</div>
                    <div className="profileEdit__form">
                        <div className="profileInput">
                            <span className="_label">Имя</span>
                            <div className={this.inputClasses('name')}>
                                <InputControl
                                    type="text"
                                    value={item.name}
                                    onChange={this.handleChange('name')}
                                    disabled={busy}
                                />
                                <span className="_error-message">{this.errorMessage('name')}</span>
                            </div>
                        </div>
                        <div className="profileInput">
                            <span className="_label">Фамилия</span>
                            <div className={this.inputClasses('lastName')}>
                                <InputControl
                                    type="text"
                                    value={item.lastName}
                                    onChange={this.handleChange('lastName')}
                                    disabled={busy}
                                />
                                <span className="_error-message">{this.errorMessage('lastName')}</span>
                            </div>
                        </div>
                        <div className="profileInput">
                            <span className="_label">Отчество</span>
                            <div className={this.inputClasses('midName')}>
                                <InputControl
                                    type="text"
                                    value={item.midName}
                                    onChange={this.handleChange('midName')}
                                    disabled={busy}
                                />
                                <span className="_error-message">{this.errorMessage('midName')}</span>
                            </div>
                        </div>
                        <div className="profileInput">
                            <span className="_label">Дата рождения</span>
                            <InputDatepicker
                                date={item.birthDate}
                                onChange={this.handleChange('birthDate')}
                                errMsg={this.errorMessage('birthDate')}
                                format="DD-MM-YYYY"
                            />
                        </div>
                        <div className="profileInput">
                            <span className="_label">Город</span>
                            <div className={this.inputClasses('city_id')}>
                                <SelectControl
                                    value={item.city_id}
                                    onChange={this.handleChange('city_id')}
                                    disabled={busy}
                                    options={cities}
                                />
                            </div>
                        </div>
                        <div className="profileInput">
                            <span className="_label">О себе</span>
                            <div className={this.inputClasses('about')}>
                                <InputControl
                                    value={item.about}
                                    onChange={this.handleChange('about')}
                                    multiline
                                    disabled={busy}
                                />
                                <span className="_error-message">{this.errorMessage('about')}</span>
                            </div>
                        </div>
                    </div>
                    <div className="profileEdit__title">Пароль</div>
                    <div className="profileEdit__form">
                        <div className="profileInput">
                            <span className="_label">Старый пароль</span>
                            <div className={this.inputClasses('oldPassword')}>
                                <InputControl
                                    type="password"
                                    value={item.oldPassword}
                                    onChange={this.handleChange('oldPassword')}
                                    disabled={busy}
                                />
                                <span className="_error-message">{this.errorMessage('oldPassword')}</span>
                            </div>
                        </div>
                        <div className="profileInput">
                            <span className="_label">Новый пароль</span>
                            <div className={this.inputClasses('newPassword')}>
                                <InputControl
                                    type="password"
                                    value={item.newPassword}
                                    onChange={this.handleChange('newPassword')}
                                    disabled={busy}
                                />
                                <span className="_error-message">{this.errorMessage('newPassword')}</span>
                            </div>
                        </div>
                        <div className="profileInput">
                            <span className="_label">Повторите пароль</span>
                            <div className={this.inputClasses('confirmedPassword')}>
                                <InputControl
                                    type="password"
                                    value={item.confirmedPassword}
                                    onChange={this.handleChange('confirmedPassword')}
                                    disabled={busy}
                                />
                                <span className="_error-message">{this.errorMessage('confirmedPassword')}</span>
                            </div>
                        </div>

                    </div>
                    <div className="profileEdit__title">Контакты</div>
                    <div className="profileEdit__form">
                        <div className="profileInput">
                            <span className="_label">Email</span>
                            <div className={this.inputClasses('email')}>
                                <InputControl
                                    type="text"
                                    value={item.email}
                                    onChange={this.handleChange('email')}
                                    disabled={busy}
                                />
                                <span className="_error-message">{this.errorMessage('email')}</span>
                            </div>
                        </div>
                        <div className="profileInput">
                            <span className="_label">Телефон</span>
                            <div className={this.inputClasses('phone')}>
                                <PhoneControl
                                    type="text"
                                    placeholder="Телефон"
                                    value={item.phone}
                                    onChange={this.handleChange('phone')}
                                    disabled={busy}
                                />
                                <span className="_error-message">{this.errorMessage('phone')}</span>
                            </div>
                        </div>
                    </div>
                    <div className="profileEdit__title">Данные</div>
                    <div className="profileEdit__form">
                        <div className="profileInput">
                            <span className="_label">Рост</span>
                            <div className={this.inputClasses('height')}>
                                <InputControl
                                    type="number"
                                    value={item.height}
                                    onChange={this.handleChange('height')}
                                    disabled={busy}
                                />
                                <span className="_error-message">{this.errorMessage('height')}</span>
                            </div>
                        </div>
                        <div className="profileInput">
                            <span className="_label">Вес</span>
                            <div className={this.inputClasses('weight')}>
                                <InputControl
                                    type="number"
                                    value={item.weight}
                                    onChange={this.handleChange('weight')}
                                    disabled={busy}
                                />
                                <span className="_error-message">{this.errorMessage('weight')}</span>
                            </div>
                        </div>
                        <div className="profileInput">
                            <span className="_label">Пол</span>
                            <div className="_checkRadio">
                                <RadioControl
                                    options={{ 0: 'Мужчина', 1: 'Женщина' }}
                                    value={item.gender}
                                    onChange={this.handleChange('gender')}
                                    disabled={busy}
                                />
                                <span className="_error-message">{this.errorMessage('gender')}</span>
                            </div>
                        </div>
                    </div>
                    <div className="_error-message">{commonErrors}</div>
                    <div className="profileEdit__form">
                        <button className="profileEdit__btn profileEdit__btn--green" type="submit" disabled={busy}>
                            Сохранить
                        </button>
                    </div>
                </div>
            </form>
        )
    }
}

const mapStateToProps = state => ({
    item: state.authState.user
})

export default connect(mapStateToProps)(ProfileEditMainComponent)
