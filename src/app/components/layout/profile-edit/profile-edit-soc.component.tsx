import * as React from 'react'
import { connect } from 'react-redux'
import IconComponent, { IconList } from '../../blocks/icon/icon.component'
import config from '../../../core/config'
import ErrorBlockComponent from '../../blocks/error-block.component'
import ExternalLink from '../../blocks/external-link'

enum SocStateList {
    tied = 'tied',
    totie = 'totie',
    tountie = 'tountie',
}

interface IProps {
    user: any
}

class ProfileEditSocComponent extends React.Component<IProps, {}> {
    get searchParams() {
        const url = new URL(window.location.href)
        const searchParams = new URLSearchParams(url.search)
        return searchParams
    }

    getTitleBtn(key: SocStateList, provider: string): React.ReactFragment {
        const { api_host } = config
        switch (key) {
            case SocStateList.tied:
                return <div className={`_btn _btn--${SocStateList.tied}`}>Привязано</div>
            case SocStateList.totie:
                return <ExternalLink className={`_btn _btn--${SocStateList.totie}`} href={`${api_host}/auth/social/${provider}?target=${window.location.href}`}>Привязать</ExternalLink>
            case SocStateList.tountie:
                return <ExternalLink className={`_btn _btn--${SocStateList.tountie}`} href={`${api_host}/auth/social/${provider}/unlink`}>Отвязать</ExternalLink>
            default:
                return ''
        }
    }

    hasSoc(provider: string) {
        const { social_accounts } = this.props.user
        if (!!social_accounts) {
            const sa_index = social_accounts.findIndex(sa => sa.provider == provider)
            return sa_index > -1
        } else {
            return false
        }
    }

    getButton(provider: string) {
        return this.hasSoc(provider) ? this.getTitleBtn(SocStateList.tountie, provider) : this.getTitleBtn(SocStateList.totie, provider)
    }

    render() {
        return (
            <React.Fragment>
                <div className="profileEdit__wrap">
                    <div className="profileEdit__title">Социальные сети</div>
                    <div className="profileInputSoc">
                        <div className="_item">
                            <div className="_icon bg-facebook">
                                <IconComponent icon={IconList.sqFacebook}/>
                            </div>
                            <div className="_title">Facebook</div>
                            {this.getButton('facebook')}
                        </div>
                        <div className="_item">
                            <div className="_icon bg-odnoklassniki">
                                <IconComponent icon={IconList.sqOdnoklassniki}/>
                            </div>
                            <div className="_title">Одноклассники</div>
                            {this.getButton('odnoklassniki')}
                        </div>
                        <div className="_item">
                            <div className="_icon bg-mailru">
                                <IconComponent icon={IconList.sqMailru}/>
                            </div>
                            <div className="_title">Mail.ru</div>
                            {this.getButton('mailru')}
                        </div>
                        <div className="_item">
                            <div className="_icon bg-vkontakte">
                                <IconComponent icon={IconList.sqVkontakte}/>
                            </div>
                            <div className="_title">ВКонтакте</div>
                            {this.getButton('vkontakte')}
                        </div>
                    { this.searchParams.get('error') ?
                        <ErrorBlockComponent
                            className="_errorBlock"
                            title="Не удалось привязать соц. сеть"
                            text={this.searchParams.get('error')}
                            /*
                            list={[
                                'Решение проблемы',
                                'Решение проблемы описание',
                                'Решение проблемы',
                            ]}
                            */
                        />
                     : '' }
                    </div>
                </div>
             </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    user: state.authState.user
})

export default connect(mapStateToProps)(ProfileEditSocComponent)
