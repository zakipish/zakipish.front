import * as React from 'react'

interface IProps {
    url?: string
    urls?: string[]

    active?: number
    onSelect?: (i: number) => void
    
    children: any
}

interface IState {
    styleAct: {
        left: string
        width: string
    }
}

class TabLinksComponent extends React.Component<IProps, IState> {

    tabsElem: React.RefObject<HTMLDivElement> = React.createRef()
    tabs: NodeListOf<Element>

    state = {
        styleAct: {
            left: '0px',
            width: '0px',
        }
    }

    componentDidMount() {
        let { url, urls, active } = this.props
        this.tabs = this.tabsElem.current.querySelectorAll('.tablinks__item')
        setTimeout(() => {
            if (url && urls) { 
                let linkActive = this.getLinkAct()
                if (linkActive !== active) {
                    this.onSelect(linkActive)
                    return true
                }
            }
            this.setActStyle(active)
        }, 100)
    }

    onSelect(i: number) {
        this.setActStyle(i)
        this.props.onSelect(i)
    }

    getLinkAct() {
        let actIndex = 0
        let { url, urls } = this.props
        urls.forEach((el, i) => {
            if (el === url) {
                actIndex = i
            }
        })
        return actIndex
    }

    setActStyle(actIndex) {
        let offset = 40 
        let left = 0 
        let width = 0 
        let act = true
        this.tabs.forEach((el, i) => {
            if(act) {
                if (i > 0) {
                    left += this.tabs[i - 1].clientWidth + offset
                }
                if (i === actIndex) {
                    width = el.clientWidth
                    act = false
                }
            }
        });
        let styleAct = { left: left + 'px', width: width + 'px' }
        this.setState({styleAct})
    }

    render() {
        let { children, active } = this.props
        let { styleAct } = this.state
        if (children && !Array.isArray(children)) {
            children = [children]
        }
        return (
            <div className="tablinks" ref={this.tabsElem}>
                <div className="tablinks__scroll">
                    <div className="tablinks__act" style={styleAct}></div>
                    { children && children.map((el, i) => { return(
                        <div
                            key={i}
                            className={`tablinks__item ${active == i ? 'active' : ''}`}
                            onClick={() => this.onSelect(i)}
                        >{el}</div>
                    )}) }
                </div>
            </div>
        )
    }
}

export default TabLinksComponent
