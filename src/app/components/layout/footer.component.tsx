import * as React from 'react'
import ExternalLink from '../blocks/external-link'

interface IProps {}

class FooterComponent extends React.Component<IProps, {}> {
    render() {
        return (
            <div className="footer">
                <div className="footer__wrap">
                    <div className="footer__logo">
                        <img src="/images/footer-logo.svg" alt="#"/>
                    </div>
                    <div className="footer__nav">
                        <ExternalLink
                            className="footer__navItem"
                            href="http://clubs.zakipish.ru"
                            target="_blank"
                        >Клубам</ExternalLink>
                    </div>
                    <div className="footer__copy">© 2019 Все права защищены. Используя сайт, вы принимаете условия <a href="https://clubs.zakipish.ru/doc/soglashenie-s-polzovatelem">Пользовательского соглагения</a></div>
                </div>
            </div>
        )
    }
}

export default FooterComponent
