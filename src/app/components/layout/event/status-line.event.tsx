import * as React from 'react'

interface Props {
    list: Array<{
        tit: string
        val: string
    }>
}

class StatusLineEvent extends React.Component<Props, {}> {
    render() {
        let { list } = this.props
        return (
            <div className="eventStateLine">
                {list.map((el, i) =>
                    <div className="eventStateLine__item" key={i}>
                        <div className="eventStateLine__val">{el.val}</div>
                        <div className="eventStateLine__tit">{el.tit}</div>
                    </div>
                )}
            </div>
        )
    }
}

export default StatusLineEvent