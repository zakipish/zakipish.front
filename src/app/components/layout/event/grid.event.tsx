import * as React from 'react'
import SwipeDetect from '../../../core/SwipeDetect'

interface Props {
    className?: string
    style?: React.CSSProperties
    mobileSwipe?: boolean
}

class GridEvent extends React.Component<Props, {}> {

    static defaultProps = {
        className: '',
        mobileSwipe: false,
    }

    refGrid: React.RefObject<HTMLDivElement> = React.createRef()
    refWrap: React.RefObject<HTMLDivElement> = React.createRef()
    swipedetect
    slideIndex: number = 0

    componentDidMount() {
        let { mobileSwipe } = this.props
        let refGrid = this.refGrid.current
        if (!refGrid || !mobileSwipe) return
        this.swipedetect = new SwipeDetect(refGrid, this.onSwipe)
    }

    componentWillUnmount() {
        if(this.swipedetect) { 
            this.swipedetect.detstroy()
        }
    }

    onSwipe = (dir) => {
        if (dir === 'right') {
            this.setSlide(-1)
        } else if (dir === 'left') {
            this.setSlide(1)
        }
    }

    setSlide = (addIndex) => {
        let refWrap = this.refWrap.current
        if (!refWrap) return
        let { children } = this.props
        let slideIndex = this.slideIndex
        let len = Array.isArray(children) ? children.length - 1 : 0
        slideIndex += addIndex
        if (slideIndex > len) {
            slideIndex = 0
        } else if (slideIndex < 0) {
            slideIndex = len
        }
        let offset = window.innerWidth <= 640 ? 10 : 20
        refWrap.style.transform = `translateX(calc((-100vw + ${offset}px) * ${slideIndex}))`
        this.slideIndex = slideIndex
        this.forceUpdate()
    }

    renderDots = () => {
        let { children } = this.props
        let lenDot = Array.isArray(children) ? children.length - 1 : 0
        let dots: React.ReactNodeArray = []
        for (let i = 0; i < lenDot; i++) {
            let className = i === this.slideIndex ? 'act' : ''
            dots.push(<i key={i} className={className}></i>)
        }
        return (
            <div className="eventGrid__dots">
                {dots}
            </div>
        )
    }

    render() {
        let { children, mobileSwipe, className, style } = this.props
        let childrenLen = Array.isArray(children) ? children.length : 1

        return (
            <div
                className={`eventGrid ${className} ${mobileSwipe ? '--mobileSwipe' : '' }`}
                style={style}
                ref={this.refGrid}
            >
                <div
                    className="eventGrid__wrap"
                    ref={this.refWrap}
                >
                    {children}
                </div>
                {(childrenLen > 1 && mobileSwipe) && (
                    this.renderDots()
                )}
            </div>
        )
    }
}

export default GridEvent