import * as React from 'react'
import IconComponent, { IconList } from '../../blocks/icon/icon.component'

export interface EventSlide {
    image: string
    title?: string 
}

interface Props {
    slides: EventSlide[]
}

interface State {}


class SliderEvent extends React.Component<Props, State> {
    
    state = {
        active: 0
    }

    onSlide = (addIndex: number) => {
        let { slides } = this.props
        let lenIndex = slides.length - 1
        let { active } = this.state
        active += addIndex
        if (active < 0) {
            active = lenIndex
        } else if (active > lenIndex) {
            active = 0
        }
        this.setState({active})
    }

    render() {
        let { active } = this.state
        let { slides } = this.props
        return (
            <div className="eventSlider">
                {slides.map((slide, i) => 
                    <div 
                        key={i}
                        className={`eventSlider__item ${i === active ? ' --act' : ''}`}
                        style={{backgroundImage: `url(${slide.image})`}}
                    >
                        <div 
                            className="eventSlider__tit"
                            dangerouslySetInnerHTML={{__html: slide.title}}
                        ></div>
                    </div>
                )}
                {slides.length > 1 && (
                    <div className="eventSlider__control">
                        <IconComponent 
                            className="eventSlider__btn" 
                            icon={IconList.slideLeft}
                            onClick={() => this.onSlide(-1)}
                        />
                        <div className="eventSlider__state">
                            {active + 1} из {slides.length}
                        </div>
                        <IconComponent 
                            className="eventSlider__btn" 
                            icon={IconList.slideRight}
                            onClick={() => this.onSlide(1)}
                        />
                    </div>
                )}
            </div>
        )
    }
}

export default SliderEvent
