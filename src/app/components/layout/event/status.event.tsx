import * as React from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'

import { pluralize } from '../../../core/utils'

interface Props {
    price?: string
    interval?: {
        from: moment.Moment
        to: moment.Moment
    }
    min?: number
    max?: number
    justLen?: number
    likeLen?: number
    party?: Array<{
        image?: string
        text?: string
    }> 
    onRemove?: {
        to?: string
        onClick?: (e?) => void
        order?: number
    }
    onJoin?: {
        to?: string
        onClick?: (e?) => void
        order?: number
    }
    onLike?: {
        to?: string
        onClick?: (e?) => void
        order?: number
    }
    typeParty?: number
    onSetTypeParty?: (i: number) => void
}


class StatusEvent extends React.Component<Props, {}> {

    minPartyLen = 6

    render() {
        let { 
            price, interval, min, max, justLen, likeLen, party,
            onRemove, onJoin, onLike, typeParty, onSetTypeParty 
        } = this.props
        
        let dataStr = interval.from.format('DD MMMM, HH:mm') + interval.to.format(' - HH:mm')
        let diffMin = interval.to.diff(interval.from, 'minute')
        let timeStr = `${Math.floor(diffMin / 60)} ${pluralize(Math.floor(diffMin / 60), ['час', 'часа', 'часов'])}${diffMin % 60 > 0 ? ` ${diffMin % 60} ${pluralize(diffMin % 60, ['минута', 'минуты', 'минут'])}` : ``}`

        let partyAddLen = party ? party.length - this.minPartyLen : 0
        partyAddLen = partyAddLen > 0 ? partyAddLen : 0
        partyAddLen = partyAddLen <= 99 ? partyAddLen : 99

        return (
            <div className="eventState">
                <div className="eventState__row">
                    <div className="eventState__h3">Входной билет</div>
                    <div className="eventState__h1">{price}<i>₽</i></div>
                    <div className="eventState__btns">
                        {onRemove && (
                            <Link 
                                to={onRemove.to || '#'} 
                                className="eventState__btn --orange"
                                onClick={onRemove.onClick}
                                style={onRemove.order > 0 && {order: onRemove.order}}
                            >Отказаться</Link>
                        )}
                        {onJoin && (
                            <Link 
                                to={onJoin.to || '#'} 
                                className="eventState__btn --green"
                                onClick={onJoin.onClick}
                                style={onJoin.order > 0 && {order: onJoin.order}}
                            >Присоединиться</Link>
                        )}
                        {onLike && (
                            <Link 
                                to={onLike.to || '#'} 
                                className="eventState__btn"
                                onClick={onLike.onClick}
                                style={onLike.order > 0 && {order: onLike.order}}
                            >Возможно пойду</Link>
                        )}
                    </div>
                </div>
                <div className="eventState__row">
                    <div className="eventState__h2">{dataStr}</div>
                    <div className="eventState__h3">{interval.from.format('dddd')}</div>
                </div>
                <div className="eventState__row">
                    <div className="eventState__h2">{timeStr}</div>
                    <div className="eventState__h3">Продолжительность</div>
                    {(min >= 0 && max >= 0) && (
                        <React.Fragment>
                            <br/>
                            <div className="eventState__h2">От {min} до {max} человек</div>
                            <div className="eventState__h3">Число участников</div>
                        </React.Fragment>
                    )}
                </div>
                <div className="eventState__row">
                    {(justLen >= 0 && likeLen >= 0) && (
                        <div className="eventState__paramList">
                            {(justLen >= 0) && (
                                <div 
                                    className={`eventState__param ${typeParty === 0 ? ' --blue' : ''}`}
                                    onClick={() => onSetTypeParty(0)}
                                >
                                    <div className="eventState__h2">{justLen}</div>
                                    <div className="eventState__h3">Идут</div>
                                </div>
                            )}
                            {(likeLen >= 0) && (
                                <div 
                                    className={`eventState__param ${typeParty === 1 ? ' --blue' : ''}`}
                                    onClick={() => onSetTypeParty(1)}
                                >
                                    <div className="eventState__h2">{likeLen}</div>
                                    <div className="eventState__h3">Возможно</div>
                                </div>
                            )}
                        </div>
                    )}
                    {party && (
                        <div className="eventState__cirList">
                            {partyAddLen > 0 && (
                                <div className="eventState__cir --add">{partyAddLen}+</div>
                            )}
                            {party.map((el, i) => {
                                if (i < this.minPartyLen) {
                                    return (
                                        <div 
                                            key={i}
                                            className="eventState__cir"
                                            style={{backgroundImage: `url(${el.image || '#'})`}}
                                        >{el.text || ''}</div>
                                    )
                                }
                            })}
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

export default StatusEvent
