import * as React from 'react'
import { Link } from 'react-router-dom';

interface Props {
    vip?: boolean
    image?: string
    title?: string
    price?: string
    btn?: {
        type: string
        to?: string
        onClick?: (e?) => void
    }
}

class EventLabel extends React.Component<Props, {}> {
    render() {
        let { 
            vip, image, title,
            price, btn
        } = this.props
        image = image ? image : '#'
        return (
            <div className="eventLabel">
                {(!price || !btn) ? (
                    <React.Fragment>
                        <div 
                            className="eventLabel__img"
                            style={{backgroundImage: `url(${image})`}}
                        >
                            {vip && (
                                <div 
                                    className="eventLabel__vip"
                                    style={{backgroundImage: `url(/images/vip.png)`}}
                                ></div>
                            )}
                        </div>
                        {title && (
                            <div className="eventLabel__tit">{title}</div>
                        )}
                    </React.Fragment>
                ) : (
                    <React.Fragment>
                        <div className="eventLabel__price">
                            <div className="eventState__h3">Входной билет</div>
                            <div className="eventState__h1">{price}<i>₽</i></div>
                        </div>
                        <div className="eventLabel__btn">
                            {btn.type === 'remove' && (
                                <Link 
                                    to={btn.to || '#'} 
                                    className="eventState__btn --orange"
                                    onClick={btn.onClick}
                                >Отказаться</Link>
                            )}
                            {btn.type === 'join' && (
                                <Link 
                                    to={btn.to || '#'} 
                                    className="eventState__btn --green"
                                    onClick={btn.onClick}
                                >Присоединиться</Link>
                            )}
                            {btn.type === 'like' && (
                                <Link 
                                    to={btn.to || '#'} 
                                    className="eventState__btn"
                                    onClick={btn.onClick}
                                >Возможно пойду</Link>
                            )}
                        </div>
                    </React.Fragment>
                )}
            </div>
        )
    }
}

export default EventLabel
