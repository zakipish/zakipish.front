import * as React from 'react'
import { Link } from 'react-router-dom'

interface Props {
    title: string
    subtit: string
    party?: Array<{
        image?: string
        text?: string
    }> 
    btn?: {
        type: string
        to?: string
        onClick?: (e?) => void
    }
}

class ActionLineEvent extends React.Component<Props, {}> {

    minPartyLen = 6

    render() {
        let { title, subtit, party, btn } = this.props
        let partyAddLen = party ? party.length - this.minPartyLen : 0
        partyAddLen = partyAddLen > 0 ? partyAddLen : 0
        partyAddLen = partyAddLen <= 99 ? partyAddLen : 99
        return (
            <div className="eventActionLine">
                {title && <div className="eventActionLine__title">{title}</div>}
                {subtit && <div className="eventActionLine__subtitle">{subtit}</div>}
                <div className="eventActionLine__body">
                    {party && (
                        <div className="eventActionLine__cirList">
                            {partyAddLen > 0 && (
                                <div className="eventActionLine__cir">{partyAddLen}+</div>
                            )}
                            {party.map((el, i) => {
                                if (i < this.minPartyLen) {
                                    return (
                                        <div 
                                            key={i}
                                            className="eventActionLine__cir"
                                            style={{backgroundImage: `url(${el.image || '#'})`}}
                                        >{el.text || ''}</div>
                                    )
                                }
                            })}
                        </div>
                    )}
                    {btn && (
                        <React.Fragment>
                            {btn.type === 'remove' && (
                                <Link 
                                    to={btn.to || '#'} 
                                    className="eventActionLine__btn --orange"
                                    onClick={btn.onClick}
                                >Отказаться</Link>
                            )}
                            {btn.type === 'join' && (
                                <Link 
                                    to={btn.to || '#'} 
                                    className="eventActionLine__btn --green"
                                    onClick={btn.onClick}
                                >Присоединиться</Link>
                            )}
                            {btn.type === 'like' && (
                                <Link 
                                    to={btn.to || '#'} 
                                    className="eventActionLine__btn"
                                    onClick={btn.onClick}
                                >Возможно пойду</Link>
                            )}
                        </React.Fragment>
                    )}
                </div>
            </div>
        )
    }
}

export default ActionLineEvent
