import * as React from 'react'
import { Link } from 'react-router-dom'
import { YMaps, Map, GeoObject } from 'react-yandex-maps'

import { goToAnchor } from "../../../core/utils"
import RatingComponent from '../../blocks/rating/rating.component'
import ListIconComponent from '../../blocks/list-icon.component'
import { IconList } from '../../blocks/icon/icon.component'

export interface IFaceCardInform {
    type?: string
    avatar?: string
    title: string
    link?:string
    reviews?: any[]
    options?: Array<{
        className?: string
        tit: string
        val: string
    }>
    params?: {
        title?: string
        defaultIcon?: IconList | string
        list: Array<{
            src?: string
            title?: string
        }>
    }
    geo?: number[]
}

interface IFaceCardInformState {
    mapHidden: boolean
}

class InformFaceCard extends React.Component<IFaceCardInform, IFaceCardInformState> {

    state: IFaceCardInformState = {
        mapHidden: !!this.props.geo
    }

    refContainer: React.RefObject<HTMLDivElement> = React.createRef()

    get className() {
        let className = '_container'
        let { avatar, geo } = this.props
        const { mapHidden } = this.state
        if (geo) className += ` --geo`
        if (avatar) className += ` --avatar`
        if (mapHidden) className += ' --geoHide'
        return className
    }

    toggleGeo = () => {
        this.setState({ mapHidden: !this.state.mapHidden })
    }

    render() {
        const { mapHidden } = this.state
        let { type, avatar, link, title, reviews, options, params, geo } = this.props

        const mapCenter = geo && [geo[1] - 0.04, geo[0] - 0.005]

        return (
            <div className={this.className} ref={this.refContainer}>
                <Link to={link||"#"} className="_text">
                    { avatar &&
                        <div
                            className="_text__avatar"
                            style={{
                                backgroundImage: `url(${avatar})`
                            }}
                        ></div>
                    }
                    <div className="_text__tit">
                        { type && <span className="--name">{type}: </span> }
                        <span>{title}</span>
                    </div>
                    { reviews &&
                        <div className="_text__reviews">
                            <RatingComponent act={0} max={5}/>
                            <span className="--link">{reviews.length} отзывов</span>
                        </div>
                    }
                    { options &&
                        <ul className="_text__options">
                            { options.map((el, i) => { return(
                                <li key={i} className={el.className || ''} onClick={link ? goToAnchor("#contact") : null}>
                                    <span>{el.tit}: </span>{el.val}
                                </li>
                            )}) }
                        </ul>
                    }
                </Link>
                {geo && (
                    <div className="_geo">
                        {mapHidden ? (
                            <div className="static-map" onClick={this.toggleGeo}>
                                <img src={`https://static-maps.yandex.ru/1.x/?ll=${mapCenter[0]},${mapCenter[1]}&size=650,248&z=13&l=map&pt=${geo[1]},${geo[0]},comma`} />
                            </div>
                        ) : (
                            <>
                                <div className="_geo__toggle" onClick={this.toggleGeo}>Скрыть</div>
                                <YMaps>
                                    <Map
                                        defaultState={{ center: geo, zoom: 17 }}
                                        width="100%" height="100%"
                                    >
                                        <GeoObject
                                            geometry={{
                                                type: 'Point',
                                                coordinates: geo,
                                            }}
                                            options={{
                                                iconLayout: 'default#image',
                                                iconImageHref: '/images/geopoint.png',
                                                iconImageSize: [79, 97],
                                                iconOffset: [-43, -55]
                                            }}
                                        />
                                    </Map>
                                </YMaps>
                            </>
                        )}
                    </div>
                )}
                {params &&
                    <ListIconComponent
                        className="--landFace"
                        title={params.title}
                        scroll={true}
                        defaultIcon={params.defaultIcon}
                        list={params.list}
                    />
                }
            </div>
        )
    }
}

export default InformFaceCard
