import * as React from 'react'
import { YMaps, Map, GeoObject } from 'react-yandex-maps'

interface IProps {
    src?: string
    adress?: string
    geoSrc?: string
    coordinates?: number[]
    name?: string
    switcher?: boolean
}

interface IState {
    avatarOpen: boolean
}

class AvatarFaceCard extends React.Component<IProps, IState> {
    state: IState = {
        avatarOpen: true
    }

    handleClick = () => {
        const { coordinates, switcher } = this.props
        const { avatarOpen } = this.state

        if (switcher && coordinates)
            this.setState({ avatarOpen: !avatarOpen })
    }

    render() {
        let { src, adress, geoSrc, coordinates, name } = this.props
        const { avatarOpen } = this.state

        return (
            <div className="_avatar">
                { avatarOpen || !coordinates ?
                    <img
                        className="_avatar__img"
                        src={src || '/images/playground-avatar-default.png'}
                        alt="#"
                    />
                :
                    <YMaps>
                        <Map
                            defaultState={{ center: coordinates, zoom: 17 }}
                            width={295} height={256}
                        >
                            <GeoObject
                                geometry={{
                                    type: 'Point',
                                    coordinates: coordinates,
                                }}
                                options={{
                                    iconLayout: 'default#image',
                                    iconImageHref: '/images/geopoint.png',
                                    iconImageSize: [79, 97],
                                    iconOffset: [-43, -55]
                                }}
                            />
                        </Map>
                    </YMaps>
                }
                <div className="_avatar__geo" onClick={this.handleClick}>
                    <div className="_avatar__geoTit">{avatarOpen ? adress : name}</div>
                    <img
                        src={avatarOpen ? geoSrc || '/images/playg-geo.png' : src || '/images/playground-avatar-default.png'}
                        alt="#"
                    />
                </div>
            </div>
        )
    }
}

export default AvatarFaceCard
