import * as React from 'react'
import { Link } from 'react-router-dom';

export enum LandFaceCardTypes {
    small = 'small',
    large = 'large',
    avatar = 'avatar',
    list = 'list',
}

interface IProps {
    type?: string
    bookingLink?: string
}

class FaceCard extends React.Component<IProps, {}> {

    get className() {
        let className = 'face-card'
        let { type } = this.props
        if (type) className += ` --${type}`
        return className
    }

    render() {
        let { children, bookingLink } = this.props
        return (
            <div className={this.className}>
                {children}
                {bookingLink && 
                    <Link 
                        to={bookingLink} 
                        className="_bookingBtn"
                    >Забронировать</Link>
                }
            </div>
        )
    }
}

export default FaceCard