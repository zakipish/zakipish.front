import * as React from 'react'
import { Link } from 'react-router-dom'
import InformFaceCard, { IFaceCardInform } from './inform.face-card'

interface IProps extends IFaceCardInform {
    avatar?: string
    link?: string
}

class ListItemFaceCard extends React.Component<IProps, {}> {

    render() {
        let { avatar, link,  ...rest } = this.props
        return (
            <div className="face-card --list">
                <InformFaceCard link={link} {...rest}/>
                <Link to={link} className="_avatar">
                    <img src={avatar} alt="#"/>
                </Link>
                <Link to={link + "/booking"} className="_btn">Играть</Link>
            </div>
        )
    }
}

export default ListItemFaceCard
