import * as React from 'react'
import SliderComponent from '../../blocks/slider.component'
import {goToAnchor} from "../../../core/utils";

interface IProps {
    list: string[]
}

class SliderFaceCard extends React.Component<IProps, {}> {
    render() {
        let { list } = this.props
        return (
            <SliderComponent 
                className="_slider"
                prev="_slider__arr --prev" 
                next="_slider__arr --next"
                list="_slider__list"
                length={list.length}
            >
                { list.map((el, i) => { return(
                    <div key={i} className="_slider__item">
                            <img onClick={goToAnchor("#photo")} src={el} alt=""/>
                    </div>
                )}) }
            </SliderComponent>
        )
    }
}

export default SliderFaceCard