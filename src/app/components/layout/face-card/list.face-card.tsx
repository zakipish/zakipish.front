import * as React from 'react'

interface IProps {
    children: any
}

class ListFaceCard extends React.Component<IProps, {}> {
    render() {
        let { children } = this.props
        return (
            <div className="face-card-list">
                {children}
            </div>
        )
    }
}

export default ListFaceCard