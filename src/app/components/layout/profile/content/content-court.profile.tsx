import * as React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { history } from '../../../../core/history'
import { User } from '../../../../models/user'
import { Game } from '../../../../models/game'
import { Event } from '../../../../models/event'
import { getFriends } from '../../../../services/friends-service'

import { inviteFriend as gameInviteFriend } from '../../../../services/games-service'
import { inviteFriend as eventInviteFriend } from '../../../../services/events.service'

import IconComponent from '../../../blocks/icon/icon.component'
import SharingComponent from '../../../blocks/sharing.component'
import ManList from '../../../blocks/list/man.list'
import Loader from '../../../layout/loader'

interface IProps {
    currentUser: User
    isInTheGame: boolean
    game: Game | Event
    party: ICourtParty[]
    state: ICourtState
    onInvite: () => void
}

interface IState {
    friends?: User[]
    friendsLoading: boolean
    invitationDisabled: boolean
    sharing: boolean
    invite: boolean
}

export interface ICourtParty {
    level?: {
        bg: string
        title: string
    }
    url?: string
    image?: string
}

interface ICourtState {
    min?: number
    now: number
    must?: number
    max?: number
}

class ContentCourtProfile extends React.Component<IProps, IState> {
    state: IState = {
        friendsLoading: true,
        invitationDisabled: false,
        sharing: false,
        invite: false
    }

    componentDidMount() {
        this.bootstrap()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.game !== this.props.game)
            this.filterFriends()
    }

    bootstrap = () => {
        const { game, currentUser } = this.props

        if (currentUser) {
            getFriends()
                .then(friends => {
                    if (friends) {
                        friends = friends.filter(el => !game.users.map(user => user.id).includes(+el.id))
                        this.setState({ friends, friendsLoading: false })
                    }
                })
        } else {
            this.setState({ friends: [], friendsLoading: false })
        }
    }

    filterFriends = () => {
        let { friends } = this.state
        const { game } = this.props
        if (game && friends) {
            friends = friends.filter(el => !game.users.map(user => user.id).includes(+el.id))
            this.setState({ friends })
        }
    }

    handleInvitation = (game_id: number, user_id: number) => () => {
        const { onInvite, game } = this.props
        this.setState({ invitationDisabled: true }, () => {
            if (game instanceof Game) {
                gameInviteFriend(game_id, user_id)
                .then(onInvite)
            }

            if (game instanceof Event) {
                eventInviteFriend(game_id, user_id)
                .then(onInvite)
            }
        })
    }

    onClickCourtBtn = (btn: string) => () => {
        const { sharing, invite } = this.state
        const { currentUser } = this.props

        switch(btn) {
            case 'share':
                this.setState({ sharing: !sharing, invite: false })
                break
            case 'invite':
                if (currentUser)
                    this.setState({ sharing: false, invite: !invite })
                break
        }
    }

    render() {
        const { sharing, invite, invitationDisabled, friendsLoading, friends } = this.state
        let { party, state, isInTheGame, game } = this.props
        let leftParty = state.now > 8 ? state.now - 8 : null
        return (
            <div className="court">
                <div className="court__wrap">
                    <div className="court__header">
                        <div className="court__tit">{state.now}/{state.max || state.must} присоединились</div>
                        <div className="court__subtit">Минимум {state.min || state.must || state.max} игроков</div>
                    </div>
                    <div className="court__court">
                        <img className="court__courtImg" src="/images/court.svg" alt="#"/>
                        <ul>
                            {party.map((el, i) => { 
                                let bg = (el.level && el.level.bg) ? `#${el.level.bg}` : ''
                                return(
                                    <li key={i}>
                                        <Link 
                                            to={el.url || '#'}
                                            className="court__avatar"
                                            style={{ borderColor: bg}}
                                        >
                                            {el.image ? <img src={el.image} alt="#"/> : ''}
                                        </Link>
                                        {el.level ?
                                            <span 
                                                style={{ backgroundColor: bg}} 
                                                className="tag tag--blue"
                                            >{el.level.title}</span>
                                        : ''}
                                    </li>
                                )
                            })}
                            {leftParty ?
                                <li>
                                    <div className="court__avatar">
                                        <span>+{leftParty}</span>
                                    </div>
                                </li>
                            : ''}
                        </ul>
                    </div>
                    <div className="court__act">
                        <div
                            className={`court__btn ${sharing ? '--darkBlue' : ''}`}
                            onClick={this.onClickCourtBtn('share')}
                        >
                            <span>поделиться</span>
                            <IconComponent icon="shared" width="18px" />
                        </div>
                        <div
                            className={`court__btn ${invite ? '--darkBlue' : ''}`}
                            onClick={this.onClickCourtBtn('invite')}
                        >
                            <span>пригласить в игру</span>
                            <IconComponent icon="plus" width="18px" />
                        </div>
                    </div>
                    { sharing ?
                        <SharingComponent
                            list={['vkontakte', 'odnoklassniki', 'facebook', 'twitter', 'gplus', 'pinterest', 'telegram', 'whatsapp']}
                            classNameLikely="likely-big"
                            title=""
                        />
                    : '' }
                </div>

                { invite && isInTheGame &&
                    <Loader state={friendsLoading}>
                        {friends &&
                            <ManList
                                list={friends.map(man => ({
                                    name: man.name,
                                    surname: man.lastName,
                                    level: 'begginer',
                                    btns: [{
                                        title: 'пригласить',
                                        icon: 'plus',
                                        onClick: this.handleInvitation(game.id, man.id),
                                        disabled: invitationDisabled
                                    }]
                                }))}
                            />
                        }
                    </Loader>
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    currentUser: state.authState.user,
})

export default connect(mapStateToProps)(ContentCourtProfile)
