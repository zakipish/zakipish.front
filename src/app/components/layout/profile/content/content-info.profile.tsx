import * as React from 'react'

interface IProps {
    info?: string
    children?: any
}

class ContentInfoProfile extends React.Component<IProps, {}> {
    render() {
        let { info, children } = this.props
        return (
            <div className="profileContent-state">
                { children && <div className="_content">{children}</div> }
                { info && <div className="_info"><span>{info}</span></div> }
            </div>
        )
    }
}

export default ContentInfoProfile