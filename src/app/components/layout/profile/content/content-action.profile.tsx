import * as React from 'react'
import { Link } from 'react-router-dom'
import IconComponent, { IconList } from '../../../blocks/icon/icon.component'

interface IProps {
    list?: Array<{
        title: string
        url?: string
        bg?: string
        icon?: IconList | string 
        onClick?: () => void
        disabled?: boolean
    }>
}

class ContentActionProfile extends React.Component<IProps, {}> {

    getItemClass = (el) => {
        let className = '_btn'
        if (el.bg) className += ` --${el.bg}`
        if (el.disabled) className += ` --disabled`
        return className
    }
    
    render() {
        let { list } = this.props
        return (
            <div className="profileContent-action">
                { list ? list.map((el, i) => { return(
                    <div 
                        key={i} 
                        className={this.getItemClass(el)}
                        onClick={() => !el.disabled && el.onClick && el.onClick()}
                    >
                        { el.url ? 
                            <Link to="url">{el.title}</Link>
                            : 
                            <span>{el.title}</span>
                        }
                        { el.icon && <IconComponent className="_btn__icon" icon={el.icon}/>}
                    </div>
                )}) : '' }
            </div>
        )
    }
}

export default ContentActionProfile
