import * as React from 'react'
import { Link } from 'react-router-dom'

interface IProps {
    className?: string
    list: IProfileList[]
    type: string
}

export interface IProfileList {
    title: string
    value?: string
    price?: number
    image?: string
    priceBg?: string
    url?: string
}

class ContenListProfile extends React.Component<IProps, {}> {
    
    render() {
        let { className, list, type } = this.props
        return (
            <ul className={`profileContent-list --${type} ${className ? className : ''}`}>
                { list.map((el, i) => { return(
                    <li key={i}>
                        { el.image ? 
                            <div className="_img">
                                <img src={el.image} alt="#"/>
                            </div>
                        : '' }
                        <div className="_text">
                            { el.value ? 
                                <div className="_val">
                                    {el.url ? 
                                        <Link to={el.url}>{el.value}</Link>
                                        : 
                                        el.value 
                                    }
                                </div>
                            : '' }
                            <div className="_tit">{el.title}</div> 
                            { el.price ? <div className={`_price --${el.priceBg || ''}`}>{el.price}</div> : '' }
                        </div>
                    </li>
                )})}
            </ul>
        )
    }
}

export default ContenListProfile