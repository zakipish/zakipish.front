import * as React from 'react'
import LabelComponent, { LabelStatus } from '../../../blocks/label.component'
import { IconList } from '../../../blocks/icon/icon.component'

interface IProps {
    date: string
    time: string
    price: string
}

class ContentBookingProfile extends React.Component<IProps, {}> {

    render() {
        let { date, time, price } = this.props
        return (
            <div className="profileContentBooking">
                <div className="_fromTo">
                    <div className="_fromTo__item">
                        <div className="_fromTo__tit">Дата игры</div>
                        <div className="_fromTo__label">
                            <LabelComponent
                                title={date}
                                status={LabelStatus.animation}
                                icon={IconList.calendar}
                            />
                        </div>
                    </div>
                    <div className="_fromTo__item">
                        <div className="_fromTo__tit">Время игры</div>
                        <div className="_fromTo__label">
                            <LabelComponent
                                title={time}
                                status={LabelStatus.animation}
                                icon={IconList.time}
                            />
                        </div>
                    </div>
                </div>
                <div className="_price">
                    <div className="_price__txt">
                        <div className="_price__tit">Стоимость</div>
                        <div className="_price__val">{price}<span>руб.</span></div>
                    </div>
                    <div className="_price__btn">Забронированть</div>
                </div>
            </div>
        )
    }
}

export default ContentBookingProfile