import * as React from 'react'

interface IProps {
    title?: string
}

class ContentProfile extends React.Component<IProps, {}> {
    render() {
        let { title, children } = this.props
        return (
            <div className="profileContent">
                <div className="profileContent__wrap">
                    { title ? <div className="profileContent__tit">{title}</div> : '' }
                    { children }
                </div>
            </div>
        )
    }
}

export default ContentProfile
