import * as React from 'react'
import IconComponent from 'src/app/components/blocks/icon/icon.component';

interface IProps {
    className?: string 
    list?: IGameList[] 
    subtitle?: string //пример: Виды спорта, в которые вы играли
}

interface IGameList {
    title: string
    icon: string
    width: string
    height: string
}

class HeaderGamelistProfile extends React.Component<IProps, {}> {
    render() {
        let { className, list, subtitle } = this.props
        return (
            <div className={`profileHeader__gamelist ${className ? className : ''}`}>
                { list ? 
                    <ul>
                        { list.map((el, i) => { return(
                            <li key={i} className={i > 0 ? '_stroke' : ''}>
                                <IconComponent 
                                    icon={el.icon} 
                                    width={el.width} 
                                    height={el.height}
                                />
                                <span>{el.title}</span>
                            </li>
                        )}) }
                    </ul>
                : '' }
                { subtitle && <div className="profileHeader__subtit">{subtitle}</div> }
            </div>
        )
    }
}

export default HeaderGamelistProfile