import * as React from 'react'
import IconComponent, { IconList } from '../../../blocks/icon/icon.component'
import { SocialAccount } from '../../../../models/social-account'
import IconToPopup from '../../../blocks/icon-to-popup'
import ExternalLink from '../../../blocks/external-link'

export interface IProfileHeader {
    className?: string
    title: string
    icon?: IconList | string
    soc?: SocialAccount[]
    settingTitle?: string
    setting?: React.ReactFragment
    settingsHide?: boolean
    onSettingsToggle?: (state?: boolean) => void
    children?: any
}

interface IProfileState {}

class HeaderProfile extends React.Component<IProfileHeader, IProfileState> {

    render() {
        const { onSettingsToggle, settingsHide, className, title, icon, soc, setting, children } = this.props

        return (
            <div className={`profileHeader ${className ? className : ''}`}>
                <div className="profileHeader__wrap">
                    <div className="profileHeader__tit">
                        { icon && <IconComponent className="profileHeader__icon" icon={icon}/> }
                        <span>{title}</span>
                        { setting &&
                            <IconToPopup icon={IconList.setting} hide={settingsHide} onToggle={onSettingsToggle} className="--md-right">
                                {setting}
                            </IconToPopup>
                        }
                    </div>
                    {/* nickname ? <div className="profileHeader__subtit">@{nickname}</div> : '' */}
                    { soc ?
                        <div className="profileHeader__soc">{
                            soc.map((el, i) => { return(
                                <ExternalLink key={i} href={el.profile_url} className={`bg-${el.provider}`} target="_blank">
                                    <IconComponent icon={el.provider}/>
                                </ExternalLink>
                            )})
                        }</div>
                    : '' }
                    { children && <div className="profileHeader__cnt">{children}</div> }
                </div>
            </div>
        )
    }
}

export default HeaderProfile
