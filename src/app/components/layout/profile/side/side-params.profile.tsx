import * as React from 'react'

interface IProps {
    list?: Array<{
        title: string
        value?: string
    }> 
    icons?: Array<{
        src?: string
        title?: string
    }>
}

class SideParamsProfile extends React.Component<IProps, {}> {
    render() {
        let { list, icons } = this.props
        return (
            <div className="profileSideParams">
                { list ? 
                    <ul className="_params">
                        { list.map((el, i) => { return(
                            <li key={i}>
                                <div className="_params__val">{el.value || '-'}</div>
                                <div className="_params__tit">{el.title || '-'}</div>
                            </li>
                        )}) }
                    </ul>
                : '' }
                { icons ? 
                    <ul className="_icons">
                        { icons.map((el, i) => { return(
                            <li key={i}>
                                <img src={el.src || '/images/side-params.svg'} alt="#"/>
                                <div className="_icons__tit">{el.title || '-'}</div>
                            </li>
                        )}) }
                    </ul>
                : '' }
            </div>
        )
    }
}

export default SideParamsProfile
