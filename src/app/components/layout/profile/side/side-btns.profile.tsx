import * as React from 'react'
import { Link } from 'react-router-dom'
import IconComponent from '../../../blocks/icon/icon.component'

interface IProps {
    buttons?: IProfileSideButton[]
}

export interface IProfileSideButton {
    theme: string
    title: string
    icon?: string
    act: string | Function
}

class SideBtnsProfile extends React.Component<IProps, {}> {

    renderButtons(): React.ReactFragment {
        let { buttons } = this.props
        if (buttons) {
            return buttons.map((el, i) => {
                let btnProps = {
                    key: i,
                    className: `profileSide__btnSec ${el.theme ? `--${el.theme}` : ''}`
                }
                let icon = el.icon ? <IconComponent icon={el.icon} width="18px" height="18px"/> : ''
                let title = <span>{el.title}</span>
                switch (typeof el.act) {
                    case 'string':
                        return(
                            <Link {...btnProps} to={el.act}>
                                {title}{icon}
                            </Link>
                        )
                    case 'function':
                        let func: Function = el.act
                        return(
                            <div {...btnProps} onClick={() => func()}>
                                {title}{icon}
                            </div>
                        )
                    default:
                        return null
                }
            })
        }
        return null
    }
    
    render() {
        return this.renderButtons()
    }
}

export default SideBtnsProfile