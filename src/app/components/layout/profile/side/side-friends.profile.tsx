import * as React from 'react'
import { Link } from 'react-router-dom'

interface IProps {
    link?: boolean
    friends: IProfileSideFriends[]
}

export interface IProfileSideFriends {
    url: string
    name: string
    image: string
}

class SideFriendsProfile extends React.Component<IProps, {}> {
    render() {
        let { friends, link } = this.props
        return (
            <div className="profileSide__sec">
                <div className="profileSide__secTit">
                    { link ?
                        <Link to="/profile/friends">{friends.length} друзей</Link>
                    :
                        <span>{friends.length} друзей</span>
                    }
                </div>
                <ul>{
                    friends.map((el, i) => { return(
                        <li key={i}>
                            <Link to={el.url}>
                                <div className="profileAvatar profileAvatar--sm">
                                    <img src={el.image ? el.image : '/images/avatar-md.jpg'} alt="#"/>
                                </div>
                                <p>{el.name}</p>
                            </Link>
                        </li>
                    )})
                }</ul>
            </div>
        )
    }
}

export default SideFriendsProfile
