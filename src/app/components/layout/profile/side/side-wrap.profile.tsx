import * as React from 'react'

interface IProps {
    className?: string
}

class SideWrapProfile extends React.Component<IProps, {}> {
    render() {
        let { children, className } = this.props
        return (
            <div className={`profileSide ${className ? className : ''}`} >
                {children}
            </div>
        )
    }
}

export default SideWrapProfile