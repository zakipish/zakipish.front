import * as React from 'react'
import { Link } from 'react-router-dom'
import { History } from 'history'

interface IProps {
    history?: History
}

class SideEditProfile extends React.Component<IProps, {}> {
    render() {
        let { history } = this.props
        return (
            <div className="profileEditNav">
                <div className="profileEditNav__btn">Редактировать профиль</div>
                { history ?
                    <div className="profileEditNav__list">
                        <Link to="/profile/edit/avatar" target="_self">Редактировать фото</Link>
                        <Link to="/profile/edit/sport" target="_self">Спорт</Link>
                        <Link to="/profile/edit/soc" target="_self">Социальные сети</Link>
                        <Link to="/profile/edit" target="_self">Основное</Link>
                    </div>
                :
                    <div className="profileEditNav__list">
                        <Link to="/profile/edit/avatar">Редактировать фото</Link>
                        <Link to="/profile/edit/sport">Спорт</Link>
                        <Link to="/profile/edit/soc">Социальные сети</Link>
                        <Link to="/profile/edit">Основное</Link>
                    </div>
                }
        </div> 
        )
    }
}

export default SideEditProfile