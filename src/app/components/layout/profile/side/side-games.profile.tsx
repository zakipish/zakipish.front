import * as React from 'react'
import IconComponent from '../../../blocks/icon/icon.component'
import IconFetchComponent from '../../../blocks/icon-fetch.component'

interface IProps {
    games?: IProfileSideGame[]
}

export interface IProfileSideGame {
    title: string
    icon: string
    level?: { display: string, bg?: string }
}

class SideGamesProfile extends React.Component<IProps, {}> {

    render() {
        let { games } = this.props
        return (
            <div className="profileSide__sec">
                <div className="profileSide__secTit">Во что я играю</div>
                <ul>{
                    games.map((el, i) => { return(
                        <li key={i}>
                            {el.icon ?
                                <IconFetchComponent src={el.icon} className="gameIcon" />
                            :
                                <IconComponent icon="defaultGame" className="gameIcon" />

                            }
                            <p>{el.title}</p>
                            <div style={el.level.bg ? { background: `#${el.level.bg}` } : {}}
                                className={`tag tag--green`}
                            >{el.level.display}</div>
                        </li>
                    )})
                }</ul>
            </div>
        )
    }
}

export default SideGamesProfile
