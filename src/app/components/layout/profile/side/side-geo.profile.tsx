import * as React from 'react'

export interface IProfileGeoProps {
    title?: string
    src?: string
    onClick?: () => void
    coordinates?: number[]
}

class SideGeoProfile extends React.Component<IProfileGeoProps, {}> {
    render() {
        let { title, src, onClick } = this.props
        src = src ? src : '/images/playg-geo.png'
        return (
            <div className="profileAvatarGeo" onClick={onClick}>
                <div className="_tit">{title}</div>
                <img src={src} alt="#"/>
            </div>
        )
    }
}

export default SideGeoProfile
