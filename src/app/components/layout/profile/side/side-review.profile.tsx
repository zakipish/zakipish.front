import * as React from 'react'
import RatingComponent, { IRating } from '../../../blocks/rating/rating.component'
import IconComponent, { IconList } from '../../../blocks/icon/icon.component'

interface IProps {
    title?: string
    rating?: IRating
}

class SideReviewProfile extends React.Component<IProps, {}> {
    render() {
        let { title, rating } = this.props
        return (
            <div className="profileSideReview">
                <div className="_header">
                    <div>
                        <div className="_header__tit">{title}</div>
                        <RatingComponent {...rating}/>
                    </div>
                    <div className="_header__arr">
                        <IconComponent icon={IconList.selectArr}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default SideReviewProfile