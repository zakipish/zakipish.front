import * as React from 'react'
import { History } from 'history'
import { YMaps, Map, GeoObject } from 'react-yandex-maps'

import SideEditProfile from './side-edit.profile'
import SideGamesProfile, { IProfileSideGame } from './side-games.profile'
import { IProfileSideFriends } from './side-friends.profile'
import SideBtnsProfile, { IProfileSideButton } from './side-btns.profile'
import SideGeoProfile, { IProfileGeoProps } from './side-geo.profile'

export interface IProfileSide {
    className?: string
    edit?: boolean
    avatar?: string
    name?: string
    balance?: number
    buttons?: IProfileSideButton[]
    games?: IProfileSideGame[]
    friends?: IProfileSideFriends[]
    history?: History
    geo?: IProfileGeoProps
    switcher?: boolean
}

interface IProfileSideState {
    avatarOpen: boolean
    imageHeight?: number
}

class SideProfile extends React.Component<IProfileSide, IProfileSideState> {

    sideElem: React.RefObject<HTMLDivElement> = React.createRef()
    avatarImg: React.RefObject<HTMLImageElement> = React.createRef()
    wrapElem: HTMLDivElement

    state: IProfileSideState = {
        avatarOpen: true
    }

    componentDidMount() {
        this.setWrapHeight()
        this.wrapElem = document.querySelector('.profile__wrap')
        let avatarImg = this.avatarImg.current
        if(avatarImg) avatarImg.onload = this.setWrapHeight
        window.addEventListener('resize', this.setWrapHeight, false)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.setWrapHeight, false)
    }

    setWrapHeight = () => {
        this.setState({ imageHeight: this.avatarImg.current.clientHeight }, () => {
            let sideElem = this.sideElem.current
            if (window.innerWidth >= 640 && sideElem && this.wrapElem) {
                this.wrapElem.style.minHeight = sideElem.clientHeight + 'px'
            }
        })
    }

    get geo() {
        const { geo, avatar, name } = this.props
        const { avatarOpen } = this.state

        if (!geo)
            return null

        return avatarOpen ? {
            title: geo.title,
            src: geo.src
        } : {
            title: name,
            src: avatar
        }
    }

    handleClick = () => {
        const { avatarOpen } = this.state
        const { geo, switcher } = this.props
        if (switcher && geo)
            this.setState({ avatarOpen: !avatarOpen })
    }

    render() {
        let { className, avatar, edit, buttons, games, history, geo, children } = this.props
        const { avatarOpen, imageHeight } = this.state
        avatar = avatar ? avatar : '/images/avatar-lg.jpg'
        return (
            <div 
                className={`profileSide ${className ? className : ''}`} 
                ref={this.sideElem}
            >
                <div className="profileAvatar" style={{ height: imageHeight }}>
                    { avatarOpen || !geo || !geo.coordinates ?
                        <img src={avatar} alt="#" ref={this.avatarImg}/>
                    :
                        <YMaps>
                            <Map
                                defaultState={{ center: geo.coordinates, zoom: 17 }}
                                width={330} height={imageHeight}
                            >
                                <GeoObject
                                    geometry={{
                                        type: 'Point',
                                        coordinates: geo.coordinates,
                                    }}
                                    options={{
                                        iconLayout: 'default#image',
                                        iconImageHref: '/images/geopoint.png',
                                        iconImageSize: [79, 97],
                                        iconOffset: [-43, -55]
                                    }}
                                />
                            </Map>
                        </YMaps>
                    }
                    { edit ? <SideEditProfile history={history}/> : '' }
                    { this.geo ? <SideGeoProfile onClick={this.handleClick} {...this.geo}/> : '' }
                </div>
                <SideBtnsProfile buttons={buttons}/>
                { games ? <SideGamesProfile games={games}/> : '' }
                {children}
            </div>
        )
    }
}

export default SideProfile
