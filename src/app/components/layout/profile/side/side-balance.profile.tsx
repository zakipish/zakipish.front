import * as React from 'react'
import IconComponent from '../../../blocks/icon/icon.component'

interface IProps {
    balance: number
}

class SideBalanceProfile extends React.Component<IProps, {}> {
    render() {
        let { balance } = this.props
        return (
            <div className="profileSide__sec profileBalance">
                <div>
                    <div className="profileBalance__tit">Баланс</div>
                    <div className="profileBalance__num">{balance} Р</div>
                </div>
                <div className="profileBalance__btn">
                    <IconComponent icon="purse" fill="#fff"/>
                </div>
            </div>
        )
    }
}

export default SideBalanceProfile