import * as React from 'react'
import { Link } from 'react-router-dom'

interface IProps {
    title?: string
    offer?: string
    src?: string
    url?: string
}

class SideClubProfile extends React.Component<IProps, {}> {
    render() {
        let { title, offer, src, url } = this.props
        src = src ? src : '/images/profile-side-club.png'
        return (
            <Link to={url} className="profileSideClub">
                <div>
                    <div className="_tit">{title}</div>
                    <div className="_offer">{offer}</div>
                </div>
                <img src={src} alt=""/>
            </Link>
        )
    }
}

export default SideClubProfile
