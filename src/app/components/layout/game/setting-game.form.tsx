import * as React from 'react'

import WrapForm from '../form/wrap.form'
import RowForm from '../form/row.form'
import FieldForm from '../form/field.form'
import BtnsForm from '../form/btns.form'
import InputControl from '../../form/input-control'
import SelectControl from '../../form/select-control'
import { reqPut } from "../../../core/api"
import { Game } from '../../../models/game'
import { GameLevel } from '../../../models/game-level'
import * as _ from "lodash"

interface IProps {
    gameLevels: GameLevel[]
    game: Game
    onSave?:(data:any)=>void
}

interface IState {
    game: any
    gameBackup: any
}

class SettingGameForm extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props)
        this.state = {
            game: this.prepareFormData(props.game),
            gameBackup: this.cloneModel(props.game)
        }
    }

    prepareFormData = (game: Game) => {
        let age = null

        switch(game.age_start) {
            case(null):
                age = 0
                break
            case(15):
                age = 1
                break
            case(18):
                age = 2
                break
            case(25):
                age = 3
                break
            case(30):
                age = 4
                break
            case(36):
                age = 5
                break
            case(45):
                age = 6
                break
            default:
                age = 0
                break
        }
        game.age = age
        return game
        /*
        return {
            age: age,
            min_players: game.min_players,
            max_players: game.max_players,
            game_level_id: game.game_level_id
        }
        */
    }

    cloneModel = (model) => {
        return _.cloneDeep(model)
    }

    agesCollection = {
        0: 'до 15',
        1: '15-17',
        2: '18-24',
        3: '25-29',
        4: '30-35',
        5: '36-45',
        6: '45+'
    }

    resetForm = () => {
        const { gameBackup } = this.state
        this.setState({ game: this.prepareFormData(gameBackup) })
    }

    prepareOptions = (options) => {
        let result = {}

        for (let option of options){
            result[option.id] = option.name
        }

        return result
    }

    onChange = (key) => (value) => {
        let { game } = this.state
        game[key] = value
        this.setState({ game })
    }

    handleSubmit = (e)=>{
        e.preventDefault()

        const { game, gameBackup } = this.state
        const { onSave } = this.props

        let data = {
            name: game.name,
            min_players: game.min_players,
            max_players: game.max_players,
            game_level_id: game.game_level_id
        }

        if (game.age){
            switch(parseInt(game.age)) {
                case 0:
                    data['age_start'] = null
                    data['age_end'] = 15
                    break
                case 1:
                    data['age_start'] = 15
                    data['age_end'] = 17
                    break
                case 2:
                    data['age_start'] = 18
                    data['age_end'] = 24
                    break
                case 3:
                    data['age_start'] = 25
                    data['age_end'] = 29
                    break
                case 4:
                    data['age_start'] = 30
                    data['age_end'] = 35
                    break
                case 5:
                    data['age_start'] = 36
                    data['age_end'] = 45
                    break
                case 6:
                    data['age_start'] = 45
                    data['age_end'] = null
                    break
            }
        }

        reqPut(`/games/${gameBackup.id}`, JSON.stringify(data))
        .then(response => {
            this.setState({ game: this.prepareFormData(new Game(response.data)), gameBackup: response.data })

            return response
        })
        .then(response => { if (onSave) onSave(response.data) })
        .catch(e => console.log(e.message))
        .finally()
    }

    get minPlayersCollection() {
        let playersCollection = {}
        for (let i = 2; i <= 25; i++) {
            playersCollection[i] = i
        }

        return playersCollection
    }

    get maxPlayersCollection() {
        const { min_players } = this.state.game
        let playersCollection = {}
        const start = min_players ? min_players : 2
        for (let i = start; i <= 25; i++) {
            playersCollection[i] = i
        }

        return playersCollection
    }

    render() {
        const { game } = this.state
        const { gameLevels } = this.props


        return (
            <form onSubmit={this.handleSubmit}>
                <WrapForm className="--popup">
                    <RowForm title="Настройки игры">
                        <FieldForm title="Название игры">
                            <InputControl
                                value={game.name}
                                onChange={this.onChange('name')}
                            />
                        </FieldForm>
                    </RowForm>
                    <RowForm>
                        <FieldForm title="Возраст икроков">
                            <SelectControl
                                options={this.agesCollection}
                                value={game.age}
                                onChange={this.onChange('age')}
                            />
                        </FieldForm>
                    </RowForm>
                    <RowForm>
                        <FieldForm title="Игроков">
                            <SelectControl
                                options={this.maxPlayersCollection}
                                value={game.max_players}
                                onChange={this.onChange('max_players')}
                            />
                        </FieldForm>
                        <FieldForm title="Минимум" info="Дополнительная информация">
                            <SelectControl
                                options={this.minPlayersCollection}
                                value={game.min_players}
                                onChange={this.onChange('min_players')}
                            />
                        </FieldForm>
                    </RowForm>
                    <RowForm>
                        <FieldForm title="Уровень игроков">
                            <SelectControl
                                options={gameLevels.reduce((arr, level) => {
                                    arr[level.id] = level.name
                                    return arr
                                }, {})}
                                value={game.game_level_id}
                                onChange={this.onChange('game_level_id')}
                            />
                        </FieldForm>
                    </RowForm>
                    <BtnsForm>
                        <span className="--gray" onClick={this.resetForm}>Cбросить</span>
                        <button className="--darkBlue" type="submit">Сохранить</button>
                    </BtnsForm>
                </WrapForm>
            </form>
        )
    }
}

export default SettingGameForm
