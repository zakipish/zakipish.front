import * as React from 'react'
import { History } from 'history'
import IconComponent, { IconList } from '../blocks/icon/icon.component'

interface IProps {
    className?: string
    history: History
}

class CrossPopupComponent extends React.Component<IProps, {}> {

    clickCross() {
        let { history } = this.props
        if (history.length > 2) {
            history.goBack()
        } else {
            history.push('/')
        }
    }

    render() {
        let { className } = this.props
        return (
            <div className={className} onClick={() => this.clickCross()}>
                <IconComponent icon={IconList.cross}/>
            </div>
        )
    }
}

export default CrossPopupComponent