import * as React from 'react'
import moment from 'moment'
import { getMinutes, selectedPriceReducer, selectedDurationReducer, minToStr, getDateRangeSelected } from './util.planner'

import * as PlannerService from './service.planner'
import { Game } from '../../models/game'
import Price from '../../models/price'

import HeaderPlanner from './header.planner'
import DatesPlanner from './dates.planner'
import TimesPlanner from './times.planner'
import GridPlanner from './grid.planner'
import BodyPlanner from './body.planner'
import TaskListPlanner, { GameSpace } from './task-list.planner'
import SelectPlanner from './select.planner'
import PopupPlanner, { PopupPlannerProps } from './popup.planner'

interface IProps {
    id?: number
    clubId?: number
}

interface IState {
    loading: boolean
    date: moment.Moment    
    prices: Price[]    
    games: Game[]  
    timeStart: string
    timeEnd: string  
    selectedList: PlannerSelectDay[]
    spaceCrood: SpaceCrood
    popupPos: {
        top?: number
        left?: number
        right?: number
        bottom?: number
    }
    fromMin: number
    toMin: number
    lineLen: number
    rowLen: number
    interval: number
    hideSelectFunc: boolean
}

export interface PlannerSelectDay {
    date: string
    durations: Array<{
        from?: string
        to?: string
        price?: number
    }>
}

export interface SpaceCrood {
    x1: number
    x2: number
    y1: number
    y2: number
}

class Planner extends React.Component<IProps, IState> {

    refGrid: React.RefObject<HTMLDivElement> = React.createRef()
    refTasks: React.RefObject<HTMLDivElement> = React.createRef()
    blockCell: number = null
    popupType: string = 'booking'
    gameId: number = 0

    reqParamBooking = {
        date: '',
        from: '',
        to: '',
    }
    
    state = {
        loading: true,
        date: moment().startOf('week'),
        prices: [],
        games: [],
        timeStart: '',
        timeEnd: '',
        selectedList: [],
        spaceCrood: { x1: -1, x2: -1, y1: -1, y2: -1 },
        popupPos: { top: 0, left: 0},
        fromMin: 0,
        toMin: 420,
        lineLen: 7,
        rowLen: 7,
        interval: 60,
        hideSelectFunc: false
    }

    componentDidMount() {
        this.setDate()
        window.addEventListener('resize', this.onResize)
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.onResize)
    }

    onResize = () => {
        let { hideSelectFunc } = this.state 
        let w = window.innerWidth
        hideSelectFunc = (w < 940) ? false : true
        if (hideSelectFunc !== this.state.hideSelectFunc) {
            this.setState({hideSelectFunc})
        }
    }

    setDate = async (addNum: number = 0, key: string = '') => {
        let { id } = this.props
        
        this.setState({loading: true, spaceCrood: { x1: -1, x2: -1, y1: -1, y2: -1 }}, async () => {
            let date = await PlannerService.setDate(this.state.date, addNum, key)
            let { prices, timeStart, timeEnd, interval } = await PlannerService.reqPrices(id, date)
            let games = await PlannerService.reqGames(id, date)       
            let gridVar = this.validateVar(date, timeStart, timeEnd, interval)
    
            this.setState({loading: false, date, prices, games, timeStart, timeEnd, ...gridVar}, () => {
                this.onResize()
            })
        })
        
    }

    validateVar = (date, timeStart, timeEnd, inInterval) => {
        let { fromMin, toMin, lineLen, rowLen, interval } = this.state
        let dateStr = date.format('YYYY-MM-DD')
        if (inInterval > 0) { 
            interval = inInterval
            fromMin = getMinutes(moment(`${dateStr} ${timeStart}`))
            toMin = getMinutes(moment(`${dateStr} ${timeEnd}`))
            toMin += (toMin % interval > 0) ? (interval - (toMin % interval)) : 0
            lineLen = Math.floor((toMin - fromMin) / interval)
        }
        return { fromMin, toMin, lineLen, rowLen, interval }
    }

    popupProps = (): PopupPlannerProps => {
        let { selectedList, popupPos } = this.state
        let price = 0
        let duration = 0
        if (selectedList.length > 0) {
            this.reqParamBooking = getDateRangeSelected(selectedList)[0]
            price = selectedList.reduce(selectedPriceReducer, 0)
            duration = selectedList.reduce(selectedDurationReducer, 0)
        }
        let durationStr = minToStr(duration)
        let { bg, btn } = this.popupPropsType()
        let className = duration ? ' --show' : ' --hide' 
        return {
            className, bg, btn, popupPos,
            title: `${price} РУБ`,
            subtitle: durationStr,
        }
    }

    popupPropsType(): PopupPlannerProps {
        let props = {
            bg: '',
            btn: {},
        }
        if (this.popupType === 'booking') {
            let { date, from, to } = this.reqParamBooking
            let dateStr = encodeURIComponent(date)
            let timeStart = encodeURIComponent(from)
            let timeEnd = encodeURIComponent(to)
            let playgId = this.props.id
            let clubId = this.props.clubId
            props.bg = 'blue'
            props.btn = {
                title: 'Забронировать',
                url: `/club/${clubId}/playg/${playgId}/booking?date=${dateStr}&timeStart=${timeStart}&timeEnd=${timeEnd}`
            }
        } else if (this.popupType === 'game') {
            props.bg = 'green'
            props.btn = {
                title: 'Присоединиться',
                url: `/game/${this.gameId}`
            }
        }
        return props
    }

    popupSpace(spaceCrood, cellSize) {
        let { lineLen, rowLen } = this.state
        let { width, height } = cellSize
        let { x1, x2, y1, y2} = spaceCrood
        let x = x2 < (rowLen - 2) ? (x2 + 1) : (x1 - 1)
        let y = y2 < (lineLen - 3) ? (y2 + 1) : (y1 - 3)
        return {
            top: y * height,
            left: x * width,
        }
    }

    setSelected = (selectedList, spaceCrood, cellSize) => {
        let popupPos = this.popupSpace(spaceCrood, cellSize)
        this.setState({selectedList, spaceCrood, popupPos})
    }
    
    cleanSelected = () => {
        this.popupType = null
        this.setState({
            selectedList: [], 
            spaceCrood: { x1: -1, x2: -1, y1: -1, y2: -1 },
            popupPos: { top: 0, left: 0}
        })
    }

    selectBooking = (spaceCrood, cellSize) => {
        if (this.popupType === 'game') {
            this.cleanSelected()
            return
        }
        this.popupType = 'booking'
        this.gameId = 0
        spaceCrood = this.checkSpace(spaceCrood)
        let selectedList = this.getSelectedList(spaceCrood)
        this.setSelected(selectedList, spaceCrood, cellSize)
    }

    selectGame = (game: Game, space: GameSpace, cellSize) => {
        if (this.popupType === 'booking') {
            this.cleanSelected()
            return
        }
        this.popupType = 'game'
        if (this.gameId !== game.id) {
            this.gameId = game.id
            let spaceCrood = {
                x1: Math.floor(space.x / cellSize.width),
                x2: Math.floor(space.x / cellSize.width),
                // x2: Math.floor((space.x + space.width) / cellSize.x),
                y1: Math.floor(space.y / cellSize.height),
                y2: Math.floor((space.y + space.height) / cellSize.height) - 1,
            }
            let selectedList: PlannerSelectDay[] = [{
                date: game.date,
                durations: [{
                    from: game.interval[0],
                    to: game.interval[1],
                    price: +game.price
                }]
            }]
            this.setSelected(selectedList, spaceCrood, cellSize)
        }
    }
    
    getSelectedList = (spaceCrood: SpaceCrood): PlannerSelectDay[] => {
        let { x1, x2, y1, y2 } = spaceCrood
        let { prices } = this.state
        let fromPrice = this.getPrice(x1, y1)
        let toPrice = this.getPrice(x2, y2)
        let price = 0
        for (let i = y1; i <= y2; i++) {
            let priceItem = this.getPrice(x1, i)
            if (priceItem) price += +(priceItem.value)
        }
        return [{
            date: prices[x1] ? prices[x1].date : '',
            durations: [{
                from: fromPrice ? fromPrice.timeStart : '',
                to: toPrice ? toPrice.timeEnd : '',
                price: price
            }]
        }]
    }

    checkSpace = (spaceCrood) => {
        let { x1, x2, y1, y2 } = spaceCrood
        for (let i = y1; i <= y2; i++) {
            let priceItem = this.getPrice(x1, i)
            if (priceItem) {
                if (priceItem.state !== 'open') {
                    y2 = i - 1
                    break
                }
            }
        }
        return { x1, x2, y1, y2 }
    }

    getPrice(x, y): Price {
        let { prices } = this.state
        if (prices[x]) {
            if (prices[x].prices[y]) {
                return prices[x].prices[y]
            }
        }
    }
    
    render() {
        let { 
            date, prices, games, loading,
            fromMin, toMin, lineLen, rowLen, interval,
            spaceCrood,
            hideSelectFunc
        } = this.state
        let popupProps = this.popupProps()
        return (
            <div className="rpl">
                <HeaderPlanner> 
                    {/* <FilterPlanner/> */}
                    <DatesPlanner
                        date={date}
                        setDate={this.setDate}
                    />
                </HeaderPlanner>
                <BodyPlanner loading={loading}>
                    <TimesPlanner fromMin={fromMin} toMin={toMin} interval={interval}/>
                    <div ref={this.refGrid}>
                        {hideSelectFunc ? (
                            <SelectPlanner 
                                oneRow={true}
                                onSelected={this.selectBooking}
                                spaceCrood={spaceCrood}
                                setSpaceCrood={(spaceCrood) => this.setState({spaceCrood})}
                                popups={<PopupPlanner {...popupProps}/>}
                                refTasks={this.refTasks}
                            >
                                <GridPlanner prices={prices} lineLen={lineLen} rowLen={rowLen} spaceCrood={spaceCrood} setSpaceCrood={this.selectBooking}>
                                    <div ref={this.refTasks}>
                                        {!loading && (
                                            <TaskListPlanner
                                                games={games}
                                                date={date}
                                                interval={interval}
                                                gridRef={this.refGrid}
                                                onSelected={this.selectGame}
                                            />
                                        )}
                                    </div>
                                </GridPlanner>
                            </SelectPlanner>
                        ) : (
                            <GridPlanner prices={prices} lineLen={lineLen} rowLen={rowLen} spaceCrood={spaceCrood} setSpaceCrood={this.selectBooking}>
                                <div ref={this.refTasks}>
                                    {!loading && (
                                        <TaskListPlanner
                                            games={games}
                                            date={date}
                                            interval={interval}
                                            gridRef={this.refGrid}
                                            onSelected={this.selectGame}
                                        />
                                    )}
                                </div>
                            </GridPlanner>
                        )}
                    </div>
                </BodyPlanner>
                {!hideSelectFunc && (
                    <PopupPlanner {...popupProps}/>
                )}
            </div>
        )
    }
}

export default Planner
