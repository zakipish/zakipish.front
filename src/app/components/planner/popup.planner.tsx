import * as React from 'react'
import { Link } from 'react-router-dom'

export interface PopupPlannerProps {
    className?: string
    title?: string
    subtitle?: string
    bg?: string
    btn?: {
        title?: string
        url?: string
        onClick?: Function
        bg?: string
    }
    popupPos?: {
        top?: number
        left?: number
        right?: number
        bottom?: number
    }
}

class PopupPlanner extends React.Component<PopupPlannerProps, {}> {

    get className() {
        let out = 'rpl-popup'
        let { className, bg } = this.props
        if (className) out += ` ${className}`
        if (bg) out += ` --${bg}`
        return out
    }

    get classNameLink() {
        let out = 'rpl-popup__btn'
        let { bg } = this.props.btn
        out += bg ? ` --${bg}` : ' --orange'
        return out
    }

    get style() {
        let { top, left, right, bottom } = this.props.popupPos
        let style = {}
        if (!isNaN(top)) style['top'] = `${top}px`
        if (!isNaN(left)) style['left'] = `${left}px`
        if (!isNaN(bottom)) style['bottom'] = `${bottom}px`
        if (!isNaN(right)) style['right'] = `${right}px`
        return style
    }

    render() {
        let { title, subtitle, btn } = this.props
        return (
            <div className={this.className} style={this.style}>
                <div className="rpl-popup__wrap">
                    <div className="rpl-popup__tit">{title}</div>
                    {subtitle && <div className="rpl-popup__sub">{subtitle}</div>}
                    <Link 
                        to={btn.url ? btn.url : '#'} 
                        className={this.classNameLink}
                        onClick={() => btn.onClick && btn.onClick()}
                    >{btn.title}</Link>
                </div>
            </div>
        )
    }
}

export default PopupPlanner