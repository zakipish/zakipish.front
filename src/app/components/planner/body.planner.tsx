import * as React from 'react'

interface IProps {
    children: any
    loading: boolean
}

class BodyPlanner extends React.Component<IProps, {}> {

    get loaderClass() {
        let className = 'rpl-body__loader'
        let { loading } = this.props
        if (loading) className += ' --show'
        return className
    }

    render() {
        let { children } = this.props
        return (
            <div className="rpl-body">
                {children}
                <div className={this.loaderClass}>
                    <img src="/images/loader.gif" alt="#"/>
                </div>
            </div>
        )
    }
}

export default BodyPlanner
