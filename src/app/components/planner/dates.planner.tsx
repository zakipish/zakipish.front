import * as React from 'react'
import moment from 'moment'
import IconComponent, { IconList } from '../blocks/icon/icon.component'

interface IProps {
    date: moment.Moment
    setDate: (addNum: number, key: string) => void
}

interface IDay {
    className: string
    day: number
    month: string
    week: string
}

class DatesPlanner extends React.Component<IProps> {

    refElem: React.RefObject<HTMLDivElement> = React.createRef()

    leftDay: number = null
    nowDate: moment.Moment = moment()
    date: moment.Moment

    componentDidMount() {
        this.setCellSize()
        window.addEventListener('resize', this.setCellSize)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.setCellSize)
    }

    setCellSize = () => {
        setTimeout(() => {
            let parent = this.refElem.current.parentNode.parentNode 
            let cell = parent.querySelector('._cell')
            let dateItems: any = this.refElem.current.querySelectorAll('._item')
            dateItems.forEach(item => {
                item.style.width = (cell.clientWidth + 1) + 'px'
            })
        }, 100)
    }

    renderItems(): React.ReactFragment {
        let itemsElem = []
        let { date } = this.props
        this.date = moment(date)
        this.setLeftDay()
        for (let i = 0; i < 7; i++) {
            let addDay = i === 0 ? 0 : 1
            let { className, day, month, week } = this.getDay(addDay)
            itemsElem.push(
                <div key={i} className={className}>
                    <div className="_item__wrap">
                        <div className="_item__tit">
                            <i>{day}</i>
                            <span>{month}</span>
                        </div>
                        <div className="_item__sub">{week}</div>
                    </div>
                </div>
            )
        }
        return itemsElem
    }

    setLeftDay() {
        this.leftDay = this.date.daysInMonth() - this.date.date()
    }

    getDay(addDay: number): IDay {
        let date = this.date
        if (addDay > 0) date.add(addDay, 'day')
        
        let day = date.date()
        let now = this.checkNowDate(date)
        let other = this.checkOtherDay(day)
        
        let className = '_item'
        if (now) className += ' --now'
        if (other) className += ' --other'
        
        let month = moment.months(date.month())
        let week =  moment.weekdays(date.day())

        return { className, day, month, week }
    }

    checkNowDate(date: moment.Moment): boolean {
        let state = false
        let dateState = date.date() === this.nowDate.date()
        let monthState = date.month() === this.nowDate.month()
        let yearState = date.year() === this.nowDate.year()
        if (dateState && monthState && yearState) {
            state = true
        }
        return state
    }

    checkOtherDay(num: number): boolean {
        let state = false
        if (this.leftDay < 7) {
            if (num >= 1 && num <= 7) {
                state = true
            }
        }
        return state
    }

    render() {
        let { setDate } = this.props
        return (
            <div className="rpl-dates" ref={this.refElem}>
                <IconComponent 
                    icon={IconList.slideLeft} 
                    className="_arr" 
                    onClick={() => setDate(-1, 'w')}
                />
                <div className="_wrap">
                    {this.renderItems()}
                </div>
                <IconComponent 
                    icon={IconList.slideRight} 
                    className="_arr" 
                    onClick={() => setDate(1, 'w')}
                />
            </div>
        )
    }
}

export default DatesPlanner