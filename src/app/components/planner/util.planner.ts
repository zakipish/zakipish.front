import moment from 'moment'

export const minToStr = (min) => {
    let h = Math.floor(min / 60) + ''
    let m = min % 60 + ''
    h = h.length < 2 ? `0${h}` : h
    m = m.length < 2 ? `0${m}` : m
    return `${h}:${m}`
}

export const getMinutes = (date: moment.Moment) => {
    return date.hour() * 60 + date.minute()
}

export const selectedPriceReducer = (resItem, item) => {
    let { durations } = item
    let itemPrice = durations.reduce((resDur, dur) => {
        return resDur + dur.price
    }, 0)
    return resItem + itemPrice
}

export const selectedDurationReducer = (resItem, item) => {
    let { date, durations } = item
    let itemDuration = durations.reduce((resDur, dur) => {
        let from = moment(`${date} ${dur.from}`)
        let to = moment(`${date} ${dur.to}`)
        let diff = to.diff(from, 'minutes')
        return resDur + diff
    }, 0)
    return resItem + itemDuration
}

export const getDateRangeSelected = (selected) => {
    return selected.map(item => {
        let { date } = item
        let { from } = item.durations[0]
        let { to } = item.durations[item.durations.length - 1]
        return { date, from, to }
    })
}
