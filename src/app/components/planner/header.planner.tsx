import * as React from 'react'

interface IProps {
    children: any
}

class HeaderPlanner extends React.Component<IProps, {}> {
    render() {
        let { children } = this.props
        return (
            <div className="rpl-header">
                {children}
            </div>
        )
    }
}

export default HeaderPlanner