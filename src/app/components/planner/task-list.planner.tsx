import * as React from 'react'
import moment from 'moment'
import { Game } from '../../models/game'

interface IProps {
    games: Game[]
    date: moment.Moment
    interval: number
    gridRef: React.RefObject<HTMLDivElement>
    onSelected: (game: Game, space: GameSpace, cellSize) => void
}

interface IState {
    cellSize: {
        width: number
        height: number
    }
}

export interface GameSpace {
    y: number
    x: number
    width: number
    height: number
}

class TaskListPlanner extends React.Component<IProps, IState> {

    setCellTimer;
    weekDay: number = 0;
    timeSection: number = 0;
    durationMin: number = 0;

    state = {
        cellSize: {
            width: 0,
            height: 0,
        }
    }

    componentDidMount() {
        this.setCellSize()
        window.addEventListener('resize', this.setCellSize)
    }
    
    componentWillUnmount() {
        clearTimeout(this.setCellTimer)
        window.removeEventListener('resize', this.setCellSize)
    }

    setCellSize = () => {
        if (this.setCellTimer) clearTimeout(this.setCellTimer)
        this.setCellTimer = setTimeout(() => {
            let cellElem = this.props.gridRef.current.querySelector('._cell')
            let { clientHeight, clientWidth } = cellElem
            let cellSize = {
                width: clientWidth + 1,
                height: clientHeight + 1,
            }
            this.setState({cellSize})
        }, 300)
    }

    renderCards() {
        let gamesElem = []
        let { cellSize } = this.state
        let { games, interval } = this.props
        games.forEach((el, i) => {
            let game = new Game(el)
            this.setCardSpace(game)
            let space = this.getCardSpace()
            let className = '_task'
            if (this.durationMin === interval) className += ' --small'
            gamesElem.push(
                <div 
                    key={i} 
                    className={className} 
                    style={this.getCardStyle(space)}
                    onClick={() => this.props.onSelected(game, space, cellSize)}
                >
                    <div className="_task__tit">{el.name}</div>
                    <div className="_task__sub"><span>Участники</span> 0 / 0</div>
                    <div className="_task__price">{el.price} РУБ</div>
                </div>
            )
        })
        return gamesElem
    }

    setCardSpace(game: Game) {
        let from = game.fromDate
        let to = game.toDate

        let fromDay = from.date()
        let propsDay = this.props.date.date()

        let monthState = from.month() === this.props.date.month()
        if (!monthState) fromDay += this.props.date.daysInMonth()

        this.weekDay = fromDay - propsDay
        this.timeSection = from.hour() * 60 + from.minute()

        let durationDate = moment.duration(to.diff(from))
        this.durationMin = durationDate.hours() * 60 + durationDate.minutes()
    }

    getCardSpace(): GameSpace {
        let { interval } = this.props
        let { width, height } = this.state.cellSize
        let minuteHeight = height / interval
        return {
            y: this.timeSection * minuteHeight,
            x: this.weekDay * width,
            width: width,
            height: this.durationMin * minuteHeight,
        }
    }

    getCardStyle(space) {
        return {
            top: space.y + 'px',
            left: space.x + 'px',
            width: space.width + 'px',
            height: space.height + 'px',
        }
    }

    render() {
        return (
            <div className="rpl-tasks">
                {this.renderCards()}
            </div>
        )
    }
}

export default TaskListPlanner