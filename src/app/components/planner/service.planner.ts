import moment from 'moment'
import { reqGet } from '../../core/api'
import { Game } from './../../models/game'
import Price from '../../models/price'

export const setDate = (inDate: moment.Moment, addNum, key): moment.Moment => {
    let date = moment(inDate)
    if (addNum) {
        if (addNum > 0) {
            date.add(addNum, key)
        } 
        else if (addNum < 0) {
            date.subtract(Math.abs(addNum), key)
        }
        if (key === 'w') date.startOf('week')
        if (key === 'M') {
            date.startOf('month')
            if (date.day() !== 1) {
                let addDay = 7 - date.day() + 1
                date.add(addDay, 'd')
            }
        }
    }
    return date
}

export const reqPrices = async (id: number , date: moment.Moment) => {
    let dateRange = getDateRangeParam(date)
    let res = await reqGet(`/playgrounds/${id}/prices`, {...dateRange})
    let timeEnd = res.data.timeEnd
    if (timeEnd === '24:00') timeEnd = "23:59:59"
    return {
        prices: res.data.dates.map(el => new Price(el)),
        timeStart: res.data.timeStart,
        timeEnd: timeEnd,
        interval: res.data.interval,
    }
}

export const reqGames = async (id: number, date: moment.Moment) => {
    let dateRange = getDateRangeParam(date)
    let res: any = await reqGet(`/playgrounds/${id}/games`, {...dateRange})
    return res.data.map(el => new Game(el))
}

const getDateRangeParam = (date: moment.Moment) => {
    return {
        dateStart: moment(date).format('YYYY-MM-DD'),
        dateEnd: moment(date).add(6, 'd').format('YYYY-MM-DD'), 
    }
}