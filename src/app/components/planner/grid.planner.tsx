import * as React from 'react'
import DatePrice from '../../models/datePrice'
import Price from '../../models/price'
import { SpaceCrood, PlannerSelectDay } from './planner'

interface IProps {
    prices: DatePrice[]
    rowLen: number
    lineLen: number
    children?: any
    spaceCrood?: SpaceCrood
    setSpaceCrood?: (spaceCrood: SpaceCrood, cellSize) => void
}

class GridPlanner extends React.Component<IProps, {}> {

    refWrap: React.RefObject<HTMLDivElement> = React.createRef()

    onSelected = (x, y) => {
        let { spaceCrood, setSpaceCrood } = this.props
        let { x1, x2, y1, y2 } = spaceCrood
        let price = this.getPrice(x, y)
        let clear = (price.state !== 'open') || y1 === y || y2 === y
        if (clear) {
            this.clearSpaceCrood()
            return
        } 
        if (x === x1) {
            if (y1 < 0) y1 = y
            if (y1 > y) {
                y1 = y2 = y
            } else {
                y2 = y
            }
        } else {
            y1 = y2 = y
        }
        x1 = x2 = x
        setSpaceCrood({x1, x2, y1, y2}, this.cellSize)
    }
    
    clearSpaceCrood = () => {
        this.props.setSpaceCrood({x1: -1, x2: -1, y1: -1, y2: -1}, this.cellSize)
    }

    get cellSize() {
        let refWrap = this.refWrap.current
        let { clientHeight, clientWidth } = refWrap.querySelector('._cell')
        return {
            width: clientWidth + 1,
            height: clientHeight + 1,
        }
    }

    renderRows(): React.ReactFragment {
        let rowsElem = []
        let { rowLen, prices, spaceCrood } = this.props
        let loading = !prices || !prices.length
        for (let x = 0; x < rowLen; x++) {
            let className = '_row'
            if (x >= spaceCrood.x1 && x <= spaceCrood.x2) className += ` rowSelect`
            rowsElem.push(
                <div key={x} className={className}>
                    {!loading ? (
                        this.renderCells(x) 
                    ) : (
                        <div className="_cell"></div>
                    )}
                </div>
            )
        }
        return rowsElem
    }

    renderCells(x: number): React.ReactFragment {
        let linesElem = []
        let { lineLen, spaceCrood } = this.props
        for (let y = 0; y < lineLen; y++) {
            let price = this.getPrice(x, y)
            let className = '_cell'
            if (price.state) className += ` --${price.state}`
            if (
                x >= spaceCrood.x1 && x <= spaceCrood.x2 &&
                y >= spaceCrood.y1 && y <= spaceCrood.y2
            ) className += ` cellSelect`
            linesElem.push(
                <div key={y} className={className} onClick={() => this.onSelected(x, y)}>
                    <span>{price.value} РУБ</span>
                </div>
            )
        }
        return linesElem
    }

    getPrice(x, y): Price {
        let { prices } = this.props
        if (prices[x]) {
            if (prices[x].prices[y]) {
                return prices[x].prices[y]
            }
        }
        return new Price()
    }

    render() {
        let { children } = this.props
        return (
            <div className="rpl-grid" ref={this.refWrap}>
                {this.renderRows()}
                {children}
            </div>
        )
    }
}

export default GridPlanner