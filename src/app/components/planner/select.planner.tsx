import * as React from 'react'
import moment from 'moment'
import { PlannerSelectDay, SpaceCrood } from './planner'

interface IProps {
    refTasks?: React.RefObject<HTMLDivElement>
    children?: any
    popups?: any
    oneRow?: boolean
    spaceCrood: SpaceCrood
    setSpaceCrood: (spaceCrood: SpaceCrood) => void
    onSelected: (
        spaceCrood: SpaceCrood,
        cellSize: { width: number, height: number }
    ) => void
}

enum DirectV {
    up = 'up',
    down = 'down',
}

enum DirectH {
    left = 'left',
    right = 'right',
}

class SelectPlanner extends React.Component<IProps, {}> {

    refWrap: React.RefObject<HTMLDivElement> = React.createRef()
    refBlock: React.RefObject<HTMLDivElement> = React.createRef()
    refPopups: React.RefObject<HTMLDivElement> = React.createRef()

    selectStateMove: boolean = false

    startCrood = { x: 0, y: 0 }
    endCrood = { x: 0, y: 0 }
    space = { left: 0, top: 0, right: 0, bottom: 0 }
    
    cellSize = { width: 0, height: 0 }
    wrapOffset = { x: 0, y: 0 }
    scrollBody = { x: 0, y: 0 }
    cursorCrood = { x: 0, y: 0 }

    directV: DirectV = null
    directH: DirectH = null
    blocking: number = null

    minBlockHeight = 10

    // selectedList: PlannerSelectDay[] = []

    taskClick: boolean = false

    componentDidMount() {
        this.onResize()
        window.addEventListener('keydown', this.onKeyPress)
        window.addEventListener('mouseup', this.onCursorUp)
        window.addEventListener('resize', this.onResize)
    }
    
    componentWillUnmount() {
        window.addEventListener('keydown', this.onKeyPress)
        window.removeEventListener('mouseup', this.onCursorUp)
        window.removeEventListener('resize', this.onResize)
    }

    onKeyPress = (e) => {
        if (e.key === 'Escape') {
            e.preventDefault()
            this.hideSelect()
        }
    }

    onResize = () => {
        setTimeout(() => {
            this.setCellSize()
            this.setOffset()
        }, 300)
    }
    
    setCellSize() {
        let refWrap = this.refWrap.current
        if (!refWrap) return
        let { clientHeight, clientWidth } = refWrap.querySelector('._cell')
        this.cellSize = {
            width: clientWidth + 1,
            height: clientHeight + 1,
        }
    }

    setOffset() {
        let refWrap = this.refWrap.current
        if (!refWrap) return
        let rect = refWrap.getBoundingClientRect()
        this.wrapOffset.x = rect.left + window.pageXOffset
        this.wrapOffset.y = rect.top + window.pageYOffset
    }
    
    get scrollParent() {
        let refWrap = this.refWrap.current
        let rplElem = refWrap.parentElement.parentElement.parentElement
        return {
            x: rplElem.scrollLeft,
            y: rplElem.scrollTop,
        }
    }

    onCursorDown = (e) => { 
        let { refTasks } = this.props
        let refPopups = this.refPopups.current
        let popupClick = refPopups.contains(e.target)
        this.taskClick = false
        if (refTasks) {
            let tasksElem = refTasks.current.querySelectorAll('._task')
            tasksElem.forEach((task) => {
                if (task.contains(e.target)) {
                    this.taskClick = true
                }
            })
            if (this.taskClick) {
                this.hideSelect()
            }
        }
        if (popupClick === false && this.taskClick === false) {
            this.initSpace(e)
            this.transformSpace()
        }
    }

    onCursorUp = (e) => { 
        this.onSelected()
        this.selectStateMove = false 
        let refWrap = this.refWrap.current
        if (refWrap.contains(e.target) === false) {
            this.hideSelect()
        }
    }

    onCursorMove = (e) => {
        if (this.selectStateMove) {
            this.setSpace(e)
            this.transformSpace()
        }
    }

    initSpace(e) {
        let { clientWidth, clientHeight } = this.refWrap.current
        let scroll = this.scrollParent
        this.selectStateMove = true 
        this.space.left = this.startCrood.x = e.pageX + scroll.x - this.wrapOffset.x
        this.space.top = this.startCrood.y = e.pageY + scroll.y - this.wrapOffset.y
        this.space.right = clientWidth - (e.pageX + scroll.x - this.wrapOffset.x)
        this.space.bottom = clientHeight - (e.pageY + scroll.y - this.wrapOffset.y)

        if (this.props.oneRow === true) {
            this.space.right = this.startCrood.x = clientWidth
        }
    }

    setSpace = (e) => {
        let { clientWidth, clientHeight } = this.refWrap.current
        let scroll = this.scrollParent
        let x = this.endCrood.x = e.pageX + scroll.x - this.wrapOffset.x
        let y = this.endCrood.y = e.pageY + scroll.y - this.wrapOffset.y
        if ( x > this.startCrood.x ) {
            this.space.right = clientWidth - x
            this.directH = DirectH.left
        } else {
            this.space.left = x
            this.directH = DirectH.right
        }
        if ( y > this.startCrood.y ) {
            this.space.bottom = clientHeight - y
            this.directV = DirectV.down
        } else {
            this.space.top = y
            this.directV = DirectV.up
        }
    }

    transformSpace = () => {
        let refBlock = this.refBlock.current
        refBlock.style.top = this.space.top + 'px'
        refBlock.style.left = this.space.left + 'px'
        refBlock.style.right = this.space.right + 'px'
        refBlock.style.bottom = this.space.bottom + 'px'

        let { clientHeight } = refBlock
        if (clientHeight < this.minBlockHeight) {
            this.hideSelect()
        } else {
            this.showSelect()
        }
    }

    hideSelect = () => {
        this.refBlock.current.classList.remove('--show')
        this.clearSelectCell()
    }
    
    showSelect = () => {
        this.onSelectCell()
        this.refBlock.current.classList.add('--show')
    }

    onSelectCell() {
        let { width, height } = this.cellSize
        let { offsetTop, offsetLeft, offsetHeight, offsetWidth } = this.refBlock.current
        let x1 = Math.floor(offsetLeft / width)
        let x2 = Math.floor((offsetLeft + offsetWidth) / width)
        let y1 = Math.floor(offsetTop / height)
        let y2 = Math.floor((offsetTop + offsetHeight) / height)
        
        if (this.props.oneRow === true) x2 = x1
        this.props.setSpaceCrood({ x1, x2, y1, y2 })
    }


    clearSelectCell = () => {
        this.blocking = null
        if (this.taskClick === false) {
            this.props.onSelected({x1: -1, x2: -1, y1: -1, y2: -1}, this.cellSize)
        }
    }

    onSelected() {
        if (this.selectStateMove) {
            let { clientHeight } = this.refBlock.current
            if (clientHeight > this.minBlockHeight) {
                this.props.onSelected(this.props.spaceCrood, this.cellSize)
            }
        }
    }

    render() {
        let { children, popups } = this.props
        return (
            <div 
                className="rpl-select-wrap" 
                ref={this.refWrap}
                onMouseDown={this.onCursorDown}
                onMouseMove={this.onCursorMove}
            >
                {children}
                <div className="rpl-select-block" ref={this.refBlock}></div>
                <div ref={this.refPopups}>
                    {popups}
                </div>
            </div>
        )
    }
}

export default SelectPlanner