import * as React from 'react'
import { minToStr } from './util.planner'

interface IProps {
    fromMin: number
    toMin: number
    interval: number
}

class TimesPlanner extends React.Component<IProps, {}> {

    renderItems(): React.ReactFragment {
        let itemsElem = []
        let { fromMin, toMin, interval } = this.props
        for (let min = fromMin; min < toMin; min += interval) {
            let minStr = minToStr(min)
            itemsElem.push(
                <div key={min} className="_item">{minStr}</div>    
            )  
        } 
        return itemsElem
    }

    render() {
        return (
            <div className="rpl-times">
                {this.renderItems()}
            </div>
        )
    }
}

export default TimesPlanner