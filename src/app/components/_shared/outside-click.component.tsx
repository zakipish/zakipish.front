import * as React from 'react'

export interface IOutsideClickProps {
    hide?: boolean
    onHide?: Function
}

export interface IOutsideClickState {
    hide?: boolean
}

class OutsideClickComponent<P extends IOutsideClickProps, S extends IOutsideClickState> extends React.Component<P, S> {

    outsideRef: React.RefObject<HTMLDivElement> = React.createRef()

    componentDidMount() {
        document.addEventListener('mousedown', this.onClickOutside)
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.onClickOutside)
    }

    get hide(): boolean {
        return this.props.hide !== undefined ? this.props.hide : this.state.hide
    }

    checkContains = (e): boolean => {
        let selectRef = this.outsideRef.current
        return !selectRef.contains(e.target)
    }

    onHide = () => {
        if (this.props.onHide) this.props.onHide()
    }

    onClickOutside = (e) => {
        if(!this.hide && this.checkContains(e)) {
            this.onHide()
        }
    }

}

export default OutsideClickComponent
