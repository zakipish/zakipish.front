import * as React from 'react'
import { renderToStaticMarkup } from 'react-dom/server'
import { Router } from 'react-router'
// @ts-ignore
import { Clusterer, Map, Placemark, YMaps } from 'react-yandex-maps'
import { history } from '../../core/history'
import CardPlaygroundComponent, { ICardPlaygroundProps } from '../cards/card-playground.component'
import EventCard, { Props as GameCardProps } from '../cards/event.card'

interface ISectionMapProps {
  points: IPoint[]
  width: number
  height: number
  cardClassName?: string
}

interface IPoint {
  type?: string
  coordinates: number[]
  card?: GameCardProps | ICardPlaygroundProps
}

class SectionMapComponent extends React.Component<ISectionMapProps> {

  get centerOfMap() {
    const { points } = this.props

    const xSum = points.reduce((acc, point) => acc + point.coordinates[0], 0)
    const ySum = points.reduce((acc, point) => acc + point.coordinates[1], 0)

    const x = xSum / points.length
    const y = ySum / points.length
    return [x, y]
  }

  public getClassname() {
    const { cardClassName } = this.props
    const classname = 'card-list__item'
    return `${classname} ${cardClassName ? cardClassName : ''}`
  }

  public getCardElementByType(el, i) {
    const { type, card } = el
    switch (type) {
      case 'game':
        return <EventCard
          key={i}
          {...card}
        />
      case 'event':
        return <EventCard
          key={i}
          {...card}
        />
      case 'playground':
        return <CardPlaygroundComponent
          key={i}
          className={this.getClassname()}
          {...card}
        />
      default:
        return null
    }
  }

  public render() {
    const { points, width, height } = this.props
    // FIXME!
    // tslint:disable-next-line:no-console
    console.log({ center: this.centerOfMap })

    if (!Array.isArray(points) || points.length < 1) {
      return ''
    }

    return (
      <YMaps>
        <Map
          defaultState={{ center: this.centerOfMap, zoom: 10, controls: ['zoomControl'] }}
          width={width} height={height}
          modules={['control.ZoomControl']}
        >
          <Clusterer
            options={{
              preset: 'islands#invertedVioletClusterIcons',
              clusterDisableClickZoom: true,
              clusterHideIconOnBalloonOpen: false,
              geoObjectHideIconOnBalloonOpen: false
            }}
            modules={['clusterer.addon.balloon', 'clusterer.addon.hint']}
          >
            {points.filter(point => !!point.coordinates).map((point, i) => (
              <Placemark
                key={i}

                geometry={{
                  type: 'Point',
                  coordinates: point.coordinates
                }}

                properties={{
                  clusterCaption: point.card && point.card.title || '-',
                  balloonContent: renderToStaticMarkup(
                    <Router history={history}>
                      {this.getCardElementByType(point, i)}
                    </Router>
                  )
                }}

                options={{
                  balloonMinWidth: 328,
                  balloonMinHeight: point.type === 'event' ? 415 : point.type === 'game' ? 403 : null,
                  iconLayout: 'default#image',
                  iconImageHref: point.type === 'event' ? '/assets/images/event-point.png' : '/assets/images/default-point.png',
                  iconImageSize: [44, 71],
                  iconOffset: [-44, -35]
                }}

                modules={['geoObject.addon.balloon', 'geoObject.addon.hint']}
              />
            ))}
          </Clusterer>
        </Map>
      </YMaps>
    )
  }

  protected unique = (array: any[]) => {
    return array.filter((value, index) => array.indexOf(value) === index)
  }
}

export default SectionMapComponent
