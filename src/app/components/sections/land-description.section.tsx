import * as React from 'react'

interface IProps {
    subtitle?: string
    title?: string
    text?: string
}

class LandDescriptionSection extends React.Component<IProps, {}> {
    render() {
        let { subtitle, title, text } = this.props
        return (
            <div className="land-desc">
                <div className="land-desc__wrap">
                    { subtitle && <div className="land-desc__subtit">{subtitle}</div> }
                    { title && <div className="land-desc__tit">{title}</div> }
                    { text && 
                        <div 
                            className="land-desc__txt" 
                            dangerouslySetInnerHTML={{__html: text}}
                        ></div> }
                </div>
            </div>
        )
    }
}

export default LandDescriptionSection