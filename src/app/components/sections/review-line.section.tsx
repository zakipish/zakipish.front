import * as React from 'react'
import moment from 'moment'
import RatingComponent from '../blocks/rating/rating.component'

interface IProps {
    title?: string
    length?: number
    list: IReviewItem[]
}

interface IReviewItem {
    name: string
    img?: string
    date?: string
    rating?: number
    comment: string
}

class ReviewLineSection extends React.Component<IProps, {}> {
    render() {
        let { title, length, list } = this.props
        return (
            <div className="review-line">
                <div className="wrap">
                    { title && 
                        <div className="review-line__tit">
                            {title}<span>{length}</span>
                        </div> 
                    }
                    <div className="review-line__list">
                        { list.map((el, i) => { return(
                            <div key={i} className="_item">
                                <div className="_item__img">
                                    { el.img ? 
                                        <img src={el.img} alt="#"/> 
                                        : 
                                        <span>{el.name[0]}</span>
                                    }
                                </div>
                                <div className="_item__body">
                                    <div className="_item__name">
                                        {el.name}<span>{el.date || moment().format('DD.MM.YYYY h:m') }</span>
                                    </div>
                                    <div className="_item__rating">
                                        <RatingComponent act={el.rating || 0}/>
                                    </div>
                                    <div className="_item__tit">Комментарий</div>
                                    <div className="_item__txt">{el.comment}</div>
                                </div>
                            </div>
                        )}) }
                    </div>
                    <div className="review-line__endLink">Смотреть ещё</div>
                </div>
            </div>
        )
    }
}

export default ReviewLineSection