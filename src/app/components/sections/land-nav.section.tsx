import * as React from 'react'
import { Link } from 'react-router-dom';
import IconComponent, { IconList } from '../blocks/icon/icon.component'
import { goToAnchor } from "../../core/utils"

interface IProps {
    stopSelector?: string 
    list: LandNavItem[]
}

interface IState {
    addTop: number
    fixed: boolean
}

interface LandNavItem {
    tit: string
    addTxt?: string
    url?: string
    anchor?: string
    className?: string
}

class LandNavSection extends React.Component<IProps, IState> {
    
    refNav: React.RefObject<HTMLDivElement> = React.createRef()
    offsetTop: number = 0

    state = {
        addTop: 0,
        fixed: false
    }

    componentDidMount() {
        this.onResize()
        window.addEventListener('scroll', this.onScroll)
        window.addEventListener('resize', this.onResize)
    }
    
    componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll)
        window.removeEventListener('resize', this.onResize)
    }

    onResize = () => {
        let { stopSelector } = this.props
        if (stopSelector) {
            let stopElem = document.querySelector(stopSelector)
            if (stopElem) {
                let addTop = stopElem.clientHeight
                this.setState({addTop})
            }
        }
    }

    onScroll = () => {
        let { addTop, fixed } = this.state
        let rect = this.refNav.current.getBoundingClientRect()
        fixed = (rect.top - addTop < 0)
        if (this.state.fixed !== fixed) {
            this.setState({fixed})
        }
    }

    getItemnav(item: LandNavItem, i: number) {
        let elem
        let { tit, addTxt, url, anchor, className } = item
        if (!className) className = ''
        className = `land-nav__item ${className}`
        if (anchor) {
            elem = (
                <div 
                    key={i} 
                    className={className} 
                    onClick={goToAnchor(anchor)}
                >{tit} {addTxt && <span>{addTxt}</span>}</div>
            )
        } else {
            elem = (
                <Link 
                    key={i} 
                    to={url || '#'} 
                    className={className}
                >{tit} {addTxt && <span>{addTxt}</span>}</Link>
            )
        }
        return elem
    }

    get className () {
        let out = 'land-nav'
        let { fixed } = this.state
        if (fixed) out += ' --fix'
        return out
    }

    get style () {
        let { addTop } = this.state
        let style = {
            top: addTop + 'px'
        }
        return style
    }
        
    render() {
        let { list } = this.props
        return (
            <div className={this.className} ref={this.refNav}>
                <div className="land-nav__shadow" style={this.style}>
                    <IconComponent className="land-nav__arr --prev" icon={IconList.slideLeft}/>
                    <div className="land-nav__wrap">
                        { list.map((el, i) => this.getItemnav(el, i)) }
                    </div>
                    <IconComponent className="land-nav__arr --next" icon={IconList.slideRight}/>
                </div>
            </div>
        )
    }
}

export default LandNavSection