import * as React from 'react'
import moment from 'moment'
import LabelComponent, { ILabel, LabelStatus } from '../blocks/label.component'
import LabelDatepicker from '../blocks/datepicker/label.datepicker'

interface IProps {
    list: ILabel[]
    onChange?: (i: number) => void
    selected?: number

    date?: moment.Moment
    onChangeDate?: (date: moment.Moment) => void
}

interface IState {
    buttonsCount: number
    loading: boolean
}

class SectionDtComponent extends React.Component<IProps, IState> {
    resizeTimeout
    listRef = React.createRef<HTMLDivElement>()
    state: IState = {
        buttonsCount: 1,
        loading: false
    }

    componentDidMount() {
        this.setButtonsCount()
        window.addEventListener('resize', this.setButtonsCount)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.setButtonsCount)
    }

    setButtonsCount = () => {
        if (this.resizeTimeout != null)
            clearTimeout(this.resizeTimeout)
        
        if (!this.state.loading) {
            this.setState({loading: true})
        }

        this.resizeTimeout = setTimeout(() => {
            const buttonMargin = 3

            if (this.listRef.current == null)
                return false

            const { clientWidth, children } = this.listRef.current

            if (children.length < 1)
                return false

            const buttonsCount = Math.floor((clientWidth - 51) / (children[0].clientWidth + buttonMargin * 2))
            this.setState({buttonsCount, loading: false})
        }, 200)
    }

    onClick = (i: number) => (e : any) => {
        const { onChange, selected, list } = this.props
        if (i !== selected && !list[i].disabled) {
            if (typeof onChange === 'function')
                onChange(i)
        }
    }

    render() {
        const { list, selected, date, onChangeDate } = this.props
        const { buttonsCount, loading } = this.state
        
        return (
            <div className="dt-list" ref={this.listRef}>
                { list ? list.slice(0, buttonsCount).map((el, i) =>
                    <LabelComponent
                        key={i}
                        onClick={this.onClick(i)}
                        status={el.disabled ? LabelStatus.disable :
                            i === selected ? LabelStatus.animation :
                            null}
                        {...el}
                    />
                ) : '' }
                { (date && !loading) ?
                    <LabelDatepicker
                        date={date}
                        onChange={onChangeDate}
                        autoHide={true}
                        disablePast={true}
                    />
                : '' }
            </div>
        )
    }
}

export default SectionDtComponent
