import * as React from 'react'

interface IProps {
    className?: string
}

class TabsSection extends React.Component<IProps, {}> {
    get className() {
		let str = 'tabsSection'
		let { className } = this.props
		str += className ? ` ${className}` : ''
		return str
	}

    render() {
        let { children } = this.props
        return (
            <div className={this.className}>
                {children}
            </div>
        )
    }
}

export default TabsSection
