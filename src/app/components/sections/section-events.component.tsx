import * as React from 'react'

import { Event, EventType } from '../../models/event'

import SectionComponent from '../../components/layout/section.component'
import EventCard from '../../components/cards/event.card'
import GridEvent from '../../components/layout/event/grid.event'
import Button from '../../components/blocks/button'
import Loader from '../../components/layout/loader'

interface Props {
    events: Event[]
    total?: number
    loading: boolean
    onDownload?: () => void
}

class SectionEventsComponent extends React.Component<Props> {
    render() {
        const { events, total, loading, onDownload } = this.props

        return (
            <SectionComponent title={`НАЙДЕНО КИПИША ${events.length}`}>
                <GridEvent>
                    { events && events.map(event => (
                        <EventCard
                            key={event.id}
                            type={event.type == EventType.Game ? "game" : "meeting"}
                            image={event.images && event.images.length > 0 && event.images[0].url}
                            title={event.title}
                            club={event.club}
                            gameLevel={event.gameLevel}
                            sportName={event.sport.name}
                            address={event.address || event.club.address}
                            price={event.price + ''}
                            players={event.users}
                            max_players={event.max_players}
                            min_players={event.min_players}
                            date={{
                                from: event.intervalStartMoment,
                                to: event.intervalEndMoment,
                            }}
                            url={`/events/${event.slug}`}
                        />
                   ))}
                </GridEvent>
                <hr/>
                <br/>
                {loading ? (
                    <Loader state={loading} loaderClassName="--auto-height" />
                ) : (
                        total > 2 && events.length < total && (
                            <Button
                                offerTop={`Вы посмотрели ${events.length} из ${total}`}
                                onClick={onDownload}
                            >Загрузить еще</Button>
                        )
                )}
            </SectionComponent>
        )
    }
}

export default SectionEventsComponent
