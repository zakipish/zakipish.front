import * as React from 'react'
import ScrollIconComponent, { IScrollIconLi } from '../blocks/scroll-icon/scroll-icon.component'
import FilterComponent, { IFilterProps } from '../layout/filter.component'

interface IProps {
    scrollIcon: IFaceScroll[]
}

export interface IFaceScroll {
    list?: IScrollIconLi[]
    filter?: IFilterProps
    classname?: string
}

class SectionFaceComponent extends React.Component<IProps, {}> {
    render() {
        let { scrollIcon } = this.props
        return (
            <div className="face">
                <div className="face__wrap">
                    { scrollIcon ? scrollIcon.map((el, i) => { return (
                        <div className={`face__scroll ${el.classname ? el.classname : ''}`} key={i}>
                            <ScrollIconComponent
                                list={el.list}
                                filter={ el.filter ?
                                    <FilterComponent
                                        search={el.filter.search}
                                        list={el.filter.list}
                                    />
                                : null }
                            />
                        </div>
                    )}) : '' }
                </div>
            </div>
        )
    }
}

export default SectionFaceComponent
