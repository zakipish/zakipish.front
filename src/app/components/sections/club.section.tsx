import * as React from 'react'
import { Link } from 'react-router-dom'
import SwipeDetect from '../../core/SwipeDetect'
import {resizeImage} from '../UI/Image'

interface Props {
    to?: string
    logo?: string
    title?: string
    address?: string
    toAddress?: string
    mobileSwipe?: boolean
}

interface State {
    expanded: boolean
}

class ClubSection extends React.Component<Props, State> {
    state: State = {
        expanded: false
    }

    static defaultProps: Props = {
        to: '#',
        logo: '#',
        title: '-',
        address: '-',
        toAddress: '#',
        mobileSwipe: true,
    }

    refBody: React.RefObject<HTMLDivElement> = React.createRef()
    swipedetect
    slideIndex: number = 0

    componentDidMount() {
        let { mobileSwipe } = this.props
        let refBody = this.refBody.current
        if (!refBody || !mobileSwipe) return
        this.swipedetect = new SwipeDetect(refBody, this.onSwipe)
    }

    componentWillUnmount() {
        if(this.swipedetect) {
            this.swipedetect.detstroy()
        }
    }

    onSwipe = (dir) => {
        if (dir === 'right') {
            this.setSlide(-1)
        } else if (dir === 'left') {
            this.setSlide(1)
        }
    }

    setSlide = (addIndex) => {
        let refBody = this.refBody.current
        if (!refBody) return
        let { children } = this.props
        let slideIndex = this.slideIndex
        let len = Array.isArray(children) ? children.length - 1 : 0
        slideIndex += addIndex
        if (slideIndex > len) {
            slideIndex = 0
        } else if (slideIndex < 0) {
            slideIndex = len
        }
        let offset = window.innerWidth <= 640 ? 10 : 20
        refBody.style.transform = `translateX(calc((-100vw + ${offset}px) * ${slideIndex}))`
        this.slideIndex = slideIndex
        this.forceUpdate()
    }

    renderDots = () => {
        let { children } = this.props
        let lenDot = Array.isArray(children) ? children.length - 1 : 0
        let dots: React.ReactNodeArray = []
        for (let i = 0; i < lenDot; i++) {
            let className = i === this.slideIndex ? 'act' : ''
            dots.push(<i key={i} className={className}></i>)
        }
        return (
            <div className="clubSec__dots">
                {dots}
            </div>
        )
    }

    expand = () => {
        this.setState({ expanded: true })
    }

    render() {
        const { expanded } = this.state
        const { children, to, title, address, toAddress, mobileSwipe } = this.props
        const childrenLen = Array.isArray(children) ? children.length : 1

        const logo = resizeImage(this.props.logo, 50, 50)

        return (
            <div className={`clubSec ${mobileSwipe ? '--mobileSwipe' : '' }`}>
                <div className="clubSec__top">
                    <Link
                        to={to}
                        className="clubSec__logo"
                        style={{backgroundImage: `url(${logo})`}}
                    ></Link>
                    <div className="clubSec__text">
                        <Link to={to} className="clubSec__tit">{title}</Link>
                        <div className="clubSec__subtit">
                            <span>{address}</span>
                            <Link to={toAddress}>Посмотреть на карте</Link>
                        </div>
                    </div>
                </div>
                <div
                    className="clubSec__body"
                    ref={this.refBody}
                >
                    {expanded || childrenLen <= 2 ? (
                        children
                    ) : (
                        <>
                            {children[0]}
                            {children[1]}
                        </>
                    )}
                </div>

                {(childrenLen > 1 && mobileSwipe) && (
                    this.renderDots()
                )}

                {!expanded && childrenLen > 2 && (
                    <div className="clubSec__bottom" onClick={this.expand}>
                        Посмотреть все залы
                    </div>
                )}
            </div>
        )
    }
}

export default ClubSection
