import * as React from 'react'

import { Game } from '../../models/game'

import SectionComponent from '../../components/layout/section.component'
import EventCard from '../../components/cards/event.card'
import GridEvent from '../../components/layout/event/grid.event'
import Button from '../../components/blocks/button'
import Loader from '../../components/layout/loader'

interface Props {
    games: Game[]
    total?: number
    loading: boolean
    onDownload?: () => void
}

class SectionGamesComponent extends React.Component<Props> {
    render() {
        const { games, total, loading, onDownload } = this.props

        return (
            <SectionComponent title={`НАЙДЕНО ИГР ${games.length}`}>
                <GridEvent>
                    { games.map(game => (
                        <EventCard
                            key={game.id}
                            type="game"
                            image={game.playground.avatar && game.playground.avatar.url}
                            title={game.name}
                            club={game.playground.club}
                            gameLevel={game.gameLevel}
                            sportName={game.sport.name}
                            address={game.playground.club.address}
                            price={game.price + ''}
                            players={game.users}
                            max_players={game.max_players}
                            min_players={game.min_players}
                            date={{
                                from: game.startTime,
                                to: game.endTime,
                            }}
                            url={`/game/${game.id}`}
                        />
                    ))}
                </GridEvent>
                <hr/>
                <br/>
                {loading ? (
                    <Loader state={loading} />
                ) : (
                    total && total > 2 && games.length < total && (
                        <Button
                            offerTop={`Вы посмотрели ${games.length} из ${total}`}
                            onClick={onDownload}
                        >Загрузить еще</Button>
                    )
                )}
            </SectionComponent>
        )
    }
}

export default SectionGamesComponent
