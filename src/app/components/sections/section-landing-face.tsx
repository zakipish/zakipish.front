import * as React from 'react'

interface IProps {
    children: any
}

class SectionLandingFace extends React.Component<IProps, {}> {

    refWrap: React.RefObject<HTMLDivElement> = React.createRef() 

    componentDidMount() {
        setTimeout(() => {
            this.setHeight()
            window.addEventListener('resize', this.setHeight)
        }, 300)
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.setHeight)
    }

    setHeight = () => {
        let refWrap = this.refWrap.current
        if (!refWrap) return null
        let cards = Array.from(refWrap.children).filter(el => (el.classList[0] === 'face-card'))
        if (!cards || cards.length < 2) return null
        refWrap.style.height = 'auto'
        let height = 0
        cards.forEach(card => {
            let h = card.clientHeight
            if (h > height) height = h
        })
        refWrap.style.height = height + 'px'
    }

    render() {
        let { children } = this.props
        return (
            <div className="face-card-block">
                <div className="land-page__wrap">
                    <div className="face-card-block__wrap" ref={this.refWrap}>
                        {children}
                    </div>
                </div>
            </div>
        )
    }
}

export default SectionLandingFace