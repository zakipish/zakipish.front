import * as React from 'react'
import LabelComponent, { ILabel, LabelStatus } from '../blocks/label.component'

interface IProps {
    list: ILabel[]
    onStartChange?: (i: number) => void
    onEndChange?: (i: number) => void
    selectedStart?: number
    selectedEnd?: number
}

class SectionTtComponent extends React.Component<IProps, {}> {
    onClick = (i: number) => () => {
        const { selectedStart, selectedEnd, list } = this.props

        if (list[i].disabled)
            return false

        if (i !== selectedStart && i !== selectedEnd) {
            if (typeof selectedStart === 'undefined' || selectedStart === null) {
                this.startChange(i)
            } else {
                if (typeof selectedEnd === 'undefined' || selectedEnd === null) {
                    if (i > selectedStart) {
                        const containsDisabled = list.some((el, j) => {
                            return j > selectedStart && j < i && el.disabled
                        })

                        if (containsDisabled) {
                            this.startChange(i)
                            this.endChange(null)
                        } else {
                            this.endChange(i)
                        }
                    } else {
                        const containsDisabled = list.some((el, j) => {
                            return j > i && j < selectedStart && el.disabled
                        })

                        this.startChange(i)

                        if (containsDisabled) {
                            this.endChange(null)
                        } else {
                            this.endChange(selectedStart)
                        }
                    }
                } else {
                    this.startChange(i)
                }
            }
        }
    }

    startChange = (i: number) => {
        const { onStartChange, onEndChange } = this.props
        if (typeof onStartChange === 'function') {
            onStartChange(i)
            onEndChange(null)
        }
    }

    endChange = (i: number) => {
        const { onEndChange } = this.props
        if (typeof onEndChange === 'function')
            onEndChange(i)
    }

    render() {
        const { list, selectedStart, selectedEnd } = this.props

        return (
            <div className="dt-list --mobScroll">
                { list ? list.map((el, i) => {
                    const selected =
                        selectedStart === i ? true :
                            selectedEnd === i ? true :
                                 (selectedStart < i && selectedEnd > i) ? true : false
                    return <LabelComponent
                        key={i}
                        onClick={this.onClick(i)}
                        status={el.disabled ? LabelStatus.disable : selected ? LabelStatus.animation : null}
                        {...el}
                    />
                }) : '' }
            </div>
        )
    }
}

export default SectionTtComponent
