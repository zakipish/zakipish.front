import * as React from 'react'
import IconComponent, { IconList } from '../blocks/icon/icon.component'
import PopupGallary from '../blocks/popup-gallary'

interface IProps {
    title?: string
    list: string[]
}

interface IState {
    animate: boolean
    lenView: number
    list: string[]
}

class LandSliderSection extends React.Component<IProps, IState> {

    refListWrap: React.RefObject<HTMLDivElement> = React.createRef()
    index: number = 1
    act: number = 1
    animate: boolean = false

    state = {
        animate: false,
        lenView: 4,
        list: this.props.list,
        listLen: this.props.list.length
    }
    
    componentDidMount() {
        this.transformList()
        setTimeout(() => {
            this.setWrapShift()
        }, 300)
        window.addEventListener('resize', this.setWrapShift)
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.setWrapShift)
    }

    transformList = () => {
        let { list, listLen, lenView } = this.state
        let lenState = (listLen >= lenView)
        if (lenState) {
            let copyList = JSON.parse(JSON.stringify(list))
            for (let i = 0; i < lenView; i++) {
                list.push(copyList.splice(0, 1)[0])
            }
            this.setState({list})
        } else {
            this.act = 0
        }
    }

    getItemClassName = (i: number) => {
        let className = '_item'
        let { lenView } = this.state
        let act = this.act
        if (i < act || i >= (act + lenView)) className += ' --hide'
        if (i === act - 1) className += ' --prev'
        if (i === act + 1) className += ' --next'
        if (i === act) className += ' --active'
        return className
    }

    setActive =  (dir: number) => {
        let { list, listLen, lenView } = this.state
        let lenState = (listLen >= lenView)
        if (!this.animate && lenState && dir !== 0) {
            this.act += dir
            this.animate = true
            if (dir > 0) { //next
                this.index += dir
                for (let i = 0; i < dir; i++) {
                    list.push(list.splice(0, 1)[0])
                }
            } else { // prev
                this.index--
                list.unshift(list.splice(list.length - 1, 1)[0])
            }
            this.checkIndex()
            this.setWrapShift()
            setTimeout(() => {
                this.act = 1
                this.animate = false
                this.setState({list}) 
                this.setWrapShift()
            }, 300)
        }
    }

    checkIndex () {
        let { listLen } = this.state
        if (this.index < 1) {
            this.index = listLen
        } else if (this.index > listLen) {
            this.index = 1
        }
    }

    setWrapShift = async () => {
        let { lenView } = this.state
        let refListWrap = this.refListWrap.current
        refListWrap.classList.toggle('--animate', this.animate)
        let { clientWidth } = refListWrap
        let shift = (clientWidth - (clientWidth * 0.33) - 20) / (lenView - 1)
        let items = refListWrap.querySelectorAll('._item')
        items.forEach((item, i) => {
            let addHide = (i < this.act || i >= (this.act + lenView))
            let addPrev = (i === this.act -1)
            let addNext = (i === this.act + 1)
            let addActive = (i === this.act)
            item.classList.toggle('--hide', addHide)
            item.classList.toggle('--prev', addPrev)
            item.classList.toggle('--next', addNext)
            item.classList.toggle('--active', addActive)
        })
        refListWrap.style.transform = `translateX(-${shift * this.act}px)`
    }

    render() {
        let { title } = this.props
        let { list, listLen, lenView } = this.state
        let lenState = (listLen >= lenView)
        return (
            <div className="land-slider">
                <div className="_wrap">
                    { title && <div className="_tit">{title}</div> }
                    <div className="_list">
                        <div className="_list__wrap" ref={this.refListWrap}>
                            { list.map((el, i) => { 
                                let style = { backgroundImage: `url('${el}')` }
                                return(
                                    <div key={i} className={'_item'} onClick={() => this.setActive(i - 1)}>
                                        <div className="_item__img" style={style}>
                                            <IconComponent
                                                className="_item__icon"
                                                icon="fullsize"
                                                onClick={() => PopupGallary.show(el)}
                                            />
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                    {lenState && (
                        <div className="_nav">
                            <div className="_nav__wrap">
                                <IconComponent 
                                    className="_nav__arr" 
                                    icon={IconList.slideLeft}
                                    onClick={() => this.setActive(-1)}
                                />
                                <div className="_nav__state">{this.index} из {listLen}</div>
                                <IconComponent 
                                    className="_nav__arr" 
                                    icon={IconList.slideRight}
                                    onClick={() => this.setActive(1)}
                                />
                            </div>
                        </div>
                    )}
                </div>
                <PopupGallary/>
            </div>
        )
    }
}

export default LandSliderSection