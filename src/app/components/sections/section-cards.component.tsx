import * as React from 'react'
import CardGameComponent, { ICardGameProps } from '../cards/card-game.component'
import CardPlaygroundComponent, { ICardPlaygroundProps } from '../cards/card-playground.component'

interface ISectionCardProps {
    list: ISectionCardsItem[]
    cols?: number
    cardClassName?: string
}

export interface ISectionCardsItem {
    type: string
    card: ICardGameProps | ICardPlaygroundProps
}

class SectionCardsComponent extends React.Component<ISectionCardProps, {}> {

    getClassname(type) {
        let { cols, cardClassName } = this.props
        let classname = 'card-list__item'
        if (cols) {
            return `${classname} ${classname}--${cols} ${cardClassName ? cardClassName : ''}`
        } else {
            return `${classname} ${classname}--${type} ${cardClassName ? cardClassName : ''}`
        }
    }

    getCardElementByType(el, i){
        let { type, card } = el
        switch (type) {
            case 'game':
                return <CardGameComponent
                    key={i}
                    classname={this.getClassname(type)}
                    id={el.id}
                    url={`/game/${el.id}`}
                    {...card}
                />
            case 'playground':
                return <CardPlaygroundComponent
                    key={i}
                    classname={this.getClassname(type)}
                    {...card}
                />
            default:
                return null
        }
    }

    render() {
        let { list } = this.props
        return (
            <div className="card-list">
                { list ? list.map((el, i) => {
                    return this.getCardElementByType(el, i)
                }) : '' }
            </div>
        )
    }
}

export default SectionCardsComponent
