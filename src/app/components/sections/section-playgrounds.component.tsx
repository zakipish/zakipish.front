import * as React from 'react'
import PlaygCard from '../../components/cards/playg.card'
import SectionComponent from '../../components/layout/section.component'
import ClubSection from '../../components/sections/club.section'
// import Config from '../../core/config'
import { Club } from '../../models/club'

interface IPropTypes {
  clubs: Club[]
  mode: string
  queryParams: string
}

class SectionPlaygroundsComponent extends React.Component<IPropTypes> {
  public render() {
    const { clubs, mode, queryParams } = this.props
    const playgroundsCount = clubs.map(club => club.playgrounds ? club.playgrounds.length : 0).reduce((acc, count) => acc + count, 0)

    return <SectionComponent title={`НАЙДЕНО ПЛОЩАДОК ${playgroundsCount}`}>
      {clubs.map(club => <ClubSection
        key={club.slug}
        title={club.name}
        address={club.address}
        logo={club.logo ? club.logo.url : '/images/club-logo.png'}
        to={`/club/${club.slug}`}
      >
        {club.playgrounds && club.playgrounds.map(playground => <PlaygCard
          key={playground.slug}
          to={`/playground/${playground.slug}`}
          toCreate={`/club/${club.slug}/${playground.slug}/${mode}?${queryParams}`}
          title={playground.name}
          image={playground.avatar ? playground.avatar.url : '/images/default-playground.png'}
          price={playground.min_price}
          status={playground.hasSchedule ? 'В зале есть расписание' : 'В зале нет расписания'}
          params={[
            { tit: 'Время работы',  val: playground.weekScheduleStr },
            { tit: 'Тип площадки',  val: playground.typeName },
            { tit: 'Тип покрытия',  val: playground.surfacesNames }
          ]}
        />)}
      </ClubSection>)}
    </SectionComponent>
  }
}

export default SectionPlaygroundsComponent
