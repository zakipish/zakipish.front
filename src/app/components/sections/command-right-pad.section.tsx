import * as React from 'react'
import './command-right-pad.section.sass'

interface IPropTypes {
  new?: boolean
}

export default class CommandRightPad extends React.Component<IPropTypes> {
  protected static defaultProps = {
    new: false
  }

  protected colors = {
    red: '#F70C0C',
    orange: '#F7990C',
    yellow: '#F7D20C',
    green: '#1FF70C',
    cyan: '#0CBFF7',
    blue: '#0C86F7',
    purple: '#820CF7'
  }

  public render() {
    const colors: React.ReactNode[] = []

    for (const color in this.colors) {
      if (!this.colors.hasOwnProperty(color)) {
        continue
      }
      colors.push(<div key={color} className='CommandRightPad__ColorPicker' style={{
        background: this.colors[color]
      }} />)
    }

    return <div className={'CommandRightPad'}>
      <label>Изображение команды</label>
      <div className='CommandRightPad__imageUpload'>
      </div>
      <label>Цвет команды</label>
      <div className='CommandRightPad__ColorPickerContainer'>
        {colors}
      </div>
    </div>
  }
}
