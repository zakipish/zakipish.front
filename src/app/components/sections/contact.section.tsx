import * as React from 'react'
import IconComponent from '../blocks/icon/icon.component'
import ExternalLink from '../blocks/external-link'
import { YMaps, Map, GeoObject } from 'react-yandex-maps'

interface IProps {
    title?: string
    list?: Array<{
        key: string
        value?: React.ReactFragment
    }>
    soc?: Array<{
        provider: string
        url?: string
    }>,
    coordinates?: number[]
}

interface IState {
    loading: boolean,
    geoWidth?: number,
    geoHeight?: number
}

class ContactSection extends React.Component<IProps, IState> {
    geoBlockRef = React.createRef<HTMLDivElement>()
    resizeTimeout

    state: IState = {
        loading: true
    }

    componentDidMount() {
        setTimeout(this.setGeoSizes, 200)
        window.addEventListener('resize', this.updateGeoSizes)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateGeoSizes)
    }

    updateGeoSizes = () => {
        if (this.resizeTimeout != null)
            clearTimeout(this.resizeTimeout)

        this.resizeTimeout = setTimeout(() => {
            this.setState({ loading: true }, this.setGeoSizes)
        }, 200)
    }

    getUrl = (url: string) => {
        if (!url)
            return null

        const http = url.indexOf('http') == 0
        if(http) {
            return url
        } else {
            return `http://${url}`
        }
    }

    setGeoSizes = () => {
        const geoBlock = this.geoBlockRef.current

        if (geoBlock) {
            const { clientWidth, clientHeight } = geoBlock
            this.setState({ loading: false, geoWidth: clientWidth, geoHeight: clientHeight })
        }
    }

    render() {
        const { title, list, soc, coordinates } = this.props
        const { loading, geoWidth, geoHeight } = this.state

        return (
            <div className="contact">
                { coordinates ?
                    <div className="contact__geo" ref={this.geoBlockRef}>
                        { !loading ?
                            <YMaps>
                                <Map
                                    defaultState={{ center: coordinates, zoom: 17 }}
                                    width={geoWidth} height={geoHeight}
                                >
                                    <GeoObject
                                        geometry={{
                                            type: 'Point',
                                            coordinates: coordinates,
                                        }}
                                        options={{
                                            iconLayout: 'default#image',
                                            iconImageHref: '/images/geopoint.png',
                                            iconImageSize: [79, 97],
                                            iconOffset: [-43, -55]
                                        }}
                                    />
                                </Map>
                            </YMaps>
                        : '' }
                    </div>
                : '' }

                <div className="contact__txt">
                    <div className="contact__tit">{ title || 'Контакты' }</div>
                    { list &&
                        <ul className="contact__list">
                            { list.map((el, i) => { return(
                                <li key={i}>
                                    <div className="contact__key">{el.key}</div>
                                    <div className="contact__val">{el.value || '-'}</div>
                                </li>
                            )}) }
                        </ul>
                    }
                    { soc &&
                        <div className="contact__soc">
                            { soc.map((el, i) => { return(
                                <ExternalLink
                                    href={this.getUrl(el.url) || '#'}
                                    key={i}
                                    target="_blank"
                                >
                                    <IconComponent
                                        className={`contact__socItem bg-${el.provider}`}
                                        icon={el.provider}
                                    />
                                </ExternalLink>
                            )}) }
                        </div>
                    }
                </div>
            </div>
        )
    }
}

export default ContactSection
