import * as React from 'react'
import OutsideClickComponent, { IOutsideClickProps, IOutsideClickState } from '../_shared/outside-click.component'
import IconComponent, { IconList } from '../blocks/icon/icon.component'

interface IProps extends IOutsideClickProps {
    className?: string
    prefix?: string
    
    value?: number | string
    options: Object
    
    placeholder?: string
    emptyTitle?: string

    disabled?: boolean
    disableKeysSelect?: boolean
    autoHide?: boolean
    
    onSelect: (key: number | string) => void
    onSearch?: (key: number | string) => void
    onClickEmpty?: (value: string) => void
    
    inputProps?: any
}

interface IState extends IOutsideClickState {
    fieldValue: string
}

class UiSelectControl extends OutsideClickComponent<IProps, IState> {

    refInput: React.RefObject<HTMLDivElement> = React.createRef()
    defaultPrefix: string = 'uiSelect'

    oldFilter: string = ''
    numKey: number = 0
    startFind: number = 3

    state = {
        hide: true,
        fieldValue: this.props.options[this.props.value] || ''
    }

    componentDidMount(...args) {
        super.componentDidMount.apply(this, args)
        window.addEventListener('keydown', this.onKeyPress)
    }

    componentWillUnmount(...args) {
        super.componentDidMount.apply(this, args)
        window.removeEventListener('keydown', this.onKeyPress)
    }

    get autoHide() {
        const { autoHide } = this.props

        if (typeof autoHide === 'undefined')
            return true

        return autoHide
    }

    resetTmp() {
        this.numKey = 0
        this.oldFilter = null
    }

    onHide = () => {
        if (!this.state.hide) {
            this.outsideRef.current.querySelector('input').blur()
            let fieldValue = this.props.options[this.props.value] || ''
            this.setState({hide: true, fieldValue})
        }
        this.resetTmp()
    }

    onShow = () => {
        if (this.state.hide) {
            this.outsideRef.current.querySelector('input').focus()
            this.setState({hide: false})
        }
        this.resetTmp()
    }

    onToggle = () => {
        if (this.state.hide) {
            this.onShow()
        } else {
            this.onHide()
        }
    }

    onSelectByClick = (key) => {
        let { hide } = this.state
        this.props.onSelect(key)
        let fieldValue = key ? this.props.options[key] : ''
        if (this.autoHide) {
            this.resetTmp()
            hide = true
        }
        this.setState({hide, fieldValue})
    }

    onSelect = (key) => {
        this.props.onSelect(key)
        let fieldValue = key ? this.props.options[key] : ''
        this.setState({ fieldValue })
    }

    onSearch = (e) => {
        let filter = e.target.value
        let filterLen = filter.length
        this.numKey = filterLen > this.startFind ? this.numKey + 1 : 3
        this.oldFilter = filter
        this.setState({fieldValue: filter})
        let { onSearch } = this.props
        if (onSearch) onSearch(e.target.value)
    }

    getOptionKeys = () => {
        let optionKeys = Object.keys(this.props.options)
        if (this.numKey >= this.startFind) {
            optionKeys = this.filterOptions(optionKeys)
        }
        return optionKeys
    }

    filterOptions = (optionKeys) => {
        let { options } = this.props
        let filter = this.oldFilter ? this.oldFilter.toLowerCase() : this.state.fieldValue.toLowerCase()
        optionKeys = optionKeys.filter(key => {
            let value = options[key].toLowerCase()
            return value.indexOf(filter) >= 0 ? true : false
        })
        return optionKeys
    }

    onKeyPress = (e) => {
        const { disableKeysSelect } = this.props
        let status = !this.state.hide
        if (status) {
            if (!disableKeysSelect) {
                if (e.key === 'ArrowDown') {
                    e.preventDefault()
                    this.selectByKey(1)
                }
                else if (e.key === 'ArrowUp') {
                    e.preventDefault()
                    this.selectByKey(-1)
                }
            }

            if (e.key === 'Escape') {
                e.preventDefault()
                this.onHide()
            }
        }
    }
    
    selectByKey = (addIndex) => {
        let { value } = this.props
        let optionKeys = this.getOptionKeys()
        let actIndex = optionKeys.indexOf(value + '')
        let nextIndex = actIndex + addIndex
        let key = optionKeys[nextIndex]
        if (key) this.onSelect(key)
    }

    get classsName() {
        let { className } = this.props
        className = className ? ` ${className}` : ' '
        className += this.hide ? ' --hide' : ' --show'
        return className
    }

    render() {
        let { prefix, inputProps, placeholder, options, value, onClickEmpty, emptyTitle } = this.props
        let { fieldValue } = this.state
        let optionKeys = this.getOptionKeys()
        
        prefix = prefix ? prefix : this.defaultPrefix
        placeholder = placeholder ? placeholder : ''
        emptyTitle = emptyTitle ? emptyTitle : 'Пусто'
        
        return (
            <div 
                className={`${prefix}${this.classsName}`}
                ref={this.outsideRef} 
                onClick={this.onShow}
            >
                <IconComponent
                    className={`${prefix}__arr`}
                    icon={IconList.selectArr}
                    onClick={this.onHide}
                />
                <div className={`${prefix}__input`} ref={this.refInput}>
                    <input 
                        {...inputProps}
                        value={fieldValue}
                        autoComplete="off"
                        placeholder={placeholder}
                        onChange={this.onSearch}
                    />
                </div>
                <div className={`${prefix}__list`}>
                    <ul>
                        { optionKeys.length ?
                            <React.Fragment>
                                <li onClick={() => this.onSelect('')}>{placeholder}</li>
                                {optionKeys.map((key, i) => {
                                    return <li
                                        key={i}
                                        className={key === value ? '--act' : ''}
                                        onClick={() => this.onSelectByClick(key)}
                                    >
                                        <IconComponent icon={IconList.ok}/>
                                        <div>{options[key]}</div>
                                    </li>
                                })}
                            </React.Fragment>
                            :
                            <li className="--empty" onClick={() => onClickEmpty(fieldValue)}>{emptyTitle}</li>
                        }
                    </ul>
                </div>
            </div>
        )
    }
}

export default UiSelectControl