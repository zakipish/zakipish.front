import * as React from "react";
import InputControl, { InputControlProps } from './input-control'

class PhoneControl extends React.Component<InputControlProps> {
    handleChange = (value: string) => {
        const { onChange } = this.props

        //only 10
        if(value.length > 10) {
            return false
        }

        if(value.match(/^\d{0,10}$/)) {
            onChange(value)
        }
    }

    render() {
        const { onChange, disabled, ...rest } = this.props
        const inputWrapClassName=`_inputWrap ${disabled ? 'disabled' : ''}`

        return (
            <div className={inputWrapClassName}>
                <div className="_beforeInc">+7</div>
                <InputControl
                    disabled={disabled}
                    onChange={this.handleChange}
                    {...rest}
                />
            </div>
        )
    }
}

export default PhoneControl
