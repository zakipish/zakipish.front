import * as React from "react";

export interface InputControlProps {
    value?: string | number
    onChange: (value: string) => void
    multiline?: boolean
    type?: string
    placeholder?: string
    disabled?: boolean
    className?: string
}

class InputControl extends React.Component<InputControlProps> {
    render() {
        const { value, onChange, multiline, ...rest } = this.props
        if (multiline) {
            return (
                <textarea
                    onChange={(e) => onChange(e.target.value)}
                    value={value || ''}
                    {...rest}
                />
            )
        } else {
            return (
                <input
                    value={value || ''}
                    onChange={(e) => onChange(e.target.value)}
                    {...rest}
                />
            )
        }
    }
}

export default InputControl
