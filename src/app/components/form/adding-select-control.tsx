import * as React from "react";
import IconComponent, { IconList } from '../blocks/icon/icon.component'
import InputTipComponent from '../blocks/input-tip/input-tip.component'
import OutsideClickComponent, { IOutsideClickProps, IOutsideClickState } from '../_shared/outside-click.component'

interface AddingSelectControlProps extends IOutsideClickProps {
    title?: string
    options?: any[]
    placeholder?: string
    onAdd: (el) => void
    hide?: boolean
    onHide?: Function
    onCross?: () => void
}

interface AddingSelectControlState extends IOutsideClickState {
    value: string
    tipHidden: boolean
    selected: any
    hide?: boolean
}

class AddingSelectControl extends OutsideClickComponent<AddingSelectControlProps, AddingSelectControlState> {
    inputRef: React.RefObject<HTMLInputElement>;

    constructor(props) {
        super(props)
        this.state = {
            value: '',
            tipHidden: true,
            selected: null
        }
        this.inputRef = React.createRef()
    }

    onChange = (e: any) => {
        const value = e.target.value
        this.setState({ value: value, selected: null })
    }

    focusInput = () => {
        if (this.inputRef.current) {
            this.inputRef.current.focus()
        }
    }

    onSelect = (el: any) => {
        this.hideTip()
        this.setState({ value: el.label, selected: el })
    }

    showTip = () => {
        this.setState({ tipHidden: false })
    }

    hideTip = () => {
        this.setState({ tipHidden: true })
    }

    toggleTip = () => {
        this.setState({ tipHidden: !this.state.tipHidden })
    }

    onHide = () => {
        this.hideTip()
    }

    onAdd = () => {
        const { selected } = this.state
        this.props.onAdd(selected)
        this.setState({ selected: null, value: '' })
    }

    render() {
        const { options, onCross } = this.props
        const { selected, value, tipHidden } = this.state

        return (
            <div className="profileInput profileInput--mobileFill">
                <span className="_label">Виды спорта</span>
                <div className="_input --adding">
                    <div className="profileSportSelect__inputBlock" ref={this.outsideRef}>
                        <div className="_btn _btn--arr" onClick={this.focusInput}>
                            <IconComponent icon={IconList.selectArr} stroke="#7A8288"/>
                        </div>
                        <input
                            ref={this.inputRef}
                            type="text"
                            placeholder="Введите вид спорта"
                            value={value}
                            onFocus={this.showTip}
                            onChange={this.onChange}
                        />
                        <InputTipComponent
                            hide={tipHidden}
                            list={options}
                            search={value}
                            onSelect={this.onSelect}
                        />
                        <div
                            className="profileSportSelect__cross"
                            onClick={onCross}
                        >
                            <IconComponent icon="cross"/>
                        </div>
                    </div>
                    <button
                        type="button"
                        onClick={this.onAdd}
                        disabled={!selected}
                        className="profileSportSelect__btnBig"
                    >
                        Добавить
                    </button>
                </div>
            </div>
        )
    }
}

export default AddingSelectControl
