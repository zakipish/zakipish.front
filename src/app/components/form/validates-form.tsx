import * as React from "react"
import * as _ from "lodash"
import { parsePhoneNumber } from 'libphonenumber-js'

export interface ValidatesFormProps {
    item: any
    onSubmit?: () => void
}

export interface ValidatesFormState {
    item?: any
    draft?: any
    busy?: boolean
    errors?: any
    message?: string
    validated: {
        [x: string]: {
            valid: boolean
            errors: any[]
        }
    },
    commonErrors: Array<string>
}

const defaultCommonValidationConfig = {
    requiredAtLeast: [],
    equalFields: [],
}

const defaultValidationConfig = {
    present: false,
    minLength: 0,
    maxLength: 0,
    email: false,
    phone: false,
    requiredWith: []
}

export default abstract class ValidatesForm<P extends ValidatesFormProps = ValidatesFormProps, S extends ValidatesFormState = ValidatesFormState> extends React.Component<P, S> {
    validationConfig: {} = {}
    commonValidationConfig: {} = {}

    state: any = {}

    constructor(props) {
        super(props)
        const formItem = this.cloneModel(this.props.item)
        formItem.oldPassword = null;
        formItem.newPassword = null;
        formItem.confirmedPassword = null;
        this.state = {
            busy: false,
            validated: {},
            item: formItem
        }
    }


    commonValidator = (): { valid: boolean, errors: Array<string> } => {
        const { item } = this.state
        const computedConfig = { ...defaultCommonValidationConfig, ...this.commonValidationConfig }
        let valid = true
        let errors = []

        if (!this.validateRequiredAtLeast(item, computedConfig.requiredAtLeast)) {
            valid = false
            const fields = computedConfig.requiredAtLeast.map(f => f.label)

            if(fields.length > 1) {
                const lastField = fields.splice(-1,1)
                errors.push(`Одно из полей ${fields.join(', ')} или ${lastField} не может быть пустым`)
            } else {
                errors.push(`${fields[0]} не может быть пустым`)
            }
        }

        if (!this.validateEqualFields(item, computedConfig.equalFields)) {
            valid = false
            const fields = computedConfig.equalFields.map(f => f.label)

            const lastField = fields.splice(-1,1)
            errors.push(`${fields.join(', ')} и ${lastField} должны совпадать`)
        }

        this.setState({ commonErrors: errors })

        return { valid, errors }
    }

    validateRequiredAtLeast(item: any, fields: any[]): boolean {
        if (!!fields && fields.length > 0) {
            const check_reducer = (acc, field) => acc || !!item[field.field]
            return fields.reduce(check_reducer, false)
        }

        return true
    }

    validateEqualFields(item: any, fields: any[]): boolean {
        if (!!fields && fields.length > 0) {
            const initialField = item[fields[0].field]
            return fields.reduce((reducer,field)=>reducer && (item[field.field] === initialField),true)
        }
        return true
    }

    validateWholeForm = () => {
        let { validated, item } = this.state
        const fields = Object.keys(this.validationConfig)
        fields.forEach(field => validated[field] = this.validate(field, item[field]))
        this.setState({ validated })

        const check_reducer = (acc, val) => acc && val.valid
        const specifiedFieldsValid = Object.values(validated).reduce(check_reducer, true)
        const commonValid = this.commonValidator().valid
        return specifiedFieldsValid && commonValid
    }

    errorMessage = (name: string) => {
        const { validated } = this.state
        if (!!validated[name]) {
            let errors_str = validated[name].errors.join(' ')
            errors_str = errors_str.charAt(0).toUpperCase() + errors_str.slice(1); //capitalize
            return errors_str
        }
    }

    isValid = (name: string) => {
        const { validated } = this.state
        if (!!validated[name]) {
            return validated[name].valid
        } else {
            return true
        }
    }

    validateField = (field) => {
        const { item, validated } = this.state
        const validationResult = this.validate(field, item[field])
        const new_validated = { ...validated, [field] : validationResult }
        this.setState({ validated: new_validated })
    }

    // MAIN VALIDATOR
    validate = (field, value) => {
        const computedConfig = { ...defaultValidationConfig, ...this.validationConfig[field] }
        let valid = true
        let errors = []
        const { item } = this.state

        if (!this.validatePresent(value, computedConfig.present)) {
            valid = false
            errors.push('не может быть пустым')
        }

        if (!this.validateMinLength(value, computedConfig.minLength)) {
            valid = false
            errors.push(`минимальная длина равна ${computedConfig.minLength}`)
        }

        if (!this.validateEmail(value, computedConfig.email)) {
            valid = false
            errors.push('не является адресом электронной почты')
        }

        if (!this.validatePhone(value, computedConfig.phone)) {
            valid = false
            errors.push('не является номером телефона')
        }

        if (!this.validateRequiredWith(value, item, computedConfig.requiredWith)) {
            valid = false
            errors.push('не может быть пустым')
        }

        return { valid, errors }
    }

    // VALIDATION METHODS

    validateRequiredWith(value, item: any, fields: any[]): boolean {
        if (!!fields && fields.length > 0) {
            const presentAtLeast = fields.reduce((acc, field) => acc || !!item[field], false)

            if (presentAtLeast) {
                return !!value
            }
        }

        return true
    }

    validatePresent(value, present) {
        if (present) {
            return !!value
        }

        return true
    }

    validateMinLength(value, minLength) {
        if (!value || value.length == undefined) {
            return true
        }

        return value.length >= minLength
    }

    validateMaxLength(value, maxLength) {
        if (!value || maxLength === 0) {
            return true
        }

        return value.length >= maxLength
    }

    validateEmail(value, email) {
        if (!value || !email) {
            return true
        }

        const re = /^\s*(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$/
        return re.test(String(value).toLowerCase());
    }

    validatePhone(value, phone) {
        if (!value || !phone) {
            return true
        }

        try {
            return parsePhoneNumber(`+7${value}`, 'RU').isValid()
        } catch(e) {
            return false
        }
    }

    handleChange = (name: string) => (value: any) => {
        let { item } = this.state
        item[name] = value

        this.setState({ item })
        this.validateField(name)
    }

    cloneModel = (model) => {
        return _.cloneDeep(model)
    }

    handleSubmit = (item) => {
        this.setState({ item, busy: false })
        if (typeof this.props.onSubmit === 'function')
            this.props.onSubmit()
    }

    handleErrors = (response) => {
        const apiValidated = {}

        Object.keys(response.errors).forEach(key => apiValidated[key] = { valid: false, errors: response.errors[key] })
        this.setState({ validated: apiValidated, busy: false })

    }

    setBusy = () => {
        this.setState({ busy: true })
    }

    unsetBusy = () => {
        this.setState({ busy: false })
    }
}

