import * as React from "react";
import IconComponent from '../blocks/icon/icon.component'

interface IRadioControlProps {
    value?: string
    onChange: (value: string) => void
    options: {}
    disabled?: boolean
}

class RadioControl extends React.Component<IRadioControlProps> {

    handleOptionChange = (e) => {
        const { onChange } = this.props
        onChange(e.target.value)
    }

    render() {
        const { value, onChange, options, disabled, ...rest } = this.props
        return (
            Object.keys(options).map((option, i) => (
                <label className={disabled ? 'disabled' : ''} key={i}>
                    <input
                        type="radio"
                        value={option}
                        checked={value == option}
                        onChange={this.handleOptionChange}
                        disabled={disabled}
                    />
                    <i><IconComponent icon="ok"/></i>
                    <span>{options[option]}</span>
                </label>
            ))
        )
    }
}

export default RadioControl
