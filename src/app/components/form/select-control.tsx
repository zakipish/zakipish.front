import * as React from "react";


export interface SelectControlProps {
    value?: string|number
    onChange: (value: string) => void
    placeholder?: string
    disabled?: boolean
    options: { [x : number]: string }
}

class SelectControl extends React.Component<SelectControlProps> {
    render() {
        const { disabled, options, placeholder, value, onChange, ...rest } = this.props
        return (
            <select
                onChange={(e) => onChange(e.target.value)}
                value={value || ''}
                disabled={disabled}
                {...rest}
            >
                <option>{placeholder}</option>
                {Object.keys(options).map((key, i) => (
                    <option key={i} value={key}>{options[key]}</option>
                ))}
            </select>
        )
    }
}

export default SelectControl
