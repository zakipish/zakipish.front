import * as React from 'react'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, { formatDate, parseDate } from 'react-day-picker/moment'

import 'moment/locale/ru'

interface IProps {
    value?: string | Date
    onChange: (value: string | Date) => void
    disabled?: boolean
}

class DateControl extends React.Component<IProps, {}> {
    format = "DD.MM.YYYY"

    getFormatDate(date = new Date) {
        return formatDate(date, this.format).toString()
    }

    render() {
        let { value, onChange, disabled } = this.props
        let placeholder = this.getFormatDate()
        return (
            <DayPickerInput
                formatDate={formatDate}
                parseDate={parseDate}
                format={this.format}
                placeholder={placeholder}
                value={value}
                onDayChange={(date) => onChange(date)}
                dayPickerProps={{
                    localeUtils: MomentLocaleUtils,
                    locale: "ru",
                }}
                inputProps={{disabled}}
            />
        )
    }
}

export default DateControl
