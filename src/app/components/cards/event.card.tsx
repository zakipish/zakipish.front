import * as React from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'; moment.locale('ru')
import { User } from '../../models/user'
import { Club } from '../../models/club'
import { GameLevel } from '../../models/game-level'

import Progress, { ProgressProps } from '../blocks/progress'
import ListPlayer from '../blocks/list-player'

export interface Props {
    type: ('game' | 'meeting')
    image: string
    title: string
    club: Club
    gameLevel?: GameLevel
    sportName?: String
    address: string
    price: string
    players: User[]
    max_players?: number
    min_players?: number
    date: {
        from: moment.Moment
        to: moment.Moment
    }
    mobileView: boolean
    url: string
}

class EventCard extends React.Component<Props, {}> {
    static defaultProps: Props = {
        type: 'game',
        image: '#',
        title: '-',
        club: new Club({
            name: '-',
            logo: {
                url: '#',
            },
        }),
        address: '-',
        price: 'Вход свободный',
        players: [
            new User({avatar_thumb_url: '#'}),
            new User({avatar_thumb_url: '#'}),
            new User({avatar_thumb_url: '#'}),
            new User({avatar_thumb_url: '#'}),
            new User({avatar_thumb_url: '#'}),
            new User({avatar_thumb_url: '#'}),
            new User({avatar_thumb_url: '#'}),
            new User({avatar_thumb_url: '#'}),
        ],
        max_players: 100,
        date: {
            from: moment(),
            to: moment(),
        },
        mobileView: false,
        url: '#',
    }

    get progressProps(): ProgressProps {
        let { players, max_players, min_players } = this.props
        return {
            title: 'Участников 0 / ' + max_players,
            subtitle: min_players > 0 ? `Минимум ${min_players} игроков` : undefined,
            from: 0,
            to: max_players,
            progress: players ? players.length : 0,
        }
    }

    get className(): string {
        const { type, mobileView } = this.props
        return `eventCard --${type} ${mobileView && '--mobile-view'}`
    }

    render() {
        const { image, title, players, club, gameLevel, sportName, address, price, date, url } = this.props
        const clubLogoUrl = club.logo ? club.logo.url : '#'

        return (
            <div className={this.className}>
                <div
                    className="eventCard__img"
                    style={{backgroundImage: `url(${image})`}}
                >
                    <img src="/images/event-meeting-label.svg" alt="#"/>
                </div>
                <div className="eventCard__text">
                    <div className="eventCard__tit">{title}</div>
                    <div className="eventCard__progress">
                        <Progress {...this.progressProps}/>
                    </div>
                    <div className="eventCard__party">
                        <ListPlayer
                            images={players && players.map(user => user.avatar_thumb_url)}
                            views={4}
                        />
                        {gameLevel && (
                            <div style={gameLevel.color ? { background: `#${gameLevel.color}` } : {}}
                                className={`tag tag--green`}
                            >{gameLevel.engName}</div>
                        )}
                        {sportName && <div className="eventCard__sport">{sportName}</div>}
                    </div>
                    <div className="eventCard_hr"></div>
                    <div className="eventCardAuthor">
                        <div
                            className="eventCardAuthor__img"
                            style={{backgroundImage: `url(${clubLogoUrl})`}}
                        ></div>
                        <div className="eventCardAuthor__text">{club.name}</div>
                    </div>
                    <div className="eventCardPlace">
                        <div
                            className="eventCardPlace__img"
                            style={{backgroundImage: "url('/images/event-geopoint.png')"}}
                        ></div>
                        <div className="eventCardPlace__text">{address}</div>
                    </div>
                    <div className="eventCard__param eventCardParam">
                        <div className="eventCardParam__item">
                            <div className="eventCardPrice">
                                <span>{price}</span>
                                {!isNaN(+price) && <i>руб</i>}
                            </div>
                        </div>
                        <div className="eventCardParam__item">
                            <div className="eventCard__date eventCardDate">
                                <div className="eventCardDate__col">
                                    <div className="eventCardDate__h1">{date.from.format('DD')}</div>
                                    <div className="eventCardDate__h2">{date.from.format('MMMM')}</div>
                                </div>
                                <div className="eventCardDate__col">
                                    <div className="eventCardDate__h2">{date.from.format('dddd')}</div>
                                    <div className="eventCardDate__h2">{date.from.format('hh:mm - ')}{date.to.format('hh:mm')}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="eventCard__btn">
                        <Link to={url}>Подробнее</Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default EventCard
