import * as React from 'react'
import { Link } from 'react-router-dom'

interface Props {
    image?: string
    title?: string
    btnTitle: string
    btnTo: string
    btnOnClick: (e?) =>void
}

class MoreEventCard extends React.Component<Props, {}> {

    static defaultProps = {
        image: '/images/event-more-card-def.png',
        title: '-',
        btnTitle: '-',
        btnTo: '#',
        btnOnClick: (e?) => {},
    }

    render() {
        let { image, title, btnTitle, btnTo, btnOnClick } = this.props
        return (
            <div
                className="eventCardMore"
                style={{backgroundImage: `url(${image})`}}
            >
                <div className="eventCardMore__tit">{title}</div>
                <Link
                    to={btnTo}
                    className="eventCardMore__btn"
                    onClick={btnOnClick}
                >{btnTitle}</Link>
            </div>
        )
    }
}

export default MoreEventCard
