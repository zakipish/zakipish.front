import * as React from 'react'
import { Link } from 'react-router-dom'
import IconComponent, {IconList} from '../blocks/icon/icon.component'

export interface ICardGameProps {
  url?: string
  id?: number
  classname?: string
  header?: {
    title: string
    icon?: IconList
    tags?: Array<{
      title: string
      bg?: string
    }>
  }
  title: string
  text: string
  options?: Array<{
    label?: {
      icon?: IconList
      text: string
    }
    text: string
  }>,
  state: {
    must: number
    now: number
    min?: number
  },
  actions: Array<{
    title: string
    bg?: string
    mode?: string
    onClick?: () => void
    disabled?: boolean
  }>,
  party?: Array<{
    image?: string
    url?: string
    bg?: string
    text?: string
  }>

}

class CardGameComponent extends React.Component<ICardGameProps, {}> {

  protected static getBtnClassName(el) {
    let className = '_btn'
    className += el.bg ? ` --${el.bg}` : ''
    className += el.mode ? ` --${el.mode}` : ''
    className += el.disabled ? ' --disabled' : ''
    return className
  }

  public render() {
    const {url, header, text, options, state, actions, party } = this.props

    const title = this.props.title ? this.props.title : '-'
    header.title = header.title ? header.title : '-'

    return (
      <div className={this.getClassName()}>
        <Link to={url} className='_content'>
          { header ?
            <div className='_hdr'>
              <div className='_hdr__icon'>
                <IconComponent
                  icon={header.icon || 'football'}
                  fill='#414658'
                  width='28px'
                  height='28px'
                />
              </div>
              <div className='_hdr__tit'>{header.title}</div>
              { header.tags ?
                <div className='_hdr__tags'>{
                  header.tags.map((el, i) => {
                    return(
                      <span className={`_hdr__tag tag tag--${el.bg || 'green'}`} key={i}>{el.title}</span>
                    )
                  })
                }</div>
                : '' }
            </div>
            : '' }
          <div className='_body'>
            <div className='_tit'>{title}</div>
            <div className='_txt'>{text}</div>
            { options ?
              <ul className='_opts'>{
                options.map((el, i) => {return (
                  <li className='_opts__line' key={i}>
                    { el.label ?
                      <div className='_opts__label'>
                        <IconComponent icon={el.label.icon || 'geo'} fill='#33394E'/>
                        <span>{el.label.text}</span>
                      </div>
                      : '' }
                    <div className='_opts__txt'>{el.text}</div>
                  </li>
                )})
              }</ul>
              : '' }
            { state ?
              <div className='_state'>
                <div className='_state__tit'>Участники {state.now} / {state.must}</div>
                <div className='_state__txt'>Минимум {state.min || state.must} игроков</div>
              </div>
              : '' }
          </div>
        </Link>
        { party ?
          <div className='_party'>
            <div className='_party__tit'>Участники</div>
            <div className='_party__num'>{state.now} / {state.must}</div>
            <ul>{
              party.map((el, i) => { return(
                <li key={i}>
                  <div className={`_party__avatar ${el.bg ? `_party__avatar--${el.bg}` : ''}`}>
                    <Link to={el.url} style={{margin: 0}}>
                      { el.image ? <img src={el.image} alt='#'/> : el.text ? el.text : '' }
                    </Link>
                  </div>
                </li>
              )})
            }</ul>
            <div className='_party__subtit'>Минимум {state.min || state.must} игроков</div>
          </div>
          : '' }
        { actions ?
          <div className='_act'>{
            actions.map((el, i) =>
              el.onClick ? (
                <div className={CardGameComponent.getBtnClassName(el)} key={i} onClick={el.onClick}>
                  <span>{el.title}</span>
                </div>
              ) : el.disabled ? (
                <div className={CardGameComponent.getBtnClassName(el)} key={i}>
                  <span>{el.title}</span>
                </div>
              ) : (
                <Link to={url} className={CardGameComponent.getBtnClassName(el)} key={i}>
                  <span>{el.title}</span>
                </Link>
              )
            )
          }</div>
          : '' }
      </div>
    )
  }

  protected getClassName() {
    const {party, state, classname} = this.props
    let className = 'cardGame'
    className += party ? ' cardGame--party' : ''
    className += (state.now === state.must) ? ' cardGame--complete' : ''
    className += classname ? ` ${classname}` : ''
    return className
  }
}

export default CardGameComponent
