import * as React from 'react'
import { Link } from 'react-router-dom'
import IconComponent from '../blocks/icon/icon.component'
import RatingComponent, { IRating } from '../blocks/rating/rating.component'
import {resizeImage} from '../UI/Image'

export interface ICardPlaygroundProps {
  classname?: string
  image?: string
  geo?: string
  link?: string
  title?: string
  text?: string
  price?: string
  rating?: IRating
  url?: string
}

class CardPlaygroundComponent extends React.Component<ICardPlaygroundProps, {}> {
  public render() {
    const { url, classname, geo, link, title, text, price, rating } = this.props
    const image = resizeImage(this.props.image || 'default-playground.png', 350, 112)
    return (
      <div className={`cardPlayground ${classname ? classname : ''}`}>
        <div className='_image'>
          <img src={image} alt='#'/>
          { geo ?
            <div className='_image__geo'>
              <IconComponent icon='geo' fill='#fff'/>
              <span>{geo}</span>
            </div>
            : '' }
          { link ?
            <div className='_image__link'>
              <IconComponent icon='link' stroke='#fff'/>
            </div>
            : '' }
        </div>
        <div className='_body'>
          { title ? <Link to={url} className='_tit'>{title}</Link> : '' }
          { text ? <div className='_txt'>{text}</div> : '' }
          <div className='_param'>
            <div className='_price'>{price ? `от ${price} Р` : ''}</div>
            <RatingComponent {...rating}/>
          </div>
        </div>
      </div>
    )
  }
}

export default CardPlaygroundComponent
