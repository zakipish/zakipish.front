import * as React from 'react'
import { Link } from 'react-router-dom'
import IconComponent, { IconList } from '../blocks/icon/icon.component'

interface IProps {
    url?: string
    title?: string
    img?: string
    onAdd?: (e?) => void
}

class FriendCard extends React.Component<IProps, {}> {

    get className() {
        let str = 'friend-card'
        let { onAdd } = this.props
        if (onAdd) str += ' --onadd' 
        return str
    }

    render() {
        let { url, title, img, onAdd } = this.props
        url = url ? url : '#'
        img = img ? img : '/images/avatar-lg.jpg'
        return ( 
            <div className={this.className}> 
                <Link 
                    to={url} 
                    className="friend-card__img" 
                    style={{backgroundImage: `url(${img})`}}
                ></Link>
                <div className="friend-card__body">
                    <Link to={url} className="friend-card__tit">{title}</Link>
                    {onAdd && (
                        <IconComponent
                            icon={IconList.plusCir} 
                            className="friend-card__add"
                            onClick={onAdd}
                        />
                    )}
                </div>
            </div>
        )
    }
}

export default FriendCard
