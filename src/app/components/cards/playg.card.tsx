import * as React from 'react'
import { Link } from 'react-router-dom'
import IconComponent, { IconList } from '../blocks/icon/icon.component'
import {resizeImage} from '../UI/Image'

interface IProps {
  to: string
  toCreate?: string
  title: string
  image: string
  params?: Array<{
    tit: string
    val: string
  }>
  price?: number
  status: string
}

class PlaygCard extends React.Component<IProps, {}> {

  protected static defaultProps = {
    to: '#',
    title: '-',
    image: '#',
    status: 'В зале нет расписания'
  }

  public render() {
    const { to, toCreate, title, params, price, status } = this.props

    const image = resizeImage(this.props.image, 290, 190)

    return <div className='plagCard'>
      <div
        className='plagCard__image'
        style={{backgroundImage: `url(${image})`}}
      >
        <div className='plagCard__imageBottom'>
          <div className='plagCard__status'>
            <span>{status}</span>
          </div>
          {price ? (
            <div className='plagCard__price'>
              <span>{price}</span> <i>руб.</i>
            </div>
          ) : (
            <div className='plagCard__price --small'>
              <span>Стоимость<br/>по запросу</span>
            </div>
          )}
        </div>
      </div>
      <div className='plagCard__text'>
        <div className='plagCard__tit'>
          <Link to={to}>{title}</Link>
        </div>
        {(params && params.length > 0) && (
          <div className='plagCardParam'>
            {params.map((param, i) =>
              <div key={i} className='plagCardParam__item'>
                <div className='plagCardParam__tit'>{param.tit}</div>
                <div className='plagCardParam__val'>{param.val}</div>
              </div>
            )}
          </div>
        )}
      </div>
      <div className='plagCard__state'>
        {price ? (
          <div className='plagCard__price'>
            <span>{price}</span> <i>руб.</i>
          </div>
        ) : (
          <div className='plagCard__price --small'>
            <span>Стоимость по запросу</span>
          </div>
        )}
        <Link to={toCreate} className='plagCard__btn'>Выбрать</Link>
      </div>
    </div>
  }
}

export default PlaygCard
