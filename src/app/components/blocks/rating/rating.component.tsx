import * as React from 'react'
import IconComponent from '../icon/icon.component'

export interface IRating {
    act?: number
    max?: number
    select?: boolean
}

interface IState {
    act: number
    max: number
    initAct: number
}

class RatingComponent extends React.Component<IRating, IState> {
    
    ratingElem: React.RefObject<HTMLDivElement>

    constructor(props: IRating) {
        super(props)

        let {act, max} = props
        if (act > max) act = max

        this.state = {
            initAct: act || 0,
            act: act || 0,
            max: max || 5
        }

        this.ratingElem = React.createRef()
    }
    
    componentDidMount() {
        if (this.props.select) {
            this.ratingElem.current.addEventListener('mouseover', (e) => this.onMouseOver(e))
            this.ratingElem.current.addEventListener('mouseout', (e) => this.onMouseOut(e))
        }
    }

    onMouseOver(e) {
        if (e.path[0].localName === 'path') {
            let act = e.path[2].dataset.key
            if (this.state.act !== act) {
                this.setState({act: e.path[2].dataset.key})
            }
        }
    }
    
    onMouseOut(e) {
        if (e.path[0].localName !== 'path') {
            this.setState({act: this.state.initAct || 0})
        }
    }

    get disColor(): string {
        return '#BDBDBD'
    }

    get actColor(): string {
        let { act, max } = this.state
        let mid = max / act
        if (act === max) {
            return '#27AE60'
        } else if (act >= mid) {
            return '#F68623'
        } else if (act < mid && act > 0) {
            return '#EB5757'
        }
        return this.disColor
    }

    get actArr(): Array<number> {
        let arr = []
        for (let i = 1; i <= this.state.act; i++) arr.push(i)
        return arr
    }

    get disArr(): Array<number> {
        let { act, max } = this.state
        let arr = []
        for (let i = (act + 1); i <= max; i++) arr.push(i)
        return arr
    }

    render() {
        return (
            <div className="rating" ref={this.ratingElem} >
                { this.actArr.map(el => { return (
                    <span key={el} data-key={el}>
                        <IconComponent icon="star" fill={this.actColor}/>
                    </span>
                )})}
                { this.disArr.map(el => { return (
                    <span key={el} data-key={el}>
                        <IconComponent icon="star" fill={this.disColor}/>
                    </span>
                )})}                
            </div>
        )
    }
}

export default RatingComponent