import * as React from 'react'
import IconComponent from './icon/icon.component'

interface IProps {}
interface IState {
    hide: boolean
    src: string
}

class PopupGallary extends React.Component<IProps, IState> {
    
    refImg: React.RefObject<HTMLImageElement> = React.createRef()
    static _this

    constructor(props) {
        super(props)
        PopupGallary._this = this
        this.state = {
            hide: true,
            src: ''
        }
    }

    static show = (src) => {
        PopupGallary._this.setState({hide: false, src})
    }

    hide = (e) => {
        if(!this.refImg.current.contains(e.target)) {
            this.setState({hide: true, src: ''})
        }
    }
    
    render() {
        let { hide, src } = this.state
        return (
            <div className={`popupGallary ${hide ? ' --hide' : ''}`} onClick={this.hide}>
                <IconComponent className="popupGallary__cross" icon="cross"/>
                <img ref={this.refImg} src={src} alt="#"/>
            </div>
        )
    }
}

export default PopupGallary