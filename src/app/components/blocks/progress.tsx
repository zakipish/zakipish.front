import * as React from 'react'

export interface ProgressProps {
    className?: string
    style?: React.CSSProperties
    title?: string
    subtitle?: string
    from: number
    to: number
    progress: number
}

class Progress extends React.Component<ProgressProps, {}> {

    static defaultProps = {
        className: '',
        from: 0,
        to: 10,
        progress: 0,
    }

    render() {
        let { className, style, title, subtitle, from, to, progress } = this.props
        let len = to - from
        let step = len / 100
        let proc = (progress - from) / step
        return (
            <div className={`progress ${className}`} style={style}>
                <div className="progress__line">
                    <div
                        className="progress__res"
                        style={{width: `${proc}%`}}
                    ></div>
                </div>
                {(title || subtitle) && (
                    <div className="progress__txt">
                        {title && <div className="progress__tit">{title}</div>}
                        {subtitle && <div className="progress__subtit">{subtitle}</div>}
                    </div>
                )}
            </div>
        )
    }
}

export default Progress