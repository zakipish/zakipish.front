import * as React from 'react'

interface IProps {
    list: GarmashkaItem[]
}

interface GarmashkaItem {
    name: string 
    items?: string[] 
    img?: string
}

interface IState {
    act: number
    viewLine: number
}

class GarmoshkaSlider extends React.Component<IProps, IState> {

    refWrap: React.RefObject<HTMLDivElement> = React.createRef()

    state = {
        act: 0,
        viewLine: 2
    }

    componentDidMount() {
        setTimeout(() => this.onResize(), 300)
        window.addEventListener('resize', this.onResize)
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.onResize)
    }

    onResize = () => {
        let refWrap = this.refWrap.current
        let len = this.props.list.length * 60
        let wrapWidth = refWrap.clientWidth - len
        let viewLine = Math.floor(wrapWidth / 300)
        this.setState({viewLine})
    }

    setActState = (act) => {
        this.setState({act})
    }

    checkShow = (i) => {
        let state = false
        let len = this.props.list.length - 1
        let { act, viewLine } = this.state
        if (i === act) state = true
        if (i < act + viewLine && i > act) state = true
        if (act + viewLine > len) {
            if (i > len - viewLine) state = true
        }
        return state
    }

    render() {
        let { list } = this.props
        let { act, viewLine } = this.state
        return (
            <div className="garmoshka">
                { list &&
                    <div className="garmoshka__list" ref={this.refWrap}>
                        { list.map((el, i) => { 
                            el.img = el.img ? el.img : '/assets/userfiles/garmoshka-1.png'
                            let show = this.checkShow(i)
                            return(
                                <div 
                                    key={i} 
                                    className={`_item ${show ? '--show' : ''}`} 
                                    style={{zIndex: (list.length - i)}}
                                    onClick={() => this.setActState(i)}
                                >
                                    <div className="_item__tit">
                                        <span>{el.name}</span>
                                    </div>
                                    <div className="_item__content">
                                        <div 
                                            className="_item__img"
                                            style={{backgroundImage: `url('${el.img}')`}}
                                        ></div>
                                        { el.items && 
                                            <ul className="_item__props">
                                                { el.items.map((item, j) => { return(
                                                    <li key={j}>{item} -</li>
                                                )}) }
                                            </ul>
                                        }
                                        <div className="_item__name">{el.name}</div>
                                    </div>
                                </div>
                            )
                        }) }
                    </div>
                }
            </div>
        )
    }
}

export default GarmoshkaSlider