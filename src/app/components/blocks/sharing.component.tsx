import * as React from 'react'
import './sharing.component.sass'

interface IProps {
  list: string[]
  className?: string
  classNameLikely?: string
  title?: string
}

class SharingComponent extends React.Component<IProps> {
  public componentDidMount() {
    const likely = require('ilyabirman-likely')
    // Finds all the widgets in the DOM and initializes them
    likely.initiate()
  }

  public render() {
    const { list, className, classNameLikely, title } = this.props

    return <div className={`sharing-wrap ${className}`}>
      <div className={`likely ${classNameLikely}`}>
        {list.map(soc => <div className={soc} role='link'>{title != null ? title : 'Share'}</div>)}
      </div>
    </div>
  }
}

export default SharingComponent
