import * as React from 'react'
import moment from 'moment'
import LabelComponent, { ILabel, LabelStatus } from '../label.component'

export interface TimepickerProps {
    value?: TimepickerInterval
    times?: TimepickerItem[]
    emptyTitle?: string
    onChange: (value: TimepickerInterval) => void
}

export interface TimepickerItem extends ILabel, TimepickerInterval { 
    status: LabelStatus
}

export interface TimepickerInterval {
    from: moment.Moment
    to: moment.Moment
}

class Timepicker extends React.Component<TimepickerProps, {}> {

    fromIndex = -1
    toIndex = -1

    onChange = (index: number) => {
        let { value, times, onChange } = this.props
        this.setSelectIndex(index)
        value = {
            from: times[this.fromIndex].from,
            to: times[this.toIndex].to
        }
        onChange(value)
    }

    setSelectIndex = (index: number) => {
        if (this.fromIndex <= index && this.toIndex >= index) {
            this.fromIndex = this.toIndex = index
        }
        if (this.fromIndex < 0 || this.fromIndex > index) {
            this.fromIndex = index
        }
        if (this.toIndex < 0 || this.toIndex < index) {
            this.toIndex = index
        }
        let { times } = this.props
        let disable = false
        for (let i = this.fromIndex; i <= this.toIndex; i++) {
            if (disable) break
            let item = times[i]
            if (!item || this.checkDisableItem(item)) {
                disable = true
                if (index > i) {
                    this.fromIndex = this.toIndex
                } else if (index < i) {
                    this.toIndex = this.fromIndex
                }
            }
        }
    }

    checkDisableItem = (item) => {
        return (item.status === LabelStatus.disable || item.disabled)
    }

    checkActiveItem = (index) => {
        return (index >= this.fromIndex && index <= this.toIndex)
    }

    get times() {
        let { times } = this.props
        if (times && times.length > 0) {
            return times.map((item, i) => {
                let from = item.from.format('HH:mm')
                let to = item.to.format('HH:mm')
                let disable = this.checkDisableItem(item)
                let active = this.checkActiveItem(i)
                item.status = !disable && active ? LabelStatus.animation : item.status
                return ({
                    ...item,
                    className: '--smallTime',
                    title: `${from} - ${to}`,
                    onClick: () => !disable ? this.onChange(i) : null
                })
            })
        }
        return null
    }

    render() {
        let { emptyTitle } = this.props
        let times = this.times
        return (
            <div className="tp">
                {times ? (times.map((item, i) => (
                    <LabelComponent key={i} {...item}/>
                ))) : (
                    <span className="tp__empty">{emptyTitle || 'Пусто'}</span>
                )}
            </div>
        )
    }
}

export default Timepicker
