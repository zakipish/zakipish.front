import * as React from 'react'
import moment from 'moment'

import LabelComponent, { ILabel } from '../label.component'
import { TimepickerProps } from './timepicker'
import { IconList } from '../icon/icon.component'
import PopupTimepicker from './popup.timepicker'

interface IProps extends ILabel {
    timepicker: TimepickerProps
    autoHide?: boolean
}

interface IState {
    hide: boolean
}

class LabelTimepicker extends React.Component<IProps, IState> {

    refElem: React.RefObject<HTMLDivElement> = React.createRef()
    refChildren: React.RefObject<HTMLDivElement> = React.createRef()

    state = {
        hide: true
    }

    toggle = (e) => {
        let { disabled } = this.props
        if (!disabled) {
            let hide = !this.state.hide
            let contain = this.refChildren.current.contains(e.target)
            if (hide && !contain || !hide) {
                this.setState({hide})
            }
        }
    }

    onChange = (value) => {
        let { autoHide, timepicker } = this.props
        if (autoHide) {
            this.setState({hide: true})
        }
        if (timepicker && timepicker.onChange) {
            timepicker.onChange(value)
        }
    }

    get title () {
        let { title, timepicker } = this.props
        let { value } = timepicker
        if (title && value) {
            let from = value.from.format('HH:mm')
            let to = value.to.format('HH:mm')
            title = `${from} - ${to}`
        }

        if (title && !value)
            title = '00:00 - 00:00'

        return title
    }

    render() {
        let { hide } = this.state
        let { timepicker, ...labelProps } = this.props
        timepicker.onChange = this.onChange
        labelProps.title = this.title
        return (
            <div ref={this.refElem}>
                <LabelComponent
                    icon={IconList.time}
                    onClick={this.toggle}
                    {...labelProps}
                >
                    <div className="label-time" ref={this.refChildren}>
                        <PopupTimepicker
                            {...timepicker}
                            hide={hide}
                            refWrap={this.refElem}
                            onToggle={this.toggle}
                        />
                    </div>
                </LabelComponent>
            </div>
        )
    }
}

export default LabelTimepicker
