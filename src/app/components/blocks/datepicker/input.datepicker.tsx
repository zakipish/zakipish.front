import * as React from 'react'
import moment from 'moment'

import IconComponent, { IconList } from '../icon/icon.component'
import PopupDatepicker from './popup.datepicker'

interface IProps {
    date: moment.Moment | string
    errMsg?: string
    format?: string
    onChange: (dateStr: string) => void
    autoHide?: boolean
}

interface IState {
    popupHide: boolean
    date: moment.Moment
    dateStr: string
    error: boolean
}

class InputDatepicker extends React.Component<IProps, IState> {

    refElem: React.RefObject<HTMLDivElement> = React.createRef()

    inputValue: string = ''
    inputError: boolean = false

    constructor(props) {
        super(props)
        this.state = {
            popupHide: true,
            error: false,
            date: this.initDate(),
            dateStr: this.initDateStr()
        }
    }

    initDate = () => {
        let { date } = this.props
        if (!moment.isMoment(date)) {
            date = date ? moment(date) : moment()
        }
        return date
    }
    
    initDateStr = () => {
        let inDate = this.props.date
        if (inDate) {
            let date = this.initDate()
            return date.format(this.props.format)
        } else {
            return ''
        }
    }

    onToggle = (state = null) => {
        if (typeof state !== 'boolean') {
            state = !this.state.popupHide
        }
        this.setState({popupHide: state})
    }

    onChange = (inDate) => {
        let date = moment(inDate)
        let dateStr = date.format(this.props.format)
        let error = false
        if (dateStr !== 'Invalid date') {
            this.props.onChange(dateStr)
        } else {
            date = this.state.date
            dateStr = this.state.dateStr
            error = true
        }
        this.setState({date, dateStr, error})
    }

    onInputChange = (e) => {
        let dateStr = e.target.value
        let { format } = this.props
        if (dateStr.length === format.length) {
            let date = moment(dateStr, format)
            this.onChange(date)
        } else {
            this.setState({error: true, dateStr})
        }
    }

    onPopupChange = (inDate) => {
        let { autoHide } = this.props
        this.onChange(inDate)
        if (autoHide) {
            this.onToggle(false)
        }
    }

    render() {
        let { format, errMsg } = this.props
        let { popupHide, date, dateStr, error } = this.state
        return (
            <div className="dp-input" ref={this.refElem}>
                <div className={`_input ${error ? '--error' : ''}`}>
                    <input 
                        type="text" 
                        value={dateStr} 
                        onChange={this.onInputChange}
                        onFocus={() => this.onToggle(false)}
                    />
                    <div className="_btn" onClick={() => this.onToggle()}>
                        <IconComponent icon={IconList.calendar}/>
                    </div>
                    { errMsg && error ? <span className="_error-message">{errMsg}</span> : '' }
                </div>
                <PopupDatepicker
                    date={date} 
                    refWrap={this.refElem}
                    hide={popupHide}
                    onChange={this.onChange}
                    onToggle={() => this.onToggle()} 
                    format={format}
                />
            </div>
        )
    }
}

export default InputDatepicker