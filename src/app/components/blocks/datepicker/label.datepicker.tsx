import * as React from 'react'
import moment from 'moment'

import LabelComponent, { ILabel } from '../label.component'
import PopupDatepicker from './popup.datepicker'
import { IconList } from '../icon/icon.component'

interface IProps extends ILabel {
    date: moment.Moment
    autoHide?: boolean
    disablePast?: boolean
    onChange?: (date: moment.Moment) => void
}

interface IState {
    hide: boolean
}

class LabelDatepicker extends React.Component<IProps, IState> {
    
    refElem: React.RefObject<HTMLDivElement> = React.createRef()
    refChildren: React.RefObject<HTMLDivElement> = React.createRef()

    state = {
        hide: true
    }

    toggle = (e) => {
        let { disabled } = this.props
        if (!disabled) {
            let hide = !this.state.hide
            let contain = this.refChildren.current.contains(e.target)
            if (hide && !contain || !hide) {
                this.setState({hide})
            } 
        }
    }

    onChange = (date) => {
        let { autoHide, onChange } = this.props
        if (autoHide) {
            this.setState({hide: true})
        }
        if (onChange) {
            onChange(date)
        }
    }

    get title () {
        let { title, date } = this.props
        if (title && date) {
            title = date.format('D MMMM')
        }
        return title
    }

    render() {
        let { hide } = this.state
        let { date, onChange, disablePast, ...labelProps } = this.props
        let title = this.title
        return (
            <div ref={this.refElem}>
                <LabelComponent
                    icon={IconList.calendar}
                    onClick={this.toggle}
                    {...labelProps}
                    title={title}
                >
                    <div className="label-calendar" ref={this.refChildren}>
                        <PopupDatepicker
                            hide={hide}
                            date={date}
                            onToggle={this.toggle}
                            onChange={this.onChange}
                            refWrap={this.refElem}
                            disablePast={disablePast}
                        />
                    </div>
                </LabelComponent>
            </div>
        )
    }
}

export default LabelDatepicker