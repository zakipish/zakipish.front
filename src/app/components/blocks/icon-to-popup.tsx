import * as React from 'react'
import IconComponent, { IconList } from './icon/icon.component'

interface IProps {
    className?: string
    icon: IconList | string
    children?: any
    hide?: boolean
    onToggle?: (hide: boolean) => void
}

interface IState {
    hide: boolean
}

class IconToPopup extends React.Component<IProps, IState> {

    refWrap: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props) {
        super(props)

        this.state = {
            hide: true
        }

        this.onClickOutside = this.onClickOutside.bind(this)
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.onClickOutside)
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.onClickOutside)
    }

    get isControlled() {
        return this.props.hide != null && this.props.onToggle != null
    }

    get hide() {
        return this.isControlled ? this.props.hide : this.state.hide
    }

    onClickOutside(e) {
        let outSideRef =  this.refWrap.current
        let containState = outSideRef.contains(e.target)
        if(!this.hide && !containState) {
            this.togglePopup(!this.hide)()
        }
    }

    togglePopup = (state?: boolean) => () => {
        const { onToggle } = this.props

        if (this.isControlled){
            onToggle(state ? state : !this.props.hide)
        } else {
            this.setState({ hide: state != null ? state : !this.state.hide })
        }
    }

    get className() {
        let outClassName = 'iconToPopup'
        let { className } = this.props

        if (className) outClassName += ` ${className}`
        outClassName += ` ${this.hide ? '--popupHide' : '--popupShow'}`

        return outClassName
    }

    render() {
        let { icon, children } = this.props
        return (
            <div className={this.className} ref={this.refWrap}>
                <div className="_btn" onClick={this.togglePopup()}>
                    <IconComponent icon={icon} />
                </div>
                <div className="_popup">
                    <IconComponent
                        className="_popup__cross"
                        icon={IconList.cross}
                        onClick={this.togglePopup(false)}
                    />
                    <div className="_popup__body">{children}</div>
                </div>
            </div>
        )
    }
}

export default IconToPopup
