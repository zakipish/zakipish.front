import * as React from 'react'

interface IProps {
    className?: string
    title?: string
    text?: string
    list?: string[]
}

class ErrorBlockComponent extends React.Component<IProps, {}> {
    render() {
        let { className, title, text, list } = this.props
        return (
            <div className={`ErrorBlock ${className ? className : ''}`}>
                <div className="_tit">{title}</div>
                <p>{text}</p>
                { list ?  
                    <ul>
                        { list.map((el, i) => <li key={i}>{el}</li>)}
                    </ul>
                : null }
            </div>
        )
    }
}

export default ErrorBlockComponent