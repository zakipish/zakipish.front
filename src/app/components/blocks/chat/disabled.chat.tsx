import * as React from 'react'
import { Link } from 'react-router-dom';

interface Props {
    image?: string
    text?: string
    btn?: {
        text: string
        to?: string
        onClick?: (e?) => void
    }
}

class DisabledChat extends React.Component<Props, {}> {
    render() {
        let { image, text, btn } = this.props
        return (
            <div className="disChat">
                {image && (
                    <img src={image} alt="#"/>
                )}
                {text && (
                    <div className="disChat__text">{text}</div>
                )}
                {btn && (
                    <Link 
                        to={btn.to || '#'} 
                        className="disChat__btn"
                        onClick={btn.onClick}
                    >{btn.text}</Link>
                )}
            </div>
        )
    }
}

export default DisabledChat
