import * as React from 'react'
import {connect} from 'react-redux'
// @ts-ignore
import openSocket from 'socket.io-client'
import {reqGet, reqPost} from '../../../core/api'
import config from '../../../core/config'
import {User} from '../../../models/user'
import IconComponent from '../icon/icon.component'
import MessageComponent, {IMessage, MessageType} from './message.component'

interface IChatMessage {
  id: number
  message: string
  type: number
  own: boolean
  time: string
  date: string
  user: {
    id: number
    name: string
    color: string
  }
}

interface IProps {
  chat_id: number
  playersCount: number
  user: User
  fixed?: boolean
  disabled?: boolean
  disabledElem?: React.ReactNode
}

interface IState {
  text: string
  date: string
  dateHide: boolean
  messages: IMessage[]
}

class ChatComponent extends React.Component<IProps, IState> {

  public chatElem: React.RefObject<HTMLDivElement> = React.createRef()
  public bodyElem: React.RefObject<HTMLDivElement> = React.createRef()
  public timerForScroll: NodeJS.Timeout | any
  public timerForDateLabel: NodeJS.Timeout | any
  public socket: any

  public state = {
    messages: [],
    text: '',
    date: '',
    dateHide: true
  }

  public componentDidMount() {
    if (this.props.fixed) {
      setTimeout(() => {
        this.setFixedSize()
        window.addEventListener('resize', this.setFixedSize)
      }, 300)
    }
    // noinspection JSIgnoredPromiseFromCall
    this.bootstrap()
  }

  public componentWillUnmount() {
    if (this.props.fixed) {
      window.removeEventListener('resize', this.setFixedSize)
    }
    this.socket.close()
  }

  public bootstrap = async () => {
    const { chat_id } = this.props
    const domain = config.api_host
    this.socket = openSocket(domain, {path: '/ws/socket.io', query: `key=${chat_id}`})
    reqGet(`/chats/${chat_id}/messages`)
      .then(response => {
        const messages = response.data.map(m => this.convertMessage(m))
        this.setState({ messages }, this.scrollChat)
      })
    this.socket.on('message', data => {
      const { messages } = this.state
      messages.push(this.convertMessage(JSON.parse(data)))
      this.setState({ messages }, this.scrollChat)
    })
  }

  public setFixedSize = () => {
    const chatElem = this.chatElem.current
    if (chatElem) {
      chatElem.classList.remove('--fixed')
      chatElem.style.top = null
      chatElem.style.left = null
      chatElem.style.width = null
      chatElem.style.height = null

      const { left, width, height } = chatElem.getBoundingClientRect()
      let { top } = chatElem.getBoundingClientRect()
      top += window.pageYOffset
      chatElem.style.top = top + 'px'
      chatElem.style.left = left + 'px'
      chatElem.style.width = width + 'px'
      chatElem.style.height = height + 'px'
      chatElem.classList.add('--fixed')
    }
  }

  public convertMessage(message: IChatMessage): IMessage {
    const { user } = this.props
    const type = message.type === 1 ?
      user && message.user.id === user.id ? MessageType.push : MessageType.pull
      :
      MessageType.noti

    return {
      type,
      user: {
        name: message.user.name,
        bg: message.user.color ? message.user.color : '414658'
      },
      message: message.message,
      time: message.time,
      date: message.date
    }
  }

  public scrollChat = () => {
    this.bodyElem.current.scrollTop = this.bodyElem.current.scrollHeight
  }

  public onChangeText = e => {
    this.setState({ text: e.target.value })
  }

  public onBodyScroll = () => {
    if (this.state.dateHide) { this.setState({dateHide: false}) }
    clearTimeout(this.timerForScroll)
    clearTimeout(this.timerForDateLabel)
    this.timerForScroll = setTimeout(() => {
      const { children, scrollTop } = this.bodyElem.current
      let findChildIndex = null
      for (let i = 0; i < children.length; i++) {
        const el: any = children[i]
        if (el.offsetTop <= scrollTop) {
          findChildIndex = i
        } else {
          break
        }
      }
      const findChild = this.state.messages[findChildIndex]
      if (findChild) {
        if (findChild.date !== this.state.date) {
          this.setState({date: findChild.date, dateHide: false})
        }
      }
    }, 10)
    this.timerForDateLabel = setTimeout(() => {
      this.setState({dateHide: true})
    }, 1000)
  }

  public onKeyDown = (e: any) => {
    if (e.keyCode === 13 && !e.shiftKey) {
      e.preventDefault()

      const { text } = this.state
      this.sendMessage(text)
      this.setState({ text: '' })
    }

    return true
  }

  public sendMessage = (message: string) => {
    const { chat_id } = this.props
    const data = {
      message,
      type: 1
    }
    // noinspection JSIgnoredPromiseFromCall
    reqPost(`/chats/${chat_id}/messages`, JSON.stringify(data))
  }

  public render() {
    const { messages, dateHide, text } = this.state
    const { playersCount, disabled, disabledElem } = this.props

    return (
      <div className='chat' ref={this.chatElem}>
        {disabled && (
          <div className='chat__disabled'>
            {disabledElem}
          </div>
        )}
        <div className='chat__header'>
          <div className='chat__title'>Чат для участников ({playersCount})</div>
          <div className={`chat__date ${dateHide ? '_hide' : '_show'}`}>{this.state.date}</div>
        </div>
        { !messages ?
          <div className='chat__empty'>
            <IconComponent icon='chat' fill='rgba(0, 0, 0, 0.14)' />
            <span>В данном чате пока нет сообщений</span>
          </div>
          :
          <div
            className='chat__body'
            ref={this.bodyElem}
            onScroll={this.onBodyScroll}
          >
            { messages.map((el, i) => { return(
              <MessageComponent key={i} disabled={disabled} {...el}/>
            )})}
          </div>
        }
        <div className='chat__footer'>
          {/*
                    <div className="chat__typing">
                        <span>Антон Иванов, Иван Годлывоа ывдлао</span>... печатает
                    </div>
                    */}
          <div className='chat__textarea'>
            <div className='_autoResize'>
              <div className='_shadow'>{this.state.text}</div>
              <textarea
                placeholder='Введите сообщение'
                onChange={this.onChangeText}
                onKeyDown={this.onKeyDown}
                value={text}
              />
            </div>
          </div>
          <div className='chat__sendBtn' onClick={() => this.sendMessage(text)}>
            <IconComponent icon='send' fill='#414658'/>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.authState.user
})

export default connect(mapStateToProps)(ChatComponent)
