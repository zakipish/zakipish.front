import * as React from 'react'
import IconComponent, { IIcon } from '../icon/icon.component';

export enum MessageStatus {
    sent = 1,
    read = 2,
}

export enum MessageType {
    push = 'push',
    pull = 'pull',
    noti = 'noti',
}

export interface IMessage {
   type: MessageType
   user?: IMessageUser
   message: string
   time?: string
   date: string
   status?: MessageStatus
   disabled?: boolean
}

interface IMessageUser {
    name: string
    bg?: string
}

class MessageComponent extends React.Component<IMessage, {}> {

    messageClassStr(type: MessageType): string {
        return `message ${type ? `message--${type}` : ''}`
    }

    messageTitleClassStr(user: IMessageUser): string {
        return 'message__tit'
    }

    messageTitleStyle(user: IMessageUser): any {
        if (user.bg) {
            return {
                color: `#${user.bg}`
            }
        } else {
            return {}
        }
    }

    iconStatus(status: MessageStatus): IIcon {
        switch (status) {
            case MessageStatus.sent:
                return { icon: 'ok', width: '12px', height: '8px' }
            case MessageStatus.read:
                return { icon: 'doubleOk', width: '14px', height: '8px' }
            default:
                return { icon: 'ok', width: '12px', height: '8px' }
        }
    }

    stars = (min: number, max: number) => {
        const count = Math.floor(Math.random() * (max - min)) + min
        let str = ''
        for (let i = 0; i < count; i++) {
            str += '*'
        }

        return str
    }

    render() {
        let { type, user, message, time, status, disabled } = this.props
        return (
            <div className={this.messageClassStr(type)}>
                { user ?
                    <div className={this.messageTitleClassStr(user)} style={this.messageTitleStyle(user)}>{disabled ? this.stars(3, 10) : user.name}</div>
                : '' }
                <div className="message__text">{disabled ? this.stars(3, 20) : message}</div>
                { time || status ?
                    <div className="message__footer">
                        { time ?
                            <div className="message__time">{time}</div>
                        : '' }
                        { status ?
                            <div className="message__status">
                                <IconComponent {...this.iconStatus(status)}/>
                            </div>
                        : '' }
                    </div>
                : '' }
            </div>
        )
    }
}

export default MessageComponent
