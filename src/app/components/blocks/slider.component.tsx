import * as React from 'react'
import IconComponent, { IconList } from './icon/icon.component';

interface IProps {
    className?: string
    prev?: string
    next?: string
    list?: string
    length?: number
    children: any
}

interface IState {
    slideIndex: number
}

class SliderComponent extends React.Component<IProps, IState> {

    refSlider: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props: IProps) {
        super(props)
        this.state = {
            slideIndex: 0
        }
    }

    onSlide = (addIndex: number) => {
        let refSlider = this.refSlider.current
        let len = refSlider.children.length
        let { slideIndex } = this.state
        slideIndex += addIndex
        if (slideIndex >= 0 && slideIndex < len) {
            let { clientWidth } = refSlider.children[0]
            refSlider.style.transform = `translateX(-${clientWidth * slideIndex}px)`
            this.setState({slideIndex})
        }
    }

    render() {
        let { className, prev, next, list, children, length } = this.props
        return (
            <div className={className || ''}>
                {length>1?
                <IconComponent
                    className={prev}
                    icon={IconList.slideLeft}
                    onClick={() => this.onSlide(-1)}
                />:null}
                {length>1?
                 <IconComponent
                    className={next}
                    icon={IconList.slideRight}
                    onClick={() => this.onSlide(1)}
                />:null}
                <div ref={this.refSlider} className={list}>
                    {children}
                </div>
            </div>
        )
    }
}

export default SliderComponent