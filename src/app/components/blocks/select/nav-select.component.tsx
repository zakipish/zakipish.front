import * as React from 'react'
import { IStyledRef } from '../../../core/types'
import { Select, SelectHeader, SelectLi, ISelectProps } from './select.styled'
import IconComponent, { IconList } from '../icon/icon.component'

interface IProps {
    refWrap?: React.RefObject<HTMLDivElement>
    title: string
    hide: boolean
    onToggle: Function
    children: any
    color?: string
    hideArr?: boolean
    theme?: string
    right?: boolean
}

class NavSelectComponent extends React.Component<IProps, {}> {

    selectRef: IStyledRef<HTMLDivElement, ISelectProps> = React.createRef()

    constructor(props: IProps) {
        super(props)
        this.onClickOutside = this.onClickOutside.bind(this)
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.onClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.onClickOutside);
    }

    onClickOutside(e) {
        let { hide, refWrap } = this.props
        if(!hide) {
            let selectRef = refWrap ? refWrap.current : this.selectRef.current
            if (!selectRef.contains(e.target)) {
                this.props.onToggle()
            }
        }
    }

    render() {
        let { title, hide, onToggle, children, color, hideArr, theme, right } = this.props
        let count = children.length
        if (children && !Array.isArray(children)) {
            children = [children]
        }
        return (
            <Select ref={this.selectRef} hide={hide} count={count} theme={theme} right={right}>
                <SelectHeader color={color} onClick={() => onToggle()}>
                    <span>{title}</span>
                    {!hideArr ?
                        <i>
                            <IconComponent
                                icon={IconList.selectArr}
                                fill={color ? color : 'rgba(255,255,255,0.7)'}
                            />
                        </i>
                    : '' }
                </SelectHeader>
                {count ?
                    <ul>
                        { children && children.map((el, i) => {return (
                            <SelectLi
                                key={i}
                                index={i + 1}
                                color={color}
                            >
                                <div>{el}</div>
                            </SelectLi>
                        )}) }
                    </ul>
                : '' }
            </Select>
        )
    }
}

export default NavSelectComponent
