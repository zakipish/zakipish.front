import * as React from 'react'
import { IStyledRef } from '../../../core/types'
import { Select, SelectHeader, SelectLi, ISelectProps } from './select.styled'
import IconComponent, { IconList } from '../icon/icon.component'

interface IProps {
    list: ISelectItem[]
    value: any
    color?: string
    hideArr?: boolean
    theme?: string
    right?: boolean
    onChange?: (value: any) => void
    onSelect?: () => void
}

interface IState {
    value: any
    list?: ISelectItem[]
    hide: boolean
}

interface ISelectItem {
    label: string
    value: any
}

class SelectComponent extends React.Component<IProps, IState> {

    selectRef: IStyledRef<HTMLDivElement, ISelectProps>;

    constructor(props: IProps) {
        super(props)
        this.state = {
            list: props.list,
            value: props.value || '',
            hide: true
        }
        this.onClickOutside = this.onClickOutside.bind(this)
        this.selectRef = React.createRef()
    }

    get isControlled(): boolean {
        return this.props.onChange !== undefined
    }

    get count(): number {
        return this.isControlled ? this.props.list.length : this.state.list.length
    }

    get selected(): ISelectItem {
        const { list, value } = this.isControlled ? this.props : this.state
        return list.find(el => el.value == value)
    }

    set selected(el: ISelectItem) {
        if (this.isControlled) {
            this.props.onChange(el.value)
            if (typeof this.props.onSelect === 'function')
                this.props.onSelect()
        } else {
            this.setState({
                value: el.value
            })
        }
    }

    toggleHide() {
        if (this.count) {
            this.setState({
                hide: !this.state.hide
            })
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.onClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.onClickOutside);
    }

    onClickOutside(e) {
        if(!this.state.hide) {
            let selectRef = this.selectRef.current
            if (!selectRef.contains(e.target)) {
                this.toggleHide()
            }
        }
    }

    render() {
        const { list, value } = this.isControlled ? this.props : this.state
        const { hide } = this.state
        const { color, hideArr, theme, right } = this.props
        return (
            <Select ref={this.selectRef} hide={hide} count={this.count} theme={theme} right={right}>
                <SelectHeader color={color} onClick={() => this.toggleHide()}>
                    <span>{this.selected ? this.selected.label : '...'}</span>
                    {!hideArr ?
                        <i>
                            <IconComponent
                                icon={IconList.selectArr}
                                fill={color ? color : 'rgba(255,255,255,0.7)'}
                            />
                        </i>
                    : '' }
                </SelectHeader>
                {this.count > 1 ?
                    <ul>
                        {list && list.map((el, i) => {return (
                            <SelectLi
                                key={i}
                                style={{ }}
                                onClick={() => { this.selected = el; this.toggleHide() }}
                                index={i + 1}
                                color={color}
                                active={el.value === value}
                            >
                                <IconComponent className="_iconOk" icon={IconList.ok}/>
                                <div className="_tit">{el.label}</div>
                            </SelectLi>
                        )}) }
                    </ul>
                : '' }
            </Select>
        )
    }
}

export default SelectComponent
export { ISelectItem }
