import styled from 'styled-components'

export interface ISelectProps {
    hide: boolean
    count: number
    theme?: string
    right?: boolean
}

interface ISelectLiProps { 
    index: number
    color?: string
    active?: boolean
}

export const 
    Select = styled.div<ISelectProps>`
        position: relative;
        font-size: 14px;
        user-select: none;
        ul {
            position: absolute;
            top: calc(100% + 10px);
            left: 0;
            padding-left: 10px;
            padding-right: 10px;
            min-width: 100%;
            border-radius: 5px;
            z-index: 100;
            text-align: left;
            overflow: hidden scroll;
            box-shadow: 0px -2px 8px rgba(0, 0, 0, 0.14);
            &::-webkit-scrollbar-track {
                background: rgba(255,255,255,0);
                border-radius: 10em;
            }
            &::-webkit-scrollbar {
                width: 5px;
                height: 5px;
            }
            &::-webkit-scrollbar-thumb {
                background: rgba(255,255,255,0.4);
                border-radius: 10em;
                cursor: pointer;
            }
        }
        li {
            display: flex;
            align-items: center;
            transition-duration: 0.3s;
            white-space: pre;
            cursor: pointer;
            padding: 5px;
            ._iconOk {
                width: 11px;
                margin-left: 0;
                margin-right: 5px;
                opacity: 1;
                visibility: visible;
                transition: 0.3s;
            }
            ._tit {
                display: inline-block;
                transition: 0.3s color, 0.3s border;
                border-bottom: 1px solid transparent;
            }
        }
        ${props => {
            if (props.theme === 'light') { return `
                color: #2A83FB;
                ul {
                    border: 1px solid #E7E7E7;
                    background: #fff;
                }
                li {
                    path {
                        fill: #2A83FB;
                    }
                    div {
                        border-color: rgba(42, 131, 251, 0.5);
                    }
                }
            `} else { return `
                color: rgba(255, 255, 255, 0.7);
                ul {
                    background: #414658;
                }
                li {
                    path {
                        fill: rgba(255, 255, 255, 0.7);
                    }
                    ._tit {
                        border-color: rgba(255, 255, 255, 0.7);
                    }
                }
            `}
        }}
        ${props => {
            let height = props.count * (16 + 10) + 20
            height = (height <= 300) ? height : 300
            if (props.hide) {
                return `
                    ul {
                        max-height: 0px;
                        padding-top: 0;
                        padding-bottom: 0;
                        border-width: 0px !important;
                        transition: 0.3s all 0.3s;
                    }
                    li {
                        transform: translateX(10px);
                        opacity: 0;
                        visibility: hidden;
                        transition-delay: 0s !important;
                    }
                `
            } else {
                return `
                    ul {
                        padding-top: 10px;
                        padding-bottom: 10px;
                        max-height: ${height}px;
                        transition: 0.3s;
                    }
                    li {
                        transform: translateX(0);
                        opacity: 1;
                        visibility: visible;
                    }
                `
            }
        }}
        ${props => {
            if (props.right) {
                return `
                    ul {
                        left: auto;
                        right: 0;
                    }
                `
            }
        }}
    `,
    SelectHeader = styled.div`
        display: flex;
        align-items: center;
        width: 100%;
        cursor: pointer;
        span {
            white-space: pre;
            color: ${props => props.color ? props.color : 'rgba(255, 255, 255, 0.7)' };
        }
        i {
            display: block;
            margin-left: 10px;
        }
    `,
    SelectLi = styled.li<ISelectLiProps>`
        transition-delay: ${props => `${props.index * 0.05}s`};
        ${props => !props.active ? `
            ._tit {
                border-color: transparent !important;
            }
            ._iconOk {
                opacity: 0 !important;
                visibility: hidden !important;
                margin-right: 0 !important;
                margin-left: -11px !important;
            }
            &:hover {
                path {
                    fill: #27AE60 !important;
                }
                ._tit {
                    color: #27AE60 !important;
                }
            }
        ` : '' }
    `
