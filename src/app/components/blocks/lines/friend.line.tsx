import * as React from 'react'
import { Link } from 'react-router-dom'
import IconComponent, { IconList } from '../icon/icon.component'

interface IProps {
    viewState?: FriendViewState | string
    className?: string
    online?: boolean
    url?: string
    title?: string
    img?: string
    onAdd?: () => void
    onCancel?: () => void
    onInvite?: () => void
    onBell?: () => void
    statusText?: string
}

export enum FriendViewState {
    req = 'req',
    list = 'list',
}

class FriendLine extends React.Component<IProps, {}> {

    get className() {
        let str = 'friend-line'
        let { className, viewState, online } = this.props
        str += className ? ` ${className}` : ''
        str += viewState ? ` --${viewState}` : ''
        str += online ? ` --online` : ''
        return str
    }

    render() {
        let { onAdd, onCancel, onInvite, onBell } = this.props
        let { url, title, img, viewState, statusText } = this.props
        url = url ? url : '#'
        img = img ? img : '/images/avatar-lg.jpg'
        return ( 
            <div className={this.className}>
                <div className="friend-line__img">
                    <Link 
                        to={url} 
                        className="_img" 
                        style={{backgroundImage: `url(${img})`}}
                    ></Link>
                    { (viewState === FriendViewState.list && onBell) && (
                        <div className="_bell" onClick={onBell}>
                            <IconComponent className="_bell__icon" icon={IconList.bell} stroke="#fff"/>
                        </div>
                    )}
                </div>
                <div className="friend-line__wrap">
                    <div className="friend-line__body">
                        <Link to={url} className="friend-line__tit"> 
                            <span>{title}</span>
                        </Link>
                        {/* <div className="friend-line__games"></div> */}
                    </div>
                    <div className="friend-line__wrapBtn">
                        { viewState === FriendViewState.req && (
                            <React.Fragment>
                                <div className="friend-line__btn --gray" onClick={onCancel}>Отказать</div>
                                <div className="friend-line__btn" onClick={onAdd}>Добавить</div>
                            </React.Fragment>
                        )}
                        { viewState === FriendViewState.list && (
                            <div className="friend-line__btn"> 
                                <div className="_link" onClick={onInvite}>
                                    <span>Пригласить в игру</span>
                                    <IconComponent className="_icon" icon={IconList.plus} stroke="#fff"/>
                                </div>
                                {onBell && (
                                    <div className="_bell" onClick={onBell}>
                                        <IconComponent className="_bell__icon" icon={IconList.bell} stroke="#fff"/>
                                    </div>
                                )}
                            </div>
                        )}
                        { statusText && <div className="friend-line__status">{statusText}</div> }
                    </div>
                </div>
            </div>
        )
    }
}

export default FriendLine
