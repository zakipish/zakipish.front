import * as React from 'react'
import IconComponent, { IconList } from './icon/icon.component'
import IconFetchComponent from '../../components/blocks/icon-fetch.component'
import {goToAnchor} from "../../core/utils";

interface IProps {
    className?: string
    title?: string
    defaultIcon?: string | IconList
    list?: Array<{
        src?: string
        title?: string
        name?: string
        href?:string
        defaultIcon?:string
    }>
    scroll: boolean
}

interface IState {
    addScroll: number
}

class ListIconComponent extends React.Component<IProps, IState> {

    refList: React.RefObject<HTMLUListElement> = React.createRef();

    state = {
        addScroll: 10
    }

    componentDidMount() {
        this.setSize()
        window.addEventListener('resize', this.setSize)
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.setSize)
    }

    setSize = () => {
        setTimeout(() => {
            this.setState({
                addScroll: this.refList.current.children[0].clientWidth
            })
        }, 100);
    }

    onAddScroll = (num: number) => {
        let refList = this.refList.current
        if (refList) {
            let { scrollLeft } = refList
            scrollLeft += this.state.addScroll * num
            refList.scrollTo(scrollLeft, scrollLeft)
        }
    }

    onClick = (e) => e.preventDefault()

    get className() {
        let outClassName = 'iconList'
        let { className } = this.props
        if (className) outClassName += ` ${className}`
        if (scroll) outClassName += ' --scroll'
        return outClassName
    }

    renderList() {
        let {list, defaultIcon} = this.props
        return (
            <ul className="iconList__list" ref={this.refList} onClick={this.onClick}>
                {list.map((el, i) => {
                    let tit = el.title || el.name
                    return (
                        <li key={i} onClick={goToAnchor(el.href)}>
                            {el.src ?
                                <IconFetchComponent
                                    className="_icon"
                                    src={el.src}
                                />
                                :
                                <IconComponent
                                    className="_icon"
                                    icon={defaultIcon || IconList.defaultGame}
                                />
                            }
                            {tit && <div className="iconList__name">{tit}</div>}
                        </li>
                    )
                })
                }
            </ul>
        )
    }

    renderScroll() {
        return (
            <div className="iconList__scroll">
                <IconComponent 
                    className="iconList__arr" 
                    icon={IconList.slideLeft}
                    onClick={() => this.onAddScroll(-1)}
                /> 
                {this.renderList()}
                <IconComponent 
                    className="iconList__arr" 
                    icon={IconList.slideRight}
                    onClick={() => this.onAddScroll(1)}
                /> 
            </div> 
        )
    }

    render() {
        let { scroll, title, list } = this.props
        return (
            <div className={this.className}>
                <div className="iconList__tit">
                    { title && (
                        <React.Fragment>
                            {title}<span>{list.length}</span>
                        </React.Fragment>
                    )}
                </div> 
                { scroll ? this.renderScroll() : this.renderList() }
            </div>
        )
    }
}

export default ListIconComponent