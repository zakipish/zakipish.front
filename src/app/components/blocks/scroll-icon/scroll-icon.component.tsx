import * as React from 'react'
import { ScrollList, ScrollIconLi, ScrollActive, ScrollMove } from './scroll-icon.styled'
import { IStyledRef } from '../../../core/types'
import IconComponent from '../icon/icon.component'
import IconFetchComponent from '../icon-fetch.component'
import FilterComponent from '../../layout/filter.component'

interface IProps {
    list: IScrollIconLi[]
    filter?: React.ReactElement<FilterComponent>
    onChange?: (i: number) => void
    activeIndex?: number
}

interface IState {
    moveStatus: boolean
    moveIndex: number
    moveMax: number
    activeIndex: number
}

interface IScrollIconLi {
    label: string
    icon?: string
    iconUrl?: string
    width?: string
    height?: string
    active?: boolean
    className?: string
}

class ScrollIconComponent extends React.Component<IProps, IState> {

    wrapElement: IStyledRef<HTMLDivElement, {}>
    listElement: React.RefObject<HTMLUListElement>
    listItemW: number

    constructor(props: IProps) {
        super(props)
        this.state = {
            moveStatus: false,
            moveIndex: 0,
            moveMax: 0,
            activeIndex: props.list.findIndex(el => el.active)
        }
        this.wrapElement = React.createRef()
        this.listElement = React.createRef()
        this.listItemW = 117
        this.onResize = this.onResize.bind(this)
    }

    componentDidMount() {
        this.checkMoveStatus()
        window.addEventListener('resize', this.onResize, false)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.onResize, false)
    }


    get isControlled() {
        return typeof this.props.activeIndex !== 'undefined'
    }

    get activeIndex() {
        return this.isControlled ? this.props.activeIndex : this.state.activeIndex
    }

    onResize() {
        if (window.innerWidth >= 640) this.checkMoveStatus()
    }

    checkMoveStatus(): void {
        let {clientWidth, scrollWidth} = this.wrapElement.current
        let moveMax = Math.ceil((scrollWidth - clientWidth) / this.listItemW)

        if (clientWidth < scrollWidth) {
            if (this.state.moveStatus === false) {
                this.setState({
                    moveStatus: true,
                    moveMax: moveMax > 0 ? moveMax : 0
                })
            } else {
                this.setState({moveMax: moveMax > 0 ? moveMax : 0})
            }
        } else {
            if (this.state.moveStatus === true ) {
                this.setState({
                    moveStatus: false,
                    moveMax: moveMax > 0 ? moveMax : 0
                })
            } else {
                this.setState({moveMax: moveMax > 0 ? moveMax : 0})
            }
        }

        this.wrapElement.current.scrollLeft = 0
    }

    moveList(moveIndex: number): void {
        let { moveMax } = this.state
        if (moveIndex > moveMax + 1) { //resize bug
            this.setState({moveIndex: 0})
            this.listElement.current.style.transform = `translateX(-${0}px)`
        } else {
            if (moveIndex >= 0 && moveIndex <= moveMax) {
                let moveLeftNext = moveIndex * this.listItemW
                this.setState({moveIndex})
                this.listElement.current.style.transform = `translateX(-${moveLeftNext}px)`
            }
        }
    }

    get activeLeft(): number {
        return this.activeIndex * this.listItemW
    }

    onClickLi(newActiveIndex) {
        if(!this.isControlled) {
            const { activeIndex } = this.state

            if (newActiveIndex != activeIndex) {
                this.setState({ activeIndex: newActiveIndex })

            }
        } else {
            const { onChange } = this.props
            if (typeof onChange === 'function')
                onChange(newActiveIndex)
        }
    }

    render() {
        let { list, filter } = this.props
        let { moveStatus, moveIndex } = this.state

        return (
            <React.Fragment>
                { filter ? 
                    <div className="face__filter">{filter}</div>
                : '' }
                <ScrollList ref={this.wrapElement}>
                    <ul ref={this.listElement}>
                        <ScrollActive style={{left: this.activeLeft + 'px'}}></ScrollActive>
                        { list.map((el, i) => { return (
                            <ScrollIconLi
                                key={i}
                                active={i === this.activeIndex}
                                onClick={() => this.onClickLi(i)}
                            >
                                { el.icon ?
                                    <IconComponent
                                        icon={el.icon}
                                        width={el.width}
                                        height={el.height}
                                        className={el.className}
                                        fill="#fff"
                                        hover={true}
                                    />
                                : '' }

                                { el.iconUrl ?
                                    <IconFetchComponent
                                        className={el.className}
                                        src={el.iconUrl}
                                    />
                                : '' }
                                <span>{el.label}</span>
                            </ScrollIconLi>
                        )}) }
                    </ul>
                </ScrollList>
                { moveStatus ?
                    <ScrollMove>
                        <span onClick={() => this.moveList(moveIndex - 1)}>
                            <IconComponent icon="slideLeft" fill="#374B88"/>
                        </span>
                        <span onClick={() => this.moveList(moveIndex + 1)}>
                            <IconComponent icon="slideRight" fill="#374B88"/>
                        </span>
                    </ScrollMove>
                : '' }
            </React.Fragment>
        )
    }
}

export default ScrollIconComponent
export { IScrollIconLi }
