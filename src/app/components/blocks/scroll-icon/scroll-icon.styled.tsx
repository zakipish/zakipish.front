import styled from 'styled-components'

interface IScrollLi {
    active?: boolean
}

export const
    ScrollList = styled.div`
        position: relative; z-index: 1;
        width: min-content; max-width: 100%;
        padding: 10px;
        background: #4577DF;
        border-radius: 20px;
        overflow: hidden;
        user-select: none;

        ul{
            position: relative;
            display: inline-flex;
            transition: 0.3s transform;
        }
        
        @media (max-width: 640px) {
            overflow-x: auto; overflow-y: hidden;
            width: calc(100% + 30px);
            max-width: calc(100% + 30px);
            margin-left: -15px;
            margin-right: -15px;
            border-radius: 0;
            ul {
                transform: none !important;
            }
        }

        &::-webkit-scrollbar-track {
            background: transparent;
            visibility: hidden;
        }
        &::-webkit-scrollbar {
            width: 0; height: 0;
            visibility: hidden;
        }
        &::-webkit-scrollbar-thumb {
            background: transparent;
            visibility: hidden;
            cursor: pointer;
        }

    `,
    ScrollIconLi = styled.li<IScrollLi>`
        display: flex; 
        flex-wrap: wrap; 
        align-items: flex-end; 
        justify-content: center;
        flex-direction: row;
        width: 117px; height: 117px;
        padding: 10px;
        border-radius: 10px;
        transition: 0.3s;
        cursor: pointer;
        z-index: 4;
        span {
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            min-height: 30px;
            font-size: 14px;
            text-align: center;
            color: #fff;
            transition: 0.3s color;
        }
        ${props => props.active ? ScrollIconLiActive : ''}
        ${props => !props.active ? ScrollIconLiHover : ''}
    `, 
    ScrollActive = styled.li`
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1;
        width: 117px;
        height: 117px;
        background: #fff;
        border-radius: 10px;
        box-shadow: 0px 2px 6px rgba(30,42,79,0.25), 0px -1px 6px rgba(30,42,79,0.28);
        transition: 0.3s;
    `, 
    ScrollMove = styled.div`
        display: flex; justify-content: flex-end;
        padding: 15px 0;
        span{
            display: flex; align-items: center; justify-content: center;
            width: 46px; height: 46px;
            background: #FFFFFF;
            border-radius: 10em;
            box-shadow: 2px -2px 6px rgba(75, 75, 75, 0.25);
            margin-left: 25px;
            cursor: pointer;
            user-select: none;
        }
        @media (max-width: 640px) {
            display: none;
        }
    `

const ScrollIconLiHover = `
    &:hover{
        span {
            text-shadow: 0 0 5px rgba(255,255,255,0.9);
        }
        path {
            filter: url(#svgHover);
        }
    }
`

const ScrollIconLiActive = `
    span {
        color: #374B88;
        transition-delay: 0.1s !important;
    }
    path {
        fill: #374B88 !important;
        transition-delay: 0.1s !important;
    }
`