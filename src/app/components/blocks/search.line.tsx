import * as React from 'react'
import IconComponent, { IconList } from './icon/icon.component'

interface Props {
    placeholder?: string
    onClickIcon?: () => void
    onChange?: (value: string) => void
}

class SearchLine extends React.Component<Props, {}> {

    static defaultProps = {
        placeholder: 'Поиск',
        onClickIcon: () => {},
        onChange: (value) => {},
    }

    onChange = (e) => {
        this.props.onChange(e.target.value)
    }

    render() {
        let { placeholder } = this.props
        return (
            <label className="searchLine">
                <input
                    type="text"
                    placeholder={placeholder}
                    onChange={this.onChange}
                />
                <div className="searchLine__icon">
                    <IconComponent icon={IconList.search}/>
                </div>
            </label>
        )
    }
}

export default SearchLine
