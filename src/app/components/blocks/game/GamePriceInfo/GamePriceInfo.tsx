import * as React from 'react'
import Button, {ButtonColors} from '../../../UI/button/button'

interface IPropTypes {
  price?: string
}

export default class GamePriceInfo extends React.Component<IPropTypes> {
  public render() {
    let content = <div style={{
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'flex-end',
      height: '100%'
    }}>
      <div className='center'>
        <p>Стоимость не указана</p>
      </div>
      <div className='center'>
        <Button
          size='small'
          text='Указать стоимость'
          color={ButtonColors.darkGray}
        />
      </div>
    </div>

    if (this.props.price) {
      content = <React.Fragment>
        <p>Стоимость</p>
        <p className='GamePriceInfo__Price'>{this.props.price}</p>
        <div className='center'>
          <Button
            text='Создать игру'
            color={ButtonColors.green}
          />
        </div>
      </React.Fragment>
    }

    return content
  }
}
