import * as React from 'react'
import Button, {ButtonColors} from '../../../UI/button/button'
import IconComponent, {IconList} from '../../icon/icon.component'

interface IPropTypes {
  place?: string
}

export default class GameLocationInfo extends React.Component<IPropTypes> {
  public render() {
    const header = <h3>
      <IconComponent icon={IconList.placemark} fill='#EB5757'/>
      ГДЕ
    </h3>
    let content = <div style={{
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      height: '100%'
    }}>
      <div>
        {header}
        <p>Место не указано</p>
      </div>
      <div className='center'>
        <Button
          size='small'
          text='Добавить место'
          color={ButtonColors.darkGray}
        />
      </div>
    </div>
    if (this.props.place) {
      content = <div style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        height: '100%'
      }}>
        <div>
          {header}
          <p>{this.props.place}</p>
        </div>
        <a>Посмотреть на карте</a>
      </div>
    }
    return content
  }
}
