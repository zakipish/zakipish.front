import formatDate from 'date-fns/format'
import locale from 'date-fns/locale/ru'
import * as React from 'react'
import Button, {ButtonColors} from '../../../UI/button/button'
import IconComponent, {IconList} from '../../icon/icon.component'

interface IPropTypes {
  interval?: {
    start: Date,
    end: Date
  }
}

export default class GameDateInfo extends React.Component<IPropTypes> {
  public render() {
    const header = <h3>
      <IconComponent icon={IconList.calendar} fill='#2A83FB'/>
      КОГДА
    </h3>
    let content = <div style={{
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      height: '100%'
    }}>
      <div>
        {header}
        <p>Дата не указана</p>
      </div>
      <div className='center'>
        <Button
          size='small'
          text='Указать дату'
          color={ButtonColors.darkGray}
        />
      </div>
    </div>

    if (this.props.interval) {
      content = <React.Fragment>
        {header}
        <p>{formatDate(this.props.interval.start, 'D MMMM, dddd', {locale})}</p>
        <p>{formatDate(this.props.interval.start, 'H:mm')} – {formatDate(this.props.interval.end, 'H:mm')}</p>
      </React.Fragment>
    }

    return content
  }
}
