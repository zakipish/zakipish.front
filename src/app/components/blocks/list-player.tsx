import * as React from 'react'

interface Props {
    images: string[]
    views: number
}

class ListPlayer extends React.Component<Props, {}> {

    static defaultProps = {
        images: ['#'],
        views: 1,
    }

    render() {
        let { images, views } = this.props
        let addLen = images ? images.length - views : 0
        addLen = addLen > 0 ? addLen : 0
        addLen = addLen <= 999 ? addLen : 999
        return (
            <div className="playerList">
                {addLen > 0 && (
                    <div className="playerList__cir --add">{addLen}</div>
                )}
                {images && images.map((image, i) => {
                    if (i < views) {
                        return (
                            <div
                                key={i}
                                className="playerList__cir"
                                style={{backgroundImage: `url(${image || '#'})`}}
                            ></div>
                        )
                    }
                })}
            </div>
        )
    }
}

export default ListPlayer
