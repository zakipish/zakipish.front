import * as React from 'react'
import { Link } from 'react-router-dom'
import IconComponent, { IconList } from '../icon/icon.component';

interface IProps {
    className?: string
    head?: React.ReactFragment
    list: IManListItem[]
}

interface IManListItem {
    url?: string
    img?: string
    level: string
    surname?: string 
    name?: string 
    subtitle?: string
    btns?: Array<{
        title: string
        icon: IconList | string
        onClick: Function
        disabled?: boolean
    }>
}

class ManList extends React.Component<IProps, {}> { 

    getLevelClass(level) {
        return level ? ` --${level.toLocaleLowerCase()}` : ''
    }

    render() {
        let { className, head, list } = this.props
        return (
            <div className={`man-list ${className ? ` ${className}` : ''}`}>
                { head &&  
                    <div className="_head">
                        {head}
                    </div>
                }
                <div className="_body">
                    { list.map((el, i) => { return(
                        <div key={i} className="_item">
                            <div className={`_item__avatar ${this.getLevelClass(el.level)}`}>
                                <span>
                                    <img src={el.img || '/images/avatar-xs.jpg'} alt="#"/>
                                </span>
                                { el.level && <div className="_item__level">{el.level}</div> }
                            </div>
                            <div className="_item__text">
                                <div className="_item__tit">
                                    <Link to={el.url || '#'}>
                                        { el.name && <span className="--name">{el.name}</span> }
                                        { el.surname && <span className="--surname">{el.surname}</span> }
                                    </Link>
                                </div>
                                {el.subtitle && <div className="_item__subtit">{el.subtitle}</div>}
                            </div>
                            { el.btns && <div className="_item__btns">{
                                el.btns.map((btn, i) => { return(
                                    <div 
                                        key={i} 
                                        className={`_item__btn ${btn.disabled ? ' --disabled' : ''}`} 
                                        onClick={() => !btn.disabled ? btn.onClick() : null}
                                    >
                                        <span>{btn.title}</span>
                                        <IconComponent className="_item__btnIcon" icon={btn.icon}/>
                                    </div>
                                )})
                            } </div> }
                        </div>
                    )}) }
                </div>
            </div>
        )
    }
}

export default ManList
