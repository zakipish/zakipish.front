import * as React from 'react'
import WrapForm from '../../layout/form/wrap.form'
import RowForm from '../../layout/form/row.form'
import FieldForm from '../../layout/form/field.form'
import SelectControl from '../../form/select-control'
import IconComponent, { IconList } from '../icon/icon.component'
import IconToPopup from '../icon-to-popup'
import FilterSettingList from './filter-setting.list'

interface IProps {}

class FilterList extends React.Component<IProps, {}> {
    render() {
        return (
            <div className="filterList">
                <div className="_select">
                    <WrapForm>
                        <RowForm>
                            <FieldForm title="Пригласить друзей из">
                                <SelectControl
                                    options={{1: 'ZaKipish'}}
                                    onChange={() => {}}
                                    value={'1'}
                                />
                            </FieldForm>
                        </RowForm>
                    </WrapForm>
                </div>
                <label className="_search">
                    <IconComponent 
                        className="_search__icon" 
                        icon={IconList.search}
                        onClick={() => {}}
                    />
                    <input type="text" placeholder="Поиск"/>
                </label>
                <div className="_setting">
                    <IconToPopup icon={IconList.filter} className="--md-right">
                       <FilterSettingList/>
                    </IconToPopup>
                </div>
            </div>
        )
    }
}

export default FilterList