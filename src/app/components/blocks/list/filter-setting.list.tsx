import * as React from 'react'

import WrapForm from '../../layout/form/wrap.form'
import RowForm from '../../layout/form/row.form'
import FieldForm from '../../layout/form/field.form'
import BtnsForm from '../../layout/form/btns.form'
import InputControl from '../../form/input-control'
import SelectControl from '../../form/select-control'
import RadioForm from '../../layout/form/radio.form';
import RadioControl from '../../form/radio-control';

interface IProps {}

interface IState {
    nameclub: string
    [x: string]: string
}

class FilterSettingList extends React.Component<IProps, IState> {
    
    constructor(props: IProps) {
        super(props)
        this.state = {
            nameclub: ''
        }
    }

    onChange(key, value) {
        this.setState({[key]: value})
    }

    render() {
        let { nameclub } = this.state
        return (
            <div>
                <WrapForm className="--popup">
                    <RowForm title="Филтр поиска">
                        <FieldForm title="Возраст икроков от">
                            <InputControl 
                                value={nameclub} 
                                onChange={(value) => this.onChange('nameclub', value)}
                            /> 
                        </FieldForm>
                        <FieldForm title="до">
                            <SelectControl
                                options={{1: '123'}}
                                value={nameclub} 
                                onChange={(value) => this.onChange('nameclub', value)}
                            /> 
                        </FieldForm>
                    </RowForm>
                    <RowForm>
                        <FieldForm title="Уровень игроков">
                            <SelectControl
                                options={{1: '123'}}
                                value={nameclub} 
                                onChange={(value) => this.onChange('nameclub', value)}
                            /> 
                        </FieldForm>
                    </RowForm>
                    <RadioForm>
                        <div className="checkRadio">
                            <RadioControl
                                onChange={() => {}}
                                options={{1: 'Мужчина', 2: 'Женщина', 3: 'Не важно'}}
                            />
                        </div>
                    </RadioForm>
                    <BtnsForm>
                        <span className="--gray">Cбросить</span>
                        <span className="--darkBlue">Сохранить</span>
                    </BtnsForm>
                </WrapForm>
            </div>
        )
    }
}

export default FilterSettingList