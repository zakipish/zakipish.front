import * as React from 'react'

interface Props {
    className?: string
    title?: string
    content?: string
    geo?: React.ReactNode
}

class Post extends React.Component<Props, {}> {

    get className() {
        let outClassName = 'post'
        let { className } = this.props 
        if (className) outClassName += ` ${className}`
        return outClassName
    }

    render() {
        let { title, content, geo } = this.props
        return (
            <div className={this.className}>
                {title && (
                    <div className="post__title">{title}</div>
                )}
                {content && (
                    <div 
                        className="post__content" 
                        dangerouslySetInnerHTML={{__html: content}}
                    ></div>
                )}
                {geo && (
                    <div className="post__geo">{geo}</div>
                )}
            </div>
        )
    }
}

export default Post