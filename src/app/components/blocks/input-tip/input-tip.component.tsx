import * as React from 'react'
import IconComponent, { IconList } from '../icon/icon.component'

interface IProps {
    className?: string
    hide: boolean
    list: ITipItem[]
    onSelect?: (el: ITipItem) => void
    search?: string
}

interface ITipItem {
    icon?: IconList | string
    label: string
    value: any
    disabled?: boolean
}

class InputTipComponent extends React.Component<IProps, {}> {

    onSelect(el: ITipItem) {
        let { onSelect } = this.props
        if (onSelect) onSelect(el)
    }

    searchList(): ITipItem[] {
        let { search, list } = this.props
        search = search.toLowerCase()
        if (search.length > 0) {
            list = list.filter(el => {
                let label = el.label.toLowerCase()
                return label.indexOf(search) >= 0 ? true : false
            })
        }
        return list
    }

    render() {
        let { className, hide } = this.props
        let list = this.searchList()
        if (!className) className = 'inputTip'
        return (
            <div className={`${className} ${hide ? `${className}--hide` : ''}`}>
                <div className={`${className}__wrap`}>
                    <ul className={`${className}__list`}>
                        {list && list.map((el, i) => { return(
                            <li
                                key={i}
                                className={`${className}__item`}
                                onClick={() => !el.disabled && this.onSelect(el)}
                                style={{transitionDelay: `${i * 0.05}s`}}
                            >
                                {el.icon ? <IconComponent icon={el.icon}/> : ''}
                                <div className={`${className}__label ${el.disabled ? 'disabled' : ''}`}>{el.label}</div>
                            </li>
                        )})}
                    </ul>
                </div>
            </div>
        )
    }
}

export default InputTipComponent
