import * as React from 'react'

interface IProps extends React.DetailedHTMLProps<React.AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement> {
}

class ExternalLink extends React.Component<IProps, {}> {
    render() {
        let { children, ...rest } = this.props
        return (
            // <noindex>
                <a {...rest} rel="nofollow">{children}</a>
            // </noindex>
        )
    }
}


export default ExternalLink