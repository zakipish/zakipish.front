import * as React from 'react'
import { NavLink } from 'react-router-dom'

import IconComponent, { IconList } from './icon/icon.component'

export enum LabelStatus {
    normal = 'normal',
    disable = 'disable',
    active = 'active',
    animation = 'animation',
}

export interface ILabel {
    className?: string
    title?: string
    text?: string
    addtext?: string
    url?: string
    icon?: IconList | string
    status?: LabelStatus
    onClick?: (e: any) => void
    disabled?: boolean
}

class LabelComponent extends React.Component<ILabel, {}> {

    get className() {
        let outClassName = 'label'
        let { className, status, title, disabled, icon } = this.props 
        status = disabled ? LabelStatus.disable : status
        if (className) outClassName += ` ${className}`
        if (status) outClassName += ` --${status}`
        if (icon) outClassName += ` --icon`
        if (!title) outClassName += ` --noneTitle`
        return outClassName
    }

    renderIcon() {
        let { icon } = this.props
        let resElem = null
        if (icon) {
            let iconProps = { icon }
            if (icon === IconList.calendar) {
                iconProps['stroke'] = '#414658'
                iconProps['height'] = '15px'
            }
            resElem = (
                <div className={`label__icon --${icon}`}>
                    <IconComponent {...iconProps}/>
                </div>
            )
        }
        return resElem
    }

    render() {
        let { title, text, addtext, url, onClick, children } = this.props
        return (
            <div className={this.className}>
                { url ?
                    <NavLink
                        to={url}
                        className="label__url"
                        activeClassName={'active'}
                    ></NavLink>
                : ''}
                <div className="label__wrap" onClick={onClick}>
                    { title ? <h5>{title}</h5> : ''}
                    { text ? <p>{text}</p> : ''}
                    { children }
                    { this.renderIcon() }
                </div>
                <div className="label__footer">
                    { addtext ? <p><i>{addtext}</i></p> : ''}
                </div>
            </div>
        )
    }
}

export default LabelComponent
