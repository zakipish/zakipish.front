import * as React from 'react'

interface IProps {
    list: IPriceListItem[]
}

export interface IPriceListItem {
    title: string
    params?: string[]
}

class PriceListComponent extends React.Component<IProps, {}> {
    render() {
        let { list } = this.props
        return (
            <div className="list-price list-price--p2">
                {list.map((el, i) => { return(
                    <div 
                        key={i} 
                        className={`_item ${el.params ? `--p${el.params.length}` : ''}`}
                    >
                        <div className="_item__tit">
                            <span>{el.title}</span>
                        </div>
                        {el.params ? el.params.map((param, j) => { return(
                            <div key={j} className="_item__param">{param}</div>
                        )})
                        : ''}
                    </div>
                )})}
            </div>
                        
        )
    }
}

export default PriceListComponent