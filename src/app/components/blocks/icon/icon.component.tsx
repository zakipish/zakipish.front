import * as React from 'react'
import iconActionsData from './icon-actions.data'
import iconGamesData from './icon-games.data'
import iconPointersData from './icon-pointers.data'
import iconSocData from './icon-soc.data'
import iconUxData from './icon-ux.data'

const data = {
  ...iconSocData,
  ...iconUxData,
  ...iconPointersData,
  ...iconActionsData,
  ...iconGamesData
}

export interface IIcon {
  icon: keyof typeof data | IconList
  width?: string
  height?: string
  stroke?: string
  fill?: string
  style?: object
  hover?: boolean
  className?: string
  onClick?: () => void
}

class IconComponent extends React.Component<IIcon> {

  public getFill(item, i): string {
    const { fill } = this.props
    if (fill) {
      return fill
    } else if (item.fills) {
      return item.fills[i]
    }
    return 'none'
  }

  public render() {
    const { icon, stroke, width, height, hover, className, onClick } = this.props
    let { style } = this.props
    const item = data[icon]
    if (!style && item.style) {
      style = item.style
    }
    return (
            <div className={className} onClick={() => onClick && onClick()}>
                <svg
                    width={width || item.width}
                    height={height || item.height}
                    viewBox={item.viewBox}
                >
                    { hover ?
                        <defs>
                            <filter id='svgHover' x='0' y='0' width='200%' height='200%'>
                                <feGaussianBlur result='blurOut' in='offOut' stdDeviation='1' />
                                <feOffset result='offOut' in='SourceGraphic' dx='0' dy='0' />
                                <feBlend in='SourceGraphic' in2='blurOut' mode='normal' />
                            </filter>
                        </defs>
                    : '' }
                    { item.paths ?
                        item.paths.map((el, i) => { return (
                            <path
                                key={i}
                                style={{transition: '0.3s', ...style}}
                                fill={this.getFill(item, i)}
                                stroke={stroke || 'none'}
                                d={el}
                            />
                        )})
                    :  ''}
                </svg>
            </div>
    )

  }
}

enum IconList {
    // actions
    ok = 'ok',
    doubleOk = 'doubleOk',
    search = 'search',
    plus = 'plus',
    plusCir = 'plusCir',
    bell = 'bell',
    court = 'court',
    purse = 'purse',
    send = 'send',

    // games
    defaultGame = 'defaultGame',
    football = 'football',
    tenis = 'tenis',
    squash = 'squash',
    bowling = 'bowling',
    darts = 'darts',
    baseball = 'baseball',
    billiards = 'billiards',
    volleyball = 'volleyball',
    chess = 'chess',
    boxing = 'boxing',

    // pointers
    selectArr = 'selectArr',
    slideLeft = 'slideLeft',
    slideRight = 'slideRight',
    dpArrLeft = 'dpArrLeft',
    dpArrRight = 'dpArrRight',

    // soc
    instagram = 'instagram',
    vkontakte = 'vkontakte',
    facebook = 'facebook',
    mailru = 'mailru',
    odnoklassniki = 'odnoklassniki',
    sqFacebook = 'sqFacebook',
    sqGplus = 'sqGplus',
    sqInstagram = 'sqInstagram',
    sqLinkedin = 'sqLinkedin',
    sqPintarest = 'sqPintarest',
    sqTumbler = 'sqTumbler',
    sqOdnoklassniki = 'sqOdnoklassniki',
    sqMailru = 'sqMailru',
    sqVkontakte = 'sqVkontakte',

    // ux
    cross = 'cross',
    like = 'like',
    comment = 'comment',
    shared = 'shared',
    rocket = 'rocket',
    calendar = 'calendar',
    time = 'time',
    geo = 'geo',
    fullsize = 'fullsize',
    star = 'star',
    link = 'link',
    setting = 'setting',
    filter = 'filter',
    chat = 'chat',
    eye = 'eye',
    upload = 'upload',
    placemark = 'placemark'
}

export { IconList }
export default IconComponent
