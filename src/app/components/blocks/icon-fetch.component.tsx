import * as React from 'react'

interface IProps {
    src: string
    className?: string
}

interface IState {
    text: string
}

class IconFetchComponent extends React.Component<IProps, IState> {

    state = {
        text: ''
    }

    componentDidMount() {
        fetch(this.props.src).then(res => {
            if (res.headers.get('content-type').indexOf('image/svg') < 0)
                return

            res.text().then(text => {
                this.setState({text})
            })
        })
    }

    render() {
        let { className } =  this.props
        return (
            <div className={className} dangerouslySetInnerHTML={{__html: this.state.text}}></div>
        )
    }
}

export default IconFetchComponent
