import * as React from 'react'
import { Link } from 'react-router-dom'

interface Props {
    to?: string
    onClick?: (e?) => void
    className?: string
    style?: React.CSSProperties
    children: string
    offerTop?: string
    offerBottom?: string
}

class Button extends React.Component<Props, {}> {

    static defaultProps = {
        to: '#',
        onClick: (e?) => {},
        children: '-',
        className: '',
    }

    render() {
        let { children, to, onClick,  className, style, offerTop, offerBottom } = this.props
        return (
            <div className={`button ${className}`}>
                {offerTop && <div className="button__offer">{offerTop}</div>}
                <Link 
                    to={to}
                    onClick={onClick}
                    className="button__wrap"
                    style={style}
                >{children}</Link>
                {offerBottom && <div className="button__offer">{offerBottom}</div>}
            </div>
        )
    }
}

export default Button