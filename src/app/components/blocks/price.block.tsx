import * as React from 'react'

interface IProps {
    className?: string
    tit?: string 
    price?: number | string
}

class PriceBlock extends React.Component<IProps, {}> {
    render() {
        let { className, tit, price } = this.props
        return (
            <div className={`price ${className ? className : ''}`}>
                <div className="price__txt">
                    <div className="price__tit">{tit || 'Стоимость'}</div>
                    <div className="price__val">{price || '0'}<span>руб.</span></div>
                </div>
                {/* <div className="price__btn">Забронированть</div> */}
            </div>
        )
    }
}

export default PriceBlock