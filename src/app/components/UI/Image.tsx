// import { css } from 'astroturf'
import * as React from 'react'
import Config from '../../core/config'

interface IPropTypes {
  src: string,
  width: number,
  height: number,
  alt: string
}

// const styles = css(`
//   .imageContainer
//     display: flex
//     justify-content: center
//     align-items: center
// `)

export const resizeImage = (image: string, width: number, height: number) => {
  const src = image.substring(image.lastIndexOf('/') + 1)
  return `${Config.imageHost}/resized/0/${width}/${height}/${src}`
}

export default class Image extends React.Component<IPropTypes> {

  public render() {
    const { width, height } = this.props
    const src = resizeImage(this.props.src, width, height)
    return <div style={{
      width: width + 'px',
      height: height + 'px'
    }}>
      <img alt='' src={src} />
    </div>
  }
}
