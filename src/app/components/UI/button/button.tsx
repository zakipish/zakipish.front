import * as React from 'react'
import IconComponent, {IconList} from '../../blocks/icon/icon.component'
import './button.sass'

export enum ButtonColors {
  blue = 'blue',
  darkGray = 'darkGray',
  green = 'green',
  transparentWhite = 'transparentWhite'
}

export interface IButtonProps {
  text?: string,
  icon?: IconList,
  iconAlignment?: 'left' | 'right',
  onClick?: (event: React.SyntheticEvent<HTMLButtonElement>) => void
  color?: ButtonColors
  size?: 'small' | 'large' | 'medium',
  type?: 'button' | 'submit',
  uppercase?: boolean,
  bold?: boolean
  iconOutline?: boolean,
  style?: React.CSSProperties
}

export default class Button extends React.Component<IButtonProps> {
  protected static defaultProps: Partial<IButtonProps> = {
    text: 'OK',
    color: ButtonColors.blue,
    size: 'medium',
    type: 'button',
    iconAlignment: 'left',
    uppercase: true,
    bold: true,
    iconOutline: true
  }

  public render() {
    let text: React.ReactNode = this.props.text
    const className: string = `UiButton --${this.props.color} --${this.props.size}
      ${this.props.uppercase && ' --uppercase'}
      ${this.props.bold && ' --bold'}
      ${this.props.iconOutline && ' --iconOutline'}
    `
    if (this.props.icon) {
      if (this.props.iconAlignment === 'left') {
        text = <React.Fragment>
          <IconComponent icon={this.props.icon}/>
          <span>{text}</span>
        </React.Fragment>
      } else {
        text = <React.Fragment>
          <span>{text}</span>
          <IconComponent icon={this.props.icon}/>
        </React.Fragment>
      }
    }
    return <button
      className={className}
      onClick={this.onClick}
      style={this.props.style || {}}
    >
      {text}
    </button>
  }

  protected onClick = (event: React.SyntheticEvent<HTMLButtonElement>) => {
    if (typeof this.props.onClick === 'function') {
      this.props.onClick(event)
    }
  }
}
