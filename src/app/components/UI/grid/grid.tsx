import * as React from 'react'
import { isNumber } from 'util'
import './grid.sass'

interface IPropTypes {
  vertical?: boolean
  gap?: number | string
  columns?: string
  rows?: string
  className?: string
}

export default class Grid extends React.Component<IPropTypes> {
  public render() {
    const { vertical, gap, columns, rows } = this.props
    let className = 'UiGrid'
    if (vertical) {
      className += ' UiGrid--vertical'
    } else {
      className += ' UiGrid--horizontal'
    }
    const style: React.CSSProperties = {}
    if (gap) {
      style.gridGap = isNumber(gap) ? gap + 'px' : gap
    }
    if (columns) {
      style.gridTemplateColumns = columns
    }
    if (rows) {
      style.gridTemplateRows = rows
    }
    if (this.props.className) {
      className += ` ${this.props.className}`
    }
    return <div className={className} style={style}>
      {this.props.children}
    </div>
  }
}
