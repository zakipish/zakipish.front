import * as React from 'react'
import Button, {IButtonProps} from '../button/button'
import './inputWithSubit.sass'

interface IPropTypes {
  type?: 'text' | 'number'
  size?: 'small' | 'medium' | 'large'
  placeholder?: string
  buttonProps?: IButtonProps
  submitText?: string
  label?: string
}

export default class InputWithSubmit extends React.Component<IPropTypes> {
  protected static defaultProps = {
    type: 'text',
    size: 'medium',
    placeholder: '',
    submitText: 'Ок'
  }

  public render() {
    const buttonProps: IButtonProps = this.props.buttonProps || {}
    buttonProps.size = this.props.size
    buttonProps.text = this.props.submitText
    return <div className={`UiInputWithSubmit --${this.props.size}`}>
      {this.props.label && <label>{this.props.label}</label>}
      <input
        type={this.props.type}
        placeholder={this.props.placeholder}
      />
      <Button
        {...buttonProps}
      />
    </div>
  }
}
