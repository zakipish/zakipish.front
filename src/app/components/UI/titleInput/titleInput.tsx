import * as React from 'react'
import './titleInput.sass'

interface IPropTypes {
  canEdit?: boolean,
  title?: string
}

export default class TitleInput extends React.Component<IPropTypes> {
  protected static defaultProps = {
    canEdit: false
  }

  public render() {
    return <div className='UiTitleInput'>
      {/*<IconComponent icon={IconList.}/>*/}
      {this.props.title || 'Введите название игры'}
    </div>
  }
}
