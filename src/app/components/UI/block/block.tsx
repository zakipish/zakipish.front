import * as React from 'react'
import './block.sass'

type PropTypes = {
  title?: string
} & React.HTMLAttributes<HTMLDivElement>

export default class Block extends React.Component<PropTypes> {
  public render() {
    const { title, children, ...otherProps } = this.props
    otherProps.className = `UiBlock ${otherProps.className || ''}`
    if (title) {
      return <div>
        {title && <h3 className='UiBlock__title'>{title}</h3>}
        <div {...otherProps}>
          {children}
        </div>
      </div>
    }
    return <div {...otherProps}>
      {children}
    </div>
  }
}
