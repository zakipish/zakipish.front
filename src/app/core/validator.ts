interface ValidateConfig {
    [x: string]: ValidateFieldConfig
}

interface ValidateFieldConfig {
    present?: boolean,
    minLength?: number,
    email?: boolean,
}

export class Validator {
    protected obj
    protected config

    constructor(obj: any, config: ValidateConfig) {
        this.obj = obj
        this.config = config

        return this
    }

    public isValid = (field: string): boolean => {
        const config = this.config[field]
        const value = this.obj[field]

        if (config.present && !this.validatePresent(value))
            return false

        if (config.minLength != null && !this.validateMinLength(value, config.minLength))
            return false

        if (config.email && !this.validateEmail(value))
            return false

        return true
    }

    validatePresent = (value: any): boolean => {
        return value != null && !!value
    }

    validateMinLength = (value: any, length: number): boolean => {
        return value != null && value.length >= length
    }

    validateEmail = (value: any): boolean => {
        const re = /^\s*(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$/
        return value != null && re.test(String(value).toLowerCase())
    }
}
