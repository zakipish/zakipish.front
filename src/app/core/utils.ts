export abstract class AssignableObject {
  constructor(data?: Object) {
    if (data) {
      Object.assign(this, data);
    }
  }
}

export const searchParams = () => {
    return (new URL(window.location.href)).searchParams
}

export const serialize = (obj) => {
    var str = []
    for (var p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]))
        }
        return str.join("&")
}

export const goToAnchor = (anchor)=>()=> {
  if (anchor) {
    let elem = document.querySelector(anchor)
    if (elem) {
      let { offsetTop } = elem
      window.scrollTo({
        top: (offsetTop - 80),
        behavior: 'smooth',
      })
    }
  }
}
export const goToAnchorDump = (anchor)=>{
  if (anchor) {
    let elem = document.querySelector(anchor)
    if (elem) {
      let { offsetTop } = elem
      window.scrollTo({
        top: (offsetTop - 80),
        behavior: 'smooth',
      })
    }
  }
}

export const pluralize = (number, titles) => {
    const cases = [2, 0, 1, 1, 1, 2]
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ]
}
