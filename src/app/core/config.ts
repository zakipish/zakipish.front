const config = {
  api_host: process.env.API_HOST,
  api_url: process.env.API_URL,
  social_redirect_url: process.env.SOCIAL_REDIRECT_URL,
  ymCounter: process.env.YM_COUNTER,
  imageHost: process.env.IMAGE_HOST || '//img.zakipish.ru'
}

export const setConfig = (key, value) => {
  config[key] = value
}

export default config
