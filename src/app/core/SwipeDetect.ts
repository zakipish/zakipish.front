export default class SwipeDetect {

    private elem
    private direct = 'none'
    private startX = 0
    private startY = 0
    private offsetDist = 30
    private callback = (direct) => {}

    constructor(elem, callback) {
        this.elem = elem
        if (callback) {
            this.callback = callback
        }
        this.init()
    }
    
    init = () => {
        this.elem.addEventListener('touchstart', this.onTouchStart, false)
        this.elem.addEventListener('touchmove', this.onTouchMove, false)
        this.elem.addEventListener('touchend', this.onTouchEnd, false)

        this.elem.addEventListener('mousedown', this.onMouseDown, false)
        this.elem.addEventListener('mousemove', this.onMouseMove, false)
        this.elem.addEventListener('mouseup', this.onMouseUp, false)
    }
    
    detstroy = () => {
        this.elem.removeEventListener('touchstart', this.onTouchStart, false)
        this.elem.removeEventListener('touchmove', this.onTouchMove, false)
        this.elem.removeEventListener('touchend', this.onTouchEnd, false)

        this.elem.removeEventListener('mousedown', this.onMouseDown, false)
        this.elem.removeEventListener('mousemove', this.onMouseMove, false)
        this.elem.removeEventListener('mouseup', this.onMouseUp, false)
    }

    private onTouchStart = (e) => {
        let touchobj = e.changedTouches[0]
        this.startAction(touchobj.pageX, touchobj.pageY)
        e.preventDefault()
    }
  
    private onTouchMove = (e) => {
        e.preventDefault()
    }
  
    private onTouchEnd = (e) => {
        let touchobj = e.changedTouches[0]
        this.endAction(touchobj.pageX, touchobj.pageY)
        e.preventDefault()
    }

    private onMouseDown = (e) => {
        this.startAction(e.pageX, e.pageY)
        e.preventDefault()
    }
  
    private onMouseMove = (e) => {
        e.preventDefault()
    }
  
    private onMouseUp = (e) => {
        this.endAction(e.pageX, e.pageY)
        e.preventDefault()
    }

    private startAction = (x, y) => {
        this.direct = 'none'
        this.startX = x
        this.startY = y
    }

    private endAction = (x, y) => {
        let distX = x - this.startX; let absDistX = Math.abs(distX)
        let distY = y - this.startY; let absDistY = Math.abs(distY)
        let isHorizon = (absDistX > this.offsetDist)
        let isVerical = (absDistY > this.offsetDist)
        if (isHorizon) {
            this.direct = (distX < 0) ? 'left' : 'right'
        } else if (isVerical) {
            this.direct = (distY < 0) ? 'up' : 'down'
        }
        this.callback(this.direct)
    }
}