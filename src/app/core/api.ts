import Config from './config'

type MethodType = ('GET'|'POST'|'PATCH'|'PUT'|'DELETE');

export const apiFetch = async (
  uri: string,
  body: BodyInit = null,
  method: MethodType = 'GET'
) => {
  let headers = {};
  headers['Accept'] = 'application/json';
  if (!(body instanceof FormData)) {
    if (body instanceof File) {
      // no content type
    } else {
      headers['Content-Type'] = 'application/json';
    }
  }

  const response = await fetch(Config.api_url + uri, {
    credentials: "include",
    headers: headers,
    method: method,
    body: body,
  }),
  json = await response.json();
  if (response.status < 200 || response.status > 300) {
    json.status = response.status
    return Promise.reject(json);
  }
  return json;
},
reqGet = async (uri, params: Object = null, all: boolean = false) => {
    let q = uri
        + (params ? (uri.indexOf('?') === -1 ? '?' : '&') + getQueryString(params) : '');
    q = q +  (all ? (q.indexOf('?') === -1 ? '?' : '&') + 'all=1' : '');
    return await apiFetch(q);
},
reqPost = async (uri, body) => {
    return await apiFetch(uri, body, 'POST');
},
reqPatch = async (uri, body) => {
    return await apiFetch(uri, body, 'PATCH');
},
reqPut = async (uri, body) => {
    return await apiFetch(uri, body, 'PUT');
},
reqDelete = async (uri) => {
    return await apiFetch(uri, null, 'DELETE');
};

function getQueryString(params) {
  return Object
  .keys(params)
  .map(k => {
      if (Array.isArray(params[k])) {
          return params[k]
              .map(val => `${encodeURIComponent(k)}[]=${encodeURIComponent(val)}`)
              .join('&')
      }

      return `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`
  })
  .join('&')
}
