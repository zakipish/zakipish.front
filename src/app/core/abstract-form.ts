import * as React from "react"
import * as _ from "lodash"
import moment from 'moment'

export interface AbstractFormProps {
    item: any
    onSubmitted?: (saved: any) => void
}

export interface AbstractFormState {
    form?: any
    draft?: any
    busy?: boolean
    errors?: any
    message?: string
    validate: {
        [x: string]: {
            valid: boolean
            errors: any[]
        }
    }
}

const defaultValidationConfig = {
    present: false,
    minLength: 0,
    email: false
}

export default abstract class AbstractForm<P extends AbstractFormProps = AbstractFormProps, S extends AbstractFormState = AbstractFormState> extends React.Component<P, S> {
    apiUri
    apiMethod

    unmounted = false
    internalOnSubmit = (data) => {}

    state: any = {}
    validationConfig: {} = {}

    constructor(props) {
        super(props)
        this.state.busy = false
        this.state.form = this.cloneModel(props.item)
        this.state.validate = {}
    }

    validForm = () => {
        const { validate } = this.state

        return !(Object.values(validate).map(field => field['valid']).filter(valid => !valid).length > 0)
    }

    validateWholeForm = () => {
        let { form } = this.state

        const keys = Object.keys(this.validationConfig)
        const validate = {}
        keys.forEach(key => validate[key] = this.validate(key, form[key]) )

        this.setState({ validate })
    }

    validate = (field, value) => {
        const computedConfig = { ...defaultValidationConfig, ...this.validationConfig[field] }
        let valid = true
        let errors = []

        if (!this.validatePresent(value, computedConfig.present)) {
            valid = false
            errors.push('не может быть пустым')
        }

        if (!this.validateMinLength(value, computedConfig.minLength)) {
            valid = false
            errors.push(`минимальная длина равна ${computedConfig.minLength}`)
        }

        if (!this.validateEmail(value, computedConfig.email)) {
            valid = false
            errors.push('не является адресом электронной почты')
        }

        return { valid, errors }
    }

    validatePresent(value, present) {
        if (present) {
            return !!value
        }

        return true
    }

    validateMinLength(value, minLength) {
        if (!value) {
            return true
        }

        return value.length >= minLength
    }

    validateEmail(value, email) {
        if (!value || !email) {
            return true
        }

        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(value).toLowerCase());
    }

    cloneModel = (model) => {
        return _.cloneDeep(model)
    }

    componentWillUnmount() {
        this.unmounted = true
    }

    handleChange = (name: string) => (value: any) => {
        const { form } = this.state
        let { validate } = this.state
        form[name] = value

        this.setState({
            form,
        })

        const { valid, errors } = this.validate(name, value)
        validate = { ...validate, [name] : { valid, errors } }
        this.setState({ validate })
    }

    errorMessage = (name: string) => {
        const { validate } = this.state
        if (!!validate[name]) {
            let errors_str = validate[name].errors.join(', ')
            errors_str = errors_str.charAt(0).toUpperCase() + errors_str.slice(1);
            return errors_str
        }
    }

    valid = (name: string) => {
        const { validate } = this.state
        if (!!validate[name]) {
            return validate[name].valid
        } else {
            return true
        }
    }

    submit = async (e?: any) => {
        if (!!e) {
            await e.preventDefault()
        }
        await this.validateWholeForm()
        if (!this.validForm()) {
            return false
        }

        const {form} = this.state
        const {item, onSubmitted} = this.props
        this.setState({
            busy: true,
            errors: null,
            message: null,
            draft: this.cloneModel(form),
        })
        const body = JSON.stringify(form)
        try {
            const data = await this.apiMethod(this.apiUri, body)

            if (typeof onSubmitted === 'function') {
                onSubmitted(data.data)
            }

            if (typeof this.internalOnSubmit === 'function') {
                this.internalOnSubmit(data.data)
            }

            !this.unmounted && this.setState({
                form: this.cloneModel(data.data),
            })

        } catch (e) {
            !this.unmounted && this.setState({
                errors: e.errors,
                message: e.message,
            })
        }
        !this.unmounted && this.setState({
            busy: false,
        })
    }

    fieldError = (key: string) => {
        const {errors, form, draft} = this.state
        return errors && errors[key] && _.isEqual(draft[key], form[key]) ? errors[key] : null
    }
}
