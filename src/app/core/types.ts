import * as React from 'react'
import { ThemedOuterStyledProps } from 'styled-components'

interface IStyledRef<T, S> 
extends React.RefObject<
    React.Component<
        ThemedOuterStyledProps<
            React.ClassAttributes<T> & React.HTMLAttributes<T> & S, 
            any
        >, 
        any, 
        any
    > & T
> {}


export { IStyledRef }