import { AssignableObject } from '../core/utils';
import { User } from './user'

export class FriendRequest extends AssignableObject {
    id: number
    user: User
    accepted: boolean

    constructor(props) {
        super(props)

        if (!(props.user instanceof User)) {
            this.user = new User(props.user)
        }
    }
}
