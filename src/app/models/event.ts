import { AssignableObject } from '../core/utils'
import { Slug } from './slug'
import { Club } from './club'
import { Playground } from './playground'
import { Sport } from './sport'
import { GameLevel } from './game-level'
import { User } from './user'
import { Image } from './image'
import moment from 'moment'

export enum EventType {
    Meeting = 1,
    Game
}

export class Event extends AssignableObject {
    id: number
    slug: string
    owner_id: number
    club_id: number
    club: Club
    playground_id: number
    playground: Playground
    sport_id: number
    sport: Sport
    game_level_id: number
    gameLevel: GameLevel
    chat_disabled: boolean
    chat_id: number
    users: User[]
    max_players: number
    min_players: number
    age_start: number
    age_end: number
    title: string
    description: string
    price: number
    type: EventType
    coordinates: number[]
    images: Image[]
    interval: string[]
    date: string
    address: string

    constructor(args) {
        super(args)

        if (!(args.club instanceof Club)) {
            this.club = new Club(args.club)
        }

        if (args.playground && !(args.playground instanceof Playground)) {
            this.playground = new Playground(args.playground)
        }

        if (args.users && args.users.length > 0 && !(args.users[0] instanceof User)) {
            this.users = args.users.map(user => new User(user))
        }
    }

    get intervalStartMoment() {
        return moment(this.date + ' ' + this.interval[0])
    }

    get intervalEndMoment() {
        return moment(this.date + ' ' + this.interval[1])
    }
}
