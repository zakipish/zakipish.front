import { AssignableObject } from '../core/utils'
import { Infrastructure } from './infrastructure'
import { Contact } from './contact'
import { Image } from './image'
import { Playground } from './playground'
import { Sport } from './sport'
import { Slug } from './slug'
import { Service } from './service'

export class Club extends AssignableObject {
    id: number
    slugs: Slug[]
    slug: string
    name: string
    city: string
    address: string
    description: string
    coordinates: string
    contacts: Contact[]
    services: Service[]
    logo: Image
    photo_url: string
    images: Image[]
    playgrounds: Playground[]
    sports: Sport[]
    // TODO sportList ???
    infrastructures: Infrastructure[]
    logo_url: string
    big_logo_url: string
    weekSchedule?: string[]
    weekDaysSchedule?: string[]
    weekEndSchedule?: string[]
    hasFeatureSchedules: boolean

    constructor(props?) {
        super(props)

        if (props && props.playgrounds && props.playgrounds.length > 0 && !(props.playgrounds[0] instanceof Playground)) {
            this.playgrounds = props.playgrounds.map(p => new Playground(p))
        }
    }

    get phone(): string {
        return this.findContactBySlug('telefon')
    }

    get sajt(): string {
        return this.findContactBySlug('sajt')
    }

    get vkontakte(): string {
        return this.findContactBySlug('vkontakte')
    }

    get facebook(): string {
        return this.findContactBySlug('facebook')
    }

    findContactBySlug(name: string) {
        const contactIndex = this.contacts.findIndex(contact => contact.contact_type.slug == name)

        if (contactIndex >= 0) {
            return this.contacts[contactIndex].value
        } else {
            return null
        }
    }

    get coordinatesArr(): number[] {
        if (this.coordinates) {
            const result = this.coordinates.match(/\(([\d,\.]*),([\d,\.]*)\)/i)

            if (result)
                return [parseFloat(result[1]), parseFloat(result[2])]

            return null
        }

        return null
    }

    get safe_photo_url() {
        return this.photo_url || '/images/default-club.png'
    }

    get safe_big_logo_url() {
        return this.big_logo_url || '/images/club-logo.png'
    }

    get safe_logo_url() {
        return this.logo_url || '/images/club-logo.png'
    }

    get safe_images() {
        return this.images && this.images.length > 0 ? this.images : [new Image({ url: '/images/default-club.png' })]
    }
}
