import { AssignableObject } from '../core/utils';

export class Surface extends AssignableObject {
    id: number
    name: string
}
