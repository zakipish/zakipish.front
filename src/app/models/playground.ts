import { AssignableObject } from '../core/utils';
import { Club } from './club'
import { Surface } from './surface'
import { Sport } from './sport'
import { Equipment } from './equipment'
import { Service } from './service'
import { Image } from './image'

export class Playground extends AssignableObject {
    id: number
    slug: string
    name: string
    type: number
    coordinates: string
    description: string
    avatar: Image
    avatar_mid_url: string
    avatar_url: string
    services: Service[]
    surfaces: Surface[]
    images: any[]
    sports: Sport[]
    equipments: Equipment[]
    club?: Club
    club_id: number
    weekSchedule?: string[]
    weekDaysSchedule?: string[]
    weekEndSchedule?: string[]
    hasFeatureSchedules: boolean
    min_price: number
    hasSchedule: boolean

    constructor(props) {
        super(props)
        if (!props || !props.club) {
            this.club = new Club()
        } else if (!(props.club instanceof Club)) {
            this.club = new Club(props.club)
        }
    }

    static types = {
        1: 'Открытая',
        2: 'Закрытая',
    }

    get typeName() {
        return Playground.types[this.type]
    }

    get surfacesNames() {
        let str = this.surfaces.map(s => s.name).join(', ')
        str = str.toLowerCase()
        str = str.charAt(0).toUpperCase() + str.slice(1)

        return str
    }

    get safe_avatar_url() {
        return this.avatar_url || '/images/default-playground.png'
    }

    get weekScheduleStr() {
        return this.weekSchedule && this.weekSchedule[0] ? `${this.weekSchedule[0]} - ${this.weekSchedule[1]}` : 'Неизвестно'
    }

    get coordinatesArr(): number[] {
        if (this.coordinates) {
            const result = this.coordinates.match(/\(([\d,\.]*),([\d,\.]*)\)/i)

            if (result)
                return [parseFloat(result[1]), parseFloat(result[2])]

            return null
        }

        return null
    }
}
