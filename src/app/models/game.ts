import moment from 'moment'
import { AssignableObject } from '../core/utils'
import { User } from './user'
import { Playground } from './playground'
import { GameLevel } from './game-level'
import { Sport } from './sport'

export class Game extends AssignableObject {
    id: number
    name: string
    slug: string
    description: string
    min_players: number
    max_players: number
    players_count: number
    age: number
    age_start: number
    age_end: number
    sport: Sport
    gameLevel: GameLevel
    game_level_id: number
    price: number
    users: User[]
    date: string
    interval: string[]
    playground_id: number
    playground: Playground
    owner_id: number
    sport_id: number
    owner: User
    chat_id: number

    viewState?: any
    duration?: string

    constructor(props) {
        super(props)
        if (!(props.playground instanceof Playground))
            this.playground = new Playground(props.playground)
    }

    get fromStr() {
        return `${this.date} ${this.interval[0]}`
    }

    get toStr() {
        return `${this.date} ${this.interval[1]}`
    }

    get fromDate() {
        return moment(this.fromStr)
    }

    get toDate() {
        return moment(this.toStr)
    }

    get startTime() {
        return moment(`${this.date} ${this.interval[0]}:00+03:00`)
    }

    get endTime() {
        return moment(`${this.date} ${this.interval[1]}:00+03:00`)
    }

    get isPast() {
        return this.startTime.isBefore(moment().utcOffset(3))
    }
}
