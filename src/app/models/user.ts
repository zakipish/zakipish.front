import { AssignableObject } from '../core/utils';
import { SocialAccount } from './social-account'
import { Sport } from './sport'
import { Game } from './game'
import { FriendRequest } from './friend-request'
import { GameLevel } from './game-level'

export enum EventParticipationType {
    Possible = 1,
    Defenitely,
    Declined
}

export class User extends AssignableObject {
    id:                 number
    email:              string
    name:               string
    phone:              string
    oldPassword:        string
    newPassword:        string
    confirmedPassword:  string
    role:               number
    lastName:           string
    midName:            string
    birthDate:          string
    about:              string
    city_id:            number
    height:             number
    weight:             number
    gender:             number
    avatar_url:         string
    avatar_thumb_url:   string
    avatar_big_thumb_url:   string
    avatar_header_url:  string
    social_accounts:    SocialAccount[]
    sports:             Sport[]
    friend_request:     FriendRequest
    friends:            User[]
    games:              Game[]
    type:               EventParticipationType
    tmp:                boolean

    constructor(props?) {
        super(props)

        if (props && props.friends && props.friends.length > 0 && !(props.friends[0] instanceof User)) {
            this.friends = props.friends.map(friend => new User(friend))
        }
    }

    fullName() {
        return this.lastName ? `${this.lastName} ${this.name}` : this.name
    }

    hasSport(sport_id: number): boolean {
        return this.sports.map(sport => sport.id).includes(sport_id)
    }

    gameLevelFor(sport_id: number): GameLevel | null {
        if (this.sports == null)
            return null

        const sportIndex = this.sports.findIndex(sport => sport.id == sport_id)
        if (sportIndex < 0)
            return null

        const gameLevel = this.sports[sportIndex].game_level
        return gameLevel
    }
}
