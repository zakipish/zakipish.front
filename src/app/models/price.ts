
class Price implements IPrice {

    timeStart = ''
    timeEnd = ''
    state = PriceStatus['empty']
    value = '0'

    constructor(data?: IPrice) {
        if (data) {
            if (data.state) {
                data.state = PriceStatus[data.state]
            } 
            Object.assign(this, data)
        }
    }
}

interface IPrice {
    timeStart?: string
    timeEnd?: string
    state?: PriceStatus
    value: string
}

enum PriceStatus {
    open = 'open',
    game = 'game',
    empty = 'empty',
    closed = 'closed',
}

export default Price
