import { AssignableObject } from '../core/utils';

export class SocialAccount extends AssignableObject {
    id:            number
    provider:      string
    provider_id:   string
    social_email:  string
    profile_url:   string
}
