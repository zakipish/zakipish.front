import { AssignableObject } from '../core/utils'

export class Contact extends AssignableObject {
    id: number
    value: string
    contact_type_id: string
    contact_type: {
        id: number
        slug: string
        name: string
    }
}
