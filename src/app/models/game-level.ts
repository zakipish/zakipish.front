import { AssignableObject } from '../core/utils';

export class GameLevel extends AssignableObject {
    id: number
    slug: string
    name: string
    engName: string
    color: string
}
