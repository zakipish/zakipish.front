import { AssignableObject } from '../core/utils'

export class Equipment extends AssignableObject {
    id: number
    name: string

    // pivot
    unit: string
    price: number
}
