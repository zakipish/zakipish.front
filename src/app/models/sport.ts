import { AssignableObject } from '../core/utils';
import { GameLevel } from './game-level'

export class Sport extends AssignableObject {
    id:             number
    slug:           string
    name:           string
    description:    string
    label:          string
    icon_url:       string
    photo_url:       string

    // pivot
    game_level_id:  number
    game_level:     GameLevel
    game_exp:       number
    games_count:    number

    static game_exps = [
        { label: 'Менее 1 года', value: 1, },
        { label: 'От 1 года', value: 2, },
        { label: '2 года', value: 3, },
        { label: '3 года', value: 4, },
        { label: '4 года', value: 5, },
        { label: '5 лет', value: 6, },
        { label: '6 лет', value: 7, },
        { label: '7 лет', value: 8, },
        { label: '8 лет', value: 9, },
        { label: '9 лет', value: 10, },
        { label: '10 лет', value: 11, },
        { label: '11 лет', value: 12, },
        { label: '12 лет', value: 13, },
        { label: '13 лет', value: 14, },
        { label: '14 лет', value: 15, },
        { label: '15 лет', value: 16, },
        { label: 'Более 15 лет', value: 17, },
    ]
}
