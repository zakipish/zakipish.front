import { AssignableObject } from '../core/utils'

export class Slug extends AssignableObject {
    value: string
    is_active: boolean
}
