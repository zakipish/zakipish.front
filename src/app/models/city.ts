import { AssignableObject } from '../core/utils'
import { Slug } from './slug'

export class City extends AssignableObject {
    id: number
    slugs: Slug[]
    slug: string
    name: string
}
