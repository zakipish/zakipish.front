import { AssignableObject } from '../core/utils'

export class Infrastructure extends AssignableObject {
    id: number
    name: string
}
