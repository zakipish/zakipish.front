import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { history } from './app/core/history'

import './style.sass'
import store from './store'
import AppComponent from './app/app.component'

import moment from 'moment'
moment.locale('ru')

ReactDOM.render(
    <Provider store={store}>
        <AppComponent history={history}/>
    </Provider>,
    document.getElementById("root")
)
