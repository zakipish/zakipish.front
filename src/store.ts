import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './app/reducers/_index.reducer'

const logger = store => next => action => {
    if (process.env.NODE_ENV !== 'production') {
        console.groupCollapsed('dispatching', action.type)
        console.log('prev state', store.getState())
        console.log('action', action)
    }
    let result = next(action)
    if (process.env.NODE_ENV !== 'production') {
        console.log('next state', store.getState())
        console.groupEnd()
    }
    return result
}

const saver = store => next => action => {
    let result = next(action)
    localStorage['zakipish-store'] = JSON.stringify(store.getState())
    return result
}

const store = createStore(
    rootReducer,
    localStorage['zakipish-store'] ? JSON.parse(localStorage['zakipish-store']) : {},
    applyMiddleware(thunk, logger, saver)
)

export default store
